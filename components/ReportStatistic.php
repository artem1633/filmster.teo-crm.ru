<?php

namespace app\components;

use Yii;
use app\helpers\DateTimeHelper;
use app\models\ReportItunesSearch;
use yii\base\Component;
use yii\helpers\VarDumper;

/**
 * Class ReportStatistic
 * @package app\components
 */
class ReportStatistic extends Component
{
    /**
     * @var array
     */
    public $filterData;


    /**
     * @var array
     */
    private $_days;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $dates = [];
        if($this->filterData['dateStart'] == null || $this->filterData['dateEnd'] == null){
            $dates[] = date('Y-m-d', (time() - (86400 * 30)));
            $dates[] = date('Y-m-d');

            $this->filterData['dateStart'] = $dates[0];
            $this->filterData['dateEnd'] = $dates[1];
        } else {
            $dates = [$this->filterData['dateStart'], $this->filterData['dateEnd']];
        }

        $this->_days = DateTimeHelper::getDates($dates[0], $dates[1]);
    }

    /**
     *
     */
    public function getStatistics($attribute)
    {
        $searchModel = new ReportItunesSearch();
        $params = ['ReportItunesSearch' => $this->filterData];
        $dataProvider = $searchModel->search($params, $attribute.', date');

//        VarDumper::dump($dataProvider->models,10, true);
//        exit;

        $dataset = [];


        for ($i = 0; $i < count($this->_days); $i++)
        {
            $day = $this->_days[$i];
            foreach ($dataProvider->models as $model){
                if($dataset[$model[$attribute]] == null){
                    $dataset[$model[$attribute]] = [];
                }

                if(!$model){
//                    var_dump($model);
//                    exit;
                    continue;
                }

                if($model['date'] == $day){
                    $dataset[$model[$attribute]][] = intval($model['count']);
                } else {
                    if($i > count($dataset[$model[$attribute]])){
                        $dataset[$model[$attribute]][] = 0;
                    }
                }
            }
        }


        $deleteIndex = [];

        if(count($this->_days) > 10){
            for ($i = 0; $i < count($this->_days); $i++){
                $index = $i + 1;
                if(($index % 3) === 0){
                    $deleteIndex[] = $i;
                }
            }
        }

        foreach ($deleteIndex as $index){
           array_splice($this->_days, $index, $index);
        }

        $deleteIndex = [];

        if(count($dataset) > 10){
            for ($i = 0; $i < count($dataset); $i++){
                $index = $i + 1;
                if(($index % 3) === 0){
                    $deleteIndex[] = $i;
                }
            }
        }

        foreach ($deleteIndex as $index){
            array_splice($dataset, $index, $index);
        }


        return [$this->_days, $dataset];
    }
}