<?php

namespace app\modules\api\controllers;

use app\components\iTunesSalesApi;
use app\helpers\DateTimeHelper;
use app\models\Films;
use app\models\ReportItunes;
use app\models\Settings;
use app\models\TechnicalInformation;
use app\models\TechnicalPlaygrounds;
use yii\helpers\VarDumper;
use yii\web\Response;
use yii\rest\ActiveController;

/**
 * Class ReportController
 * @package app\modules\api\controllers
 */
class ReportController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['call-black'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionLoadLastDay()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $reporter = new iTunesSalesApi();

        $settingToken = Settings::findByKey('access_token');
        $startParsingDateSetting = Settings::findByKey('start_parse_date');

        $accessToken = '';
        $startParsingDate = '';

        if($settingToken){
            $accessToken = $settingToken->value;
        }

        if($startParsingDateSetting){
            $day = $startParsingDateSetting->value;
        }



        $reporter->setAccessToken($accessToken)->setVendor('86908296');

        $reporter->throwErrors = false;
//
//        if($accounts = $reporter->getAccounts()){
//            //Do something with data your're good to go
//            print_r(json_encode($accounts));
//        }
//        else{
//            echo "Api  Errors : ".$reporter->getErrorsAsString();
//        }

        $accounts = $reporter->getAccounts();


//Example with first account returned from list
//account is numeric but setAccount parses raw return from the getAccounts() method
        $reporter->setAccount($accounts[0]);


            $dayFormat = str_replace('-', '', $day);

            try {
                if($data = $reporter->getSalesDailyReport($dayFormat)) { //Will get last year by default
                    //Do something with data your're good to go

//                print_r($data);
//                exit;


                    $output = [];

                    $data = json_decode(json_encode($data), true);

                    $details = $data['details'];

//                    VarDumper::dump($details, 10, true);
//                    exit;

                    foreach ($details as $detail)
                    {

                        $typeTransaction = $detail['Product Type Identifier'];

                        if($typeTransaction == 'M'){
                            $typeTransaction = 'EST';
                        } else if($typeTransaction == 'D') {
                            $typeTransaction = 'VOD';
                        } else if($typeTransaction == 'P'){
                            $typeTransaction = 'EST';
                        } else if($typeTransaction == 'S'){
                            $typeTransaction = 'Возврат';
                        }


                        $output[] = [
                            'apple_identifier' => $detail['Apple Identifier'],
                            'vendor_identifier' => $detail['Vendor Identifier'],
                            'title' => $detail['Title'],
                            'sales' => $detail['Units'],
                            'type' => ($detail['Product Type Identifier'] == 'M' ? 'EST' : 'VOD'),
                            'customer_price' => $detail['Royalty Price'],
                            'customer_currency' => $detail['Customer Currency'],
                            'country' => $detail['Country Code'],
                            'format' => $detail['Asset/Content Flavor'],
                            'genre' => $detail['Primary Genre'],
                            'begin_date' => $detail['Begin Date'],
                            'type_transaction' => $typeTransaction,
                            'date' => $day,
                        ];
                    }


                    foreach ($output as $reportData)
                    {
                        $report = new ReportItunes();
//                    $report->attributes = $reportData;

                        $report->apple_identifier = $reportData['apple_identifier'];
                        $report->vendor_identifier = $reportData['vendor_identifier'];
                        $report->title = $reportData['title'];
                        $report->sales = $reportData['sales'];
                        $report->type = $reportData['type'];
                        $report->customer_price = $reportData['customer_price'];
                        $report->customer_currency = $reportData['customer_currency'];
                        $report->country = $reportData['country'];
                        $report->format = $reportData['format'];
                        $report->genre = $reportData['genre'];
                        $report->begin_date = $reportData['begin_date'];
                        $report->date = $reportData['date'];
                        $report->type_transaction = $reportData['type_transaction'];


                        $technicalPlayground = TechnicalPlaygrounds::find()->where(['additional_value' => $report->apple_identifier])->one();

//                        if($technicalPlayground == null){
//                            $technicalPlayground = TechnicalPlaygrounds::find()->where(['value_playground' => $report->vendor_identifier])->one();
//                        }

                        if($technicalPlayground){
                            $technical = TechnicalInformation::findOne($technicalPlayground->technical_id);

                            if($technical){
                                $film = Films::findOne($technical->film_id);

                                if($film){
                                    $report->film_id = $film->id;
                                }
                            }
                        }

                        $report->save(false);
                    }
                }
                else{
                    echo "Api  Errors : ".$reporter->getErrorsAsString();
                }

                $startParsingDateSetting->value = date('Y-m-d', time() - (86400 * 2));
                $startParsingDateSetting->save(false);
            } catch (\Exception $e) {
                echo 'One error: '.$day.'<br>';
            }
    }

    public function actionTest()
    {
        VarDumper::dump(DateTimeHelper::getDates('2019-01-01', '2019-12-31'), 10, true);
    }

    public function actionLoadTotal()
    {
        set_time_limit(1200);
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $reporter = new iTunesSalesApi();

//        $settingToken = Settings::findByKey('access_token');
//
//        $accessToken = '';
//
//        if($settingToken){
//            $accessToken = $settingToken->value;
//        }

        $reporter->setAccessToken('0e150538-c9e2-46fc-871f-82911f9f76ee')->setVendor('86908296');

        $reporter->throwErrors = false;
//
//        if($accounts = $reporter->getAccounts()){
//            //Do something with data your're good to go
//            print_r(json_encode($accounts));
//        }
//        else{
//            echo "Api  Errors : ".$reporter->getErrorsAsString();
//        }

        $accounts = $reporter->getAccounts();

//Example with first account returned from list
//account is numeric but setAccount parses raw return from the getAccounts() method
        $reporter->setAccount($accounts[0]);

        $days = DateTimeHelper::getDates('2020-03-13', '2020-06-16');

        foreach ($days as $day)
        {
            $dayFormat = str_replace('-', '', $day);

            try {
                if($data = $reporter->getSalesDailyReport($dayFormat)) { //Will get last year by default
                    //Do something with data your're good to go

//                print_r($data);
//                exit;


                    $output = [];

                    $data = json_decode(json_encode($data), true);

//                    VarDumper::dump($data, 10, true);
//                    exit;


                    $details = $data['details'];

                    foreach ($details as $detail)
                    {

                        $typeTransaction = $detail['Product Type Identifier'];

                        if($typeTransaction == 'M'){
                            $typeTransaction = 'EST';
                        } else if($typeTransaction == 'D') {
                            $typeTransaction = 'VOD';
                        } else if($typeTransaction == 'P'){
                            $typeTransaction = 'EST';
                        } else if($typeTransaction == 'S'){
                            $typeTransaction = 'Возврат';
                        }

                        $output[] = [
                            'apple_identifier' => $detail['Apple Identifier'],
                            'vendor_identifier' => $detail['Vendor Identifier'],
                            'title' => $detail['Title'],
                            'sales' => $detail['Units'],
                            'type' => ($detail['Product Type Identifier'] == 'M' ? 'EST' : 'VOD'),
                            'customer_price' => $detail['Royalty Price'],
                            'customer_currency' => $detail['Customer Currency'],
                            'country' => $detail['Country Code'],
                            'format' => $detail['Asset/Content Flavor'],
                            'genre' => $detail['Primary Genre'],
                            'begin_date' => $detail['Begin Date'],
                            'type_transaction' => $typeTransaction,
                            'date' => $day,
                        ];
                    }


                    foreach ($output as $reportData)
                    {
                        $report = new ReportItunes();
//                    $report->attributes = $reportData;

                        $report->apple_identifier = $reportData['apple_identifier'];
                        $report->vendor_identifier = $reportData['vendor_identifier'];
                        $report->title = $reportData['title'];
                        $report->sales = $reportData['sales'];
                        $report->type = $reportData['type'];
                        $report->customer_price = $reportData['customer_price'];
                        $report->customer_currency = $reportData['customer_currency'];
                        $report->country = $reportData['country'];
                        $report->format = $reportData['format'];
                        $report->genre = $reportData['genre'];
                        $report->begin_date = $reportData['begin_date'];
                        $report->date = $reportData['date'];
                        $report->type_transaction = $reportData['type_transaction'];

                        $technicalPlayground = TechnicalPlaygrounds::find()->where(['additional_value' => $report->apple_identifier])->one();

                        if($technicalPlayground == null){
                            $technicalPlayground = TechnicalPlaygrounds::find()->where(['value_playground' => $report->vendor_identifier])->one();
                        }

                        if($technicalPlayground){
                            $technical = TechnicalInformation::findOne($technicalPlayground->technical_id);

                            if($technical){
                                $film = Films::findOne($technical->film_id);

                                if($film){
                                    $report->film_id = $film->id;
                                }
                            }
                        }

                        $report->save(false);
                    }
                }
                else{
                    echo "Api  Errors : ".$reporter->getErrorsAsString();
                }
            } catch (\Exception $e) {
                echo 'One error: '.$day.'<br>';
            }
        }
    }
}