<?php

namespace app\modules\api\controllers;

use app\models\Resume;
use app\models\Alert;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        // if ($result["callback_query"]) {
        //     $this->botCall($result);
        //     return true;
        // }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

        // $file = file_put_contents('income.txt', $content); // создаем текстовый файл для отладки(по желанию)


        // if (!$text) {
        //     $this->getReq('sendMessage',[
        //         'chat_id'=>'247187885',
        //         'text'=> "content = {$report}",
        //         'parse_mode'=>'HTML',
        //     ]);
        //     return false;
        // }

        if ($text == '/start') {
            $key =
                ['inline_keyboard' =>
                    [
                        [
                            ['text' => 'Да', 'callback_data' => 'start_yes'],
                            ['text' => 'Нет', 'callback_data' => 'start_no']
                        ]
                    ]
                ];
            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>" {$name} Добро пожаловать! Этот чат для общениия с клиентом по результатам пройденого теста!
               Введите код вашей анкеты?",
                //'reply_markup' => json_encode($key),
                // 'parse_mode'=>'HTML',
            ]);

            return true;

        }


        if ($text != '/start') {
            $resum = Resume::find()->where(['code' => $text])->one();

            if (!$resum) {
                $this->getReq('sendMessage',[
                    'chat_id'=> $chat_id,
                    'text'=>"{$name}, мы не нашли вашу анкету! Попробуйте еще раз",
                    //'parse_mode'=>'HTML',
                ]);
                return true;
            } else {
                $resum->connect_telegram = 1;
                $resum->telegram_chat_id = (string)$chat_id;
                if ($resum->save()) {
                    $this->getReq('sendMessage',[
                        'chat_id'=> $chat_id,
                        'text'=>"Ваша Анкета {$resum->id}",
                        //'parse_mode'=>'HTML',
                    ]);
                    return true;
                } else {
                    // $file = file_put_contents('income.txt', $resum);
                }

            }
        }

        $a = serialize($resum->getErrors());

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
            'parse_mode'=>'HTML',
        ]);

        //return true;
    }

    public function actionSend($resum, $text)
    {

        $resum = Resume::find()->where(['id' => $resum])->one();
        if (!$resum) {
            return 'Пользователь не найден!';
        }
        if (!$resum->telegram_chat_id) {
            return 'Пользователь не подключен к чату!';
        }

        $this->getReq('sendMessage',[
            'chat_id'=> $resum->telegram_chat_id,
            'text'=> $text,
            //'parse_mode'=>'HTML',
        ]);

        return true;
    }
    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function botCall($content)
    {
        $report = json_decode($content);

        $text = $content["callback_query"]["data"]; //Текст сообщения

        $chat_id = $result["callback_query"]["chat"]["id"]; //Уникальный идентификатор пользователя

        $name = $result["callback_query"]["from"]["first_name"]; //Юзернейм пользователя


        if ($text == 'start_yes') {


            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> "Отлично пришли мне код анкеты",
                'parse_mode'=>'HTML',
            ]);
            return true;
        }

        if ($text == 'start_no') {
            $key =
                ['inline_keyboard' =>
                    [
                        [
                            ['text' => 'Получил код', 'callback_data' => 'start_yes'],
                        ]
                    ],
                    "one_time_keyboard" => true,
                ];

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> "Не чего страшного, вот ссылка на анкету, можешь ее заполнить 
                и мы начнем искать тебе предложения о работе http://r.teo-job.ru",
                'reply_markup' => json_encode($key),
                'parse_mode'=>'HTML',
            ]);

            $result = $this->getReq('sendMessage',[
                'chat_id'=>'247187885',
                'text'=> "content = {$report}",
                'parse_mode'=>'HTML',
            ]);
            return true;
        }



        return true;
    }



    public function getReq($method,$params=[],$decoded=0){ //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.
        $token = '705864806:AAHqMR2H4F6UlXDvcxeS02cmL1OsFQGFNPY';

        //$proxy = "185.80.149.115:8000";

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        // curl_setopt($ch, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = '705864806:AAHqMR2H4F6UlXDvcxeS02cmL1OsFQGFNPY';
        $proxy = "185.80.149.115:8000";

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://demo.teo-job.ru/api/client/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }



    public function actionAllsend()
    {

        $alerts = Alert::find()->where(['status' => 0])->all();
        /** @var Alert $alert */
        foreach ($alerts as $alert) {
            /** @var Resume $query */
            $query = Resume::find()->where(['questionary_id' => $alert->questionary_id]);
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            // if($alert->statuses != null ) $query->andFilterWhere([ 'status_id' => $alert->statuses ]);
            // if($alert->groups != null ) $query->andFilterWhere([ 'group_id' => $alert->groups ]);
            // if($alert->vacancies != null ) $query->andFilterWhere([ 'vacancy_id' => $vacancies ]);
            $modelsTag = explode(',', $alert->tags);

            if($modelsTag != null ) $query->andFilterWhere([ 'tags' => $modelsTag ]);
            // foreach ($modelsTag as $value) {
            //     $query->andFilterWhere([ 'and',['like', 'tags', $value]]);
            // }
            // $resumesId = [];
            // foreach ($dataProvider->getModels() as $models) {
            //     $modelsTag = json_decode($models->tags);
            //     foreach ($tags as $value) {
            //         foreach ($modelsTag as $tag_id) {
            //             if($tag_id->id == $value) $resumesId [] = $models->id;
            //         }
            //     }
            // }

            //print_r( );
            echo "<pre>";
            print_r($dataProvider->getModels());
            echo "</pre>";
        }

//        $this->getReq('sendMessage',[
//            'chat_id'=> $resum->telegram_chat_id,
//            'text'=> $text,
//            //'parse_mode'=>'HTML',
//        ]);

        //return true;
    }

}
