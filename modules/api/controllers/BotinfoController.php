<?php

namespace app\modules\api\controllers;

use app\models\Questionary;
use app\models\Resume;
use app\models\Users;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        // if ($result["callback_query"]) {
        //     $this->botCall($result);
        //     return true;
        // }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

        // $file = file_put_contents('income.txt', $content); // создаем текстовый файл для отладки(по желанию)


        // if (!$text) {
        //     $this->getReq('sendMessage',[
        //         'chat_id'=>'247187885',
        //         'text'=> "content = {$report}",
        //         'parse_mode'=>'HTML',
        //     ]);
        //     return false;
        // }

        if ($text == '/start') {
            $key =
                ['inline_keyboard' =>
                    [
                        [
                            ['text' => 'Да', 'callback_data' => 'start_yes'],
                            ['text' => 'Нет', 'callback_data' => 'start_no']
                        ]
                    ]
                ];
            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>" {$name} Привет! Меня создали для того чтобы я информарировал тебя, о всех событиях
               Чтобы я начал работать введи свой код:",
                //'reply_markup' => json_encode($key),
                // 'parse_mode'=>'HTML',
            ]);

            return true;

        }


        if ($text != '/start') {
            $users = Users::find()->where(['unique_code_for_telegram' => $text])->one();

            if (!$users) {
                $this->getReq('sendMessage',[
                    'chat_id'=> $chat_id,
                    'text'=>"{$name}, мы не нашли твою компанию! Попробуйте еще раз",
                    //'parse_mode'=>'HTML',
                ]);
                return true;
            } else {
                // $users->connect_telegram = 1;
                $users->telegram_id = (string)$chat_id;
                if ($users->save()) {
                    $this->getReq('sendMessage',[
                        'chat_id'=> $chat_id,
                        'text'=>"Твоя Компания {$users->fio}. Поздравляю с успешным подключением. Жди новостей",
                        //'parse_mode'=>'HTML',
                    ]);
                    return true;
                } else {
                    // $file = file_put_contents('income.txt', $users);
                }

            }
        }

        $a = serialize($users->getErrors());

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
            'parse_mode'=>'HTML',
        ]);

        //return true;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function Sendadmin($text)
    {


        $token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';

        $url =  "https://api.telegram.org/bot{$token}/sendMessage"; //основная строка и метод
        if(count($text)){
            $url=$url.'?'.http_build_query($text);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу


        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }


    public function actionNewresume($resume)
    {
        /** @var Resume $resum */
        $resum = Resume::find()->where(['id' => $resume])->one();
        /** @var Questionary $questionary */
        $questionary = Questionary::find()->where(['id' => $resum->questionary_id])->one();
        /** @var Users $user */
        $user = Users::find()->where(['id' => $questionary->user_id])->one();

        $text = " Анкету: {$questionary->name}\n Заполнил: {$resum->fio} \n Посмотреть анкету можно:  https://demo.teo-job.ru/resume/view?id={$resum->id}";
        if (!$user) {
            return 'Пользователь не найден!';
        }
        if (!$user->telegram_id) {
            return 'Пользователь не подключен к чату!';
        }

        $this->getReq('sendMessage',[
            'chat_id'=> $user->telegram_id,
            'text'=> $text,
            //'parse_mode'=>'HTML',
        ]);

        return true;
    }
    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function botCall($content)
    {
        $report = json_decode($content);

        $text = $content["callback_query"]["data"]; //Текст сообщения

        $chat_id = $result["callback_query"]["chat"]["id"]; //Уникальный идентификатор пользователя

        $name = $result["callback_query"]["from"]["first_name"]; //Юзернейм пользователя


        if ($text == 'start_yes') {


            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> "Отлично пришли мне код анкеты",
                'parse_mode'=>'HTML',
            ]);
            return true;
        }

        if ($text == 'start_no') {
            $key =
                ['inline_keyboard' =>
                    [
                        [
                            ['text' => 'Получил код', 'callback_data' => 'start_yes'],
                        ]
                    ],
                    "one_time_keyboard" => true,
                ];

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> "Не чего страшного, вот ссылка на анкету, можешь ее заполнить 
                и мы начнем искать тебе предложения о работе https://resume.teo-job.ru/",
                'reply_markup' => json_encode($key),
                'parse_mode'=>'HTML',
            ]);

            $result = $this->getReq('sendMessage',[
                'chat_id'=>'247187885',
                'text'=> "content = {$report}",
                'parse_mode'=>'HTML',
            ]);
            return true;
        }



        return true;
    }



    public function getReq($method,$params=[],$decoded=0){ //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.
        $token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';

        //$proxy = "185.80.149.115:8000";

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        // curl_setopt($ch, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = "217.29.114.40:44523";

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://demo.teo-job.ru/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }



}
