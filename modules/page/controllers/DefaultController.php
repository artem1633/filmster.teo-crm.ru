<?php

namespace app\modules\page\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Questionary;
use app\models\Questions;
use app\models\Applications;
use app\models\Resume;
use app\models\additional\Contacts;
use yii\web\NotFoundHttpException;
use app\models\Users;
use app\models\Settings;

/**
 * Default controller for the `page` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (!isset($_GET['link'])) throw new NotFoundHttpException('The requested page does not exist.');
        $questionary = Questionary::find()->where(['link' => $_GET['link'] ])->one();
        $post = $request->post();
        $model = new Resume();
        $fio = '';
        $questions = Questions::find()->where(['questionary_id' => $questionary->id])->orderBy([ 'ordering' => SORT_ASC])->all();
        $setting = Settings::find()->where(['key' => 'telegram_chat'])->one();

        if($questionary != null){
            
            $result = [];
            if($post){

                /*echo "<pre>";
                print_r($post);
                echo "</pre>";
                die;*/

                foreach ($questions as $question) {
                    $key = $question->getTypeCode() . $question->id;
                    if(isset($post[$key])){
                        if($question->type == 0 || $question->type == 1 || $question->type == 2 || $question->type == 5){
                            if($question->type == 0) $fio = $post[$key];
                            $result [] = [
                                'question' => $question->id,
                                'value' => $post[$key],
                            ];                            
                        }else{
                            $keys = [];
                            if($question->type == 3){
                                foreach ($post[$key] as $value) {
                                    $keys [] = [
                                        'key' => $value,
                                    ];  
                                }
                            }else{
                                foreach ($post[$key] as $value) {
                                    $keys [] = [
                                        'key' => $value,
                                    ];  
                                }
                            }
                            $result [] = [
                                'question' => $question->id,
                                'value' => json_encode($keys),
                            ];
                        }
                    }else{
                        $result [] = [
                            'question' => $question->id,
                            'value' => '',
                        ];
                    }
                }

                $model->questionary_id = $questionary->id;
                $model->group_id = null;
                $model->status_id = null;
                $model->fit = null;
                $model->category = null;
                $model->is_new = 1;
                $model->mark = null;
                $model->telegram_chat_id = null;
                $model->connect_telegram = null;
                $model->new_sms = 1;
                $model->vacancy_id = $questionary->vacancy_id;
                $model->correspondence = 0;
                $model->values = json_encode($result);
                $model->fio = $fio;

                if($model->validate() && $model->save()){
                    Yii::$app->db->createCommand()->update('questionary', 
                        [ 'filling_count' => $questionary->filling_count + 1 ], 
                        [ 'id' => $questionary->id ])
                    ->execute();

                    file_get_contents("https://demo.teo-job.ru/api/botinfo/newresume?resume=".$model->id);
                    
                    $setting = Settings::find()->where(['key' => 'text_after_filling'])->one();
                    $text = str_replace ("{unique_code_for_telegram}", $model->code , $setting->text);
                    Yii::$app->session->setFlash('success', "Успешно отправлено. <br> ".$text);
                    /*Yii::$app->session->setFlash('success', "Успешно отправлено. <br> По результатам анкты с вами свяжуться! Если вы хотите получать больше предложений о работе вступите в чат телеграм @teo_job_bot и введите код анкеты  ".$model->code);*/

                    return $this->render('@app/views/questions/link', [
                        'questionary' => $questionary,
                        'questions' => $questions,
                    ]);  
                }
            }
            else{
                Yii::$app->db->createCommand()->update('questionary', 
                    [ 'count' => $questionary->count + 1 ], 
                    [ 'id' => $questionary->id ])
                ->execute();
                return $this->render('@app/views/questions/link', [
                    'questionary' => $questionary,
                    'questions' => $questions,
                ]);                
            }

        }
        else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPolicy()
    {
        return $this->render('index', [
        ]);  
    }
}
