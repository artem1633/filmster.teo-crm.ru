<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReportItunes */
?>
<div class="report-itunes-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'film_id',
            'apple_identifier',
            'vendor_identifier',
            'title',
            'sales',
            'type',
            'customer_price',
            'customer_currency',
            'country',
            'format',
            'genre',
            'begin_date',
            'date',
            'type_transaction',
        ],
    ]) ?>

</div>
