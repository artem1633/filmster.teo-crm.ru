<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReportItunes */
?>
<div class="report-itunes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
