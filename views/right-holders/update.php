<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RightHolders */
?>
<div class="right-holders-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
