<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>

<div class="users-form">
    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'user_id')->dropdownList($model->getUsersList(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'name_eng')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'tax_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'registry_code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'primary_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">Адрес компании</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'street1')->textArea(['rows' => 2]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'street2')->textArea(['rows' => 2]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <?= $form->field($model, 'country_id')->widget(kartik\select2\Select2::classname(), [
                                'data' => $model->getCountriesList(),
                                'options' => ['placeholder' => 'Выберите ...'],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                ],
                            ])?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'city_id')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'index')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">Банк и счета</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'bank_name')->textArea(['rows' => 1]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'bank_street1')->textArea(['rows' => 1]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'bank_street2')->textArea(['rows' => 1]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'bank_state')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'bank_index')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'bic_swift')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <?= $form->field($model, 'bank_account')->textInput(['type' => 'number']) ?>
                        </div>
                        <div class="col-md-2">    
                             <?= $form->field($model, 'bank_account_currency')->dropdownList($model->getCurrency(),['prompt' => 'Выберите']) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'iban')->textInput(['type' => 'number']) ?>
                        </div>
                        <div class="col-md-2">    
                            <?= $form->field($model, 'iban_currency')->dropdownList($model->getCurrency(),['prompt' => 'Выберите']) ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($model, 'correspondent_account')->textInput(['type' => 'number']) ?>
                        </div>
                        <div class="col-md-2">    
                            <?= $form->field($model, 'correspondent_account_currency')->dropdownList($model->getCurrency(),['prompt' => 'Выберите']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
