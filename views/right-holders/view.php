<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RightHolders */
?>
<div class="right-holders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'name_ru',
            'name_eng',
            'tax_id',
            'registry_code',
            'primary_number',
            'street1:ntext',
            'street2:ntext',
            'country_id',
            'city_id',
            'state',
            'index',
            'bank_name',
            'bank_street1:ntext',
            'bank_street2:ntext',
            'bank_country_id',
            'bank_state',
            'bank_index',
            'bic_swift',
            'bank_account',
            'bank_account_currency',
            'iban',
            'iban_currency',
            'correspondent_account',
            'correspondent_account_currency',
        ],
    ]) ?>

</div>
