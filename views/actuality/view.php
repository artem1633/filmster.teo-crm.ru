<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Actuality */
?>
<div class="actuality-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
