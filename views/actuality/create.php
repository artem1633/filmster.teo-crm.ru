<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Actuality */

?>
<div class="actuality-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
