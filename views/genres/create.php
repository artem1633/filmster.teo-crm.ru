<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Genres */

?>
<div class="genres-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
