<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Genres */
?>
<div class="genres-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
