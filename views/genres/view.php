<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Genres */
?>
<div class="genres-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
