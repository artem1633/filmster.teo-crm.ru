<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TechnicalInformation */

?>
<div class="technical-information-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
