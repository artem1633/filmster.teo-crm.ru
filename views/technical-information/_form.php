<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PreferenceBooks;

/* @var $this yii\web\View */
/* @var $model app\models\TechnicalInformation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="technical-information-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'playground')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getPlayGroundList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'value_playground')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'additional_value')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
           <?= $form->field($model, 'video')->dropdownList(PreferenceBooks::videoFormatList(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'audio')->dropdownList(PreferenceBooks::audioFormatList(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'duration')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
           <?= $form->field($model, 'playback_speed')->dropdownList(PreferenceBooks::playbackSpeedList(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'video_language')->dropdownList($model->getLanguages(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'audio_language')->dropdownList($model->getLanguages(),['prompt' => 'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
           <?= $form->field($model, 'source_availability')->dropdownList(PreferenceBooks::sourceAvailabilityList(),['prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'source_link')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="panel panel-danger">
            <div class="panel-heading ui-draggable-handle">
                <h5>Субтитры</h5>
            </div>
            <div class="panel-body">
                <div class="row">                    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <?= $form->field($model, 'burnt_subtitles')->checkbox(['id' => 'burnt_subtitles']) ?>
                        </div>
                    </div>
                    <div class="col-md-8" id="burnt_value">
                        <?= $form->field($model, 'burnt_value')->widget(kartik\select2\Select2::classname(), [
                            'data' => $model->getBurnt(),
                            'options' => ['placeholder' => 'Выберите ...'],
                            'size' => kartik\select2\Select2::SMALL,
                            'pluginOptions' => [
                                'tags' => true,
                                'multiple' => true,
                                'allowClear' => true,
                            ],
                        ])?>
                    </div>
                </div>

                <div class="row">    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <?= $form->field($model, 'forced_subtitles')->checkbox(['id' => 'forced_subtitles']) ?>
                        </div>
                    </div>
                    <div id="forced_value">
                        <div class="col-md-5">
                            <?= $form->field($model, 'forced_value')->widget(kartik\select2\Select2::classname(), [
                                'data' => $model->getForced(),
                                'options' => ['placeholder' => 'Выберите ...'],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [
                                    'tags' => true,
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                            ])?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'forced_files')->fileInput(); ?>
                        </div>                        
                    </div>
                </div>

                <div class="row">                    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <?= $form->field($model, 'subtitles')->checkbox(['id' => 'subtitles']) ?>
                        </div>
                    </div>
                    <div id="subtitles_value">                        
                        <div class="col-md-5">
                            <?= $form->field($model, 'subtitles_value')->widget(kartik\select2\Select2::classname(), [
                                'data' => $model->getSubtitles(),
                                'options' => ['placeholder' => 'Выберите ...'],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [
                                    'tags' => true,
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                            ])?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'subtitles_files')->fileInput(); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <?= $form->field($model, 'sdh_subtitles')->checkbox(['id' => 'sdh_subtitles']) ?>
                        </div>
                    </div>
                    <div id="sdh_value">
                        <div class="col-md-5">
                            <?= $form->field($model, 'sdh_value')->widget(kartik\select2\Select2::classname(), [
                                'data' => $model->getSdh(),
                                'options' => ['placeholder' => 'Выберите ...'],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [
                                    'tags' => true,
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                            ])?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'sdh_files')->fileInput(); ?>
                        </div>                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <?= $form->field($model, 'presense_cc')->checkbox(['id' => 'presense_cc']) ?>
                        </div>
                    </div>
                    <div id="presense_value">
                        <div class="col-md-5">
                            <?= $form->field($model, 'presense_value')->widget(kartik\select2\Select2::classname(), [
                                'data' => $model->getPresent(),
                                'options' => ['placeholder' => 'Выберите ...'],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [
                                    'tags' => true,
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                            ])?>
                        </div>
                        <div class="col-md-3">
                            <?= $form->field($model, 'presense_files')->fileInput(); ?>
                        </div>                        
                    </div>
                </div>

            </div>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS
    var checked = $('#burnt_subtitles').is(':checked');
    if(checked == 0) $('#burnt_value').hide();
    else $('#burnt_value').show();

    $('#burnt_subtitles').on('change', function() 
    {
        var checked = $('#burnt_subtitles').is(':checked');
        if(checked == 0) $('#burnt_value').hide();
        else $('#burnt_value').show();
    });

    var checked = $('#forced_subtitles').is(':checked');
    if(checked == 0) $('#forced_value').hide();
    else $('#forced_value').show();

    $('#forced_subtitles').on('change', function() 
    {
        var checked = $('#forced_subtitles').is(':checked');
        if(checked == 0) $('#forced_value').hide();
        else $('#forced_value').show();
    });

    var checked = $('#subtitles').is(':checked');
    if(checked == 0) $('#subtitles_value').hide();
    else $('#subtitles_value').show();

    $('#subtitles').on('change', function() 
    {
        var checked = $('#subtitles').is(':checked');
        if(checked == 0) $('#subtitles_value').hide();
        else $('#subtitles_value').show();
    });

    var checked = $('#sdh_subtitles').is(':checked');
    if(checked == 0) $('#sdh_value').hide();
    else $('#sdh_value').show();

    $('#sdh_subtitles').on('change', function() 
    {
        var checked = $('#sdh_subtitles').is(':checked');
        if(checked == 0) $('#sdh_value').hide();
        else $('#sdh_value').show();
    });

    var checked = $('#presense_cc').is(':checked');
    if(checked == 0) $('#presense_value').hide();
    else $('#presense_value').show();

    $('#presense_cc').on('change', function() 
    {
        var checked = $('#presense_cc').is(':checked');
        if(checked == 0) $('#presense_value').hide();
        else $('#presense_value').show();
    });

JS
);
?>