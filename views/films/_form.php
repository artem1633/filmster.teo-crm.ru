<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Films */
/* @var $form yii\widgets\ActiveForm */
$individual = 0;
$multiple = 0;
$awards = 0;

$result = explode(',', $model->tags);
$model->tags = $result;
$data = [];
foreach ($result as $value) {
    if($value != null || $value != '')
    {
        $data += [
            $value => $value,
        ];
    }
}

?>

<div class="films-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-xs-12">
              <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 col-xs-6">
              <?= $form->field($model, 'type_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getFilmsTypeList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])?>
        </div>
        <div class="col-md-3 col-xs-6">
               <?= $form->field($model, 'language_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getLanguagesList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ])?>
        </div>
    </div>
    <div class="row" id="type" <?=$model->type->key != 'serial' ? 'style="display:none"' : '' ?> >
        <div class="col-md-4 col-xs-4">
              <?= $form->field($model, 'number_season')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
              <?= $form->field($model, 'number_seriya')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4 col-xs-4">
              <?= $form->field($model, 'alternative')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'certificate')->checkbox() ?>
            <?= $form->field($model, 'hide_film')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
             <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-6">
             <?= $form->field($model, 'imdb')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
             <?= $form->field($model, 'rotten')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-6">
             <?= $form->field($model, 'eidr')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
             <?= $form->field($model, 'kinopoisk')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-xs-3">
            <?= $form->field($model, 'year_issue')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '####','options' => ['placeholder' => '9999','class'=>'form-control' ]])?>
        </div>
        <div class="col-md-3 col-xs-3">
             <?= $form->field($model, 'rellase_date')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'startView'=>'year',
                    'minViewMode'=>'months',
                    'format' => 'M-yyyy',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
       <div class="col-md-3 col-xs-3">
             <?= $form->field($model, 'duration')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3 col-xs-3">
             <?= $form->field($model, 'actuality_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getActualityList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        //'tags' => true,
                        'allowClear' => true,
                    ],
                ])?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'manufacturer_id')->textInput(['maxlength' => true]) ?>
        </div>
         <div class="col-md-6 col-xs6">
             <?= $form->field($model, 'country_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getCountryList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        //'tags' => true,
                        'allowClear' => true,
                    ],
                ])?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'genres')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getGenresList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        'multiple' => true,
                        'allowClear' => true,
                    ],
                ])?>
        </div>
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'tags')->widget(kartik\select2\Select2::classname(), [
                'data' => $data,
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'tags' => true,
                    'multiple' => true,
                    'allowClear' => true,
                ],
            ])?>
        </div>        
    </div>

    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Бюджет фильма                       
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add_input_individual"> + Добавить вариант</button>  
                    </h5>
                </div>
                <div class="panel-body">

                     <div class="row" id="individual_answer"  >
                        <div class="col-md-12">
                            <div id="dynamic_individual">
                                <?php
                                    foreach (json_decode($model->budget) as $value) {
                                        $individual++;
                                        if($individual == 1){
                                        ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> Сумма </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> Валюта </div> 
                                </div>
                                <?php } ?>

                                <div class="row" style="margin-top:10px;" id="budget<?=$individual?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="number" class="form-control" name="summa[]" value="<?=$value->summa?>" /> 
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <select class="form-control" name="currency[]" aria-required="true" aria-invalid="false">
                                            <?= $model->getCurrency($value->currency) ?>
                                        </select> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_individual" id="<?=$individual?>" class="btn btn-danger btn_remove_individual"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="individual_count" name="individual_count" value="<?=$individual?>"/> 
                            </div>
                        </div>
                    </div>

                </div>                           
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        Возрастной рейтинг фильма                     
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add_input_multiple"> + Добавить вариант</button>
                    </h5>
                </div>
                <div class="panel-body">

                    <div class="row" id="multiple"  >
                        <div class="col-md-12">
                            <div id="dynamic_multiple">
                                <?php
                                    foreach (json_decode($model->reyting) as $value) {
                                        $multiple++;
                                        if($multiple == 1){
                                ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> Страна </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> Возрастной рейтинг </div> 
                                </div>
                                <?php } ?>
                                <div class="row" style="margin-top:10px;" id="multiple<?=$multiple?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <select class="form-control" name="country[]" aria-required="true" aria-invalid="false">
                                            <?= $model->getCountrySelected($value->country) ?>
                                        </select> 
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <select class="form-control" name="youth[]" aria-required="true" aria-invalid="false">
                                            <?= $model->getYouth($value->youth) ?>                                            
                                        </select> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_multiple" id="<?=$multiple?>" class="btn btn-danger btn_remove_multiple"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="multiple_count" name="multiple_count" value="<?=$multiple?>"/> 
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        Награды
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add_input_award"> + Добавить награду</button>
                    </h5>
                </div>
                <div class="panel-body">

                    <div class="row" id="award"  >
                        <div class="col-md-12">
                            <div id="dynamic_award">
                                <?php
                                    foreach (json_decode($model->awards) as $value) {
                                        $award++;
                                        if($award == 1){
                                ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия члена группы</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">Роль члена группы</div> 
                                </div>
                                <?php } ?>
                                <div class="row" style="margin-top:10px;" id="award<?=$award?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="text" class="form-control" name="festival[]"value="<?=$value->festival?>" /> 
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <input type="text" class="form-control" name="prize[]"value="<?=$value->prize?>" /> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_award" id="<?=$award?>" class="btn btn-danger btn_remove_award"> 
                                            <i class="glyphicon glyphicon-trash"></i> 
                                        </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="award_count" name="award_count" value="<?=$award?>"/> 
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>

    </div>
    

    <input type="hidden" class="form-control" id="currencyList" name="currencyList" /> 
    <input type="hidden" class="form-control" id="countryList" name="countryList" /> 
    <input type="hidden" class="form-control" id="youthList" name="youthList" /> 
  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php 
$this->registerJs(<<<JS
    $('select#films-type_id').on('change', function() 
    {  
        var value = this.value;
        $('#type').hide();
        if(value == 1) $('#type').show();
    }
);
JS
);
?>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var indiv_count = document.getElementById('individual_count').value;
    var multiple_count = document.getElementById('multiple_count').value;
    var award_count = document.getElementById('award_count').value;

    $.get('get-currency',{ },function(data) { $('#currencyList').val(data); } );
    $.get('get-country',{ },function(data) { $('#countryList').val(data); } );
    $.get('get-youth',{ },function(data) { $('#youthList').val(data); } );

    $('#add_input_individual').click(function(){
        indiv_count++; 
        if(indiv_count == 1) $('#dynamic_individual').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6"> Сумма </div><div class="col-md-5 col-sm-5 col-xs-5"> Валюта </div> </div>');

        $('#dynamic_individual').append('<div class="row" style="margin-top:10px;" id="budget'+indiv_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="number" class="form-control" name="summa[]" /> </div><div class="col-md-5 col-sm-5 col-xs-5"> <select class="form-control" name="currency[]" aria-required="true" aria-invalid="false">'+$("#currencyList").val()+'</select> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_individual" id="'+indiv_count+'" class="btn btn-danger btn_remove_individual"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });


    $(document).on('click', '.btn_remove_individual', function(){
        var button_id = $(this).attr("id");
        $('#budget'+button_id+'').remove();
    });

    $('#add_input_multiple').click(function(){
        multiple_count++;
        if(multiple_count == 1) $('#dynamic_multiple').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6"> Страна </div><div class="col-md-5 col-sm-5 col-xs-5"> Возрастной рейтинг </div> </div>');

        $('#dynamic_multiple').append('<div class="row" style="margin-top:10px;" id="multiple'+multiple_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <select class="form-control" name="country[]" aria-required="true" aria-invalid="false">'+$("#countryList").val()+'</select> </div><div class="col-md-5 col-sm-5 col-xs-5"> <select class="form-control" name="youth[]" aria-required="true" aria-invalid="false">'+$("#youthList").val()+'</select> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_multiple" id="'+multiple_count+'" class="btn btn-danger btn_remove_multiple"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });

        $(document).on('click', '.btn_remove_multiple', function(){
            var button_id = $(this).attr("id");
            $('#multiple'+button_id+'').remove();
    });

    $('#add_input_award').click(function(){
        award_count++;
        if(award_count == 1) $('#dynamic_award').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Название фестиваля</div><div class="col-md-5 col-sm-5 col-xs-5">Приз</div></div>');

        $('#dynamic_award').append('<div class="row" style="margin-top:10px;" id="award'+award_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="festival[]" /> </div><div class="col-md-5 col-sm-5 col-xs-5"> <input type="text" class="form-control" name="prize[]" /> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_award" id="'+award_count+'" class="btn btn-danger btn_remove_award"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });

        $(document).on('click', '.btn_remove_award', function(){
            var button_id = $(this).attr("id");
            $('#award'+button_id+'').remove();
    });

});

JS
);
?>