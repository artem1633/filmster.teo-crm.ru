<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Films */

?>
<div class="films-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
