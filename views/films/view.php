<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Films */
CrudAsset::register($this);
$this->title = $model->name;
?>
<h1>
    <span>
        <a class="btn btn-info" title="Список фильмов" href="<?=Url::toRoute(['/films/index'])?>"><i style="font-size: 20px;" class="fa fa-arrow-left"></i></a>
    </span>
    <?=$model->name?>
</h1>
<div class="films-view" style="margin-top: 10px;">
    <div class="panel panel-default tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">Основное</a></li>
            <li ><a href="#tab2" data-toggle="tab" aria-expanded="false">Локализации</a></li>
            <li ><a href="#tab3" data-toggle="tab" aria-expanded="true">Техническая информация</a></li>
            <li ><a href="#tab4" data-toggle="tab" aria-expanded="true">Права</a></li>
            <!-- <li ><a href="#tab5" data-toggle="tab" aria-expanded="true">Сводная</a></li> -->
        </ul>
        <div class="panel-body tab-content">
            <div class="tab-pane active" id="tab1">
                <?= $this->render('tabs/main', [
                    'model' => $model,
                ]);?>
            </div>
            <div class="tab-pane" id="tab2">
                <?= $this->render('tabs/localization', [
                    'model' => $model,
                    'localizationProvider' => $localizationProvider,
                ]);?>
            </div>
            <div class="tab-pane" id="tab3">
                <?= $this->render('tabs/technical', [
                    'model' => $model,
                    'technicalProvider' => $technicalProvider,
                    'technical' => $technical,
                ]);?>
            </div>
            <div class="tab-pane " id="tab4">
                <?= $this->render('tabs/rights', [
                    'id' => $model->id,
                    'filmsApplication' => $filmsApplication,
                ]);?>
            </div>
            <!-- <div class="tab-pane" id="tab5">
                                  <p>Сводная</p>
                              </div>       -->                  
        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>