<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Films;
use app\models\Localization;
use app\models\TechnicalInformation;
use app\models\TechnicalPlaygrounds;
use app\models\Applications;
use app\models\FilmsApplication;
use app\models\Contract;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'poster',
        'content' => function($data){
            $localization = Localization::find()->where(['film_id' => $data->id, 'type' => 1])->one();
            if($localization != null){
                if (!file_exists('uploads/localization/' . $localization->id . '/' . $localization->poster23) || $localization->poster23 == '') {
                    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
                } else {
                    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/localization/' . $localization->id . '/'. $localization->poster23;
                }
            }
            else{
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
            }
            return Html::img($path, [ 'style' => 'width:35px;']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($data){
            return Html::a($data->name,  ['films/view', 'id' => $data->id], ['data-pjax'=>'0', /*'target' => '_blank',*/ 'data-toggle'=>'tooltip']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type_id',
        'filter' => Films::getFilmsTypeList(),
        'content' => function($data){
            return $data->type->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'year_issue',
        'width' => '40px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'width' => '80px',
        'attribute'=>'country_id',
        'filter' => Films::getCountryList(),
        'content' => function($data){
            return $data->country->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'genres',
        'content' => function($data){
            return $data->genresDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'manufacturer_id',
        'content' => function($data){
            $result = '';
            $client_contract = FilmsApplication::find()->where(['film_id' => $data->id])->all();
            if($client_contract != null) {
                foreach ($client_contract as $value) {
                    $result .= $value->application->contract->companyLic->name_ru . '<br/>';
                }
            }
            return $result;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'create_date',
        'filter' => kartik\date\DatePicker::widget([
            'model' => $searchModel,
            'attribute' => 'create_date',
            'layout' => '{input}{picker}',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
        ]),
        'format' => 'html',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'platform',
        'width'=>'180px',
        'header' => 'Площадка',
        'content' => function($data){
            $technical = TechnicalInformation::find()->where(['film_id' => $data->id])->one();
            if($technical != null) {
                $playgrounds = TechnicalPlaygrounds::find()->where(['technical_id' => $technical->id])->all();
                $result = '';
                foreach ($playgrounds as $playground) {
                    $result .= '<b>' . $playground->playground->name_ru . '</b> ' . $playground->additional_value . '<br>';
                }
                return $result;
            }
        }
    ],
       [
        'class'=>'\kartik\grid\DataColumn',
        'width'=>'100px',
        'header' => 'Индетификатор',
        'content' => function($data){
            $technical = TechnicalInformation::find()->where(['film_id' => $data->id])->one();
            if($technical != null) {
                $playgrounds = TechnicalPlaygrounds::find()->where(['technical_id' => $technical->id])->all();
                $result = '';
                foreach ($playgrounds as $playground) {
                    $result .= '<b>'.$playground->playground->name_ru.': </b>' . $playground->value_playground . '<br>';
                }
                return $result;
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'buttons',
        'width'=>'68px',
        'label' => 'Действие',
        'content' => function($data){
            $url = Url::to(['/films/view', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', $url, ['data-pjax'=>'0','title'=>'Изменить', /*'target' => '_blank',*/ 'data-toggle'=>'tooltip']);

            /*$url = Url::to(['/questionary/copy', 'id' => $data->id]);
            $copy = Html::a('<button class="btn btn-info btn-xs"><i class="fa fa-files-o"></i></button>', $url, ['role'=>'modal-remote','title'=>'Копировать', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/questions/sorting', 'questionary_id' => $data->id]);
            $sorting = Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-sort"></span></button>', $url, ['role'=>'modal-remote','title'=>'Поменять место вопросов', 'data-toggle'=>'tooltip']);

            $url = Url::to(['/alert/create', 'questionary_id' => $data->id]);
            $envelope = Html::a('<button class="btn btn-success btn-xs"><span class="glyphicon glyphicon-envelope"></span></button>', $url, ['role'=>'modal-remote','title'=>'Cделать рассылку', 'data-toggle'=>'tooltip']);*/

            $url = Url::to(['/films/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url, 
                    [
                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ]);

            return '<center>' . $update . '&nbsp; ' . $delete . '</center>';
        }
    ],
   /* [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view} {delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'], 
    ],*/

];   