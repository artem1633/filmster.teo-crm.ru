<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Films;
use app\models\TechnicalInformation;
use app\models\TechnicalPlaygrounds;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'poster',
        'content' => function($data){
            $localization = Localization::find()->where(['film_id' => $data->id, 'type' => 1])->one();
            if($localization != null){
                if (!file_exists('uploads/localization/' . $localization->id . '/' . $localization->poster23) || $localization->poster23 == '') {
                    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
                } else {
                    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/localization/' . $localization->id . '/'. $localization->poster23;
                }
            }
            else{
                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
            }
            return Html::img($path, [ 'style' => 'width:35px;']);
        }
    ],*/
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'film_id',
        //'filter' => ArrayHelper::map(Films::find()->all(), 'id', 'name'),
        'content' => function($data){
            return $data->film->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'film_id',
        'header' => 'Идентификатор площадки',
        'content' => function($data){
            $technical = TechnicalInformation::find()->where(['film_id' => $data->film_id])->one();
            if($technical != null) {
                $playgrounds = TechnicalPlaygrounds::find()->where(['technical_id' => $technical->id])->all();
                $result = '';
                foreach ($playgrounds as $playground) {
                    $result .= '<b>' . $playground->playground->name_ru . '</b> ' . $playground->value_playground . '<br>';
                }
                return $result;
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'film_id',
        'header' => 'Доп. Идентификатор площадки',
        'content' => function($data){
            $technical = TechnicalInformation::find()->where(['film_id' => $data->film_id])->one();
            if($technical != null) {
                $playgrounds = TechnicalPlaygrounds::find()->where(['technical_id' => $technical->id])->all();
                $result = '';
                foreach ($playgrounds as $playground) {
                    $result .= '<b>' . $playground->playground->name_ru . '</b> ' . $playground->additional_value . '<br>';
                }
                return $result;
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'application_id',
        'header' => 'Клиент',
        'content' => function($data){
            return $data->application->contract->companyLic->name_ru;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'application_id',
        'header' => '№ договора',
        'content' => function($data){
            return $data->application->contract->number;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'application_id',
        'header' => '№ Приложения',
        'content' => function($data){
            return $data->application->number;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'close_date',
        'content' => function($data){
            return date('d.m.Y', strtotime($data->close_date));
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'close_date',
        'header' => 'Счетчик дней до закрытия',
        'content' => function($data){
            $result = (strtotime($data->close_date) - strtotime(date('Y-m-d')))/86400;
            return $result;
        }
    ],
];   