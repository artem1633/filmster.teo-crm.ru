<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
/* @var $form yii\widgets\ActiveForm */
$individual = 0;
$multiple = 0;
$_csrf = \Yii::$app->request->getCsrfToken();

?>
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <div class="form-group field-localization-name required">
                <label class="control-label" for="localization-name"><?=$model->getAttributeLabel('name')?></label>
                <input type="text" id="localization-name<?=$model->id?>" class="form-control" name="Localization[name]" value="<?=$model->name?>" maxlength="255" aria-required="true" onchange="$.get('/localization/set-values', {'id':<?=$model->id?>, 'attribute': 'name', 'value':$('#localization-name<?=$model->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-6 col-xs-6">
            <label class="control-label" for="localization-name"><?=$model->getAttributeLabel('language_id')?></label>
            <?= kartik\select2\Select2::widget([
                'name' => 'select_language'.$model->id,
                'id' => 'select_language'.$model->id,
                'value' => $model->language_id, 
                'data' => $model->getLanguagesList(),
                'options' => [
                    'onchange'=>'
                        var a = $( "#select_language'.$model->id.'" ).val();
                        $.post( "/localization/set-values?id='.$model->id.'&attribute=language_id&value="+a, function( data ){  });
                        ' 
                ],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [ ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group field-localization-description has-success">
                <label class="control-label" for="localization-description"><?=$model->getAttributeLabel('description')?></label>
                <textarea id="localization-description<?=$model->id?>" class="form-control" name="Localization[description]" rows="3" aria-invalid="false" onchange="$.get('/localization/set-values', {'id':<?=$model->id?>, 'attribute': 'description', 'value':$('#localization-description<?=$model->id?>').val()}, function(data){} ); $.pjax.reload({container:'#main-pjax', async: false}); " ><?=$model->description?></textarea>
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Актерский состав                       
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add<?=$model->id?>"> + Добавить вариант</button>  
                    </h5>
                </div>
                <div class="panel-body">

                     <div class="row" >
                        <div class="col-md-12">
                            <div id="dynamic<?=$model->id?>">
                                <?php
                                    foreach (json_decode($model->cast) as $value) {
                                        $individual++;
                                        if($individual == 1){
                                        ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия актера/актрисы</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">Роль актера/актрисы</div> 
                                </div>
                                <?php } ?>

                                <div class="row" style="margin-top:10px;" id="budget<?=$model->id . $individual?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="text" class="form-control" name="akter<?=$model->id?>[]" id="akter<?=$model->id?>" value="<?=$value->akter?>" /> 
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <input type="text" class="form-control" name="role<?=$model->id?>[]" id="role<?=$model->id?>" value="<?=$value->role?>" /> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" style="width: 30px;" name="remove<?=$model->id?>" id="<?=$individual?>" class="btn btn-danger btn_remove<?=$model->id?>"> 
                                            <i style="margin-left: -6px;" class="glyphicon glyphicon-trash"></i> 
                                        </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="individual_count<?=$model->id?>" name="individual_count<?=$model->id?>" value="<?=$individual?>"/> 
                            </div>
                        </div>
                    </div>

                </div>                           
            </div>
        </div>

        <div class="col-md-12">
        <br><br>
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        Съемочная группа                    
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add_multiple<?=$model->id?>"> + Добавить вариант</button>
                    </h5>
                </div>
                <div class="panel-body">

                    <div class="row" >
                        <div class="col-md-12">
                            <div id="dynamic_multiple<?=$model->id?>">
                                <?php
                                    foreach (json_decode($model->film_crew) as $value) {
                                        $multiple++;
                                        if($multiple == 1){
                                ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия члена группы</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">Роль члена группы</div> 
                                </div>
                                <?php } ?>
                                <div class="row" style="margin-top:10px;" id="multiple<?=$model->id .$multiple?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="text" class="form-control" name="names_group<?=$model->id?>[]" id="names_group<?=$model->id?>" value="<?=$value->names_group?>" />
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <select class="form-control" name="group_member<?=$model->id?>[]" id="group_member<?=$model->id?>" aria-required="true" aria-invalid="false">
                                            <?= $model->getRoleSelected($value->group_member) ?>                                            
                                        </select> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" style="width: 30px;" name="remove_multiple<?=$model->id?>" id="<?=$multiple?>" class="btn btn-danger btn_remove_multiple<?=$model->id?>"> 
                                            <i style="margin-left: -6px;" class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="multiple_count<?=$model->id?>" name="multiple_count<?=$model->id?>" value="<?=$multiple?>"/> 
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>

    </div>

    <div class="row">
    <br> <br> 
        <div class="col-md-5 col-xs-12">
            <div id="poster23_file<?=$model->id?>">
                <img style="width:150px; height:225px;" src="<?=$path?>">
            </div>
            <br>   
            <div id="dropBox23<?=$model->id?>"></div>
            <button style="display:block;width:120px; height:30px;" name="button23<?=$model->id?>[]" id="button23<?=$model->id?>" onclick="document.getElementById('fileInput<?=$model->id?>').click()">Постер 2х3</button>
            <input type="file" name="fileInput<?=$model->id?>[]" style="display:none" id="fileInput<?=$model->id?>" class="poster23_image<?=$model->id?>" accept="image/*" />
        </div>

        <div class="col-md-7 col-xs-12">
            <div id="poster169_file<?=$model->id?>">
                <img style="width:262px; height:147px;" src="<?=$path169?>">
            </div>
            <br>
            <div id="dropBox169<?=$model->id?>"></div>
            <button style="display:block;width:120px; height:30px;" name="button169<?=$model->id?>[]" id="button169<?=$model->id?>" onclick="document.getElementById('fileInput_169<?=$model->id?>').click()">Постер 16х9</button>
            <input type="file" name="fileInput_169<?=$model->id?>[]" style="display:none" id="fileInput_169<?=$model->id?>" class="poster169_image<?=$model->id?>" accept="image/*" multiple length="1024"  />
        </div>

        <div class="col-md-12 col-xs-12">
            <br><br>
            <div class="panel panel-success">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        Кадры из фильма
                        <span class="pull-right">
                            <a role="modal-remote" title="Изменить" href="<?=Url::toRoute(['/localization/dropzone', 'id' => $model->id])?>">
                                <i style="font-size: 20px;" class="glyphicon glyphicon-circle-arrow-down"></i>
                            </a>
                        </span>
                    </h5>
                </div>
                <div class="panel-body">
                    <?php $images = json_decode($model->photos);
                        foreach ($images as $image) {
                    ?>                                     
                    <img style="width: 100px; height: 100px; object-fit: cover; margin-left: 5px; margin-top: 20px;" src="http://<?= $_SERVER['SERVER_NAME'].'/'.$image->path ?>" href="http://<?= $_SERVER['SERVER_NAME'].'/'.$image->path ?>">
                    <?php } ?>     
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="form-control" id="aktorsList" name="aktorsList" /> 

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
       
    /*Актерский состав*/
    $(document).on('change', '#role$model->id', function(e){
        var akters = []; var roles = [];
        $('input[name^=\'akter$model->id\']').each(function() { akters.push($(this).val()); });
        $('input[name^=\'role$model->id\']').each(function() { roles.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, akters: akters, roles: roles },
           url: '/localization/set-acters',
        });
    });

    $(document).on('change', '#akter$model->id', function(e){
        var akters = []; var roles = [];
        $('input[name^=\'akter$model->id\']').each(function() { akters.push($(this).val()); });
        $('input[name^=\'role$model->id\']').each(function() { roles.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, akters: akters, roles: roles },
           url: '/localization/set-acters',
        });
    });

    /*Съемочная группа*/
    $(document).on('change', '#names_group$model->id', function(e){
        var group_member = []; var names_group = [];
        $('select[name^=\'group_member$model->id\']').each(function() { group_member.push($(this).val()); });
        $('input[name^=\'names_group$model->id\']').each(function() { names_group.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, group_member: group_member, names_group: names_group },
           url: '/localization/set-crew',
        });
    });

    $(document).on('change', '#group_member$model->id', function(e){
        var group_member = []; var names_group = [];
        $('select[name^=\'group_member$model->id\']').each(function() { group_member.push($(this).val());  });
        $('input[name^=\'names_group$model->id\']').each(function() { names_group.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, group_member: group_member, names_group: names_group },
           url: '/localization/set-crew',
           success: function(msg){/*alert(msg);*/}
        });
    });

    /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#dropBox23$model->id").click(function(){
            $("#fileInput$model->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput$model->id\']').on('change', fileUpload);
    });

    function fileUpload(event){
        //notify user about the file upload status
        $("#dropBox23$model->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#dropBox23$model->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$model->id}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/localization/set-poster23', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#dropBox23$model->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#dropBox23$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }


    $(function(){
        //file input field trigger when the drop box is clicked
        $("#dropBox169$model->id").click(function(){
            $("#fileInput_169$model->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput_169$model->id\']').on('change', fileUpload169);
    });
    



    function fileUpload169(event){
        //notify user about the file upload status
        $("#dropBox169$model->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        if(this.files[0].size > 1024*1024*2){
           $("#dropBox169$model->id").html("Файл должен быть менее 2 МБ.");
           this.value = "";
        }
        else{
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#dropBox169$model->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$model->id}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/localization/set-poster169', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#dropBox169$model->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#dropBox169$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }
    }


    var indiv_count = document.getElementById('individual_count$model->id').value;
    var multiple_count = document.getElementById('multiple_count$model->id').value;
    var fileCollection = new Array();

    $.get('/localization/get-role',{ },function(data) { $('#aktorsList').val(data); } );

    $('#add$model->id').click(function(){
        indiv_count++; 
        if(indiv_count == 1) $('#dynamic$model->id').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия актера/актрисы</div><div class="col-md-5 col-sm-5 col-xs-5">Роль актера/актрисы</div> </div>');

        $('#dynamic$model->id').append('<div class="row" style="margin-top:10px;" id="budget$model->id'+indiv_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="akter$model->id[]" id="akter$model->id" /></div><div class="col-md-5 col-sm-5 col-xs-5"> <input type="text" class="form-control" name="role$model->id[]" id="role$model->id" /> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" style="width: 30px;" name="remove$model->id" id="'+indiv_count+'" class="btn btn-danger btn_remove$model->id"> <i style="margin-left: -6px;" class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });


    $(document).on('click', '.btn_remove$model->id', function(){
        var button_id = $(this).attr("id");
        $('#budget$model->id'+button_id+'').remove();

        var akters = []; var roles = [];
        $('input[name^=\'akter$model->id\']').each(function() { akters.push($(this).val()); });
        $('input[name^=\'role$model->id\']').each(function() { roles.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, akters: akters, roles: roles },
           url: '/localization/set-acters',
           success: function(msg){}
        }); 

    });

    $('#add_multiple$model->id').click(function(){
        multiple_count++;
        if(multiple_count == 1) $('#dynamic_multiple$model->id').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия члена группы</div><div class="col-md-5 col-sm-5 col-xs-5">Роль члена группы</div></div>');

        $('#dynamic_multiple$model->id').append('<div class="row" style="margin-top:10px;" id="multiple$model->id'+multiple_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="names_group$model->id[]" id="names_group$model->id" /> </div><div class="col-md-5 col-sm-5 col-xs-5"> <select class="form-control" name="group_member$model->id[]" id="group_member$model->id" aria-required="true" aria-invalid="false">'+$("#aktorsList").val()+'</select> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" style="width: 30px;" name="remove_multiple$model->id" id="'+multiple_count+'" class="btn btn-danger btn_remove_multiple$model->id"> <i style="margin-left: -6px;" class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });

        $(document).on('click', '.btn_remove_multiple$model->id', function(){
            var button_id = $(this).attr("id");
            $('#multiple$model->id'+button_id+'').remove();

            var group_member = []; var names_group = [];
            $('select[name^=\'group_member$model->id\']').each(function() { group_member.push($(this).val()); });
            $('input[name^=\'names_group$model->id\']').each(function() { names_group.push($(this).val()); });
            $.ajax({
               type: 'POST',
               data: {id: $model->id, group_member: group_member, names_group: names_group },
               url: '/localization/set-crew',
            });
    });

    $(document).on('change', '.poster23_image$model->id', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:200px; height:300px;" src="'+e.target.result+'"> ';
                $('#poster23_file$model->id').html('');
                $('#poster23_file$model->id').append(template);
            };
        });
    });

    $(document).on('change', '.poster169_image$model->id', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:200px; height:300px;" src="'+e.target.result+'"> ';
                $('#poster169_file$model->id').html('');
                $('#poster169_file$model->id').append(template);
            };
        });
    });

});

JS
);
?>