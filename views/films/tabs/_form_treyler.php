<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PreferenceBooks;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\TechnicalInformation */
/* @var $form yii\widgets\ActiveForm */

$_csrf = \Yii::$app->request->getCsrfToken();
?>

<div class="technical-information-form">

    <?php //$form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
           <label class="control-label"><?=$model->getAttributeLabel('video')?></label>
            <?= Html::activeDropDownList($model, 'video', PreferenceBooks::videoFormatList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'id' => 'video_'.$model->id,
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-treyler?id="+id+"&attribute=video&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <label class="control-label"><?=$model->getAttributeLabel('audio')?></label>
            <?= Html::activeDropDownList($model, 'audio', PreferenceBooks::audioFormatList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'id' => 'audio_'.$model->id,
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-treyler?id="+id+"&attribute=audio&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <div class="form-group field-treyler-duration">
                <label class="control-label" for="treyler-duration"><?=$model->getAttributeLabel('duration')?></label>
                <input type="number" id="duration_<?=$model->id?>" class="form-control" name="Treyler[duration]" value="<?=$model->duration?>" aria-invalid="false" onchange="$.get('/technical-information/set-treyler', {'id':<?=$model->id?>, 'attribute': 'duration', 'value':$('#duration_<?=$model->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label class="control-label"><?=$model->getAttributeLabel('playback_speed')?></label>
            <?= Html::activeDropDownList($model, 'playback_speed', PreferenceBooks::playbackSpeedList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'id' => 'playback_speed_'.$model->id,
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-treyler?id="+id+"&attribute=playback_speed&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <label class="control-label"><?=$model->getAttributeLabel('video_language')?></label>
            <?= Html::activeDropDownList($model, 'video_language', $model->getLanguages(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'id' => 'video_language_'.$model->id,
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-treyler?id="+id+"&attribute=video_language&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <label class="control-label"><?=$model->getAttributeLabel('audio_language')?></label>
            <?= Html::activeDropDownList($model, 'audio_language', $model->getLanguages(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'id' => 'audio_language_'.$model->id,
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-treyler?id="+id+"&attribute=audio_language&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
    </div>

    <div class="row"><br>
        <div class="col-md-4">
           <label class="control-label"><?=$model->getAttributeLabel('source_availability')?></label>
            <?= Html::activeDropDownList($model, 'source_availability', PreferenceBooks::sourceAvailabilityList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'id' => 'source_availability_'.$model->id,
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-treyler?id="+id+"&attribute=source_availability&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-8">
            <div class="form-group field-technicalinformation-source_link">
                <label class="control-label"><?=$model->getAttributeLabel('source_link')?></label>
                <input type="text" id="source_link_<?=$model->id?>" class="form-control" name="Treyler[source_link]" value="<?=$model->source_link?>" aria-invalid="false" onchange="$.get('/technical-information/set-treyler', {'id':<?=$model->id?>, 'attribute': 'source_link', 'value':$('#source_link_<?=$model->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="panel panel-danger">
            <div class="panel-heading ui-draggable-handle">
                <h5>Субтитры</h5>
            </div>
            <div class="panel-body">
                <div class="row">                    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <label>
                                <input type="checkbox" id="burnt_subtitles<?=$model->id?>" <?= $model->burnt_subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('burnt_subtitles')?>
                            </label>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-8" id="burnt_value<?=$model->id?>">
                        <label class="control-label"><?=$model->getAttributeLabel('burnt_value')?></label>
                        <?= Select2::widget([
                            'name' => 'burnt_value[]',
                            'data' => $model->getLanguages(),
                            'value' => $model->getBurnt(),
                            'size' => 'sm',
                            'options' => [ 'id' => 'treyler_burnt_value_'.$model->id, 'placeholder' => 'Выберите ...'],
                            'pluginOptions' => [
                                /*'tags' => true,*/
                                'multiple' => true,
                                'allowClear' => true,
                            ],
                            'pluginEvents' => [
                                "change" => "function() 
                                {
                                    $.get('/technical-information/set-treyler',
                                    { 'id' : ".$model->id.", 'attribute' : 'burnt_value', 'value' : JSON.stringify($(this).val()) },
                                        function(data){}
                                    );
                                }",
                            ],
                        ])
                        ?>
                    </div>
                </div>

                <div class="row">    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <label>
                                <input type="checkbox" id="forced_subtitles<?=$model->id?>" <?= $model->forced_subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('forced_subtitles')?>
                            </label>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div id="forced_value_<?=$model->id?>">
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('forced_value')?></label>
                            <?= Select2::widget([
                                'name' => 'forced_value[]',
                                'data' => $model->getLanguages(),
                                'value' => $model->getForced(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'treyler_forced_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-treyler',
                                        { 'id' : ".$model->id.", 'attribute' : 'forced_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div id="dropBoxTreyler<?=$model->id?>">
                                <label class="control-label"><?=$model->getAttributeLabel('forced_files')?></label>
                            </div>
                            <input type="file" name="forced_files<?=$model->id?>" multiple="" />
                        </div>
                    </div>
                </div>

                <div class="row">                    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <label>
                                <input type="checkbox" id="subtitles<?=$model->id?>" <?= $model->subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('subtitles')?>
                            </label>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div id="treyler_subtitles_value<?=$model->id?>">                        
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('subtitles_value')?></label>
                            <?= Select2::widget([
                                'name' => 'subtitles_value[]',
                                'data' => $model->getLanguages(),
                                'value' => $model->getSubtitles(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'tre_subtitles_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-treyler',
                                        { 'id' : ".$model->id.", 'attribute' : 'subtitles_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div id="treylerSubtitlesFiles<?=$model->id?>">
                                <label class="control-label"><?=$model->getAttributeLabel('forced_files')?></label>
                            </div>
                            <input type="file" name="subtitlesFiles<?=$model->id?>" multiple="" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <label>
                                <input type="checkbox" id="sdh_subtitles<?=$model->id?>" <?= $model->sdh_subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('sdh_subtitles')?>
                            </label>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div id="treyler_sdh_value<?=$model->id?>">
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('sdh_value')?></label>
                            <?= Select2::widget([
                                'name' => 'sdh_value[]',
                                'data' => $model->getLanguages(),
                                'value' => $model->getSdh(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'trey_sdh_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-treyler',
                                        { 'id' : ".$model->id.", 'attribute' : 'sdh_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div> 
                        <div class="col-md-3">
                            <div id="treylerSdhFiles<?=$model->id?>">
                                <label class="control-label"><?=$model->getAttributeLabel('sdh_files')?></label>
                            </div>
                            <input type="file" name="sdhFiles<?=$model->id?>" multiple="" />
                        </div>                   
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <label>
                                <input type="checkbox" id="presense_cc<?=$model->id?>" <?= $model->presense_cc == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('presense_cc')?>
                            </label>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div id="treyler_presense_value<?=$model->id?>">
                        <div class="col-md-5" >
                            <label class="control-label"><?=$model->getAttributeLabel('presense_value')?></label>
                            <?= Select2::widget([
                                'name' => 'presense_value[]',
                                'data' => $model->getLanguages(),
                                'value' => $model->getPresent(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'trey_presense_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-treyler',
                                        { 'id' : ".$model->id.", 'attribute' : 'presense_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div> 
                        <div class="col-md-3">
                            <div id="treylerPresenseFiles<?=$model->id?>">
                                <label class="control-label"><?=$model->getAttributeLabel('presense_files')?></label>
                            </div>
                            <input type="file" name="presenseFiles<?=$model->id?>" multiple="" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php //ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS
    var checked = $('#burnt_subtitles$model->id').is(':checked');
    if(checked == 0) $('#burnt_value$model->id').hide();
    else $('#burnt_value$model->id').show();

    $('#burnt_subtitles$model->id').on('change', function() 
    {
        var checked = $('#burnt_subtitles$model->id').is(':checked');
        if(checked == 0) {
            $('#burnt_value$model->id').hide();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'burnt_subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#burnt_value$model->id').show();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'burnt_subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#forced_subtitles$model->id').is(':checked');
    if(checked == 0) $('#forced_value_$model->id').hide();
    else $('#forced_value_$model->id').show();

    $('#forced_subtitles$model->id').on('change', function() 
    {
        var checked = $('#forced_subtitles$model->id').is(':checked');
        if(checked == 0) {
            $('#forced_value_$model->id').hide();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'forced_subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#forced_value_$model->id').show();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'forced_subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#subtitles$model->id').is(':checked');
    if(checked == 0) $('#treyler_subtitles_value$model->id').hide();
    else $('#treyler_subtitles_value$model->id').show();

    $('#subtitles$model->id').on('change', function() 
    {
        var checked = $('#subtitles$model->id').is(':checked');
        if(checked == 0) {
            $('#treyler_subtitles_value$model->id').hide();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#treyler_subtitles_value$model->id').show();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#sdh_subtitles$model->id').is(':checked');
    if(checked == 0) $('#treyler_sdh_value$model->id').hide();
    else $('#treyler_sdh_value$model->id').show();

    $('#sdh_subtitles$model->id').on('change', function() 
    {
        var checked = $('#sdh_subtitles$model->id').is(':checked');
        if(checked == 0) {
            $('#treyler_sdh_value$model->id').hide();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'sdh_subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#treyler_sdh_value$model->id').show();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'sdh_subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#presense_cc$model->id').is(':checked');
    if(checked == 0) $('#treyler_presense_value$model->id').hide();
    else $('#treyler_presense_value$model->id').show();

    $('#presense_cc$model->id').on('change', function() 
    {
        var checked = $('#presense_cc$model->id').is(':checked');
        if(checked == 0) {
            $('#treyler_presense_value$model->id').hide();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'presense_cc', 'value': 0}, function(data){} );
        }
        else {
            $('#treyler_presense_value$model->id').show();
            $.get('/technical-information/set-treyler', {'id':$model->id, 'attribute': 'presense_cc', 'value': 1}, function(data){} );
        }
    });



    /*Загрузка файла*/
    $('input[name^=\'forced_files$model->id\']').on('change', function(image){
        $("#dropBoxTreyler$model->id").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->forced_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'forced_files');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-treyler-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#dropBoxTreyler$model->id").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#dropBoxTreyler$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-treyler-files-value', {'id':$model->id, 'attribute': 'forced_file', 'value': JsonResult}, function(data){} );
    });

    $('input[name^=\'subtitlesFiles$model->id\']').on('change', function(image){
        $("#treylerSubtitlesFiles$model->id").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->subtitles_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'subtitles_files');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-treyler-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#treylerSubtitlesFiles$model->id").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#treylerSubtitlesFiles$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-treyler-files-value', {'id':$model->id, 'attribute': 'subtitles_file', 'value': JsonResult}, function(data){} );
    });

    $('input[name^=\'sdhFiles$model->id\']').on('change', function(image){
        $("#treylerSdhFiles$model->id").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->sdh_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'sdh_file');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-treyler-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#treylerSdhFiles$model->id").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#treylerSdhFiles$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-treyler-files-value', {'id':$model->id, 'attribute': 'sdh_file', 'value': JsonResult}, function(data){} );
    });

    $('input[name^=\'presenseFiles$model->id\']').on('change', function(image){
        $("#treylerPresenseFiles$model->id").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->presense_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'presense_files');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-treyler-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#treylerPresenseFiles$model->id").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#treylerPresenseFiles$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-treyler-files-value', {'id':$model->id, 'attribute': 'presense_file', 'value': JsonResult}, function(data){} );
    });

JS
);
?>