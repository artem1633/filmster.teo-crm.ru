<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\PreferenceBooks;
use app\models\Treyler;

?>

<div class="row">
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'technical-pjax']) ?>                                 
    <div class="col-md-12 col-sm-12">
        <div class="table-responsive">
            <?php if($technical != null) {?>
                <h3><a class="btn btn-xs btn-warning" role="modal-remote" href="<?=Url::toRoute(['/technical-information/update', 'id' => $technical->id])?>">Изменить <i class="glyphicon glyphicon-pencil" ></i></a></h3>
                <table class="table table-bordered">
                    <br>
                    <tbody>
                        <tr>
                            <th><?=$technical->getAttributeLabel('playground')?></th>
                            <th><?=$technical->getAttributeLabel('value_playground')?></th>
                            <th><?=$technical->getAttributeLabel('additional_value')?></th>
                            <th><?=$technical->getAttributeLabel('video')?></th>
                        </tr>
                        <tr>
                            <td><?=$technical->playground0->name?></td>
                            <td><?=$technical->value_playground?></td>
                            <td><?=$technical->additional_value?></td>
                            <td><?=PreferenceBooks::videoFormatList()[$technical->video]?></td>
                        </tr>
                        <tr>
                            <th><?=$technical->getAttributeLabel('audio')?></th>
                            <th><?=$technical->getAttributeLabel('duration')?></th>
                            <th><?=$technical->getAttributeLabel('playback_speed')?></th>
                            <th><?=$technical->getAttributeLabel('video_language')?></th>
                        </tr>
                        <tr>
                            <td><?=PreferenceBooks::audioFormatList()[$technical->audio]?></td>
                            <td><?=$technical->duration?></td>
                            <td><?=PreferenceBooks::playbackSpeedList()[$technical->playback_speed]?></td>
                            <td><?=$technical->languageDescription($technical->video_language)?></td>
                        </tr>
                        <tr>
                            <th><?=$technical->getAttributeLabel('audio_language')?></th>
                            <th><?=$technical->getAttributeLabel('source_availability')?></th>
                            <th><?=$technical->getAttributeLabel('source_link')?></th>
                            <th><?=$technical->getAttributeLabel('burnt_subtitles')?></th>
                        </tr>
                        <tr>
                            <td><?=$technical->languageDescription($technical->audio_language)?></td>
                            <td><?=PreferenceBooks::sourceAvailabilityList()[$technical->source_availability]?></td>
                            <td><?=$technical->source_link?></td>
                            <td><?=$technical->burntSubtitles()?></td>
                        </tr>
                        <tr>
                            <th>
                                <?=$technical->getAttributeLabel('forced_subtitles') .
                                ($technical->forced_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'technical-information/' . $technical->id . '/' . $technical->forced_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' ) ?>
                                    
                            </th>
                            <th>
                                <?=$technical->getAttributeLabel('subtitles') .
                                ($technical->subtitles_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'technical-information/' . $technical->id . '/' . $technical->subtitles_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' )  ?>
                                    
                            </th>
                            <th>
                                <?=$technical->getAttributeLabel('sdh_subtitles') .
                                ($technical->sdh_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'technical-information/' . $technical->id . '/' . $technical->sdh_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' ) ?>
                            </th>
                            <th>
                                <?=$technical->getAttributeLabel('presense_cc') .
                                ($technical->presense_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'technical-information/' . $technical->id . '/' . $technical->presense_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' ) ?>
                            </th>
                        </tr>
                        <tr>
                            <td><?=$technical->forcedValue()?></td>
                            <td><?=$technical->subtitlesValue()?></td>
                            <td><?=$technical->sdhValue()?></td>
                            <td><?=$technical->presense_cc ? $technical->presenseValue():''?></td>
                        </tr>
                    </tbody>
           		</table>
                <h3><a class="btn btn-xs btn-success" role="modal-remote" href="<?=Url::toRoute(['/technical-information/create-treyler', 'technical_information_id' => $technical->id])?>"> Добавить трейлер <i class="fa fa-plus" ></i></a></h3>
                <br>      
                <div class="panel-group accordion">
                    <?php 
                        $treylers = Treyler::find()->where(['technical_information_id' => $technical->id])->all();
                        $i = 0;
                        foreach ($treylers as $treyler) {
                            $i++;
                        ?>
                        <div class="panel panel-danger">
                            <div class="panel-heading ui-draggable-handle">
                                <h4 class="panel-title">
                                    <a href="#<?=$treyler->id?>">
                                        Трейлер #<?=$i?>
                                    </a>
                                </h4>
                                    <span class="pull-right">
                                        <a role="modal-remote" title="Изменить" href="<?=Url::toRoute(['/technical-information/update-treyler', 'id' => $treyler->id])?>">
                                            <i class="glyphicon glyphicon-pencil"></i> &nbsp;
                                        </a>
                                        <?= Html::a('<span class="text-danger glyphicon glyphicon-trash"></span>', ['/technical-information/delete-treyler', 'id' => $treyler->id], [
                                            'role'=>'modal-remote','title'=>'Удалить', 
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-toggle'=>'tooltip',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]) ?>
                                    </span>
                            </div>
                            <div class="panel-body" id="<?=$treyler->id?>" style="display: none;">
                                <table class="table table-bordered">
                                    <br>
                                    <tbody>
                                        <tr>
                                            <th><?=$treyler->getAttributeLabel('video')?></th>
                                            <th><?=$treyler->getAttributeLabel('audio')?></th>
                                            <th><?=$treyler->getAttributeLabel('duration')?></th>
                                            <th><?=$treyler->getAttributeLabel('playback_speed')?></th>
                                        </tr>
                                        <tr>
                                            <td><?=PreferenceBooks::videoFormatList()[$treyler->video]?></td>
                                            <td><?=PreferenceBooks::audioFormatList()[$treyler->audio]?></td>
                                            <td><?=$treyler->duration?></td>
                                            <td><?=PreferenceBooks::playbackSpeedList()[$treyler->playback_speed]?></td>
                                        </tr>
                                        <tr>
                                            <th><?=$treyler->getAttributeLabel('video_language')?></th>
                                            <th><?=$treyler->getAttributeLabel('audio_language')?></th>
                                            <th><?=$treyler->getAttributeLabel('source_availability')?></th>
                                            <th><?=$treyler->getAttributeLabel('source_link')?></th>
                                        </tr>
                                        <tr>
                                            <td><?=$treyler->languageDescription($technical->video_language)?></td>
                                            <td><?=$treyler->languageDescription($technical->audio_language)?></td>
                                            <td><?=PreferenceBooks::sourceAvailabilityList()[$treyler->source_availability]?></td>
                                            <td><?=$treyler->source_link?></td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <?=$treyler->getAttributeLabel('forced_subtitles') .
                                                ($treyler->forced_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'treyler/' . $treyler->id . '/' . $treyler->forced_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' ) ?>
                                                    
                                            </th>
                                            <th>
                                                <?=$treyler->getAttributeLabel('subtitles') .
                                                ($treyler->subtitles_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'treyler/' . $treyler->id . '/' . $treyler->subtitles_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' )  ?>
                                                    
                                            </th>
                                            <th>
                                                <?=$treyler->getAttributeLabel('sdh_subtitles') .
                                                ($treyler->sdh_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'treyler/' . $treyler->id . '/' . $treyler->sdh_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' ) ?>
                                            </th>
                                            <th>
                                                <?=$treyler->getAttributeLabel('presense_cc') .
                                                ($treyler->presense_file != null ? ' &nbsp;' . Html::a(' <i class="fa fa-download"></i>', ['/technical-information/send-document', 'file' => 'treyler/' . $treyler->id . '/' . $treyler->presense_file],['title'=> 'Скачать файлу', 'data-pjax' => 0]) : '' ) ?>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td><?=$treyler->forcedValue()?></td>
                                            <td><?=$treyler->subtitlesValue()?></td>
                                            <td><?=$treyler->sdhValue()?></td>
                                            <td><?=$treyler->presenseValue()?></td>
                                        </tr>
                                        <tr>
                                            <th><?=$treyler->getAttributeLabel('burnt_subtitles')?></th>
                                            <td colspan="3"><?=$treyler->burntSubtitles()?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php  } ?>

                </div>
            <?php } else {?>
                <h3><a class="btn btn-xs btn-warning" role="modal-remote" href="<?=Url::toRoute(['/technical-information/create', 'film_id' => $model->id])?>"> Добавить <i class="fa fa-plus" ></i></a></h3>
             <?php } ?>
   		</div>
	</div>
	<?php Pjax::end() ?> 
</div>