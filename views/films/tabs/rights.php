<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\PreferenceBooks;


?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'rights-pjax']) ?>
    <div class="row">
        <!-- <div class="col-md-12" style="margin-bottom: 20px;">
            <?php // Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/rights/create', 'film_id' => $id],['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info btn-xs'])?>
        </div> -->
<?php
    $i = 0;
    foreach ($filmsApplication as $model) {
        $i++;
        if($i == 1) {
            $color = '#95b75d';
            $class = 'success';
        }
        else {
            $color = '#E04B4A';
            $class = 'danger';
        }
?>
    <div class="col-md-12">
         <div class="footer-widget-panel panel-group" id="accordion1">
            <div class="panel panel-<?=$class?>">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        <a class="<?=$i == 1 ? '' : 'collapsed' ?>" data-toggle="collapse" data-parent="#accordion1" href="#discussionId<?=$model->id?>">
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-down"></i>                            
                            </span> 
                            <div style="display:inline-block; background-color: <?=$color?>; width: 15px; height: 15px; border-radius: 50%; "></div>
                                Договор № <?=$model->application->number ?> (от <?=$model->application->date_cr?> )
                        </a>
                        <!-- <span class="pull-right">
                            <a role="modal-remote" href="<?php //Url::toRoute(['/rights/update', 'id' => $model->id])?>">
                                <i class="fa fa-gear"></i> &nbsp;
                            </a>
                        </span> -->
                    </h5>
                </div> 
                <div id="discussionId<?=$model->id?>" class="collapse <?=$i == 1 ? 'in' : '' ?> ">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <b> Приложение к договору #<?=$model->application->contract->number?> </b>
                        </div>
                        <div class="row" style="padding-top : 30px; ">
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                Дата от<br>
                                <b>-</b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                Дата до<br>
                                <b>-</b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('min_warranty')?><br>
                                <b><?=$model->getMinWarranty()?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('tex_adap')?><br>
                                <b><?=$model->getTexAdap()?></b>
                            </div>
                             <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('maintenance')?><br>
                                <b><?=$model->getMaintenance()?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('territory')?><br>
                                <b><?=$model->getTerritory()?></b>
                            </div>
                        </div>
                        <div class="row" style="padding-top : 30px; ">
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                Роляти<br>
                                <b><?=$model->rolyati_x . '/' . $model->rolyati_y?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('platforms')?><br>
                                <b><?=$model->getPlatform()?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('price')?><br>
                                <b><?=PreferenceBooks::priceList()[$model->price]?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('est_open')?><br>
                                <b><?=$model->est_open != null ? date('d.m.Y', strtotime($model->est_open) ) : '-' ?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('vod_open')?><br>
                                <b><?=$model->vod_open != null ? date('d.m.Y', strtotime($model->vod_open) ) : '-' ?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('close_date')?><br>
                                <b><?=$model->close_date != null ? date('d.m.Y', strtotime($model->close_date) ) : '-' ?></b>
                            </div>
                        </div>
                        <?php //if($model->application->settings == 1) { ?>
                        <div class="row" style="padding-top : 30px; ">
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('svod')?><br>
                                <b><?=$model->svod?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('pvod')?><br>
                                <b><?=$model->pvod?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('avod')?><br>
                                <b><?=$model->avod?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('catch_up')?><br>
                                <b><?=$model->catch_up?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('tv')?><br>
                                <b><?=$model->tv?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('flat_rate')?><br>
                                <b><?=$model->flat_rate . ' ' . $model->getCurrency()[$model->flat_rate_currency] ?></b>
                            </div>
                        </div>
                        <?php //} ?>
                    </div>
                </div>                         
            </div>
        </div>
    </div>

<?php  } ?>
    
</div>
<?php Pjax::end() ?> 