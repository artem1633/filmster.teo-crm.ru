<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Localization;
use app\models\TechnicalPlaygrounds;
use app\models\PreferenceBooks;

//$this->title = 'fff';
$localization = Localization::find()->where(['film_id' => $model->id, 'type' => 1])->one();
if($localization != null){
    if (!file_exists('uploads/localization/' . $localization->id . '/' . $localization->poster23) || $localization->poster23 == ''){
        $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
    } else {
        $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/localization/' . $localization->id . '/'. $localization->poster23;
    }
}
else{
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
}

?>

<div class="row">
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'main-pjax']) ?>  <h3>Информация <a class="btn btn-xs btn-warning" role="modal-remote" href="<?=Url::toRoute(['/films/update', 'id' => $model->id])?>"><i class="fa fa-pencil" ></i></a></h3>  
    <hr>                             
    <div class="col-md-8 col-sm-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('name')?></label>
                    <input type="text" id="info-name<?=$model->id?>" class="form-control" name="Info[name]" value="<?=$model->name?>" maxlength="255" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'name', 'value':$('#info-name<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-6">
                    <label class="control-label"><?=$model->getAttributeLabel('language_id')?></label>
                     <?= kartik\select2\Select2::widget([
                                'name' => 'main_language'.$model->id,
                                'id' => 'main_language'.$model->id,
                                'value' => $model->language_id, 
                                'data' => $model->getLanguagesList(),
                                'options' => [
                                    'placeholder' => 'Выберите',
                                    'onchange'=>'
                                        var a = $( "#main_language'.$model->id.'" ).val();
                                        $.post( "/films/set-values?id='.$model->id.'&attribute=language_id&value="+a, function( data ){  });
                                        ' 
                                ],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [ ],
                            ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <input type="checkbox" id="info-queue-order<?=$model->id?>" value="<?=$model->certificate == 1 ? 0 : 1 ?>" <?=$model->certificate ? "checked" : ""?> onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'certificate', 'value':$('#info-queue-order<?=$model->id?>').val()}, function(data){} );"> <label class="control-label"><?=$model->getAttributeLabel('certificate')?></label>
            </div>
            <br>
            <br>
            <div class="col-md-12">
                <input type="checkbox" id="info-queue-order-hide<?=$model->id ?>" value="<?=$model->certificate == 1 ? 0 : 1 ?>" <?=$model->hide_film ? "checked" : ""?> onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'hide_film', 'value':$('#info-queue-order-hide<?=$model->id?>').val()}, function(data){} );"> 
                <label class="control-label"><?=$model->getAttributeLabel('hide_film')?> </label>
                <div class="help-block"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('description')?></label>
                    <textarea id="film-description<?=$model->id?>" class="form-control" name="Films[description]" rows="3" aria-invalid="false" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'description', 'value':$('#film-description<?=$model->id?>').val()}, function(data){$.pjax.reload({container:'#main-pjax', async: false});} );" ><?=$model->description?></textarea>
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('year_issue')?></label>
                    <input type="number" id="info-year_issue<?=$model->id?>" placeholder="____" class="form-control" name="Info[year_issue]" value="<?=$model->year_issue?>" maxlength="4" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'year_issue', 'value':$('#info-year_issue<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-4">
                <label class="control-label"><?=$model->getAttributeLabel('rellase_date')?></label>
                <?= kartik\date\DatePicker::widget([
                    'name' => 'rellase_date'.$application->id,
                    'value' => $model->rellase_date !== null ? $model->getReleaseDate() : '',
                    'options' => ['id' => 'rellase_date'.$model->id, 'placeholder' => 'Выберите'],
                    'language' => 'ru-RU',
                    'layout' => '{picker}{input}',
                    'pluginEvents' => [
                        "changeDate" => "function(e) {  $.get('/films/set-values', {'id':".$model->id.", 'attribute': 'rellase_date', 'value':$('#rellase_date".$model->id."').val() }, function(data){ } ) }",
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'startView'=>'year',
                        'minViewMode'=>'months',
                        'format' => 'M-yyyy',
                        //'todayHighlight' => true,
                    ]
                ]);?>
            </div>
            <div class="col-md-4">
                <label class="control-label"><?=$model->getAttributeLabel('country_id')?></label>
                        <?= kartik\select2\Select2::widget([
                                'name' => 'country_id'.$model->id,
                                'id' => 'country_id'.$model->id,
                                'value' => $model->country_id, 
                                'data' => $model->getCountryList(),
                                'options' => [
                                    'placeholder' => 'Выберите',
                                    'onchange'=>'
                                        var a = $( "#country_id'.$model->id.'" ).val();
                                        $.post( "/films/set-values?id='.$model->id.'&attribute=country_id&value="+a, function( data ){  });
                                        ' 
                                ],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [ ],
                            ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <label class="control-label"><?=$model->getAttributeLabel('actuality_id')?></label>
                <?= kartik\select2\Select2::widget([
                                'name' => 'actuality_id'.$model->id,
                                'id' => 'actuality_id'.$model->id,
                                'value' => $model->actuality_id, 
                                'data' => $model->getActualityList(),
                                'options' => [
                                    'placeholder' => 'Выберите',
                                    'onchange'=>'
                                        var a = $( "#actuality_id'.$model->id.'" ).val();
                                        $.post( "/films/set-values?id='.$model->id.'&attribute=actuality_id&value="+a, function( data ){  });
                                        ' 
                                ],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [ ],
                            ]) ?>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('manufacturer_id')?></label>
                    <input type="text" id="info-manufacturer_id<?=$model->id?>" placeholder="-" class="form-control" name="Info[manufacturer_id]" value="<?=$model->manufacturer_id?>" maxlength="255" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'manufacturer_id', 'value':$('#info-manufacturer_id<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('duration')?></label>
                    <input type="number" id="info-duration<?=$model->id?>" class="form-control" name="Info[duration]" value="<?=$model->duration?>" maxlength="255" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'duration', 'value':$('#info-duration<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="row">       
            <div class="col-md-4">
                 <label class="control-label"><?=$model->getAttributeLabel('genres')?></label>
                 <?php 
                    $result = [];
                    foreach (json_decode($model->genres) as $value) {
                        $result [] = $value->value;
                    }
                        ?>
                            <?= kartik\select2\Select2::widget([
                                'name' => 'genres',
                                'data' => $model->getGenresList(),
                                'value' => $result,
                                'size' => 'sm',
                                'options' => [ 'id' => 'genres'.$model->id,'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'multiple' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/films/set-genres',
                                        { 'val' : JSON.stringify($(this).val()), 'id' : $(this).attr('id') },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ]);?>

            </div>
            <div class="col-md-4">
                <label class="control-label"><?=$model->getAttributeLabel('tags')?></label>
                <?php
                    $result = explode(',', $model->tags);
                    $model->tags = $result;
                    $data = [];
                    foreach ($result as $value) {
                        if($value != null || $value != '')
                        {
                            $data += [
                                $value => $value,
                            ];
                        }
                    }
                ?>
                    <?= kartik\select2\Select2::widget([
                                    'name' => 'tags'.$model->id,
                                    'id' => 'tags'.$model->id,
                                    'value' => $model->tags, 
                                    'data' => $data,
                                    'options' => [
                                        'placeholder' => 'Выберите',
                                        'onchange'=>'
                                            var a = $( "#tags'.$model->id.'" ).val();
                                            $.post( "/films/set-values?id='.$model->id.'&attribute=tags&value="+a, function( data ){  });
                                            ' 
                                    ],
                                    'size' => kartik\select2\Select2::SMALL,
                                    'pluginOptions' => [ 
                                        'tags' => true,
                                        'multiple' => true,
                                        'allowClear' => true,
                                    ],
                                ]) ?>
            </div>
            <div class="col-md-4">
                    <label class="control-label"><?=$model->getAttributeLabel('type_id')?></label>
                    
                            <?= kartik\select2\Select2::widget([
                                'name' => 'type_id'.$model->id,
                                'id' => 'type_id'.$model->id,
                                'value' => $model->type_id, 
                                'data' => $model->getFilmsTypeList(),
                                'options' => [
                                    'placeholder' => 'Выберите',
                                    'onchange'=>'
                                        var a = $( "#type_id'.$model->id.'" ).val();
                                        $.post( "/films/set-values?id='.$model->id.'&attribute=type_id&value="+a, function( data ){  });
                                        if($( "#type_id'.$model->id.'" ).val()==1)
                                            $("#polya").show(1000);
                                        else
                                            $("#polya").hide(1000);
                                        ' 
                                ],
                                'size' => kartik\select2\Select2::SMALL,
                                'pluginOptions' => [ ],
            ]) ?>
            </div>
        </div>
        <br>
        <?php if ($model->type_id !==1 )
            $class = 'none';
        else
            $class = '';
         ?>
          <div class="row" id="polya" style="display:<?=$class?>;">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('number_season')?></label>
                    <input type="number" id="info-number_season<?=$model->id?>" class="form-control" name="Info[number_season]" value="<?=$model->number_season?>" placeholder="-" maxlength="255" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'number_season', 'value':$('#info-number_season<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('number_seriya')?></label>
                    <input type="number" id="info-number_seriya<?=$model->id?>" class="form-control" name="Info[number_seriya]" value="<?=$model->number_seriya ?>" placeholder="-" maxlength="255" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'number_seriya', 'value':$('#info-number_seriya<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><?=$model->getAttributeLabel('alternative')?></label>
                    <input type="number" id="info-alternative<?=$model->id?>" class="form-control" name="Info[alternative]" value="<?=$model->alternative ?>" placeholder="-" maxlength="255" aria-required="true" onchange="$.get('/films/set-values', {'id':<?=$model->id?>, 'attribute': 'alternative', 'value':$('#info-alternative<?=$model->id?>').val()}, function(data){} );" >
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
<br>


<!-- BEGIN FORMA -->
    <div class="row">
         <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Бюджет фильма                       
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add_budget" id="add_budget<?=$model->id?>"> + Добавить вариант</button>  
                    </h5>
                </div>
                <div class="panel-body">

                     <div class="row"  >
                        <div class="col-md-12">
                            <div id="dynamic_budget<?=$model->id?>">
                                <?php
                                    foreach (json_decode($model->budget) as $value) {
                                        $individual++;
                                        if($individual == 1){
                                        ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> Сумма </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> Валюта </div> 
                                </div>
                                <?php } ?>

                                <div class="row" style="margin-top:10px;" id="budget<?=$model->id . $individual?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="number" class="form-control" name="summa<?=$model->id?>[]" id="summa<?=$model->id?>" value="<?=$value->summa?>" /> 
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <select class="form-control" name="currency<?=$model->id?>[]" id="currency<?=$model->id?>" aria-required="true" aria-invalid="false">
                                            <?= $model->getCurrency($value->currency) ?>
                                        </select> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_individual<?=$model->id?>" id="<?=$individual?>" class="btn btn-danger btn_remove_individual<?=$model->id?>"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="budget_count<?=$model->id?>" name="budget_count<?=$model->id?>" value="<?=$individual?>"/> 
                            </div>
                        </div>


                    </div>

                </div>                           
            </div>
        </div>  

        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Возрастной рейтинг фильма                   
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add_reyting" id="add_reyting<?=$model->id?>"> + Добавить вариант</button>  
                    </h5>
                </div>
                <div class="panel-body">

                     <div class="row"  >
                        <div class="col-md-12">
                            <div id="dynamic_reyting<?=$model->id?>">
                                <?php
                                    foreach (json_decode($model->reyting) as $value) {
                                        $reyting++;
                                        if($reyting == 1){
                                        ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> Страна </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> Возрастной рейтинг </div> 
                                </div>
                                <?php } ?>

                                <div class="row" style="margin-top:10px;" id="reyting<?=$model->id . $reyting?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <select class="form-control" name="country<?=$model->id?>[]" id="country<?=$model->id?>" aria-required="true" aria-invalid="false">
                                            <?= $model->getCountrySelected($value->country) ?>
                                        </select>  
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <select class="form-control" name="youth<?=$model->id?>[]" id="youth<?=$model->id?>" aria-required="true" aria-invalid="false">
                                            <?= $model->getYouth($value->youth) ?>
                                        </select> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_reyting<?=$model->id?>" id="<?=$reyting?>" class="btn btn-danger btn_remove_reyting<?=$model->id?>"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="reyting_count<?=$model->id?>" name="reyting_count<?=$model->id?>" value="<?=$reyting?>"/> 
                            </div>
                        </div>            
                    </div>

                </div>                           
            </div>
        </div> 

        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Награды                 
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add_award" id="add_award<?=$model->id?>"> + Добавить вариант</button>  
                    </h5>
                </div>
                <div class="panel-body">

                     <div class="row"  >
                        <div class="col-md-12">
                            <div id="dynamic_award<?=$model->id?>">
                                <?php
                                    foreach (json_decode($model->awards) as $value) {
                                        $award++;
                                        if($award == 1){
                                        ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> Имя и фамилия члена группы </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> Роль члена группы </div> 
                                </div>
                                <?php } ?>

                                <div class="row" style="margin-top:10px;" id="award<?=$model->id . $award?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="text" class="form-control" name="festival<?=$model->id?>[]" id="festival<?=$model->id?>" value="<?=$value->festival?>" />
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <input type="text" class="form-control" name="prize<?=$model->id?>[]" id="prize<?=$model->id?>" value="<?=$value->prize?>" />
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_award<?=$model->id?>" id="<?=$award?>" class="btn btn-danger btn_remove_award<?=$model->id?>"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="award_count<?=$model->id?>" name="award_count<?=$model->id?>" value="<?=$award?>"/> 
                            </div>
                        </div>            
                    </div>

                </div>                           
            </div>
        </div>   

    </div>
    <input type="hidden" class="form-control" id="currencyList" name="currencyList" /> 
    <input type="hidden" class="form-control" id="countryList" name="countryList" /> 
    <input type="hidden" class="form-control" id="youthList" name="youthList" /> 


    <!-- Ploshadka dobavit boshlandi -->

    <br>
<div class="row">
    
     <a onclick = "alert('Успешно добавлено!');$.post('/technical-information/create-playground?technical_id=<?=$model->id?>'); $.pjax.reload({container:'#main-pjax', async: false}); $.pjax.reload({container:'#technical-pjax', async: false}); " class="btn btn-xs btn-success" style="margin-bottom: 20px;margin-left: 10px;" data-pjax="0"> 
            Добавить площадку <i class="fa fa-plus" ></i>
        </a>
    <?php 
        $playgrounds = TechnicalPlaygrounds::find()->where(['technical_id' => $model->id])->all();
        foreach ($playgrounds as $playground) {
    ?>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label"><?=$playground->getAttributeLabel('playground_id')?></label>
            <?= kartik\select2\Select2::widget([
                    'name' => 'playground_new'.$playground->id,
                    'id' => 'playground_new'.$playground->id,
                    'data' => $playground->getPlayGroundList(),
                    'value' => $playground->playground_id,
                    'size' => 'sm',
                    'options' => [ 'placeholder' => 'Выберите ...'],
                    'pluginEvents' => [
                        "change" => "function() 
                        {
                            $.get('/technical-information/set-playground',
                            { 'id' : ".$playground->id.", 'value' : $(this).val() },
                                function(data){ $.pjax.reload({container:'#technical-pjax', async: false}); $.pjax.reload({container:'#main-pjax', async: false}); }
                            );
                        }",
                    ],
                ])
            ?>
        </div>
        <div class="col-md-3">
            <div class="form-group">
            <label class="control-label"><?=$playground->getAttributeLabel('value_playground')?></label>
            <input type="text" id="value_playground_new<?=$playground->id?>" class="form-control" name="TechnicalPlaygrounds[value_playground_new]" value="<?=$playground->value_playground?>" maxlength="255" aria-invalid="false" onchange="$.get('/technical-information/set-values-to-technical-playground', {'id':<?=$playground->id?>, 'attribute': 'value_playground', 'value':$('#value_playground_new<?=$playground->id?>').val()}, function(data){} );" >
            <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
            <label class="control-label"><?=$playground->getAttributeLabel('additional_value')?></label>
            <input type="text" id="additional_value_new<?=$playground->id?>" class="form-control" name="TechnicalPlaygrounds[additional_value_new]" value="<?=$playground->additional_value?>" maxlength="255" aria-invalid="false" onchange="$.get('/technical-information/set-values-to-technical-playground', {'id':<?=$playground->id?>, 'attribute': 'additional_value', 'value':$('#additional_value_new<?=$playground->id?>').val()}, function(data){} );">
            <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
            <label class="control-label"><?=$playground->getAttributeLabel('territory')?></label>
            <input type="text" id="territory<?=$playground->id?>" class="form-control" name="TechnicalPlaygrounds[territory]" value="<?=$playground->territory?>" maxlength="255" aria-invalid="false" onchange="$.get('/technical-information/set-values-to-technical-playground', {'id':<?=$playground->id?>, 'attribute': 'territory', 'value':$('#territory<?=$playground->id?>').val()}, function(data){} );">
            <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-1" style="padding-top:22px;">
            <a class="btn btn-default" role="modal-remote" title="Загрузить данные" 
                href="/technical-information/import-values?film_id=<?=$playground->technical->film_id?>&id=<?=$playground->additional_value?>&playground_id=<?=$playground->id?>" >
                <span class="text-success glyphicon glyphicon-import"></span>
            </a>
        </div>
        <div class="col-md-1" style="padding-top:22px;">
                <a onclick = "alert('Успешно выполнено!'); $.post('/technical-information/delete-playground?id=<?=$playground->id?>'); $.pjax.reload({container:'#technical-pjax', async: false}); $.pjax.reload({container:'#main-pjax', async: false});" class="btn btn-danger" title="Удалить" > 
                    <span class="glyphicon glyphicon-trash"  style="color: #fff;"></span>
                </a>
        </div>
    </div>
    <?php } ?>

</div>
<!-- Ploshadka dobavit tugadi -->

<!-- END FORMA -->

	</div>
    <div class="col-md-4 col-sm-12">
        <div class="table-responsive">
            <!-- <h3>Постер <a class="btn btn-xs btn-success" role="modal-remote" href="<?php //Url::toRoute(['/films/poster', 'id' => $model->id])?>"><i class="fa fa-cloud-download"></i></a></h3> -->
            <br>
            <div class="col-md-12">
                <center>
                    <?=Html::img($path, [
                        'style' => 'width:200px; height:300px;',
                        //'class' => 'img-circle',
                    ])?>
                </center>
            </div>
            <div class="col-md-12">
                <hr>
                <hr>
            </div>
            <div class="col-md-12">
                <?=$model->description?>
            </div>
        </div>
    </div>
	<?php Pjax::end() ?> 
</div>


<?php 
$this->registerJs(<<<JS

$(document).ready(function(){

    $("#type_id").click(function(){
       $("#polya").show(1000);
    });


    $(document).on('change', '#summa$model->id', function(e){
        var currency = []; var summa = [];
        $('input[name^=\'summa$model->id\']').each(function() { summa.push($(this).val()); });
        $('select[name^=\'currency$model->id\']').each(function() { currency.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, currency: currency, summa: summa },
           url: '/films/set-budget',
        });
    });

    $(document).on('change', '#currency$model->id', function(e){
        var currency = []; var summa = [];
        $('input[name^=\'summa$model->id\']').each(function() { summa.push($(this).val()); });
        $('select[name^=\'currency$model->id\']').each(function() { currency.push($(this).val());  });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, currency: currency, summa: summa },
           url: '/films/set-budget',
           success: function(msg){/*alert(msg);*/}
        });
    });

    /* 3 qism uchun */

       $(document).on('change', '#festival$model->id', function(e){
        var prize = []; var festival = [];
        $('input[name^=\'festival$model->id\']').each(function() { festival.push($(this).val()); });
        $('input[name^=\'prize$model->id\']').each(function() { prize.push($(this).val()); });
        
        $.ajax({
           type: 'POST',
           data: {id: $model->id, prize: prize, festival: festival },
           url: '/films/set-award',
        });
    });

    $(document).on('change', '#prize$model->id', function(e){
        var prize = []; var festival = [];
        $('input[name^=\'festival$model->id\']').each(function() { festival.push($(this).val()); });
        $('input[name^=\'prize$model->id\']').each(function() { prize.push($(this).val()); });
        
        $.ajax({
           type: 'POST',
           data: {id: $model->id, prize: prize, festival: festival },
           url: '/films/set-award',
           success: function(msg){/*alert(msg);*/}
        });
    });

    /*2 qism uchun*/

    $(document).on('change', '#country$model->id', function(e){
        var youth = []; var country = [];
        $('select[name^=\'country$model->id\']').each(function() { country.push($(this).val()); });
        $('select[name^=\'youth$model->id\']').each(function() { youth.push($(this).val()); });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, youth: youth, country: country },
           url: '/films/set-reyting',
        });
    });

    $(document).on('change', '#youth$model->id', function(e){
        var youth = []; var country = [];
        $('select[name^=\'country$model->id\']').each(function() { country.push($(this).val());  });
        $('select[name^=\'youth$model->id\']').each(function() { youth.push($(this).val());  });
        $.ajax({
           type: 'POST',
           data: {id: $model->id, youth: youth, country: country },
           url: '/films/set-reyting',
           success: function(msg){/*alert(msg);*/}
        });
    });
    

    var budget_count = document.getElementById('budget_count$model->id').value;
    var reyting_count = document.getElementById('reyting_count$model->id').value;
    var award_count = document.getElementById('award_count$model->id').value;

     $.get('get-currency',{ },function(data) { $('#currencyList').val(data); } );
     $.get('get-country',{ },function(data) { $('#countryList').val(data); } );
     $.get('get-youth',{ },function(data) { $('#youthList').val(data); } );

    $('#add_budget$model->id').click(function(){
        budget_count++;
        if(budget_count == 1) $('#dynamic_budget$model->id').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Сумма</div><div class="col-md-5 col-sm-5 col-xs-5">Валюта</div></div>');

        $('#dynamic_budget$model->id').append('<div class="row" style="margin-top:10px;" id="budget$model->id'+budget_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="summa$model->id[]" id="summa$model->id" /> </div><div class="col-md-5 col-sm-5 col-xs-5"> <select class="form-control" name="currency$model->id[]" id="currency$model->id" aria-required="true" aria-invalid="false">'+$("#currencyList").val()+'</select> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_individual$model->id" id="'+budget_count+'" class="btn btn-danger btn_remove_individual$model->id"> <i  class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });

        $(document).on('click', '.btn_remove_individual$model->id', function(){
            var button_id = $(this).attr("id");
            $('#budget$model->id'+button_id+'').remove();

            var currency = []; var summa = [];
            $('select[name^=\'currency$model->id\']').each(function() { currency.push($(this).val()); });
            $('input[name^=\'summa$model->id\']').each(function() { summa.push($(this).val()); });
            $.ajax({
               type: 'POST',
               data: {id: $model->id, currency: currency, summa: summa },
               url: '/films/set-budget',
            });
    });
/*AVARD uchun */

    $('#add_award$model->id').click(function(){
        award_count++;
        if(award_count == 1) $('#dynamic_award$model->id').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия члена группы</div><div class="col-md-5 col-sm-5 col-xs-5">Роль члена группы</div></div>');

        $('#dynamic_award$model->id').append('<div class="row" style="margin-top:10px;" id="award$model->id'+award_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="festival$model->id[]" id="festival$model->id" /> </div><div class="col-md-5 col-sm-5 col-xs-5"><input type="text" class="form-control" name="prize$model->id[]" id="prize$model->id" /> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_award$model->id" id="'+award_count+'" class="btn btn-danger btn_remove_award$model->id"> <i  class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });
            
            $(document).on('click', '.btn_remove_award$model->id', function(){
            var button_id = $(this).attr("id");
            $('#award$model->id'+button_id+'').remove();

            var prize = []; var festival = [];
            $('input[name^=\'festival$model->id\']').each(function() { festival.push($(this).val()); });
             $('input[name^=\'prize$model->id\']').each(function() { prize.push($(this).val()); });
            $.ajax({
               type: 'POST',
               data: {id: $model->id, prize: prize, festival: festival },
               url: '/films/set-award',
            });
            });


    $('#add_reyting$model->id').click(function(){
        reyting_count++;
        if(reyting_count == 1) $('#dynamic_reyting$model->id').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Страна</div><div class="col-md-5 col-sm-5 col-xs-5"> Возрастной рейтинг</div></div>');

        $('#dynamic_reyting$model->id').append('<div class="row" style="margin-top:10px;" id="reyting$model->id'+reyting_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <select class="form-control" name="country$model->id[]" id="country$model->id" aria-required="true" aria-invalid="false">'+$("#countryList").val()+'</select></div><div class="col-md-5 col-sm-5 col-xs-5"> <select class="form-control" name="youth$model->id[]" id="youth$model->id" aria-required="true" aria-invalid="false">'+$("#youthList").val()+'</select> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button"  name="remove_reyting$model->id" id="'+reyting_count+'" class="btn btn-danger btn_remove_reyting$model->id"> <i  class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });



        $(document).on('click', '.btn_remove_individual$model->id', function(){
            var button_id = $(this).attr("id");
            $('#budget$model->id'+button_id+'').remove();

            var currency = []; var summa = [];
            $('select[name^=\'currency$model->id\']').each(function() { currency.push($(this).val()); });
            $('input[name^=\'summa$model->id\']').each(function() { summa.push($(this).val()); });
            $.ajax({
               type: 'POST',
               data: {id: $model->id, currency: currency, summa: summa },
               url: '/films/set-budget',
            });
    });

/*Reyting uchun*/

            $(document).on('click', '.btn_remove_reyting$model->id', function(){
            var button_id = $(this).attr("id");
            $('#reyting$model->id'+button_id+'').remove();

            var youth = []; var country = [];
            $('select[name^=\'country$model->id\']').each(function() { country.push($(this).val()); });
            $('select[name^=\'youth$model->id\']').each(function() { youth.push($(this).val()); });
            $.ajax({
               type: 'POST',
               data: {id: $model->id, youth: youth, country: country },
               url: '/films/set-reyting',
            });
    });
/* AVARD*/
   
});

JS
);
?>