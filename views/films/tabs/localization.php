<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\web\JsExpression;


?>

<?= Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/localization/create', 'film_id' => $model->id], ['role'=>'modal-remote','title'=> 'Добавить', 'style' => 'margin-left:10px;margin-bottom:10px;', 'class'=>'btn btn-info btn-xs'])?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'localization-pjax']) ?>
    <div class="row">
<?php
    foreach ($localizationProvider->getModels() as $model) {
		if (!file_exists('uploads/localization/'.$model->id.'/'.$model->poster23) || $model->poster23 == '') {
		    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
		} else {
		    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/localization/'.$model->id.'/'.$model->poster23;
		}
		if (!file_exists('uploads/localization/'.$model->id.'/'.$model->poster169) || $model->poster169 == '') {
		    $path169 = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
		} else {
		    $path169 = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/localization/'.$model->id.'/'.$model->poster169;
		}
        if($model->type == 1) {
            $color = '#95b75d';
            $class = 'success';
            $name = 'Основной';
        }
        else {
            $color = '#E04B4A';
            $class = 'danger';
            $name = 'Дополнительный';
        }
?>
    <div class="col-md-6 col-sm-12 col-xs-12">
    	<div class="panel panel-<?=$class?>">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                    <div style="display:inline-block; background-color: <?=$color?>; width: 15px; height: 15px; border-radius: 50%; "></div>
                    <?= $name?>
                        <span class="pull-right">
                            <!-- <a role="modal-remote" title="Изменить" href="<?php //Url::toRoute(['/localization/update', 'id' => $model->id])?>">
                                <i class="glyphicon glyphicon-pencil"></i> &nbsp;
                            </a> -->
                            <?= $model->type == 2 ? Html::a('<span class="text-danger glyphicon glyphicon-trash"></span>', ['/localization/delete', 'id' => $model->id], [
		                        'role'=>'modal-remote','title'=>'Удалить', 
		                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
		                        'data-request-method'=>'post',
		                        'data-toggle'=>'tooltip',
		                        'data-confirm-title'=>'Подтвердите действие',
		                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
		                    ]) : '' ?>
                        </span>
                    </h5>
                </div> 
                <div class="panel-body">
	    			<?= $this->render('_form_loc', [
	                    'model' => $model,
	                    'number' => $model->id,
                        'path' => $path,
                        'path169' => $path169,
	                ]); ?>
	            </div>
	        </div>

    </div>

<?php  } ?>
    
</div>
<?php Pjax::end() ?> 