<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;


?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'rights-pjax']) ?>
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px;">
            <?= Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/rights/create', 'film_id' => $id],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info btn-xs'])?>
        </div>
<?php
    foreach ($filmsdataProvider->getModels() as $model) {
        if($model->actual == 1) {
            $color = '#95b75d';
            $class = 'success';
        }
        else {
            $color = '#E04B4A';
            $class = 'danger';
        }
?>
    <div class="col-md-12">
         <div class="footer-widget-panel panel-group" id="accordion1">
            <div class="panel panel-<?=$class?>">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        <a class="<?=$model->actual ? '' : 'collapsed' ?>" data-toggle="collapse" data-parent="#accordion1" href="#discussionId<?=$model->id?>">
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-down"></i>                            
                            </span> 
                            <div style="display:inline-block; background-color: <?=$color?>; width: 15px; height: 15px; border-radius: 50%; "></div>
                                Договор № <?=$model->number ?> (от <?=$model->date?> )
                        </a>
                        <span class="pull-right">
                            <a role="modal-remote" href="<?=Url::toRoute(['/rights/update', 'id' => $model->id])?>">
                                <i class="fa fa-gear"></i> &nbsp;
                            </a>
                        </span>
                    </h5>
                </div> 
                <div id="discussionId<?=$model->id?>" class="panel-collapse collapse <?=$model->actual ? 'in' : '' ?> ">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <b> Приложение к договору #<?=$model->application_number?> </b>
                        </div>
                        <div class="row" style="padding-top : 30px; ">
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('begin_date')?><br>
                                <b><?=$model->begin_date != null ? date('d.m.Y', strtotime($model->begin_date) ) : '' ?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('end_date')?><br>
                                <b><?=$model->end_date != null ? date('d.m.Y', strtotime($model->end_date) ) : '' ?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('min_warranty')?><br>
                                <b><?=$model->min_warranty?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('adaptation')?><br>
                                <b><?=$model->adaptation?></b>
                            </div>
                             <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('maintenance')?><br>
                                <b><?=$model->maintenance?></b>
                            </div>
                        </div>
                        <div class="row" style="padding-top : 30px; ">
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('teritory')?><br>
                                <b><?=$model->teritory?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('rolyati')?><br>
                                <b><?=$model->rolyati?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('area')?><br>
                                <b><?=$model->area?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('price')?><br>
                                <b><?=$model->price?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('model_begin')?><br>
                                <b><?=$model->model_begin != null ? date('d.m.Y', strtotime($model->model_begin) ) : '' ?></b>
                            </div>
                            <div class="col-md-2 col-xs-6 col-sm-4">
                                <?=$model->getAttributeLabel('model_end')?><br>
                                <b><?=$model->model_end != null ? date('d.m.Y', strtotime($model->model_end) ) : '' ?></b>
                            </div>
                        </div>
                    </div>
                </div>                         
            </div>
        </div>
    </div>

<?php  } ?>
    
</div>
<?php Pjax::end() ?> 