<?php

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\PreferenceBooks;
use app\models\Treyler;

?>

<div class="row">
    <?php Pjax::begin(['enablePushState' => false, 'id' => 'technical-pjax']) ?>                                 
    <div class="col-md-12 col-sm-12">
        <div class="table-responsive">           
            <?= $this->render('_form_tech', [
                'model' => $technical,
            ]);?>

            <h3>
                <a onclick = "alert('Успешно добавлено!');$.post('/technical-information/create-treyler?technical_information_id=<?=$technical->id?>'); $.pjax.reload({container:'#technical-pjax', async: false}); " class="btn btn-xs btn-success" style="margin-top: 20px;" data-pjax="0"> 
                    Добавить трейлер <i class="fa fa-plus" ></i>
                </a>
            </h3>
            <br>      
            <div class="col-md-8">
                    <?php 
                        $treylers = Treyler::find()->where(['technical_information_id' => $technical->id])->all();
                        $i = 0;
                        foreach ($treylers as $treyler) {
                            $i++;
                        ?>
                        <div class="panel panel-success">
                            <div class="panel-heading ui-draggable-handle">
                                <h5 class="panel-title">
                                    Трейлер #<?=$i?>
                                </h5>
                                <span class="pull-right"><!-- 
                                    <a role="modal-remote" title="Изменить" href="<?php //Url::toRoute(['/technical-information/update-treyler', 'id' => $treyler->id])?>">
                                        <i class="glyphicon glyphicon-pencil"></i> &nbsp;
                                    </a> -->
                                    <?= $i > 1 ? Html::a('<span class="text-danger glyphicon glyphicon-trash"></span>', ['/technical-information/delete-treyler', 'id' => $treyler->id], [
                                        'role'=>'modal-remote','title'=>'Удалить', 
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-toggle'=>'tooltip',
                                        'data-confirm-title'=>'Подтвердите действие',
                                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                    ]) : '' ?>
                                </span>
                            </div>                        
                            <div class="panel-body">
                                <?= $this->render('_form_treyler', [
                                    'model' => $treyler,
                                ]);?>
                            </div>
                        </div>
                    <?php  } ?>

            </div>
   		</div>
	</div>
	<?php Pjax::end() ?> 
</div>