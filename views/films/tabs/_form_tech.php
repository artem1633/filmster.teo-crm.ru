<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PreferenceBooks;
use kartik\select2\Select2;
use app\models\TechnicalPlaygrounds;

/* @var $this yii\web\View */
/* @var $model app\models\TechnicalInformation */
/* @var $form yii\widgets\ActiveForm */
$_csrf = \Yii::$app->request->getCsrfToken();
?>

<div class="row">
    <div class="col-md-10">
        <a onclick = "alert('Успешно добавлено!');$.post('/technical-information/create-playground?technical_id=<?=$model->id?>'); $.pjax.reload({container:'#technical-pjax', async: false}); $.pjax.reload({container:'#main-pjax', async: false}); " class="btn btn-xs btn-success" style="margin-bottom: 20px;margin-left: 10px;" data-pjax="0"> 
            Добавить площадку <i class="fa fa-plus" ></i>
        </a>
    <?php 
        $playgrounds = TechnicalPlaygrounds::find()->where(['technical_id' => $model->id])->all();
        foreach ($playgrounds as $playground) {
    ?>
    <div class="row">
        <div class="col-md-3">
            <label class="control-label"><?=$playground->getAttributeLabel('playground_id')?></label>
            <?= Select2::widget([
                    'name' => 'playground_old'.$playground->id,
                    'id' => 'playground_old'.$playground->id,
                    'data' => $playground->getPlayGroundList(),
                    'value' => $playground->playground_id,
                    'size' => 'sm',
                    'options' => [ 'placeholder' => 'Выберите ...'],
                    'pluginEvents' => [
                        "change" => "function() 
                        {
                            $.get('/technical-information/set-playground',
                            { 'id' : ".$playground->id.", 'value' : $(this).val() },
                                function(data){ $.pjax.reload({container:'#technical-pjax', async: false}); 
                                $.pjax.reload({container:'#main-pjax', async: false}); }
                            );
                        }",
                    ],
                ])
            ?>
        </div>
        <div class="col-md-3">
            <div class="form-group">
            <label class="control-label"><?=$playground->getAttributeLabel('value_playground')?></label>
            <input type="text" id="value_playground<?=$playground->id?>" class="form-control" name="TechnicalPlaygrounds[value_playground]" value="<?=$playground->value_playground?>" maxlength="255" aria-invalid="false" onchange="$.get('/technical-information/set-values-to-technical-playground', {'id':<?=$playground->id?>, 'attribute': 'value_playground', 'value':$('#value_playground<?=$playground->id?>').val()}, function(data){} );" >
            <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
            <label class="control-label"><?=$playground->getAttributeLabel('additional_value')?></label>
            <input type="text" id="additional_value<?=$playground->id?>" class="form-control" name="TechnicalPlaygrounds[additional_value]" value="<?=$playground->additional_value?>" maxlength="255" aria-invalid="false" onchange="$.get('/technical-information/set-values-to-technical-playground', {'id':<?=$playground->id?>, 'attribute': 'additional_value', 'value':$('#additional_value<?=$playground->id?>').val()}, function(data){} );">
            <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
            <label class="control-label"><?=$playground->getAttributeLabel('territory')?></label>
            <input type="text" id="territory_1<?=$playground->id?>" class="form-control" name="TechnicalPlaygrounds[territory]" value="<?=$playground->territory?>" maxlength="255" aria-invalid="false" onchange="$.get('/technical-information/set-values-to-technical-playground', {'id':<?=$playground->id?>, 'attribute': 'territory', 'value':$('#territory_1<?=$playground->id?>').val()}, function(data){} );">
            <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-1" style="padding-top:22px;">
            <a class="btn btn-default" role="modal-remote" title="Загрузить данные" 
                href="/technical-information/import-values?film_id=<?=$playground->technical->film_id?>&id=<?=$playground->additional_value?>&playground_id=<?=$playground->id?>" >
                <span class="text-success glyphicon glyphicon-import"></span>
            </a>
            <!-- <a class="btn btn-default" title="Загрузить данные" onclick = "$.post('/technical-information/import-values?film_id=<?php //$playground->technical->film_id?>'); $.pjax.reload({container:'#technical-pjax', async: false}); $.pjax.reload({container:'#main-pjax', async: false}); alert('Успешно выполнено!'); "> 
                <span class="text-success glyphicon glyphicon-import"></span>
            </a> -->
        </div>
        <div class="col-md-1" style="padding-top:22px;">
                <a class="btn btn-default" title="Удалить" onclick = "$.post('/technical-information/delete-playground?id=<?=$playground->id?>'); $.pjax.reload({container:'#technical-pjax', async: false}); $.pjax.reload({container:'#main-pjax', async: false}); alert('Успешно выполнено!'); "> 
                    <span class="text-danger glyphicon glyphicon-trash"></span>
                </a>
            <!-- </button>  -->
        </div>
    </div>
    <?php } ?>

    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label class="control-label" for="technicalinformation-audio"><?=$model->getAttributeLabel('video')?></label>
            <?= Html::activeDropDownList($model, 'video', PreferenceBooks::videoFormatList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-values?id="+id+"&attribute=video&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <label class="control-label" for="technicalinformation-audio"><?=$model->getAttributeLabel('audio')?></label>
            <?= Html::activeDropDownList($model, 'audio', PreferenceBooks::audioFormatList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-values?id="+id+"&attribute=audio&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <div class="form-group field-technicalinformation-duration">
                <label class="control-label" for="technicalinformation-duration"><?=$model->getAttributeLabel('duration')?></label>
                <input type="number" id="duration<?=$model->id?>" class="form-control" name="TechnicalInformation[duration]" value="<?=$model->duration?>" aria-invalid="false" onchange="$.get('/technical-information/set-values', {'id':<?=$model->id?>, 'attribute': 'duration', 'value':$('#duration<?=$model->id?>').val()}, function(data){} ); $.pjax.reload({container:'#main-pjax', async: false}); " >
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label class="control-label" for="technicalinformation-playback_speed"><?=$model->getAttributeLabel('playback_speed')?></label>
            <?= Html::activeDropDownList($model, 'playback_speed', PreferenceBooks::playbackSpeedList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-values?id="+id+"&attribute=playback_speed&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <label class="control-label" for="technicalinformation-video_language"><?=$model->getAttributeLabel('video_language')?></label>
            <?= Html::activeDropDownList($model, 'video_language', $model->getLanguages(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-values?id="+id+"&attribute=video_language&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-4">
            <label class="control-label" for="technicalinformation-audio_language"><?=$model->getAttributeLabel('audio_language')?></label>
            <?= Html::activeDropDownList($model, 'audio_language', $model->getLanguages(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-values?id="+id+"&attribute=audio_language&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
    </div>

    <div class="row"><br>
        <div class="col-md-4">
            <label class="control-label" for="technicalinformation-source_availability"><?=$model->getAttributeLabel('source_availability')?></label>
            <?= Html::activeDropDownList($model, 'source_availability', PreferenceBooks::sourceAvailabilityList(), 
                [
                    'prompt'=>'Выберите', 
                    'class'=>"form-control",
                    'onchange' => '
                        var id = '.$model->id.';
                        $.post( "/technical-information/set-values?id="+id+"&attribute=source_availability&value="+$(this).val(), function( data ){});
                    ',
                ]
            );?>
        </div>
        <div class="col-md-8">
            <div class="form-group field-technicalinformation-source_link">
                <label class="control-label" for="technicalinformation-source_link"><?=$model->getAttributeLabel('source_link')?></label>
                <input type="text" id="source_link<?=$model->id?>" class="form-control" name="TechnicalInformation[source_link]" value="<?=$model->source_link?>" aria-invalid="false" onchange="$.get('/technical-information/set-values', {'id':<?=$model->id?>, 'attribute': 'source_link', 'value':$('#source_link<?=$model->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <div class="panel panel-danger">
            <div class="panel-heading ui-draggable-handle">
                <h5>Субтитры</h5>
            </div>
            <div class="panel-body">
                <div class="row">                    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <div class="form-group field-burnt_subtitles">
                            <input type="hidden" name="TechnicalInformation[burnt_subtitles]" value="0">
                                <label>
                                    <input type="checkbox" id="burnt_subtitles" name="TechnicalInformation[burnt_subtitles]" value="1"  <?= $model->burnt_subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('burnt_subtitles')?>
                                </label>
                            <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" id="burnt_value">
                        <label class="control-label"><?=$model->getAttributeLabel('burnt_value')?></label>
                        <?= Select2::widget([
                            'name' => 'burnt_value',
                            'data' => $model->getLanguages(),
                            'value' => $model->getBurnt(),
                            'size' => 'sm',
                            'options' => [ 'id' => 'burnt_value_'.$model->id, 'placeholder' => 'Выберите ...'],
                            'pluginOptions' => [
                                /*'tags' => true,*/
                                'multiple' => true,
                                'allowClear' => true,
                            ],
                            'pluginEvents' => [
                                "change" => "function() 
                                {
                                    $.get('/technical-information/set-values',
                                    { 'id' : ".$model->id.", 'attribute' : 'burnt_value', 'value' : JSON.stringify($(this).val()) },
                                        function(data){}
                                    );
                                }",
                            ],
                        ])
                        ?>
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <div class="form-group field-forced_subtitles">
                            <input type="hidden" name="TechnicalInformation[forced_subtitles]" value="0">
                                <label>
                                    <input type="checkbox" id="forced_subtitles" name="TechnicalInformation[forced_subtitles]" value="1"  <?= $model->forced_subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('forced_subtitles')?>
                                </label>
                            <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div id="forced_value">
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('forced_value')?></label>
                            <?= Select2::widget([
                                'name' => 'forced_value',
                                'data' => $model->getLanguages(),
                                'value' => $model->getForced(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'forced_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-values',
                                        { 'id' : ".$model->id.", 'attribute' : 'forced_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?php // $form->field($model, 'forced_files')->fileInput(['id' => 'forced_files']); ?>
                            <div id="dropBox1">
                                <label class="control-label"><?=$model->getAttributeLabel('forced_files')?></label>
                            </div>
                            <input type="file" name="fileInput_technical" multiple="" />
                        </div>                        
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">                    
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <div class="form-group field-subtitles">
                            <input type="hidden" name="TechnicalInformation[subtitles]" value="0">
                                <label>
                                    <input type="checkbox" id="subtitles" name="TechnicalInformation[subtitles]" value="1"  <?= $model->subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('subtitles')?>
                                </label>
                            <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div id="subtitles_value">                        
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('subtitles_value')?></label>
                            <?= Select2::widget([
                                'name' => 'subtitles_value',
                                'data' => $model->getLanguages(),
                                'value' => $model->getSubtitles(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'subtitles_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-values',
                                        { 'id' : ".$model->id.", 'attribute' : 'subtitles_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?php // $form->field($model, 'subtitles_files')->fileInput(); ?>
                            <div id="dropBox2">
                                <label class="control-label"><?=$model->getAttributeLabel('subtitles_file')?></label>
                            </div>
                            <input type="file" name="uploadSubtitlesFile" multiple=""/>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <div class="form-group field-sdh_subtitles">
                            <input type="hidden" name="TechnicalInformation[sdh_subtitles]" value="0">
                                <label>
                                    <input type="checkbox" id="sdh_subtitles" name="TechnicalInformation[sdh_subtitles]" value="1"  <?= $model->sdh_subtitles == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('sdh_subtitles')?>
                                </label>
                            <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div id="sdh_value">
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('sdh_value')?></label>
                            <?= Select2::widget([
                                'name' => 'sdh_value',
                                'data' => $model->getLanguages(),
                                'value' => $model->getSdh(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'sdh_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-values',
                                        { 'id' : ".$model->id.", 'attribute' : 'sdh_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div id="dropBox3">
                                <label class="control-label"><?=$model->getAttributeLabel('sdh_file')?></label>
                            </div>
                            <input type="file" name="uploadSdhFile" multiple="" />
                        </div>                        
                    </div>
                </div>

                <div class="row" style="margin-top:20px;">
                    <div class="col-md-4">
                        <div style="margin-top:20px;">
                            <div class="form-group field-presense_cc">
                            <input type="hidden" name="TechnicalInformation[presense_cc]" value="0">
                                <label>
                                    <input type="checkbox" id="presense_cc" name="TechnicalInformation[presense_cc]" value="1"  <?= $model->presense_cc == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('presense_cc')?>
                                </label>
                            <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div id="presense_value">
                        <div class="col-md-5">
                            <label class="control-label"><?=$model->getAttributeLabel('presense_value')?></label>
                            <?= Select2::widget([
                                'name' => 'presense_value',
                                'data' => $model->getLanguages(),
                                'value' => $model->getPresent(),
                                'size' => 'sm',
                                'options' => [ 'id' => 'presense_value'.$model->id, 'placeholder' => 'Выберите ...'],
                                'pluginOptions' => [
                                    /*'tags' => true,*/
                                    'multiple' => true,
                                    'allowClear' => true,
                                ],
                                'pluginEvents' => [
                                    "change" => "function() 
                                    {
                                        $.get('/technical-information/set-values',
                                        { 'id' : ".$model->id.", 'attribute' : 'presense_value', 'value' : JSON.stringify($(this).val()) },
                                            function(data){}
                                        );
                                    }",
                                ],
                            ])
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div id="dropBox4">
                                <label class="control-label"><?=$model->getAttributeLabel('presense_file')?></label>
                            </div>
                            <input type="file" name="uploadPresenseFile" multiple="" />
                        </div>                        
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?php //ActiveForm::end(); ?>
    </div>
</div>

<?php 
$this->registerJs(<<<JS
    var checked = $('#burnt_subtitles').is(':checked');
    if(checked == 0) $('#burnt_value').hide();
    else $('#burnt_value').show();

    $('#burnt_subtitles').on('change', function() 
    {
        var checked = $('#burnt_subtitles').is(':checked');
        if(checked == 0) {
            $('#burnt_value').hide();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'burnt_subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#burnt_value').show();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'burnt_subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#forced_subtitles').is(':checked');
    if(checked == 0) $('#forced_value').hide();
    else $('#forced_value').show();

    $('#forced_subtitles').on('change', function() 
    {
        var checked = $('#forced_subtitles').is(':checked');
        if(checked == 0) {
            $('#forced_value').hide();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'forced_subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#forced_value').show();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'forced_subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#subtitles').is(':checked');
    if(checked == 0) $('#subtitles_value').hide();
    else $('#subtitles_value').show();

    $('#subtitles').on('change', function() 
    {
        var checked = $('#subtitles').is(':checked');
        if(checked == 0) {
            $('#subtitles_value').hide();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#subtitles_value').show();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#sdh_subtitles').is(':checked');
    if(checked == 0) $('#sdh_value').hide();
    else $('#sdh_value').show();

    $('#sdh_subtitles').on('change', function() 
    {
        var checked = $('#sdh_subtitles').is(':checked');
        if(checked == 0) {
            $('#sdh_value').hide();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'sdh_subtitles', 'value': 0}, function(data){} );
        }
        else {
            $('#sdh_value').show();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'sdh_subtitles', 'value': 1}, function(data){} );
        }
    });

    var checked = $('#presense_cc').is(':checked');
    if(checked == 0) $('#presense_value').hide();
    else $('#presense_value').show();

    $('#presense_cc').on('change', function() 
    {
        var checked = $('#presense_cc').is(':checked');
        if(checked == 0) {
            $('#presense_value').hide();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'presense_cc', 'value': 0}, function(data){} );
        }
        else {
            $('#presense_value').show();
            $.get('/technical-information/set-values', {'id':$model->id, 'attribute': 'presense_cc', 'value': 1}, function(data){} );
        }
    });


    /*Загрузка файла*/
    $('input[name^=\'fileInput_technical\']').on('change', function(image){
        $("#dropBox1").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();
        
        var resultJsonInput = '$model->forced_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'forced_files');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#dropBox1").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#dropBox1").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-files-value', {'id':$model->id, 'attribute': 'forced_files', 'value': JsonResult}, function(data){} );
    });

    $('input[name^=\'uploadSubtitlesFile\']').on('change', function(image){
        $("#dropBox2").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->subtitles_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'subtitles_files');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#dropBox2").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#dropBox2").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-files-value', {'id':$model->id, 'attribute': 'subtitles_file', 'value': JsonResult}, function(data){} );
    });

    $('input[name^=\'uploadSdhFile\']').on('change', function(image){
        $("#dropBox3").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->sdh_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'sdh_file');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#dropBox3").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#dropBox3").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }
        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-files-value', {'id':$model->id, 'attribute': 'sdh_file', 'value': JsonResult}, function(data){} );
    });

    $('input[name^=\'uploadPresenseFile\']').on('change', function(image){
        $("#dropBox4").html(event.target.value+" загрузка...");
        files = event.target.files;
        var data = new FormData();

        var resultJsonInput = '$model->presense_file';
        if(resultJsonInput.length > 0) {
            var resultArray = JSON.parse(resultJsonInput);
        } else {
            var resultArray = Array();
        }

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            data.append('file', file, file.name);
            data.append('_csrf', '{$_csrf}');
            data.append('id', '{$model->id}');
            data.append('type', 'presense_files');

            var image = {
                name: file.name,
                size: file.size,
                type: file.type,
            };
            resultArray.push(image);

            var xhr = new XMLHttpRequest();     
            xhr.open('POST', '/technical-information/set-file', true);  
            xhr.send(data);
            xhr.onload = function () {
                var response = JSON.parse(xhr.responseText);
                if(xhr.status === 200 && response.status == 'ok'){
                    $("#dropBox4").html("Успешно загружен. Нажмите, чтобы загрузить другой.");
                }else {
                    $("#dropBox4").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                }
            };
        }

        var JsonResult = JSON.stringify(resultArray);
        $.get('/technical-information/set-files-value', {'id':$model->id, 'attribute': 'presense_file', 'value': JsonResult}, function(data){} );
    });

JS
);
?>