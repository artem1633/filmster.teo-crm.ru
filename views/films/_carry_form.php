<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="users-form">
    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'sheet_number')->widget(kartik\select2\Select2::classname(), [
                    'data' => ['2' => '2-Лист', '3' => '3-Лист', '4' => '4-Лист'],
                    'options' => [/*'placeholder' => 'Выберите ...'*/],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        /*'allowClear' => true,*/
                    ],
                ])?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'files')->fileInput(); ?>
            </div>
        </div>
    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>
    <?php ActiveForm::end(); ?>
</div>