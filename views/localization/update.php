<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
?>
<div class="localization-update">

    <?= $this->render('_form', [
        'model' => $model,
        'number' => $number,
    ]) ?>

</div>
