<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
/* @var $form yii\widgets\ActiveForm */
$individual = 0;
$multiple = 0;

?>

<div class="localization-form">

    <?php $form = ActiveForm::begin(); ?>   
    <div class="row">

        <div style="display: none;" >
            <?= $form->field($model, 'photos')->textarea(['rows' => 3]) ?>
            <input id="number" name="number" type="hidden" value="<?=$number?>">
            <input id="path" name="path" type="hidden" value="<?= "uploads/localization/".$number."/" ?>">
            <input id="url" name="url" type="hidden" value="<?= "http://" . $_SERVER['SERVER_NAME'] . '/' ?>">
        </div>

        <div class="col-md-12 col-xs-12">
                    <?= \kato\DropZone::widget([
                        'options' => [
                             'url'=> Url::to(['/localization/upload', 'number' => $number]),
                             'paramName'=>'image',
                             //'maxFilesize' => '10',
                             'addRemoveLinks' => true,
                             //'previewTemplate' => true,
                             'dictRemoveFile' => 'Удалить',
                             'dictDefaultMessage' => "Выберите файлы",
                             'acceptedFiles' => 'image/*',
                             'init' => new JsExpression("function(file){ 

                                    var resultJsonInput = $('#localization-photos').val();
                                    if(resultJsonInput.length > 0) {
                                        var resultArray = JSON.parse(resultJsonInput);
                                    } else {
                                        var resultArray = Array();
                                    }

                                    var length = resultArray.length;
                                    var path = $('#path').val();
                                    //alert(path);
                                    for (i = 0; i < length; i++) {
                                        
                                        var file_image = $('#url').val() + resultArray[i].path;
                                        //alert(file_image);
                                        var mockFile = { name: resultArray[i].name, size: resultArray[i].size, type: resultArray[i].type };
                                        this.options.addedfile.call(this, mockFile);
                                        this.options.thumbnail.call(this, mockFile, file_image);
                                        mockFile.previewElement.classList.add('dz-success');
                                        mockFile.previewElement.classList.add('dz-complete');
                                    }
             
                             }"),
                        ],
                        'clientEvents' => [
                             'complete' => "function(file, index){
                                 var image = {
                                     name: file.name,
                                     size: file.size,
                                     type: file.type,
                                     path: ($('#path').val()+file.name),
                                 }; 
                                 /*console.log(image);*/
                                 var resultJsonInput = $('#localization-photos').val();
                                 if(resultJsonInput.length > 0) {
                                     var resultArray = JSON.parse(resultJsonInput);
                                 } else {
                                     var resultArray = Array();
                                 }
                                 resultArray.push(image);
                                 var JsonResult = JSON.stringify(resultArray);
                                 $('#localization-photos').val(JsonResult);
                                 $.get('/localization/set-image',
                                 {'id':$('#number').val(), 'photos':JsonResult},
                                     function(data){  $.pjax.reload({container:'#localization-pjax', async: false}); }
                                 );

                                }",
                                'removedfile' => "function(file){
                                    $.get('/localization/remove-file',
                                    {'id':$('#number').val(), 'filename':file.name},
                                        function(data){ 

                                        var resultJsonInput = $('#localization-photos').val();
                                        if(resultJsonInput.length > 0) {
                                            var resultArray = JSON.parse(resultJsonInput);
                                        } else {
                                            var resultArray = Array();
                                        }

                                        var index = -1;
                                        var length = resultArray.length;
                                        for (i = 0; i < length; i++) {
                                            if( resultArray[i].name == file.name && resultArray[i].size == file.size && resultArray[i].type == file.type) index = i;
                                        }
                                        resultArray.splice(index,1);
                                        var JsonResult = JSON.stringify(resultArray);
                                        $('#localization-photos').val(JsonResult);
                                                            
                                        $.get('/localization/set-image',
                                        {'id':$('#number').val(), 'photos':JsonResult},
                                            function(data){ $.pjax.reload({container:'#localization-pjax', async: false}); }
                                        );

                                        }     
                                    );
                                }"
                        ],
                    ]);
                    ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
