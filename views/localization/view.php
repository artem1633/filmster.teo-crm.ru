<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
?>
<div class="localization-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'language_id',
            'name',
            'description:ntext',
            'cast:ntext',
            'film_crew:ntext',
            'awards:ntext',
            'tags:ntext',
            'poster23:ntext',
            'poster169:ntext',
            'photos:ntext',
            'film_id',
        ],
    ]) ?>

</div>
