<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Localization */

?>
<div class="localization-create">
    <?= $this->render('_form', [
        'model' => $model,
        'number' => $number,
    ]) ?>
</div>
