<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
/* @var $form yii\widgets\ActiveForm */
$individual = 0;
$multiple = 0;

?>

<div class="localization-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-xs-6">
              <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
              <?= $form->field($model, 'language_id')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getLanguagesList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true,
                    ],
                ])?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
              <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Актерский состав                       
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add_input_individual"> + Добавить вариант</button>  
                    </h5>
                </div>
                <div class="panel-body">

                     <div class="row" id="individual_answer"  >
                        <div class="col-md-12">
                            <div id="dynamic_individual">
                                <?php
                                    foreach (json_decode($model->cast) as $value) {
                                        $individual++;
                                        if($individual == 1){
                                        ?>

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия актера/актрисы</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">Роль актера/актрисы</div> 
                                </div>
                                <?php } ?>

                                <div class="row" style="margin-top:10px;" id="budget<?=$individual?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="text" class="form-control" name="akter[]" value="<?=$value->akter?>" /> 
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <input type="text" class="form-control" name="role[]" value="<?=$value->role?>" /> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_individual" id="<?=$individual?>" class="btn btn-danger btn_remove_individual"> 
                                            <i class="glyphicon glyphicon-trash"></i> 
                                        </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="individual_count" name="individual_count" value="<?=$individual?>"/> 
                            </div>
                        </div>
                    </div>

                </div>                           
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        Съемочная группа                    
                        <button type="button" class="btn btn-warning btn-xs pull-right" name="add" id="add_input_multiple"> + Добавить вариант</button>
                    </h5>
                </div>
                <div class="panel-body">

                    <div class="row" id="multiple"  >
                        <div class="col-md-12">
                            <div id="dynamic_multiple">
                                <?php
                                    foreach (json_decode($model->film_crew) as $value) {
                                        $multiple++;
                                        if($multiple == 1){
                                ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия члена группы</div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">Роль члена группы</div> 
                                </div>
                                <?php } ?>
                                <div class="row" style="margin-top:10px;" id="multiple<?=$multiple?>">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> 
                                        <input type="text" class="form-control" name="names_group[]" value="<?=$value->names_group?>" />
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5"> 
                                        <select class="form-control" name="group_member[]" aria-required="true" aria-invalid="false">
                                            <?= $model->getRoleSelected($value->group_member) ?>                                            
                                        </select> 
                                    </div> 
                                    <div class="col-md-1 col-sm-1 col-xs-1" >
                                        <button type="button" name="remove_multiple" id="<?=$multiple?>" class="btn btn-danger btn_remove_multiple"> <i class="glyphicon glyphicon-trash"></i> </button> 
                                    </div>
                                </div>
                                <?php } ?>
                                <input type="hidden" class="form-control" id="multiple_count" name="multiple_count" value="<?=$multiple?>"/> 
                            </div>
                        </div>
                    </div>
                </div>                           
            </div>
        </div>

    </div>

    <br>    
    <div class="row">

        <div class="col-md-6 col-xs-12">
            <div id="poster23_file">
                <?= $model->poster23 != null ? '<img style="width:100%; height:250px;" src="http://' . $_SERVER["SERVER_NAME"] . "/uploads/" . 'localization/'. $number . '/' . $model->poster23 .' ">' : '' ?>
            </div>
            <br>
            <?= $form->field($model, 'poster23_file')->fileInput(['class'=>"poster23_image"]); ?>
        </div>

        <div class="col-md-6 col-xs-12">
            <div id="poster169_file">
                <?= $model->poster169 != null ? '<img style="width:100%; height:250px;" src="http://' . $_SERVER["SERVER_NAME"] . "/uploads/" . 'localization/'. $number . '/' . $model->poster169 .' ">' : '' ?>
            </div>
            <br>
            <?= $form->field($model, 'poster169_file')->fileInput(['class'=>"poster169_image"]); ?>
        </div>

        <div style="display: none;" >
            <?= $form->field($model, 'photos')->textarea(['rows' => 3]) ?>
            <input id="number" name="number" type="hidden" value="<?=$number?>">
            <input id="path" name="path" type="hidden" value="<?= "uploads/localization/".$number."/" ?>">
            <input id="url" name="url" type="hidden" value="<?= "http://" . $_SERVER['SERVER_NAME'] . '/' ?>">
        </div>

        <div class="col-md-12 col-xs-12">
            <br>
            <div class="panel panel-success">
                <div class="panel-heading ui-draggable-handle">
                    <h5>Кадры из фильма</h5>
                </div>
                <div class="panel-body">
                    <?= \kato\DropZone::widget([
                        'options' => [
                             'url'=> Url::to(['/localization/upload', 'number' => $number]),
                             'paramName'=>'image',
                             //'maxFilesize' => '10',
                             'addRemoveLinks' => true,
                             'dictRemoveFile' => 'Удалить',
                             'dictDefaultMessage' => "Выберите файлы",
                             'acceptedFiles' => 'image/*',
                             'init' => new JsExpression("function(file){ 

                                    var resultJsonInput = $('#localization-photos').val();
                                    if(resultJsonInput.length > 0) {
                                        var resultArray = JSON.parse(resultJsonInput);
                                    } else {
                                        var resultArray = Array();
                                    }

                                    var length = resultArray.length;
                                    var path = $('#path').val();
                                    //alert(path);
                                    for (i = 0; i < length; i++) {
                                        
                                        var file_image = $('#url').val() + resultArray[i].path;
                                        //alert(file_image);
                                        var mockFile = { name: resultArray[i].name, size: resultArray[i].size, type: resultArray[i].type };
                                        this.options.addedfile.call(this, mockFile);
                                        this.options.thumbnail.call(this, mockFile, file_image);
                                        mockFile.previewElement.classList.add('dz-success');
                                        mockFile.previewElement.classList.add('dz-complete');
                                    }
             
                             }"),
                        ],
                        'clientEvents' => [
                             'complete' => "function(file, index){
                                 var image = {
                                     name: file.name,
                                     size: file.size,
                                     type: file.type,
                                     path: ($('#path').val()+file.name),
                                 }; 
                                 /*console.log(image);*/
                                 var resultJsonInput = $('#localization-photos').val();
                                 if(resultJsonInput.length > 0) {
                                     var resultArray = JSON.parse(resultJsonInput);
                                 } else {
                                     var resultArray = Array();
                                 }
                                 resultArray.push(image);
                                 var JsonResult = JSON.stringify(resultArray);
                                 $('#localization-photos').val(JsonResult);

                                }",
                                'removedfile' => "function(file){
                                    $.get('/localization/remove-file',
                                    {'id':$('#number').val(), 'filename':file.name},
                                        function(data){ 

                                        var resultJsonInput = $('#localization-photos').val();
                                        if(resultJsonInput.length > 0) {
                                            var resultArray = JSON.parse(resultJsonInput);
                                        } else {
                                            var resultArray = Array();
                                        }

                                        var index = -1;
                                        var length = resultArray.length;
                                        for (i = 0; i < length; i++) {
                                            if( resultArray[i].name == file.name && resultArray[i].size == file.size && resultArray[i].type == file.type) index = i;
                                        }
                                        resultArray.splice(index,1);
                                        var JsonResult = JSON.stringify(resultArray);
                                        $('#localization-photos').val(JsonResult);
                                                            
                                        $.get('/localization/set-image',
                                        {'id':$('#number').val(), 'photos':JsonResult},
                                            function(data){ }
                                        );

                                        }     
                                    );
                                }"
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="form-control" id="aktorsList" name="aktorsList" /> 
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var indiv_count = document.getElementById('individual_count').value;
    var multiple_count = document.getElementById('multiple_count').value;
    var fileCollection = new Array();

    $.get('/localization/get-role',{ },function(data) { $('#aktorsList').val(data); } );

    $('#add_input_individual').click(function(){
        indiv_count++; 
        if(indiv_count == 1) $('#dynamic_individual').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия актера/актрисы</div><div class="col-md-5 col-sm-5 col-xs-5">Роль актера/актрисы</div> </div>');

        $('#dynamic_individual').append('<div class="row" style="margin-top:10px;" id="budget'+indiv_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="akter[]" /> </div><div class="col-md-5 col-sm-5 col-xs-5"> <input type="text" class="form-control" name="role[]" /> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_individual" id="'+indiv_count+'" class="btn btn-danger btn_remove_individual"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
    });


    $(document).on('click', '.btn_remove_individual', function(){
        var button_id = $(this).attr("id");
        $('#budget'+button_id+'').remove();
    });

    $('#add_input_multiple').click(function(){
        multiple_count++;
        if(multiple_count == 1) $('#dynamic_multiple').append('<div class="row"><div class="col-md-6 col-sm-6 col-xs-6">Имя и фамилия члена группы</div><div class="col-md-5 col-sm-5 col-xs-5">Роль члена группы</div></div>');

        $('#dynamic_multiple').append('<div class="row" style="margin-top:10px;" id="multiple'+multiple_count+'"><div class="col-md-6 col-sm-6 col-xs-6"> <input type="text" class="form-control" name="names_group[]" /> </div><div class="col-md-5 col-sm-5 col-xs-5"> <select class="form-control" name="group_member[]" aria-required="true" aria-invalid="false">'+$("#aktorsList").val()+'</select> </div> <div class="col-md-1 col-sm-1 col-xs-1" ><button type="button" name="remove_multiple" id="'+multiple_count+'" class="btn btn-danger btn_remove_multiple"> <i class="glyphicon glyphicon-trash"></i> </button> </div></div>');
        });

        $(document).on('click', '.btn_remove_multiple', function(){
            var button_id = $(this).attr("id");
            $('#multiple'+button_id+'').remove();
    });

    $(document).on('change', '.poster23_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%; max-height:180px;" src="'+e.target.result+'"> ';
                $('#poster23_file').html('');
                $('#poster23_file').append(template);
            };
        });
    });

    $(document).on('change', '.poster169_image', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:100%; max-height:180px;" src="'+e.target.result+'"> ';
                $('#poster169_file').html('');
                $('#poster169_file').append(template);
            };
        });
    });

});

JS
);
?>