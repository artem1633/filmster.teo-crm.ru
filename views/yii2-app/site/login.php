<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Вход';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
        <div class="login-container lightmode">        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <br>
                <div class="login-body">
                    <div class="login-title" style="text-align: center;" ><strong>Введите данные авторизации</strong></div>

                    <?php $form = ActiveForm::begin(['id' => 'login-form', 'class' => 'form-horizontal', 'enableClientValidation' => false]); ?>

                        <div class="form-group">
                            <?= $form
                                ->field($model, 'username')
                                ->label(false)
                                ->textInput(['placeholder' => 'Логин'/*$model->getAttributeLabel('username')*/]) ?>
                        </div>

                        <div class="form-group">
                            <?= $form
                                ->field($model, 'password')
                                ->label(false)
                                ->passwordInput(['placeholder' => 'Пароль'/*$model->getAttributeLabel('password')*/]) ?>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-xs-6">
                                <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомни меня') ?>
                            </div>
                            <!-- <div class="col-md-6 col-xs-6">
                                <?php /*Html::a('Зарегистрироваться', ['register'], ['class' => 'pull-right', 'style' => ' margin-top: 5px;']);*/ ?>
                            </div> -->
                        </div>
                        <div class="form-group">
                            <?= Html::submitButton('Вход', [ 'style' => 'width:100%', 'class' => 'btn btn-info btn-block', 'name' => 'login-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>            
        </div>