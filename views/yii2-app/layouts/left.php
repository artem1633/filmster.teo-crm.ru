<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

if (!file_exists('avatars/'.Yii::$app->user->identity->foto) || Yii::$app->user->identity->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->foto;
}

?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->fio?></div>
                                <div class="profile-data-title"><?=Yii::$app->user->identity->getTypeDescription()?></div>
                            </div>
                            <div class="profile-controls">
                                <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-left']); ?>
                                <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                            </div>
                        </div>                                                                        
                    </li>
                    <!-- <li class="xn-title">Menu</li> -->
                    <li>
                        <?= Html::a('<span class="fa fa-film"></span> <span class="xn-text">Фильмы</span>', ['/films/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-money"></span> <span class="xn-text">Договоры</span>', ['/contract/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-flag-checkered"></span> <span class="xn-text">Окончание прав</span>', ['/films/end-access'], []); ?>
                    </li>
                    <?php if(Yii::$app->user->identity->type == 0){ ?>
                        <li>
                            <?= Html::a('<span class="fa fa-road"></span> <span class="xn-text">Площадки</span>', ['/platform/index'], []); ?>
                        </li>
                    <?php } ?>
                    <li>
                        <?= Html::a('<span class="fa fa-calendar"></span> <span class="xn-text">Календарь</span>', ['/contract/calendar'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-road"></span> <span class="xn-text">Отчеты</span>', ['/users/nestable'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-usd"></span> <span class="xn-text">Статистика</span>', ['/users/statistic'], []); ?>
                    </li>
                    <?php if(in_array(Yii::$app->user->identity->type, [0, 1])){ ?>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Продажи</span></a>
                            <ul>
                                <?php if(Yii::$app->user->identity->type == 0): ?>
                                    <li>
                                        <a href="/report-itunes/index"><span class="fa fa-book"></span> <span class="xn-text">Отчет</span></a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <a href="/sales/royalty"><span class="fa fa-book"></span> <span class="xn-text">Поступления</span></a>
                                </li>
                                <li>
                                    <a href="/sales/unit"><span class="fa fa-book"></span> <span class="xn-text">Единицы</span></a>
                                </li>
                            </ul>
                        </li>
                    <li><?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Клиенты</span>', ['/users/index'], []); ?></li>
                    <?php } ?>
                    <?php if(Yii::$app->user->identity->type == 0){ ?>
                        <li>
                            <?= Html::a('<span class="glyphicon glyphicon-stats"></span> <span class="xn-text">Загрузка отчета</span>', ['/report-monetary/index'], []); ?>
                        </li> 
                    <?php } ?>
                    <?php if(Yii::$app->user->identity->type == 0){ ?>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-book"></span> <span class="xn-text">Справочники</span></a>
                            <ul>
                                <li>
                                    <?= Html::a('Тип фильма', ['/films-type/index'], []); ?>
                                </li>
                                <li>
                                    <?= Html::a('Лицензиаты/Лицензиары', ['/right-holders/index'], []); ?>
                                </li>
                                <li>
                                    <?= Html::a('Актуальность релиза', ['/actuality/index'], []); ?>
                                </li>
                                <li>
                                    <?= Html::a('Настройки', ['/settings/index'], []); ?>
                                </li>
                                <!-- <li>
                                    <?php // Html::a('<span class="fa fa-university"></span>Языки', ['/languages/index'], []); ?>
                                </li>  -->
                                <!-- <li>
                                    <?php// Html::a('<span class="fa fa-university"></span>Компания производитель', ['/manufacturer/index'], []); ?>
                                </li> --> 
                            </ul>
                        </li>
                    <?php } ?>
                        <!-- <li class="xn-openable">
                            <a href="#"><span class="fa fa-book"></span><span class="xn-text">Системный справочник</span></a>
                            <ul>
                                <li>
                                    <?php // Html::a('<span class="fa fa-university"></span>Страна производства', ['/country/index'], []); ?>
                                </li> 
                                <li>
                                    <?php // Html::a('<span class="fa fa-university"></span>Жанры', ['/genres/index'], []); ?>
                                </li> 
                                <li>
                                    <?php // Html::a('<span class="fa fa-university"></span>Валюта', ['/currency/index'], []); ?>
                                </li> 
                            </ul>
                        </li> -->
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->