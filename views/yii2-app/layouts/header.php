<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use yii\helpers\Html;
use app\models\Settings;
use app\models\Resume;
use yii\helpers\Url;

?>
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" onclick="$.post('/site/menu-position');" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <li class="xn-icon-button pull-right">
                        <?= Html::a('<span class="fa fa-sign-out"></span>',['/site/logout'],['data-confirm'=> false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',]) ?>
                    </li> 
                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                    

                <section class="breadcrumb push-down-0">
                <?php if (isset($this->blocks['content-header'])) { ?>
                    <h1><?php $this->blocks['content-header'] ?></h1>
                <?php } else { ?>

                <?php } ?>

            </section>
            <div class="page-content-wrap">
                <br>                    
            </div>
