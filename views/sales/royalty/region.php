<div class="col-md-12">
    <h1>Единицы за день</h1>
	<div class="table-responsive">
	    <canvas id="charRegion" width="400" height="100"></canvas>
	</div>
	<br>
	<div class="table-responsive">
        <table class="table table-bordered table-striped table-actions">
            <thead>
				<tr>
				    <th width="10">№</th>
				    <th width="300">Страна</th>
				    <th width="100">Единицы</th>
				    <th width="200">Предыдущий период</th>
				</tr>
            </thead>
            <tbody>                                            
                <tr>
                    <td class="text-center">1</td>
                    <td>
                    	<strong><span class="label label-success">Европа</span></strong>
                    </td>
                    <td>65324</td>
                    <td>350 &nbsp;&nbsp;&nbsp; -5%</td>
                </tr>
                <tr>
                    <td class="text-center">1</td>
                    <td>
                    	<strong><span class="label label-success">США и Канада</span></strong>
                    </td>
                    <td>2345</td>
                    <td>350 &nbsp;&nbsp;&nbsp; +8%</td>
                </tr>
                <tr>
                    <td class="text-center">1</td>
                    <td>
                    	<strong><span class="label label-success">Африка, Ближный восток и Индия</span></strong>
                    </td>
                    <td>67</td>
                    <td>350 &nbsp;&nbsp;&nbsp; +24%</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>