<div class="col-md-12">
    <h1>Единицы за день</h1>
	<div class="table-responsive">
	    <canvas id="charContent" width="400" height="100"></canvas>
	</div>
	<br>
	<div class="table-responsive">
        <table class="table table-bordered table-striped table-actions">
            <thead>
				<tr>
				    <th width="10">№</th>
				    <th width="300">Название</th>
				    <th width="100">Тип контента</th>
				    <th width="100">Apple ID</th>
				    <th width="100">Единицы</th>
				    <th width="200">Предыдущий период</th>
				</tr>
            </thead>
            <tbody>                                            
                <tr>
                    <td class="text-center">1</td>
                    <td>
                    	<strong><span class="label label-success">Джуди</span></strong>
                        <p>Rosalyn Wilder, Финн Уиттрок, Руфус Сьюэлл, Рене Зеллвегер, Джесси Бакли</p>
                    </td>
                    <td>Film</td>
                    <td>12451125</td>
                    <td>185</td>
                    <td>350 &nbsp;&nbsp;&nbsp; +88%</td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td>
                    	<strong><span class="label label-success">Запятнанная репутация</span></strong>
                        <p>Nicole Kidman, Gary Sinise, Ed Harris, Anthony Hopkins, Вентворт Миллер</p>
                    </td>
                    <td>Film</td>
                    <td>12478125</td>
                    <td>138</td>
                    <td>350 &nbsp;&nbsp;&nbsp; -48%</td>
                </tr>
                <tr>
                    <td class="text-center">3</td>
                    <td>
                    	<strong><span class="label label-success">Залечь на дно в Брюгге</span></strong>
                        <p>Colin Farrell, Рэйф Файнс, Клеманс Поэзи, Джордан Прентис, Брендан Глисон</p>
                    </td>
                    <td>Film</td>
                    <td>12412125</td>
                    <td>113</td>
                    <td>350 &nbsp;&nbsp;&nbsp; +48%</td>
                </tr>
                <tr>
                    <td class="text-center">4</td>
                    <td>
                    	<strong><span class="label label-success">Капитал в XXI веке</span></strong>
                        <p>Френсис Фукуяма, Томас Пикетти, Суреш Найду, Джиллиан Тетт, Брайс Эдвардс</p>
                    </td>
                    <td>Film</td>
                    <td>12452225</td>
                    <td>83</td>
                    <td>350 &nbsp;&nbsp;&nbsp; -8%</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>