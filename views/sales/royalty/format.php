<div class="col-md-12">
    <h1>Единицы за день</h1>
	<div class="table-responsive">
	    <canvas id="chartFormat" width="400" height="100"></canvas>
	</div>
	<br>
	<div class="table-responsive">
        <table class="table table-bordered table-striped table-actions">
            <thead>
				<tr>
				    <th width="10">№</th>
				    <th width="300">Формат</th>
				    <th width="100">Единицы</th>
				    <th width="200">Предыдущий период</th>
				</tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-center">1</td>
                    <td>
                    	<strong><span class="label label-success">HD</span></strong>
                    </td>
                    <td>65324</td>
                    <td>350 &nbsp;&nbsp;&nbsp; -5%</td>
                </tr>
                <tr>
                    <td class="text-center">2</td>
                    <td>
                    	<strong><span class="label label-success">SD</span></strong>
                    </td>
                    <td>2345</td>
                    <td>350 &nbsp;&nbsp;&nbsp; +8%</td>
                </tr>
                 <tr>
                    <td class="text-center">3</td>
                    <td>
                        <strong><span class="label label-success">4K</span></strong>
                    </td>
                    <td>2345</td>
                    <td>350 &nbsp;&nbsp;&nbsp; +8%</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>