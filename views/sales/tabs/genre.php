<?php

use app\components\ReportStatistic;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use app\helpers\DateTimeHelper;

//$report = new ReportStatistic(['filterData' => ['noRelation' => $searchModel->noRelation, 'dateStart' => $searchModel->dateStart, 'dateEnd' => $searchModel->dateEnd, 'title' => $searchModel->title]]);
//$dataset = $report->getStatistics('genre');

$titles = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'name', 'name');

$films = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'id', 'name');

$dataProvider->setModels($genreData);


?>
<div class="col-md-12">
    <h1>Единицы за день</h1>
    <div class="table-responsive">
        <canvas id="chartGenre" width="400" height="100"></canvas>
    </div>
    <br>
</div>

<div class="col-md-12">
    <?=GridView::widget([
        'id'=>'crud-genre-datatable',
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<span class="not-set">[нет данных]</span>'],
        'pjax'=>true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'genre',
                'label' => 'Жанр',
                'content' => function($model){
                    return '<a data-filter="'.$model['genre'].'" style="cursor: pointer;"><strong><span class="label label-success">'.$model['genre'].'</span></strong></a>';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'label' => 'Единицы',
                'attribute'=>'sales',
            ],
        ],
        'toolbar'=> false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => false,
    ])?>
</div>

<?php

$script = <<< JS

var ctx = document.getElementById('chartGenre').getContext('2d');
var charContent = new Chart(ctx, {
    type: 'line',
    data: {
        labels: {$labels},
        datasets: {$objects},
    }
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>