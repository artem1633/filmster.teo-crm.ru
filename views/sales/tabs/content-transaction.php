<?php

use app\components\ReportStatistic;
use app\models\Localization;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\DateTimeHelper;

//$report = new ReportStatistic(['filterData' => ['noRelation' => $searchModel->noRelation, 'dateStart' => $searchModel->dateStart, 'dateEnd' => $searchModel->dateEnd, 'title' => $searchModel->title]]);
//$dataset = $report->getStatistics('type_transaction');

$titles = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'name', 'name');

$films = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'id', 'name');

$dataProvider->setModels($contentData);



?>
<div class="col-md-12">
    <h1>Единицы за день</h1>
	<div class="table-responsive">
	    <canvas id="chartContentTransaction" width="400" height="100"></canvas>
	</div>
	<br>
</div>

<div class="col-md-12">
    <?=GridView::widget([
        'id'=>'crud-type-transaction-datatable',
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<span class="not-set">[нет данных]</span>'],
        'pjax'=>true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'type_transaction',
                'label' => 'Тип транзакции',
                'content' => function($model){
                    return '<a data-filter="'.$model['type_transaction'].'" style="cursor: pointer;"><strong><span class="label label-success">'.$model['type_transaction'].'</span></strong></a>';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'label' => 'Единицы',
                'attribute'=>'sales',
            ],
        ],
        'toolbar'=> false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => false,
    ])?>
</div>
<?php

$script = <<< JS

var ctx = document.getElementById('chartContentTransaction').getContext('2d');
var charContent = new Chart(ctx, {
  	type: 'line',
  	data: {
    	labels: {$labels},
    	datasets: {$objects},
  	}
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>