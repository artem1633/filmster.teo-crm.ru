<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\form\ActiveForm;
use app\models\Films;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\additional\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Единицы';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(Yii::$app->user->identity->type == 0){
    $titles = ArrayHelper::map(Films::find()->all(), 'name', 'name');
} else {
    $titles = ArrayHelper::map(Films::find()->where(['id' => Yii::$app->user->identity->getRelatedFilmsIds()])->all(), 'name', 'name');
}

if($searchModel->currency == null){
    $searchModel->currency = 'RUB';
}



?>
<?php $form = ActiveForm::begin(['id' => "search-title-form", 'method' => 'GET']) ?>

    <div class="col-md-3">
        <?= $form->field($searchModel, 'title')->widget(\kartik\select2\Select2::class, [
            'data' => $titles,
            'pluginOptions' => [
                'tags' => true,
                'multiple' => true,
            ],
        ]) ?>
    </div>
    <div class="col-md-1">
        <?= $form->field($searchModel, 'currency')->dropDownList([
            'RUB' => 'RUB',
            'USD' => 'USD',
            'EUR' => 'EUR',
            'GBP' => 'GBP',
        ])->label('Валюта') ?>
    </div>
    <div class="col-md-2">
        <?= $form->field($searchModel, 'dateStart')->input('date')->label('Дата С') ?>
    </div>
    <div class="col-md-2">
        <?= $form->field($searchModel, 'dateEnd')->input('date')->label('Дата По') ?>
    </div>
    <div class="col-md-1">
        <div style="margin-top: 26px;">
            <?= $form->field($searchModel, 'noRelation')->checkbox()->label(false) ?>
        </div>
    </div>
    <div class="hidden">
        <?= $form->field($searchModel, 'countTitles')->hiddenInput() ?>
    </div>
    <div class="col-md-3">
        <?= \yii\helpers\Html::submitButton('Фильтровать', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px;']) ?>
    </div>
<?php ActiveForm::end() ?>
    <div class="panel panel-default tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true">Контент</a></li>
            <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false">Страна</a></li>
            <li class=""><a href="#tab3" data-toggle="tab" aria-expanded="false">Жанр</a></li>
            <li class=""><a href="#tab4" data-toggle="tab" aria-expanded="false">Тип контента</a></li>
            <!--        <li class=""><a href="#tab5" data-toggle="tab" aria-expanded="false">Тип транзакции</a></li>-->
            <li class=""><a href="#tab6" data-toggle="tab" aria-expanded="false">Формат</a></li>
            <!-- <li class=""><a href="#tab7" data-toggle="tab" aria-expanded="false">Studio/Брендинг-компания</a></li> -->
        </ul>
        <div class="panel-body tab-content">
            <div class="tab-pane active" id="tab1">
                <?= $this->render('tabs-royalty/content', [
                    //'model' => $model,
                    'objects' => $objects,
                    'labels' => $labels,
                    'searchModel' => $searchModel,
                    'contentData' => $contentData,
                    'totalSales' => $totalSales,
                    'dataProvider' => $dataProvider,
//                'dataProvider' => $dataProviderTitle,
//                'dataProviderGraphic' => $dataProviderTitleGraphic,
                ]) ?>
            </div>
            <div class="tab-pane" id="tab2">
                <?= $this->render('tabs-royalty/region', [
                    //'model' => $model,
                    'objects' => $objects,
                    'labels' => $labels,
                    'searchModel' => $searchModel,
                    'regionData' => $regionData,
                    'totalSales' => $totalSales,
                    'dataProvider' => $dataProvider,
//                'dataProvider' => $dataProviderCountry,
//                'dataProviderGraphic' => $dataProviderCountryGraphic,
                ]) ?>
            </div>
            <div class="tab-pane" id="tab3">
                <?= $this->render('tabs-royalty/genre', [
                    //'model' => $model,
                    'objects' => $objects,
                    'labels' => $labels,
                    'searchModel' => $searchModel,
                    'genreData' => $genreData,
                    'totalSales' => $totalSales,
                    'dataProvider' => $dataProvider,
//                'dataProvider' => $dataProviderGenre,
//                'dataProviderGraphic' => $dataProviderGenreGraphic,
                ]) ?>
            </div>
            <div class="tab-pane" id="tab4">
                <?= $this->render('tabs-royalty/content-type', [
                    //'model' => $model,
                    'objects' => $objects,
                    'labels' => $labels,
                    'searchModel' => $searchModel,
                    'contentTypeData' => $contentTypeData,
                    'totalSales' => $totalSales,
                    'dataProvider' => $dataProvider,
//                'dataProvider' => $dataProviderType,
//                'dataProviderGraphic' => $dataProviderTypeGraphic,
                ]) ?>
            </div>
            <div class="tab-pane" id="tab5">
                <?
                //            echo $this->render('tabs-royalty/content-transaction', [
                //		        //'model' => $model,
                //                'searchModel' => $searchModel,
                ////                'dataProvider' => $dataProviderTypeTransaction,
                ////                'dataProviderGraphic' => $dataProviderTypeTransactionGraphic,
                //		    ])
                ?>
            </div>
            <div class="tab-pane" id="tab6" style="width: 95%;">
                <?= $this->render('tabs-royalty/format', [
                    //'model' => $model,
                    'objects' => $objects,
                    'labels' => $labels,
                    'totalSales' => $totalSales,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'formatData' => $formatData,
//                'dataProvider' => $dataProviderFormat,
//                'dataProviderGraphic' => $dataProviderFormatGraphic,
                ]) ?>
            </div>
            <!-- <div class="tab-pane" id="tab7">
                Studio/Брендинг-компания
            </div> -->
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<?php
$this->registerJs(<<<JS

// var ctx = document.getElementById('charContent').getContext('2d');
// var charContent = new Chart(ctx, {
//   	type: 'line',
//   	data: {
//     	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//     	datasets: [{
//       		label: 'apples',
//       		data: [120, 190, 300, 170, 600, 340, 450],
//       		backgroundColor: "rgba(153,255,51,0.6)"
//     	}]
//   	}
// });

// var ctx = document.getElementById('charRegion').getContext('2d');
// var charRegion = new Chart(ctx, {
//   	type: 'line',
//   	data: {
//     	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//     	datasets: [{
//       		label: 'Europe',
//       		data: [120, 190, 300, 170, 600, 340, 450],
//       		backgroundColor: "rgba(153,255,51,0.6)"
//     	},
//     	{
//       		label: 'Africa',
//       		data: [123, 192, 310, 160, 550, 335, 454],
//       		backgroundColor: "rgba(103,235,59,0.6)"
//     	}]
//   	}
// });

// var ctx = document.getElementById('chartGenre').getContext('2d');
// var chartGenre = new Chart(ctx, {
//   	type: 'line',
//   	data: {
//     	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//     	datasets: [{
//       		label: 'apples',
//       		data: [120, 190, 300, 170, 600, 340, 450],
//       		backgroundColor: "rgba(153,255,51,0.6)"
//     	}]
//   	}
// });

// var ctx = document.getElementById('chartContentType').getContext('2d');
// var chartContentType = new Chart(ctx, {
//   	type: 'line',
//   	data: {
//     	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//     	datasets: [{
//       		label: 'Europe',
//       		data: [120, 190, 300, 170, 600, 340, 450],
//       		backgroundColor: "rgba(153,255,51,0.6)"
//     	},
//     	{
//       		label: 'Africa',
//       		data: [123, 192, 310, 160, 550, 335, 454],
//       		backgroundColor: "rgba(103,235,59,0.6)"
//     	}]
//   	}
// });

// var ctx = document.getElementById('chartContentTransaction').getContext('2d');
// var chartContentTransaction = new Chart(ctx, {
//   	type: 'line',
//   	data: {
//     	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//     	datasets: [{
//       		label: 'apples',
//       		data: [120, 190, 300, 170, 600, 340, 450],
//       		backgroundColor: "rgba(153,255,51,0.6)"
//     	}]
//   	}
// });
//
// var ctx = document.getElementById('chartFormat').getContext('2d');
// var chartFormat = new Chart(ctx, {
//   	type: 'line',
//   	data: {
//     	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//     	datasets: [{
//       		label: 'Europe',
//       		data: [120, 190, 300, 170, 600, 340, 450],
//       		backgroundColor: "rgba(153,255,51,0.6)"
//     	},
//     	{
//       		label: 'Africa',
//       		data: [123, 192, 310, 160, 550, 335, 454],
//       		backgroundColor: "rgba(103,235,59,0.6)"
//     	}]
//   	}
// });

JS
);
?>