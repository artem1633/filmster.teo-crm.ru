<?php

use app\components\ReportStatistic;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use app\helpers\DateTimeHelper;
use yii\helpers\ArrayHelper;

//$report = new ReportStatistic(['filterData' => ['noRelation' => $searchModel->noRelation, 'dateStart' => $searchModel->dateStart, 'dateEnd' => $searchModel->dateEnd, 'title' => $searchModel->title]]);
//$dataset = $report->getStatistics('genre');




usort($titlesArr, function($a, $b){
    return $a['sales'] < $b['sales'];
});

if($searchModel->countTitles == 100){
    if(count($titlesArr) > 100){
        $titlesArr = array_slice($titlesArr, 0, 100);
    }
} else {
    if(count($titlesArr) > 10){
        $titlesArr = array_slice($titlesArr, 0, 10);
    }
}



//\yii\helpers\VarDumper::dump($dataset[0], 10, true);

//foreach ($dataset[1] as $title => $arr){
//    $objects[] = [
//        'label' => $title,
//        'data' => $arr,
//        'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
//    ];
//}

//$labels = json_encode($dataset[0]);


$titles = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'name', 'name');

$films = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'id', 'name');



$dataProvider->setModels($genreData);


?>
<div class="col-md-12">
     <h1>Сумма <?=$totalSales?> <?=$searchModel->currency?></h1>
	<div class="table-responsive">
	    <canvas id="chartGenre" width="400" height="100"></canvas>
	</div>
	<br>
</div>

<div class="col-md-12">
    <?=GridView::widget([
        'id'=>'crud-genre-datatable',
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<span class="not-set">[нет данных]</span>'],
        'pjax'=>true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'genre',
                'label' => 'Жанр',
                'content' => function($model){
                    return '<a data-filter="'.$model['genre'].'" style="cursor: pointer;"><strong><span class="label label-success">'.$model['genre'].'</span></strong></a>';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'label' => 'Единицы',
                'attribute'=>'sales',
            ],
        ],
        'toolbar'=> false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => false,
    ])?>
</div>

<?php

$script = <<< JS

var ctx = document.getElementById('chartGenre').getContext('2d');
var charContent = new Chart(ctx, {
  	type: 'line',
  	data: {
    	labels: {$labels},
    	datasets: {$objects},
  	}
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>