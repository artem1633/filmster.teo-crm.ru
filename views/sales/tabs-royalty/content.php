<?php

use app\models\Localization;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use app\components\ReportStatistic;
use yii\helpers\Html;
use app\helpers\DateTimeHelper;


//
//\yii\helpers\VarDumper::dump($dataset[1], 10, true);
//exit;
//
//$totalArr = [];
//
//if(isset($dataset[1][0])){
//
//}
//
//foreach ($dataset[1] as $title => $arr){
//
//}

//$data = ['label' => 'Контент', 'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)"];

//foreach ()

// var_dump($allTitles);
// exit;



usort($contentData, function($a, $b){
    return $a['sales'] < $b['sales'];
});

    if(count($contentData) > 100){
        $contentData = array_slice($contentData, 0, 100);
    }



//\yii\helpers\VarDumper::dump($titlesArr, 10, true);
//exit;



// $objects[] = [
//     'label' => 'Контент',
//     'data' => $data,
//     'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
// ];

//\yii\helpers\VarDumper::dump($dataset[0], 10, true);

//foreach ($dataset[1] as $title => $arr){
//    $objects[] = [
//        'label' => $title,
//        'data' => $arr,
//        'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
//    ];
//}



$titles = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'name', 'name');

$films = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'id', 'name');

$dataProvider->setModels($contentData);

?>

<div class="col-md-12">
    <h1>Сумма <?=$totalSales?> <?=$searchModel->currency?></h1>
	<div class="table-responsive">
	    <canvas id="charContent" width="400" height="100%"></canvas>
	</div>
</div>
    <br>
    <br>


    <div class="col-md-12">
        <?=GridView::widget([
            'id'=>'crud-title-datatable',
            'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<span class="not-set">[нет данных]</span>'],
            'pjax'=>true,
            'rowOptions' => function($model, $index) use($searchModel){
                if($searchModel->countTitles != 100){
                    if($index >= 10){
                        return ['style' => 'display: none;'];
                    }
                }
            },
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'poster',
                    'label' => 'Постер',
                    'content' => function($data){
                        $localization = Localization::find()->where(['film_id' => $data['film_id'], 'type' => 1])->one();
                        if($localization != null){
                            if (!file_exists('uploads/localization/' . $localization->id . '/' . $localization->poster23) || $localization->poster23 == '') {
                                $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
                            } else {
                                $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/localization/' . $localization->id . '/'. $localization->poster23;
                            }
                        }
                        else{
                            $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/no-poster.png';
                        }
                        return Html::a(Html::img($path, [ 'style' => 'width:35px;']), ['films/view', 'id' => $data['film_id']]);
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'title',
                    'label' => 'Наименование',
                    'content' => function($model) use ($films){

                        return '<a data-filter="'.$films[$model['film_id']].'" style="cursor: pointer;"><strong><span class="label label-success">'.$model['title'].'</span></strong></a>';
                    }
                ],
//                [
//                    'class'=>'\kartik\grid\DataColumn',
//                    'attribute'=>'type',
//                    'label' => 'Тип',
//                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'apple_identifier',
                    'label' => 'Apple ID',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'label' => 'Сумма',
                    'attribute'=>'sales',
//                    'pageSummary' => true,
                ],
            ],
//            'showPageSummary' => true,
            'toolbar'=> false,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => false,
        ])?>
        <?= \yii\helpers\Html::a('Ещё', '#', ['onclick' => 'event.preventDefault(); $("#crud-title-datatable table tr[style=\'display: none;\']").show();']) ?>

    </div>

<?php

$script = <<< JS

var ctx = document.getElementById('charContent').getContext('2d');
var charContent = new Chart(ctx, {
  	type: 'line',
  	data: {
    	labels: {$labels},
    	datasets: {$objects},
  	}
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>