<?php

use app\components\ReportStatistic;
use app\models\Localization;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\DateTimeHelper;

//$report = new ReportStatistic(['filterData' => ['noRelation' => $searchModel->noRelation, 'dateStart' => $searchModel->dateStart, 'dateEnd' => $searchModel->dateEnd, 'title' => $searchModel->title]]);
//$dataset = $report->getStatistics('type_transaction');


$objects = [];

if($searchModel->dateStart == null || $searchModel->dateEnd == null){
    $dates[] = date('Y-m-d', (time() - (86400 * 30)));
    $dates[] = date('Y-m-d');
} else {
    $dates = [$searchModel->dateStart, $searchModel->dateEnd];
}


if(Yii::$app->user->identity->type == 0){
    $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andFilterWhere(['type_transaction' => $searchModel->currency])->groupBy('type_transaction')->all();
} else {
    $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andWhere(['film_id' => Yii::$app->user->identity->getRelatedFilmsIds()])->andFilterWhere(['customer_currency' => $searchModel->currency, 'title' => $searchModel->title])->groupBy('type_transaction')->all();
}

$titlesArr = []; // ['poster', 'title', 'type', 'sales']

$totalSales = 100;

foreach ($allTitles as $model){
    $sales = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andWhere(['title' => $model->title])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();

    $salesSum = 0;

    foreach ($sales as $sale)
    {
        $salesSum += $sale->sales * $sale->customer_price;
    }


    $titlesArr[] = [
        'type_transaction' => $model->type_transaction,
        'sales' => $salesSum,
    ];

    $totalSales += round($salesSum);
}

usort($titlesArr, function($a, $b){
    return $a['sales'] < $b['sales'];
});

if($searchModel->countTitles == 100){
    if(count($titlesArr) > 100){
        $titlesArr = array_slice($titlesArr, 0, 100);
    }
} else {
    if(count($titlesArr) > 10){
        $titlesArr = array_slice($titlesArr, 0, 10);
    }
}



//\yii\helpers\VarDumper::dump($titlesArr, 10, true);
//exit;

$dataProvider = new \yii\data\ArrayDataProvider(['allModels' => $titlesArr]);
$dataProvider->pagination = false;

$days = DateTimeHelper::getDates($dates[0], $dates[1]);

$data = [];

foreach ($days as $day)
{
    if(Yii::$app->user->identity->type == 0){
        $dayModels = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['type_transaction' => $searchModel->type_transaction])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();
    } else {
        $dayModels = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['type_transaction' => $searchModel->type_transaction])->andWhere(['film_id' => Yii::$app->user->identity->getRelatedFilmsIds()])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();
    }
    $sum = 0;
    foreach($dayModels as $dayModel)
    {
        $sum += $dayModel->customer_price * $dayModel->sales;
    }
    $data[] = round($sum);
}

$objects[] = [
    'label' => 'Контент',
    'data' => $data,
    'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
];

//\yii\helpers\VarDumper::dump($dataset[0], 10, true);

//foreach ($dataset[1] as $title => $arr){
//    $objects[] = [
//        'label' => $title,
//        'data' => $arr,
//        'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
//    ];
//}

//$labels = json_encode($dataset[0]);
$labels = json_encode($days);
$objects = json_encode($objects);


$titles = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'name', 'name');

$films = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'id', 'name');




?>
<div class="col-md-12">
     <h1>Сумма <?=$totalSales?> <?=$searchModel->currency?></h1>
	<div class="table-responsive">
	    <canvas id="chartContentTransaction" width="400" height="100"></canvas>
	</div>
	<br>
</div>

<div class="col-md-12">
    <?=GridView::widget([
        'id'=>'crud-type-transaction-datatable',
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '<span class="not-set">[нет данных]</span>'],
        'pjax'=>true,
        'columns' => [
            [
                'class' => 'kartik\grid\SerialColumn',
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'type_transaction',
                'label' => 'Тип транзакции',
                'content' => function($model){
                    return '<a data-filter="'.$model['type_transaction'].'" style="cursor: pointer;"><strong><span class="label label-success">'.$model['type_transaction'].'</span></strong></a>';
                }
            ],
            [
                'class'=>'\kartik\grid\DataColumn',
                'label' => 'Сумма',
                'attribute'=>'sales',
            ],
        ],
        'toolbar'=> false,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => false,
    ])?>
</div>
<?php

$script = <<< JS

var ctx = document.getElementById('chartContentTransaction').getContext('2d');
var charContent = new Chart(ctx, {
  	type: 'line',
  	data: {
    	labels: {$labels},
    	datasets: {$objects},
  	}
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>