<?php

use yii\helpers\Html;
use app\models\Applications;
use app\models\Users;
use app\models\FilmsApplication;
use app\models\ReportMonetary;
use app\models\ReportValues;
use app\models\Films;
use app\models\PreferenceBooks;
use app\models\Platform;
use app\models\Currency;
use app\models\CurrencySetting;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

//CrudAsset::register($this);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<?php
$i = 0;
foreach ($items as $item) 
{
    /*echo "<pre>";
    print_r($item['result']);
    echo "</pre>";
    die;*/
    $i++;
?>
    <div class="panel panel-danger">
        <div class="panel-heading ui-draggable-handle">
            <a class="<?=$i == 1 ? '' : 'collapsed' ?>" data-toggle="collapse" data-parent="#accordion1" href="#discussionId<?=$item['contract']->id?>">
                <span class="pull-right">
                    <i class="glyphicon glyphicon-chevron-down"></i>                            
                </span> 
                <b>Договор № <?=$item['contract']->number . " " . $item['currency']?></b>
            </a>
        </div> 
        <div id="discussionId<?=$item['contract']->id?>" class="collapse <?=$i == 1 ? 'in' : '' ?> ">
            <div class="panel-body">
            	<div class="col-md-6">
            		<h1><?=$item['sale']?></h1>
            		<span>Продажи</span>
                    <h1><?=$item['est']?></h1>
                    <span>EST</span>
                    <h1><?=$item['vod']?></h1>
                    <span>VOD</span>
                    <h1><?=$item['svod']?></h1>
                    <span>SVOD</span>
                    <h1><?=$item['pvod']?></h1>
                    <span>PVOD</span>
                    <h1><?=$item['avod']?></h1>
                    <span>AVOD</span>
            		<h1><?=$item['est'] + $item['vod'] + $item['svod'] + $item['pvod'] + $item['avod']?></h1>
            		<span>Всего</span>
            	</div>
            	<div class="col-md-6">
	               	<canvas id="myChart<?=$item['contract']->id?>" width="300" height="300"></canvas>
	               	<script>
						var ctx = document.getElementById("myChart<?=$item['contract']->id?>");
						var myChart = new Chart(ctx, {
						 	type: 'pie',
						  	data: {
						    labels: [<?=$item['filmName']?>],
						    datasets: [{
						    	label: '# of Tomatoes',
						      	data: [<?=$item['data']?>],
						      	backgroundColor: [
						        	<?=$item['backgroundColor']?>
						      	],
						      borderWidth: 1
						    }]
						  },
						options: {
						   	cutoutPercentage: 40,
						    responsive: false,
						    legend: {
								display: false,
						      	position: "bottom",
						      	labels: {
						       		fontColor: "#333",
						       		fontSize: 10
						      	}
						    }
						}
						});
	               	</script>
            	</div>
            </div>
            <div class="col-md-12">
                <?=GridView::widget([
                    'id'=>'crud-datatable'.$item['contract']->id,
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $item['result'],
                        'pagination' => [
                            'pageSize' => 1000,
                        ],
                        'sort' => [
                            'attributes' => $item['searchAttributes'],
                        ],
                    ]),
                    'filterModel' => $item['searchModel'],
                    'tableOptions' => [
                        'class' => 'table table-bordered'
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    'hover' => true,
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => array_merge(
                        $item['searchColumns']
                    ),
                    'toolbar'=> [
                        ['content' => ''],
                    ],          
                    //'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'info', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список фильмов',
                        'before'=>'',
                        'after'=>'',
                    ]
                ])?>
            </div>
        </div>                         
    </div>
<?php
	}
?>
<br/>
<?php if($contracts !== null){ ?>
<?=Html::a('Скачать xls', ['/users/export'], ['data-pjax'=>'0','title'=> 'Отправить отчет', 'class'=>'btn btn-info'])?>
<?=Html::a('Скачать xml', ['/users/xml'], ['data-pjax'=>'0', /*'target' => '_blank',*/ 'title'=> 'Отправить отчет', 'class'=>'btn btn-warning'])?>
<a href="#" onclick="$.get('/users/export', {'type':1}, function(data){ } ); alert('Успешно выполнено');" class="btn btn-success" role="button">Перезапысать</a>
<?php } ?>

<?php /*Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])*/ ?>
<?php //Modal::end(); ?>