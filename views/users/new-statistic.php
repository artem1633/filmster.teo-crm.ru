<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\additional\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-default">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Статистика</h3>
    </div>
    <div class="panel-body">
        <?= $this->render('search_statistika', [
            'post' => $post,
        ]);?>
    </div>                          
</div>
<div class="faq-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => new ArrayDataProvider([
                        'allModels' => $items[0]['result'],
                        'sort' => [
                            'attributes' => $items[0]['searchAttributes'],
                        ],
                    ]),
                    'filterModel' => $items[0]['searchModel'],
                    'pjax'=>true,
                    'responsiveWrap' => false,
                    'columns' => array_merge(
                        $items[0]['searchColumns'], 
                        [
                            ['class' => 'yii\grid\ActionColumn']
                        ]
                    ),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                            ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '<ul class="panel-controls">
                                <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>  '.
                            '</div>'
                        ],
                    ],          
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,          
                    'panel' => [
                        'type' => 'info', 
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список клиентов',
                        'before'=>'',
                        'after'=>BulkButtonWidget::widget([
                                    'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                                        ["bulk-delete"] ,
                                        [
                                            "class"=>"btn btn-danger btn-xs",
                                            'role'=>'modal-remote-bulk',
                                            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                            'data-request-method'=>'post',
                                            'data-confirm-title'=>'Подтвердите действие',
                                            'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                                        ]),
                                ]).                        
                                '<div class="clearfix"></div>',
                    ]
                ])?>
        <?php /*GridView::widget([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $result,

            ]),
            'filterModel' => $searchModel,
            'showPageSummary' => true,
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'panel' => ['type' => 'primary', 'heading' => 'Grid Grouping Example'],
            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'company_lic',
                    'filter' => ArrayHelper::map($session['Contract[rightHolders]'],'id','name_ru'),
                    'content'=>function($data){
                        return $data->companyLic->name_ru;
                    },
                    'group' => true,  // enable grouping
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'number',
                    'content' => function($data){
                        return Html::a($data->number,  ['contract/view', 'id' => $data->id], ['data-pjax'=>'0', 'data-toggle'=>'tooltip']);
                    }
                ],
            ],
        ])*/ ?>
            </div>
        </div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
