<?php

use yii\helpers\Html;
use app\models\RightHolders;
use app\models\Contract;
use app\models\Users;
use app\models\Currency;
use app\models\ReportMonetary;
use app\models\Applications;
use app\models\FilmsApplication;
use app\models\Films;
use app\models\Platform;
use app\models\ReportCheck;

$array = explode('-', $month);
$identity = Yii::$app->user->identity;

?>
<br>
<div class="dd" id="nestable">
<?php 
foreach ($users as $user) {
  ?>
    <ol class="dd-list">
      <li class="dd-item" data-id="<?=$id?>">
        <div class="dd-handle"><?= $user->fio ?></div>
        <ol class="dd-list">
            <?php
              $rightHolders = RightHolders::find()->where(['user_id' => $user->id])->all();
              foreach ($rightHolders as $value) {
                  $contracts = Contract::getContractsList($value->id, $month);
            ?>
            <li class="dd-item" data-id="<?=$id+1?>">
            <div class="dd-handle"><?=$value->name_ru?></div>
            <ol class="dd-list">
              <?php if(count($contracts) > 0) {?>
              <li class="dd-item" data-id="<?=$id+2?>">
                <div class="row">
                  <div class="col-md-12">
                    <table class="t-tab" border="1">
                       <?php
                          foreach ($contracts as $contract) {
                            $monthList = [];
                            if($contract->payment_period == 0) $monthList [] = $month;
                            else if($contract->payment_period == 1) {
                                if($array[0] == 'Мар') {
                                  $monthList [] = 'Янв' . '-' . $array[1];
                                  $monthList [] = 'Фев' . '-' . $array[1];
                                  $monthList [] = 'Мар' . '-' . $array[1];
                                }
                                if($array[0] == 'Июн') {
                                  $monthList [] = 'Апр' . '-' . $array[1];
                                  $monthList [] = 'Май' . '-' . $array[1];
                                  $monthList [] = 'Июн' . '-' . $array[1];
                                }
                                if($array[0] == 'Сен') {
                                  $monthList [] = 'Июл' . '-' . $array[1];
                                  $monthList [] = 'Авг' . '-' . $array[1];
                                  $monthList [] = 'Сен' . '-' . $array[1];
                                }
                                if($array[0] == 'Дек') {
                                  $monthList [] = 'Окт' . '-' . $array[1];
                                  $monthList [] = 'Ноя' . '-' . $array[1];
                                  $monthList [] = 'Дек' . '-' . $array[1];
                                }
                            }
                            else if($contract->payment_period == 2) {
                                if($array[0] == 'Июн') {
                                  $monthList [] = 'Янв' . '-' . $array[1];
                                  $monthList [] = 'Фев' . '-' . $array[1];
                                  $monthList [] = 'Мар' . '-' . $array[1];
                                  $monthList [] = 'Апр' . '-' . $array[1];
                                  $monthList [] = 'Май' . '-' . $array[1];
                                  $monthList [] = 'Июн' . '-' . $array[1];
                                }
                                if($array[0] == 'Дек') {
                                  $monthList [] = 'Июл' . '-' . $array[1];
                                  $monthList [] = 'Авг' . '-' . $array[1];
                                  $monthList [] = 'Сен' . '-' . $array[1];
                                  $monthList [] = 'Окт' . '-' . $array[1];
                                  $monthList [] = 'Ноя' . '-' . $array[1];
                                  $monthList [] = 'Дек' . '-' . $array[1];
                                }
                            }
                            else if($contract->payment_period == 3) {
                                  $monthList [] = 'Янв' . '-' . $array[1];
                                  $monthList [] = 'Фев' . '-' . $array[1];
                                  $monthList [] = 'Мар' . '-' . $array[1];
                                  $monthList [] = 'Апр' . '-' . $array[1];
                                  $monthList [] = 'Май' . '-' . $array[1];
                                  $monthList [] = 'Июн' . '-' . $array[1];
                                  $monthList [] = 'Июл' . '-' . $array[1];
                                  $monthList [] = 'Авг' . '-' . $array[1];
                                  $monthList [] = 'Сен' . '-' . $array[1];
                                  $monthList [] = 'Окт' . '-' . $array[1];
                                  $monthList [] = 'Ноя' . '-' . $array[1];
                                  $monthList [] = 'Дек' . '-' . $array[1];
                                }

                            $reports = ReportMonetary::find()->where(['date' => $monthList])->all();
                            $platforms = ''; $reportId = [];
                            foreach ($reports as $report) {
                                $platforms .= $report->platform->name_ru . '; ';
                                $reportId [] = $report->id;
                            }

                            $session = Yii::$app->session;
                            $session['contract_report_id_'.$contract->id] = $reportId;

                            $applicationID = [];
                            $filmsId = [];
                            $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
                            foreach ($applications as $app) {
                                $applicationID [] = $app->id;
                            }

                            $filmsApplication = FilmsApplication::find()->where(['application_id' => $applicationID ])->all();
                            foreach ($filmsApplication as $app) 
                            {
                                $filmsId [] = $app->film_id;
                            }

                            foreach ($filmsId as $film_id) 
                            {
                                $filmsApplication = FilmsApplication::find()->joinWith('application')->where(['films_application.film_id' => $film_id, 'applications.contract_id' => $contract->id ])->one();

                                $platformJson = json_decode($filmsApplication->platforms);
                                $platformNames = '';
                                foreach ($platformJson as $fff) {
                                    $platformNames .= Platform::findOne($fff->id)->name_ru . '; ';
                                }
                            }
                          $reportCheck = ReportCheck::find()->where(['report_id' => $contract->id, 'date' => $month])->one();
                          if($reportCheck == null){
                            $reportCheck = new ReportCheck();
                            $reportCheck->report_id = $contract->id;
                            $reportCheck->date = $month;
                            $reportCheck->save();
                          }
                          $cur = Currency::findOne($contract->payment_currency)->code;
                          //$cur = $contract->CurrencyDescription($contract->payment_currency);
                          if($user->localization_id == null) $user->localization_id = 1;
                        ?>
                      <tr>
                        <th class="t-td" style="width: 20%"><?= Html::a($contract->number, ['contract/view', 'id' => $contract->id]) ?></th>
                        <th class="t-td" style="width: 20%"><?=$platformNames?></th>
                        <th class="t-td" style="width: 20%" id="reportCheck_<?=$reportCheck->id?>"><?=Users::getTotalSummary($contract, $reportId, $user->localization_id) . ' ' . $cur?></th>
                          <?php if($reportCheck->check != 1 && $identity->type == 0) {?>
                        <th class="t-td" style="width: 10%">
                            <input type="number" step="0.01" id="reportCheck<?=$reportCheck->id?>" class="form-control" name="reportCheck" value="<?=$reportCheck->value?>" maxlength="10" aria-invalid="false" onchange="$.get('/report-monetary/add-value', {'id':<?=$reportCheck->id?>, 'attribute': 'value', 'localization_id': <?=$user->localization_id?>, 'contract_id': <?=$contract->id?>, 'value':$('#reportCheck<?=$reportCheck->id?>').val()}, function(data){ document.getElementById('reportCheck_<?=$reportCheck->id?>').innerHTML = data; alert('Успешно изменено!');} );">
                        </th>
                          <?php } /*else { echo $reportCheck->value; }*/ ?>
                        <th class="t-td" style="width: 10%">
                          <?=Html::a('<i class="fa fa-download fa-lg" aria-hidden="true"></i>', ['/users/export-contract?contract_id='.$contract->id.'&month='.$month],
                            ['role'=>'modal-remote','title'=> 'Скачать', 'class'=>'btn-t'])?>
                        </th>
                        <th class="t-td" style="width: 10%">
                          <?php if($reportCheck->check != 1) {?>
                            <a href="#" onclick="$.get('/report-monetary/check-value', {'id':<?=$reportCheck->id?>, 'attribute': 'value', 'localization_id': <?=$user->localization_id?>, 'contract_id': <?=$contract->id?>, 'value':$('#reportCheck<?=$reportCheck->id?>').val()}, function(data){ document.getElementById('reportCheck_<?=$reportCheck->id?>').disabled = true; alert('Успешно выполнено!');} );" class="btn-t" role="button"><i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i></a>
                          <?php } else { echo '<i class="fa fa-check-circle-o fa-lg" aria-hidden="true"></i>'; } ?>
                        </th>
                      </tr>
                       <?php } ?>
                    </table>
                  </div>
                </div>
              </li>
              <?php }?>
            </ol>
            <?php } ?>
          </li>
        </ol>
      </li>
    </ol>

<?php 
  $id+=3;
}
 ?>
</div>
