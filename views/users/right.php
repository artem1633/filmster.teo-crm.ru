<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\web\JsExpression;

?>

<?= Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['/users/create-right-holder', 'user_id' => $model->id], ['role'=>'modal-remote','title'=> 'Добавить', 'style' => 'margin-left:10px;margin-bottom:10px;', 'class'=>'btn btn-info btn-xs'])?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'right-holder-pjax']) ?>
    <div class="row">
<?php
    foreach ($right_holders as $right) {
?>
    <div class="col-md-6 col-sm-12 col-xs-12">
    	<div class="panel panel-success">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                    <?=$right->name_ru?>
                        <span class="pull-right">
                            <?= Html::a('<span class="text-danger glyphicon glyphicon-trash"></span>', ['/users/delete-right-holder', 'id' => $right->id], [
		                        'role'=>'modal-remote','title'=>'Удалить', 
		                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
		                        'data-request-method'=>'post',
		                        'data-toggle'=>'tooltip',
		                        'data-confirm-title'=>'Подтвердите действие',
		                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
		                    ]) ?>
                        </span>
                    </h5>
                </div> 
                <div class="panel-body">
	    			<?= $this->render('_form_right_holder', [
	                    'right' => $right,
	                ]); ?>
	            </div>
	        </div>

    </div>

<?php  } ?>
    
</div>
<?php Pjax::end() ?> 