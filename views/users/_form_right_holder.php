<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

?>
<div class="row">
    <div class="row">  
        <div class="col-md-6 col-xs-6">
            <div class="form-group">
            <label class="control-label"><?=$right->getAttributeLabel('name_ru')?></label>
            <input type="text" id="name_ru_<?=$right->id?>" class="form-control" name="RightHolders[name_ru]" value="<?=$right->name_ru?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'name_ru', 'value':$('#name_ru_<?=$right->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-6 col-xs-6">
            <div class="form-group">
            <label class="control-label"><?=$right->getAttributeLabel('name_eng')?></label>
            <input type="text" id="name_eng_<?=$right->id?>" class="form-control" name="RightHolders[name_eng]" value="<?=$right->name_eng?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'name_eng', 'value':$('#name_eng_<?=$right->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
    </div>

    <div class="row">
         <div class="col-md-6 col-xs-6">
            <div class="form-group">
                <label class="control-label"><?=$right->getAttributeLabel('registry_code')?></label>
                <input type="text" id="registry_code_<?=$right->id?>" class="form-control" name="RightHolders[registry_code]" value="<?=$right->registry_code?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'registry_code', 'value':$('#registry_code_<?=$right->id?>').val()}, function(data){} );" >
               <div class="help-block"></div>
           </div>
        </div>
        <div class="col-md-6 col-xs-6">
            <div class="form-group">
            <label class="control-label"><?=$right->getAttributeLabel('primary_number')?></label>
            <input type="text" id="primary_number_<?=$right->id?>" class="form-control" name="RightHolders[primary_number]" value="<?=$right->primary_number?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'primary_number', 'value':$('#primary_number_<?=$right->id?>').val()}, function(data){} );" >
               <div class="help-block"></div>
           </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xs-6">
            <div class="form-group">
                <label class="control-label"><?=$right->getAttributeLabel('tax_id')?></label>
                <input type="text" id="tax_id<?=$right->id?>" class="form-control" name="RightHolders[tax_id]" value="<?=$right->tax_id?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'tax_id', 'value':$('#tax_id<?=$right->id?>').val()}, function(data){} );" >
                <div class="help-block"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <br>
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Адрес компании</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('street1')?></label>
                            <textarea id="street1_<?=$right->id?>" class="form-control" name="RightHolders[street1]" rows="2" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'street1', 'value':$('#street1_<?=$right->id?>').val()}, function(data){} );" ><?=$right->street1?></textarea>
                            <div class="help-block"></div>    
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('street2')?></label>
                            <textarea id="street2_<?=$right->id?>" class="form-control" name="RightHolders[street2]" rows="2" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'street2', 'value':$('#street2_<?=$right->id?>').val()}, function(data){} );" ><?=$right->street2?></textarea>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('country_id')?></label>
                            <?= Html::activeDropDownList($right, 'country_id', $right->getCountriesList(), 
                                [
                                    'class'=>"form-control",
                                    'onchange' => '
                                        var id = '.$right->id.';
                                        $.post( "/users/set-right-values?id=" + id + "&attribute=country_id&value=" + $(this).val(), function( data ){});
                                    ',
                                ]
                            );?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('city_id')?></label>
                            <input type="text" id="city_id_<?=$right->id?>" class="form-control" name="RightHolders[city_id]" value="<?=$right->city_id?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'city_id', 'value':$('#city_id_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('state')?></label>
                            <input type="text" id="state_<?=$right->id?>" class="form-control" name="RightHolders[state]" value="<?=$right->state?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'state', 'value':$('#state_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('index')?></label>
                            <input type="text" id="index_<?=$right->id?>" class="form-control" name="RightHolders[index]" value="<?=$right->index?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'index', 'value':$('#index_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <br>
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Банк и счета</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_name')?></label>
                            <input type="text" id="bank_name_<?=$right->id?>" class="form-control" name="RightHolders[bank_name]" value="<?=$right->bank_name?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bank_name', 'value':$('#bank_name_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>    
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_index')?></label>
                            <input type="text" id="bank_index_<?=$right->id?>" class="form-control" name="RightHolders[bank_index]" value="<?=$right->bank_index?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bank_index', 'value':$('#bank_index_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_street1')?></label>
                            <textarea id="bank_street1_<?=$right->id?>" class="form-control" name="RightHolders[bank_street1]" rows="2" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bank_street1', 'value':$('#bank_street1_<?=$right->id?>').val()}, function(data){} );" ><?=$right->bank_street1?></textarea>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_street2')?></label>
                            <textarea id="bank_street2_<?=$right->id?>" class="form-control" name="RightHolders[bank_street2]" rows="2" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bank_street2', 'value':$('#bank_street2_<?=$right->id?>').val()}, function(data){} );" ><?=$right->bank_street2?></textarea>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_country_id')?></label>
                            <?= Html::activeDropDownList($right, 'bank_country_id', $right->getCountriesList(), 
                                [
                                    'class'=>"form-control",
                                    'onchange' => '
                                        var id = '.$right->id.';
                                        $.post( "/users/set-right-values?id=" + id + "&attribute=bank_country_id&value=" + $(this).val(), function( data ){});
                                    ',
                                ]
                            );?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-4">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_state')?></label>
                            <input type="text" id="bank_state_<?=$right->id?>" class="form-control" name="RightHolders[bank_state]" value="<?=$right->bank_state?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bank_state', 'value':$('#bank_state_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-4">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bic_swift')?></label>
                            <input type="text" id="bic_swift_<?=$right->id?>" class="form-control" name="RightHolders[bic_swift]" value="<?=$right->bic_swift?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bic_swift', 'value':$('#bic_swift_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_account')?></label>
                            <input type="text" id="bank_account<?=$right->id?>" class="form-control" name="RightHolders[bank_account]" value="<?=$right->bank_account?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'bank_account', 'value':$('#bank_account<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('bank_account_currency')?></label>
                            <?= Html::activeDropDownList($right, 'bank_account_currency', $right->getCurrency(), 
                                [
                                    'class'=>"form-control",
                                    'onchange' => '
                                        var id = '.$right->id.';
                                        $.post( "/users/set-right-values?id=" + id + "&attribute=bank_account_currency&value=" + $(this).val(), function( data ){});
                                    ',
                                ]
                            );?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('iban')?></label>
                            <input type="text" id="iban_<?=$right->id?>" class="form-control" name="RightHolders[iban]" value="<?=$right->iban?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'iban', 'value':$('#iban_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('iban_currency')?></label>
                            <?= Html::activeDropDownList($right, 'iban_currency', $right->getCurrency(), 
                                [
                                    'class'=>"form-control",
                                    'onchange' => '
                                        var id = '.$right->id.';
                                        $.post( "/users/set-right-values?id=" + id + "&attribute=iban_currency&value=" + $(this).val(), function( data ){});
                                    ',
                                ]
                            );?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('correspondent_account')?></label>
                            <input type="number" id="correspondent_account_<?=$right->id?>" class="form-control" name="RightHolders[correspondent_account]" value="<?=$right->correspondent_account?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-right-values', {'id':<?=$right->id?>, 'attribute': 'correspondent_account', 'value':$('#correspondent_account_<?=$right->id?>').val()}, function(data){} );" >
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="form-group">
                            <label class="control-label"><?=$right->getAttributeLabel('correspondent_account_currency')?></label>
                            <?= Html::activeDropDownList($right, 'correspondent_account_currency', $right->getCurrency(), 
                                [
                                    'class'=>"form-control",
                                    'onchange' => '
                                        var id = '.$right->id.';
                                        $.post( "/users/set-right-values?id=" + id + "&attribute=correspondent_account_currency&value=" + $(this).val(), function( data ){});
                                    ',
                                ]
                            );?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    