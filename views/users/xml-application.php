<?php

use app\models\Applications;
?>

<items>
<?php
foreach($contracts as $contract)
{
    $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
	echo "<contract number=\"{$contract->number}\">";
    foreach ($applications as $application) 
    {
        echo "<application number=\"{$application->id}\">";
        foreach ($filmsPrint as $films)
        {
	        if($films['contract_id'] == $contract->id && $films['application_id'] == $application->id)
	        {
	        	$xml = str_replace ("<br>", " ", $films['platformNames']);
				
	        	echo "<film name=\"{$films['filmName']}\">";
	        		echo "<rolyati>" . $films['rolyati'] . "</rolyati>";
	        		echo "<playgrounds>" . $xml . "</playgrounds>";
	        		echo "<est_count>" . $films['estCount'] . "</est_count>";
	        		echo "<est>" . $films['est'] . "</est>";
	        		echo "<vod_count>" . $films['vodCount'] . "</vod_count>";
	        		echo "<vod>" . $films['vod'] . "</vod>";
	        		echo "<svod_count>" . $films['svodCount'] . "</svod_count>";
	        		echo "<svod>" . $films['svod'] . "</svod>";
	        		echo "<pvod_count>" . $films['pvodCount'] . "</pvod_count>";
	        		echo "<pvod>" . $films['pvod'] . "</pvod>";
	        		echo "<avod_count>" . $films['avodCount'] . "</avod_count>";
	        		echo "<avod>" . $films['avod'] . "</avod>";
	        		echo "<sale>" . $films['sale'] . "</sale>";
	        		echo "<summary>" . $films['summary'] . "</summary>";
	        		echo "<rolyati_sum>" . $films['rolyati_x_summa'] . "</rolyati_sum>";
	        	echo "</film>";
	        }
	    }
	    echo "</application>";
    }
    echo "</contract>";
/*	echo "<setting>";
	echo $value;
	echo "</setting>";*/
}
?>
</items>