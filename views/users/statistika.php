<?php

use app\models\Applications;
use app\models\FilmsApplication;
use app\models\ReportMonetary;
use app\models\ReportValues;
use app\models\Films;
use app\models\PreferenceBooks;

/*echo "<pre>";
print_r($post);
echo "</pre>";
die;*/
$reportMonetary = ReportMonetary::find()->where(['date' => $months])->all();
$reportId = [];
foreach ($reportMonetary as $report) {
$reportId [] = $report->id;
}

?>

<div class="panel panel-default" id="statistic-page">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Статистика</h3>
    </div>
    <div class="panel-body">
    	<?= $this->render('search_statistika', [
		    'post' => $post,
		]);?>

		<br>
		<br>
		<div class="row">
		    <div class="col-md-12">
		         <div class="footer-widget-panel panel-group" id="accordion1">
		         	<?php
			         	if($post['check'] == 1) echo $this->render('application', [
						    'contracts' => $contracts,
						    'months' => $months,
						    'reportId' => $reportId,
						    'items' => $items,
						]);
			         	else echo $this->render('films', [
						    'items' => $items,
						    'contracts' => $contracts,
						]);
					?>
		        </div>
		    </div>
		</div>
    </div>                          
</div>