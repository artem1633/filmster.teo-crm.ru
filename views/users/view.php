<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Films */
$first = ''; $second = ''; $third = '';
if(Yii::$app->session['users-view'] === null || Yii::$app->session['users-view'] == '1') $first = 'active';
if(Yii::$app->session['users-view'] == '2') $second = 'active';
if(Yii::$app->session['users-view'] == '3') $third = 'active';

CrudAsset::register($this);
$this->title = $model->fio;
?>
<h1>
    <span>
        <a class="btn btn-info" title="Список фильмов" href="<?=Url::toRoute(['/users/index'])?>"><i style="font-size: 20px;" class="fa fa-arrow-left"></i></a>
    </span>
    <?=$model->fio?>
</h1>
<div class="films-view" style="margin-top: 10px;">
    <div class="panel panel-default tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="<?=$first?>"><a href="#tab1" data-toggle="tab" aria-expanded="false" onclick="$.get('/users/set-tab', {'tab': 'users-view', 'value':'1'}, function(data){} );">Основное</a></li>
            <li class="<?=$second?>"><a href="#tab2" data-toggle="tab" aria-expanded="false" onclick="$.get('/users/set-tab', {'tab': 'users-view', 'value':'2'}, function(data){} );">Правообладатели</a></li>
            <li class="<?=$third?>"><a href="#tab3" data-toggle="tab" aria-expanded="false" onclick="$.get('/users/set-tab', {'tab': 'users-view', 'value':'3'}, function(data){} );">Статистика</a></li>
        </ul>
        <div class="panel-body tab-content">
            <div class="tab-pane <?=$first?>" id="tab1">
                <?= $this->render('main', [
                    'model' => $model,
                ]);?>
            </div>
            <div class="tab-pane <?=$second?>" id="tab2">
                <?= $this->render('right', [
                    'model' => $model,
                    'right_holders' => $right_holders,
                ]);?>
            </div>
            <div class="tab-pane <?=$third?>" id="tab3">
                <?= $this->render('statistik', [
                    'model' => $model,
                    'post' => $post,
                    'contracts' => $contracts,
                    'months' => $months,
                ]);?>
            </div>              
        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>