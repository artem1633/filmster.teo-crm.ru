<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
/* @var $form yii\widgets\ActiveForm */
$individual = 0;
$multiple = 0;
$_csrf = \Yii::$app->request->getCsrfToken();
if (!file_exists('avatars/'.$model->foto) || $model->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->foto;
}

?>

<div class="row">

        <div class="col-md-3">
            <div id="user_logo_file<?=$model->id?>">
                <img style="width:250px; height:250px;" class="img-circle" src="<?=$path?>">
            </div>
            <br>   
            <div id="user_logo<?=$model->id?>"></div>
            
            <button style="display:block;width:120px; height:30px;" name="button23<?=$model->id?>[]" id="button23<?=$model->id?>" onclick="document.getElementById('fileInput<?=$model->id?>').click()">Логотип</button>
            <input type="file" name="fileInput<?=$model->id?>[]" style="display:none" id="fileInput<?=$model->id?>" class="poster23_image<?=$model->id?>" accept="image/*" />

        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6 col-xs-6">
					<div class="form-group">
			            <label class="control-label"><?=$model->getAttributeLabel('login')?></label>
			            <input type="text" id="user_login" class="form-control" name="Users[login]" value="<?=$model->login?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'login', 'value':$('#user_login').val()}, function(data){} );" >
			            <div class="help-block"></div>
		            </div>
				</div>
                <div class="col-md-6 col-xs-6">
					<div class="form-group">
			            <label class="control-label"><?=$model->getAttributeLabel('new_password')?></label>
			            <input type="text" id="user_new_password" class="form-control" name="Users[new_password]" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'user_new_password', 'value':$('#new_password').val()}, function(data){} );" >
			            <div class="help-block"></div>
		            </div>               
				</div>
            </div>

            <div class="row">
                <div class="col-md-4 col-xs-4">
                    <div class="form-group">
			            <label class="control-label"><?=$model->getAttributeLabel('fio')?></label>
			            <input type="text" id="user_fio" class="form-control" name="Users[fio]" value="<?=$model->fio?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'fio', 'value':$('#user_fio').val()}, function(data){} );" >
			            <div class="help-block"></div>
			        </div>
			    </div>
                <div class="col-md-4 col-xs-4">          
					<div class="form-group">
				        <label class="control-label"><?=$model->getAttributeLabel('mail')?></label>
				        <input type="text" id="user_mail" class="form-control" name="Users[mail]" value="<?=$model->mail?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'mail', 'value':$('#user_mail').val()}, function(data){} );" >
				        <div class="help-block"></div>
				    </div>
				</div>
                <div class="col-md-4 col-xs-4">
                    <label class="control-label"><?=$model->getAttributeLabel('localization_id')?></label>
                    <?= Html::activeDropDownList($model, 'localization_id', $model->getLocType(), 
                        [
                            'class'=>"form-control",
                            'onchange' => '
                                var id = '.$model->id.';
                                $.post( "/users/set-values?id="+id+"&attribute=localization_id&value="+$(this).val(), function( data ){});
                            ',
                        ]
                    );?>  
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-xs-4">         
		      		<div class="form-group">
					    <label class="control-label"><?=$model->getAttributeLabel('telephone')?></label>
					    <input type="text" id="user_telephone" class="form-control" name="Users[telephone]" value="<?=$model->telephone?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'telephone', 'value':$('#user_telephone').val()}, function(data){} );" >
					    <div class="help-block"></div>
					</div>
				</div>
                <div class="col-md-4 col-xs-4">
                    <div class="form-group">
			            <label class="control-label"><?=$model->getAttributeLabel('site')?></label>
			            <input type="text" id="user_site" class="form-control" name="Users[site]" value="<?=$model->site?>" maxlength="255" aria-invalid="false" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'site', 'value':$('#user_site').val()}, function(data){} );" >
			            <div class="help-block"></div>
		            </div>
		        </div>
                <div class="col-md-4 col-xs-4">
					<label class="control-label"><?=$model->getAttributeLabel('type')?></label>
		            <?= Html::activeDropDownList($model, 'type', $model->getType(), 
		                [
		                    'class'=>"form-control",
		                    'onchange' => '
		                        var id = '.$model->id.';
		                        $.post( "/users/set-values?id="+id+"&attribute=type&value="+$(this).val(), function( data ){});
		                    ',
		                ]
		            );?>  
				</div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
						<label class="control-label"><?=$model->getAttributeLabel('description')?></label>
						<textarea id="users-description" class="form-control" name="Users[description]" onchange="$.get('/users/set-values', {'id':<?=$model->id?>, 'attribute': 'description', 'value':$('#users-description').val()}, function(data){} );" rows="2"><?=$model->description?></textarea>
						<div class="help-block"></div>
					</div>                
				</div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-6">
					<div class="form-group">
	                    <input type="hidden" name="Users[active]" value="0">
	                    <label>
	                        <input type="checkbox" id="users_active" name="Users[active]" value="1"  <?= $model->active == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('active')?>
	                    </label>
	                	<div class="help-block"></div>
	                </div>               
				</div>
            </div>
        </div>

</div>


<?php 
$this->registerJs(<<<JS

	var fileCollection = new Array();

    $('#users_active').on('change', function() 
    {
        var checked = $('#users_active').is(':checked');
        if(checked == 0) {
            $.get('/users/set-values', {'id':$model->id, 'attribute': 'active', 'value': 0}, function(data){} );
        }
        else {
            $.get('/users/set-values', {'id':$model->id, 'attribute': 'active', 'value': 1}, function(data){} );
        }
    });

        /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#user_logo$model->id").click(function(){
            $("#fileInput$model->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput$model->id\']').on('change', fileUpload);
    });

    function fileUpload(event){
        //notify user about the file upload status
        $("#user_logo$model->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#user_logo$model->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$model->id}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/users/set-logo', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#user_logo$model->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#user_logo$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }

    $(document).on('change', '.poster23_image$model->id', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:200px; height:300px;" src="'+e.target.result+'"> ';
                $('#user_logo_file$model->id').html('');
                $('#user_logo_file$model->id').append(template);
            };
        });
    });

JS
);
?>