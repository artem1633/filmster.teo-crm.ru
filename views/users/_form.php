<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\additional\Permissions;

if (!file_exists('avatars/'.$model->foto) || $model->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/nouser.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->foto;
}

?>

<div class="users-form">

    <?php $form = ActiveForm::begin([ 'options' => ['method' => 'post', 'enctype' => 'multipart/form-data']]); ?>
    <div class="row">

        <div class="col-md-3">
            <div class="col-md-12">
                <center>
                <?=Html::img($path, [
                    'style' => 'width:180px; height:180px;',
                    'class' => 'img-circle',
                ])?>
                </center>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'file')->fileInput(); ?>
            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6 col-xs-6">
                    <?= $model->isNewRecord ? $form->field($model, 'password')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-xs-4">
                    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 col-xs-4">
                    <?= $form->field($model, 'mail')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 col-xs-4">
                    <?= $form->field($model, 'localization_id')->dropDownList($model->getLocType(), [
                        'prompt' => 'Выберите тип' 
                    ]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-xs-4">
                    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 col-xs-4">
                    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4 col-xs-4">
                    <?= $form->field($model, 'type')->dropDownList($model->getType(), [
                        'prompt' => 'Выберите тип' 
                    ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->textArea(['rows' => 2]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <?= $form->field($model, 'active')->checkbox([]) ?>
                </div>
            </div>
        </div>

    	<?php if (!Yii::$app->request->isAjax){ ?>
    	  	<div class="form-group">
    	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    	    </div>
    	<?php } ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>
