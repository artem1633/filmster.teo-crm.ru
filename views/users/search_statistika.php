<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\Users;

?>
<?php 
$layout = <<< HTML
    <span class="input-group-addon label-success">С</span>
    {input1}
    <span class="input-group-addon label-success">По</span>
    {input2}
    <span class="input-group-addon kv-date-remove" >
        <i class="glyphicon glyphicon-remove"></i>
    </span>
HTML;
 ?>

<div class="groups-form">

    <?php $form = ActiveForm::begin(['options' => ['method' => 'post', 'autocomplete'=>"off" ]]); ?>
        <div class="row">
            <div class="col-md-3">
                <label>Дата</label>
                <?= DatePicker::widget([
                    'type' => DatePicker::TYPE_RANGE,
                    'name' => 'date_time_from',
                    'value' => $post['date_time_from'],
                    'name2' => 'date_time_to',
                    'value2' => $post['date_time_to'],
                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                    'layout' => $layout,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'startView'=>'year',
                        'minViewMode'=>'months',
                        'format' => 'M-yyyy',
                        'todayHighlight' => true,
                    ]
                ])?>
            </div>
            <div class="col-md-3" <?= Yii::$app->user->identity->type == 1 || Yii::$app->user->identity->type == 2 ? 'style="display: none;"' : ''?> >
                <label>Клиенты/Компания</label>
                <?= kartik\select2\Select2::widget([
                    'name' => 'users',
                    'data' => Users::getAllUsersList(),
                    'value' => $post['users'],
                    'size' => 'sm',
                    'options' => [ 
                        'placeholder' => 'Выберите ...',
                        'onchange'=>'
                            $.post( "/users/get-contracts?id='.'"+$(this).val(), function( data ){
                                $( "#contracts" ).html( data);
                            });
                            ' 
                    ],
                    'pluginOptions' => [
                        /*'multiple' => true,*/
                        'allowClear' => true,
                    ],
                ])
                ?>
            </div>
            <div class="col-md-3">
                <label>Договоры</label>
                <?= kartik\select2\Select2::widget([
                    'name' => 'contracts',
                    'id' => 'contracts',
                    'data' => Users::getContractsList($post['users']),
                    'value' => $post['contracts'],
                    'size' => 'sm',
                    'options' => [ 'placeholder' => 'Выберите ...'],
                    'pluginOptions' => [
                        'multiple' => true,
                        'allowClear' => true,
                    ],
                ])
                ?>
            </div>
            <!-- <div class="col-md-2">
                <label style="margin-top: 25px;">
                    <input type="checkbox" name="check" value="1" <?php // $post['check'] == 1 ? 'checked=""' : '' ?> > 
                    Разбить на приложения
                </label>
            </div> -->
            <div class="col-md-1">
                <label>.</label>
                <div class="form-group">
                    <?= Html::submitButton('Показать', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>

	  	
    <?php ActiveForm::end(); ?>
    
</div>
