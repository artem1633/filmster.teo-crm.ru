<?php

use yii\helpers\Html;
use app\models\Applications;
use app\models\FilmsApplication;
use app\models\ReportMonetary;
use app\models\ReportValues;
use app\models\Films;
use app\models\PreferenceBooks;
use app\models\Platform;
use app\models\CurrencySetting;
use app\models\Currency;

$filmsPrint = [];
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
         	<?php
         		$i = 0;
         		foreach ($contracts as $contract) 
         		{
         			$i++;
         			$applications = Applications::find()->where(['contract_id' => $contract->id])->all();
         			$filmsId = [];
                    $platformId = [];
                    $applicationsId = [];
                    foreach ($applications as $application) 
                    {
                        $applicationsId [] = $application->id;
                    	$filmsApplication = FilmsApplication::find()->where(['application_id' => $application->id ])->all();
                    	foreach ($filmsApplication as $value) 
                    	{
                    		$filmsId [] = $value->film_id;
                    	}
                    }
                    $filmsId = array_unique($filmsId);
                    $filmName = "";
                    $sale = 0;
                    $est = 0;
                    $vod = 0;
                    $svod = 0;
                    $pvod = 0;
                    $avod = 0;
                    $data = "";
                    $backgroundColor = "";
                    foreach ($filmsId as $film_id) {
                    	$filmName .= "'" . Films::findOne($film_id)->name . "', ";
	                    $sale_count = (float)ReportValues::find()
	                    	->where(['report_monetary_id' => $reportId])
	                    	->andWhere(['film_id' => $film_id])
	                    	->sum('sale_count');
	                    $est += (float)ReportValues::find()
	                    	->where(['report_monetary_id' => $reportId])
	                    	->andWhere(['film_id' => $film_id])
	                    	->sum('est');
	                    $vod += (float)ReportValues::find()
	                    	->where(['report_monetary_id' => $reportId])
	                    	->andWhere(['film_id' => $film_id])
	                    	->sum('vod');
                        $svod += (float)ReportValues::find()
                            ->where(['report_monetary_id' => $reportId])
                            ->andWhere(['film_id' => $film_id])
                            ->sum('svod');
                        $pvod += (float)ReportValues::find()
                            ->where(['report_monetary_id' => $reportId])
                            ->andWhere(['film_id' => $film_id])
                            ->sum('pvod');
                        $avod += (float)ReportValues::find()
                            ->where(['report_monetary_id' => $reportId])
                            ->andWhere(['film_id' => $film_id])
                            ->sum('avod');
	                    $sale += $sale_count;
	                    $data .= $sale_count . ", ";
	                    $backgroundColor .= PreferenceBooks::getRGBA();

                        $filmsApplicationsPlatform = FilmsApplication::find()->where(['film_id' => $film_id, 'application_id' => $applicationsId ])->all();
                        foreach ($filmsApplicationsPlatform as $value) {
                            foreach (json_decode( $value->platforms ) as $val) {
                                $platformId [] = $val->id;
                            }
                        }

                        $platformId = array_unique($platformId);
                    }
         	?>
            <div class="panel panel-danger">
                <div class="panel-heading ui-draggable-handle">
                    <h5>
                        <a class="<?=$i == 1 ? '' : 'collapsed' ?>" data-toggle="collapse" data-parent="#accordion1" href="#discussionId<?=$contract->id?>">
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-down"></i>                            
                            </span> 
                            <b>Договор № <?=$contract->number?></b>
                        </a>
                    </h5>
                </div> 
                <div id="discussionId<?=$contract->id?>" class="collapse <?=$i == 1 ? 'in' : '' ?> ">
                    <div class="panel-body">
                    	<div class="col-md-6">
                    		<h1><?=$sale?></h1>
                    		<span>Продажи</span>
                            <h1><?=$est?></h1>
                            <span>EST</span>
                            <h1><?=$vod?></h1>
                            <span>VOD</span>
                            <h1><?=$svod?></h1>
                            <span>SVOD</span>
                            <h1><?=$pvod?></h1>
                            <span>PVOD</span>
                            <h1><?=$avod?></h1>
                            <span>AVOD</span>
                    		<h1><?=$est + $vod?></h1>
                    		<span>Всего</span>
                    	</div>
                    	<div class="col-md-6">
	                    	<canvas id="myChart<?=$contract->id?>" width="300" height="300"></canvas>
	                    	<script>
								var ctx = document.getElementById("myChart<?=$contract->id?>");
								var myChart = new Chart(ctx, {
								 	type: 'pie',
								  	data: {
								    labels: [<?=$filmName?>],
								    datasets: [{
								    	label: '# of Tomatoes',
								      	data: [<?=$data?>],
								      	backgroundColor: [
								        	<?=$backgroundColor?>
								      	],
								      borderWidth: 1
								    }]
								  },
								options: {
								   	cutoutPercentage: 40,
								    responsive: false,
								    legend: {
										display: true,
								      	position: "bottom",
								      	labels: {
								       		fontColor: "#333",
								       		fontSize: 10
								      	}
								    }
								}
								});
	                    	</script>
                    	</div>
                    </div>
                    <div class="col-md-12">
                    <?php
                    	$applications = Applications::find()->where(['contract_id' => $contract->id])->all();
                    	foreach ($applications as $application) 
                    	{
                    ?>
                    <div class="panel panel-success">
                        <div class="panel-heading ui-draggable-handle">
                            <h3 class="panel-title">Приложение # <?=$application->id?></h3>
                        </div>
                        <div class="panel-body panel-body-table">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-actions" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Название фильма / Год выпуска</th>
                                            <th>Роляти</th>
                                            <th>Площадка</th>
                                            <th>Кол-во</th>
                                            <th>EST</th>
                                            <th>Кол-во</th>
                                            <th>VOD</th>
                                            <th>Кол-во</th>
                                            <th>SVOD</th>
                                            <th>Кол-во</th>
                                            <th>PVOD</th>
                                            <th>Кол-во</th>
                                            <th>AVOD</th>
                                            <th>Всего кол-во продаж</th>
                                            <th>Всего сумма продажи</th>
                                            <th>Сумма роляти</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                            
                    <?php
                            //$platforms = Platform::find()->where(['id' => $platformId])->all();
                    		$filmsApplication = FilmsApplication::find()->where(['application_id' => $application->id ])->all();
                    		foreach ($filmsApplication as $value) 
                    		{
                                $reportValues = ReportValues::find()
                                    ->where(['report_monetary_id' => $reportId])
                                    ->andWhere(['film_id' => $value->film_id])
                                    ->all();

                                $satr = "";
                                $index = 0;
                                $platformCount = 0;  
                                $platformJson = json_decode($value->platforms);
                                $platforms = [];
                                $platformNames = '';
                                foreach ($platformJson as $fff) {
                                    $platforms [] = $fff->id;
                                    $platformNames .= Platform::findOne($fff->id)->name_ru . '<br>';
                                }
                                
                                $allTerritories = [];
                                foreach (json_decode($value->territory) as $territory) {
                                    $allTerritories [] = $territory->id;
                                }
                                $proverka = 0;
                                foreach ($reportValues as $reportValue) 
                                {
                                    if(in_array($reportValue->territory, $allTerritories)) $proverka = 1;
                                }
                                if($proverka === 0) continue;

                                foreach ($platforms as $platform) 
                                {
                                    $platformName = Platform::findOne($platform)->name_ru;
                                    $index++; 
                                    $est = 0; $estCount = 0;
                                    $vod = 0; $vodCount = 0;
                                    $svod = 0; $svodCount = 0;
                                    $pvod = 0; $pvodCount = 0;
                                    $avod = 0; $avodCount = 0;
                                    $summary = 0;
                                    foreach ($reportValues as $reportValue) 
                                    {
                                        if($reportValue->reportMonetary->platform_id == $platform) 
                                        {
                                            $sum = 0;
                                            if($reportValue->est !== null) { $est += $reportValue->est; $estCount += $reportValue->sale_count; $sum += $reportValue->est/* * $reportValue->sale_count*/; }
                                            if($reportValue->vod !== null) { $vod += $reportValue->vod; $vodCount += $reportValue->sale_count; $sum += $reportValue->vod/* * $reportValue->sale_count*/; }
                                            if($reportValue->svod !== null) { $svod += $reportValue->svod; $svodCount += $reportValue->sale_count; $sum += $reportValue->svod/* * $reportValue->sale_count*/; }
                                            if($reportValue->pvod !== null) { $pvod += $reportValue->pvod; $pvodCount += $reportValue->sale_count; $sum += $reportValue->pvod /** $reportValue->sale_count*/; }
                                            if($reportValue->avod !== null) { $avod += $reportValue->avod; $avodCount += $reportValue->sale_count; $sum += $reportValue->avod/* * $reportValue->sale_count*/; }

                                            $currencyValue = 0; $kurs = 0;
                                            $currencySetting = CurrencySetting::find()
                                                ->where(['currency_from_id' => $reportValue->currency, 'currency_to_id' => $contract->payment_currency])
                                                ->one();
                                            if($currencySetting != null) 
                                            {
                                                $currencyValue = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                                                $kurs = $currencySetting->currency_from_value;
                                            }
                                            else{
                                                $currencySetting = CurrencySetting::find()
                                                    ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $reportValue->currency])
                                                    ->one();
                                                if($currencySetting != null)
                                                {
                                                    $currencyValue = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                                                    $kurs = $currencySetting->currency_to_value;
                                                }
                                            }
                                            if($currencySetting != null) $summary += $currencyValue * $sum;
                                            else $summary += $sum;
                                        }
                                    }
                                    $platformCount++;
                                    $sum = $est + $vod  + $svod  + $pvod  + $avod;
                                    $sale = $estCount + $vodCount + $svodCount + $pvodCount  + $avodCount;
                                    $currency = Currency::findOne($contract->payment_currency)->code;
                                    $rolyati_x_summa = $summary*$value->rolyati_x*0.01;
                                    $summary = round($summary, 2);
                                    $rolyati_x_summa = round($rolyati_x_summa, 2);
                                    $rolyati = $rolyati_x_summa;
                                    
                                    if($value->application->uniform_condition === 1){
                                        $min_warranty = $value->application->min_warranty;
                                        $tex_adap = $value->application->tex_adap;
                                        $maintenance = $value->application->maintenance;
                                    }
                                    else{
                                        $min_warranty = $value->min_warranty;
                                        $tex_adap = $value->tex_adap;
                                        $maintenance = $value->maintenance;
                                    }

                                    if($min_warranty === null) $min_warranty = 0;
                                    if($tex_adap === null) $tex_adap = 0;
                                    if($maintenance === null) $maintenance = 0;

                                    if($min_warranty - $rolyati > 0) {
                                        $mgs = $min_warranty - $rolyati;
                                        $rolyati = 0;
                                    }
                                    else {
                                        $mgs = 0;
                                        $rolyati = $rolyati - $min_warranty;
                                    }

                                    if($mgs == 0 && $rolyati > 0) {
                                        if($tex_adap - $rolyati > 0) {
                                            $tas = $tex_adap - $rolyati;
                                            $rolyati = 0;
                                        }
                                        else {
                                            $tas = 0;
                                            $rolyati = $rolyati - $tex_adap;
                                        }
                                    }

                                    if($tas == 0 && $rolyati > 0) {
                                        if($maintenance - $rolyati > 0) {
                                            $mfs = $maintenance - $rolyati;
                                            $rolyati = 0;
                                        }
                                        else {
                                            $mfs = 0;
                                            $rolyati = $rolyati - $maintenance;
                                        }
                                    }

                                    $filmsPrint [] = [
                                        'contract_id' => $contract->id,
                                        'application_id' => $application->id,
                                        'filmsApplication_id' => $value->id,
                                        'filmName' => $value->film->name . ' (' .$value->film->year_issue .')',
                                        'rolyati' => $value->rolyati_x !== null ? ($value->rolyati_x . '/' . $value->rolyati_y ): '',
                                        'platformNames' => $platformName,
                                        'check' => $value->application->uniform_condition,
                                        'mgt' => $min_warranty,
                                        'mgs' => $mgs == null ? $min_warranty : $mgs,
                                        'tat' => $tex_adap,
                                        'tas' => $tas == null ? $tex_adap : $tas,
                                        'mft' => $maintenance,
                                        'mfs' => $mfs == null ? $maintenance : $mfs,
                                        'estCount' => $estCount,
                                        'est' => $est,
                                        'vodCount' => $vodCount,
                                        'vod' => $vod,
                                        'svodCount' => $svodCount,
                                        'svod' => $svod,
                                        'pvodCount' => $pvodCount,
                                        'pvod' => $pvod,
                                        'avodCount' => $avodCount,
                                        'avod' => $avod,
                                        'sale' => $sale,
                                        'summary' => $summary,
                                        'old_rolyati_x_summa' => $rolyati_x_summa,
                                        'rolyati_x_summa' => $rolyati,
                                    ];
                                    $satr .= 
                                    "<tr>
                                        ".($index == 1 ? "{rowSpan}" : '')."
                                        ".($index == 1 ? "{rolyati}" : '')."
                                        <td>{$platformName}</td>
                                        <td>{$estCount}</td>
                                        <td>{$est}</td>
                                        <td>{$vodCount}</td>
                                        <td>{$vod}</td>
                                        <td>{$svodCount}</td>
                                        <td>{$svod}</td>
                                        <td>{$pvodCount}</td>
                                        <td>{$pvod}</td>
                                        <td>{$avodCount}</td>
                                        <td>{$avod}</td>
                                        <td>{$sale}</td>
                                        <td>{$summary}</td>
                                        <td>{$rolyati_x_summa}</td>
                                    </tr>";
                                }
                                $satr = str_replace ("{rowSpan}", "<td rowspan=".$platformCount.">{$value->film->name}({$value->film->year_issue})</td>", $satr);
                                $satr = str_replace ("{rolyati}", "<td rowspan=".$platformCount.">".($value->rolyati_x !== null ? ($value->rolyati_x . '/' . $value->rolyati_y ): '')."</td>", $satr);
                                
                                echo $satr;
                    		}
                   	?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    <?php
                    	}
                    	?>
                    </div>
                </div>                         
            </div>
            <?php
         		}
                $session = Yii::$app->session;
                $session['filmsPrint'] = $filmsPrint;
         	?>
<?php if($contracts !== null){ ?>
<?=Html::a('Отправить отчет', ['/users/export-application'], ['data-pjax'=>'0','title'=> 'Отправить отчет', 'class'=>'btn btn-info'])?>
<?=Html::a('Скачать xml', ['/users/xml-application'], ['data-pjax'=>'0', /*'target' => '_blank',*/ 'title'=> 'Отправить отчет', 'class'=>'btn btn-warning'])?>
<a href="#" onclick="$.get('/users/export-application', {'type':1}, function(data){ } ); alert('Успешно выполнено');" class="btn btn-success" role="button">Перезапысать</a>
<?php } ?>