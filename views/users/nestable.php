<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
$this->title = 'Отчеты';

$ruMonths = [ "01" => 'Янв',
"02" => 'Фев',
"03" => 'Мар',
"04" => 'Апр',
"05" => 'Май',
"06" => 'Июн',
"07" => 'Июл',
"08" => 'Авг',
"09" => 'Сен',
"10" => 'Окт',
"11" => 'Ноя',
"12" => 'Дек',
];

$m = date('m');

$ruMonth = str_replace($m, $ruMonths[$m], $m);


?>

<h1>Отчеты</h1>

<div class="row">
    <div class="panel panel-info">
        <div class="panel-heading ui-draggable-handle">
            <h6 >Поиск</h6>
        </div>
        <div class="panel-body">
        <?php $form = ActiveForm::begin(['options' => ['method' => 'post', 'autocomplete'=>"off" ]]); ?>
            <div class="col-md-4">
                <label>Месяц</label>
                <?= kartik\date\DatePicker::widget([
                    'name' => 'month',
                    'value' => $post['month'],
                    'options' => [ 'placeholder' => 'Выберите'],
                    'size' => kartik\select2\Select2::SMALL,
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'startView'=>'year',
                        'minViewMode'=>'months',
                        'format' => 'M-yyyy',
                        'todayHighlight' => true,
                    ]
                ]);?>
            </div>
            <div class="col-md-3">
                <label>.</label>
                <div class="form-group">
                    <?= Html::submitButton('Показать', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Скачать отчет за месяц', ['users/export-all-contract', 'month' => $ruMonth.date('-Y')], ['class' => 'btn btn-info']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php if($post['month'] !== null) {?>
    <div class="row">
        <div class="col-md-12">
            <div class="footer-widget-panel panel-group" id="accordion1">
                <?= $this->render('nestaplication', [
                        'reportId' => $reportId,
                        'users' => $users,
                        'platforms' => $platforms,
                        //'reportId' => $reportId,
                        'month' => $post['month'],
                    ]);
                ?>
            </div>
        </div>
    </div>
<?php } ?>