<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FilmsType */
?>
<div class="films-type-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
        ],
    ]) ?>

</div>
