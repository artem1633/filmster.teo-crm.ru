<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FilmsType */
?>
<div class="films-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
