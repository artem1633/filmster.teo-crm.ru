<?php 
use yii\helpers\Url;
?>

<?= yii2fullcalendar\yii2fullcalendar::widget([
      'options' => [
        'lang' => 'ru',
      ],
      'events' => $events,
    ]);
?>