<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\PreferenceBooks;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $application \app\models\Applications */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="contract-form" style="padding: 10px;">

    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
        <div class="col-md-3 col-xs-3">
            <div class="col-md-12">
                <label>Роляти</label>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'rolyati_x')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9',
                    'options' => [
                        'class' =>'form-control',
                        'style' => 'width:95px; margin-left:-10px;',
                        'onchange'=>"
                            var y = 100 - $(this).val();
                            $('#filmsapplication-rolyati_y').val(y);
                        ",
                    ],
                    'clientOptions' => ['repeat' => 2, 'greedy' => false]
                ])->label(false); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'rolyati_y')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9',
                    'options' => [
                        'class' =>'form-control',
                        'style' => 'width:95px; margin-left:-5px;',
                        'onchange'=>"
                            var x = 100 - $(this).val();
                            $('#filmsapplication-rolyati_x').val(x);
                        ",
                    ],
                    'clientOptions' => ['repeat' => 2, 'greedy' => false]
                ])->label(false); ?>
            </div>
        </div>
        <div class="col-md-3 col-xs-3">
            <?= $form->field($model, 'est_open')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-3 col-xs-3">
            <?= $form->field($model, 'vod_open')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-3 col-xs-3">
            <?= $form->field($model, 'close_date')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'allTerritories')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCountryList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'allPlatforms')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getPlatformList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
            ])?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'price')->dropDownList( PreferenceBooks::priceList(), ['prompt' => 'Выберите']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'application_id')->widget(kartik\select2\Select2::class, [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Applications::find()->where(['contract_id' => $application->contract_id])->all(), 'id', 'number'),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])?>
        </div>
    </div>

    <?php //if($application->uniform_condition == 0) { ?>
    <div class="row">
        <div><hr></div>
        <div class="col-md-4 col-xs-4">
            <div class="col-md-12">
                <label>Мин.гарантия</label>
            </div>
            <div class="col-md-7">
                <?= $form->field($model, 'min_warranty')->textInput(['type' => 'number', 'style' => 'margin-left:-10px;'])->label(false) ?>                
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'min_warranty_currency')->dropDownList( $model->getCurrency(), [ 'style' => 'margin-left:-15px', 'prompt' => 'Выберите'])->label(false) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-4">
            <div class="col-md-12">
                <label>Тех.адаптация</label>
            </div>
            <div class="col-md-7">
                <?= $form->field($model, 'tex_adap')->textInput(['type' => 'number', 'style' => 'margin-left:-10px;'])->label(false) ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'text_adap_currency')->dropDownList( $model->getCurrency(), [ 'style' => 'margin-left:-15px', 'prompt' => 'Выберите'])->label(false) ?>
            </div>
        </div>
        <div class="col-md-4 col-xs-4">
            <div class="col-md-12">
                <label>Maintenance fee</label>
            </div>
            <div class="col-md-7">
                <?= $form->field($model, 'maintenance')->textInput(['type' => 'number', 'style' => 'margin-left:-10px;'])->label(false) ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'maintenance_currency')->dropDownList( $model->getCurrency(), [ 'style' => 'margin-left:-15px', 'prompt' => 'Выберите'])->label(false) ?>
            </div>
        </div>
    </div>
    <?php //} ?>

    <?php //if($application->settings == 1) { ?>
    <div class="row">
        <div><hr></div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'svod')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'pvod')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'avod')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'tv')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <!-- <div class="col-md-4 col-xs-4" style="margin-top: 22px;">
            <?php // $form->field($model, 'flat_rate_currency')->dropDownList( $model->getCurrency(), [ 'prompt' => 'Выберите'])->label(false) ?>
        </div> -->
    </div>
    <?php //} ?>

    <?php ActiveForm::end(); ?>
    
</div>
