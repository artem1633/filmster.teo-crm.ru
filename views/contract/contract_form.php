<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
?>

<div class="panel panel-success panel-hidden-controls">
    <div class="panel-heading ui-draggable-handle">
    <span>
        <a class="btn btn-info pull-left" title="Список Договопы" href="<?=Url::toRoute(['/contract/index'])?>"><i style="font-size: 20px;" class="fa fa-arrow-left"></i></a>
    </span>
        <h3 class="panel-title">Основные параметры договора</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2 col-xs-6">
                <div class="form-group field-contract-number">
                <label class="control-label" for="contract-number"><?=$model->getAttributeLabel('number')?></label>
                <input type="text" id="contract-number" class="form-control" name="Contract[number]" value="<?=$model->number?>" maxlength="255" aria-invalid="false" onchange="$.get('/contract/set-values', {'id':<?=$model->id?>, 'attribute': 'number', 'value':$('#contract-number').val()}, function(data){} );">
                <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="form-group field-contract-number">
                <label class="control-label" for="contract-date_cr"><?=$model->getAttributeLabel('date_cr')?></label>
                <?= kartik\date\DatePicker::widget([
                    'name' => 'date_cr_contract',
                    'id' => 'date_cr_contract',
                    'value' => $model->date_cr !== null ? date('d.m.Y', strtotime($model->date_cr)) : '',
                    'options' => ['id' => 'date_cr_contract', 'placeholder' => 'Выберите'],
                    'size' => kartik\select2\Select2::SMALL,
                    'layout' => '{picker}{input}',
                    'pluginEvents' => [
                        "changeDate" => "function(e) {  $.get('/contract/set-values', {'id':".$model->id.", 'attribute': 'date_cr', 'value':$('#date_cr_contract').val() }, function(data){ } ) }",
                    ],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true,
                    ]
                ]);?>
                <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="form-group field-contract-min_payment">
                <label class="control-label" for="contract-min_payment"><?=$model->getAttributeLabel('min_payment')?></label>
                <input type="number" id="contract-min_payment" class="form-control" name="Contract[min_payment]" value="<?=$model->min_payment?>" aria-invalid="false" onchange="$.get('/contract/set-values', {'id':<?=$model->id?>, 'attribute': 'min_payment', 'value':$('#contract-min_payment').val()}, function(data){} );">

                <div class="help-block"></div>
                </div>
            </div>
            <!-- <div class="col-md-2 col-xs-6">
                <label class="control-label"><?php //$model->getAttributeLabel('min_payment_currency')?></label>
                <?php /*Html::activeDropDownList($model, 'min_payment_currency', $model->getCurrency0(), 
                    [
                        'class'=>"form-control",
                        'onchange' => '
                            var id = '.$model->id.';
                            $.post( "/contract/set-values?id=" + id + "&attribute=min_payment_currency&value=" +$(this).val(), function( data ){});
                        ',
                    ]
                );*/ ?>
            </div> -->
            <div class="col-md-2 col-xs-6">
                <label class="control-label"><?=$model->getAttributeLabel('payment_period')?></label>
                <?= Html::activeDropDownList($model, 'payment_period',$model->getActivity0(), 
                    [
                        'class'=>"form-control",
                        'onchange' => '
                            var id = '.$model->id.';
                            $.post( "/contract/set-values?id="+id+"&attribute=payment_period&value="+$(this).val(), function( data ){});
                        ',
                    ]
                );?>
            </div>
            <div class="col-md-2 col-xs-6">
                <label class="control-label"><?=$model->getAttributeLabel('payment_currency')?></label>
                <?= Html::activeDropDownList($model, 'payment_currency', $model->getCurrency(), 
                    [
                        'class'=>"form-control",
                        'onchange' => '
                            var id = '.$model->id.';
                            $.post( "/contract/set-values?id=" + id + "&attribute=payment_currency&value=" +$(this).val(), function( data ){});
                        ',
                    ]
                );?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xs-6">
                <label class="control-label"><?=$model->getAttributeLabel('company_lic')?></label>
                <?= kartik\select2\Select2::widget([
                    'name' => 'company_lic',
                    'id' => 'company_lic',
                    'value' => $model->company_lic, 
                    // 'data' => $model->getRightHolders(),
                    'data' => ArrayHelper::map($session['Contract[rightHolders]'],'id','name_ru'),
                    'options' => [
                        'placeholder' => 'Выберите ...',
                        'onchange'=>'
                            var a = $( "#company_lic" ).val();
                            $.post( "/contract/set-values?id='.$model->id.'&attribute=company_lic&value="+a, function( data ){  });
                            ' 
                    ],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [ ],
                ]) ?>
            </div>
            <div class="col-md-6 col-xs-6">
                <label class="control-label"><?=$model->getAttributeLabel('company_cont')?></label>
                <?= kartik\select2\Select2::widget([
                    'name' => 'company_cont',
                    'id' => 'company_cont',
                    'value' => $model->company_cont, 
                    'data' => $model->getRightHolders(),
                    'options' => [
                        'placeholder' => 'Выберите ...',
                        'onchange'=>'
                            var a = $( "#company_cont" ).val();
                            $.post( "/contract/set-values?id='.$model->id.'&attribute=company_cont&value="+a, function( data ){  });
                            ' 
                    ],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [ ],
                ]) ?>
            </div>
            <div class="col-md-4">
                <div style="margin-top:20px;">
                    <div class="form-group field-another_contract">
                    <input type="hidden" name="Contract[another_contract]" value="0">
                        <label>
                            <input type="checkbox" id="another_contract" name="Contract[another_contract]" value="1"  <?= $model->another_contract == 1 ? 'checked=""' : '' ?> > <?=$model->getAttributeLabel('another_contract')?>
                        </label>
                    <div class="help-block"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$this->registerJs(<<<JS
    var checked = $('#another_contract').is(':checked');

    $('#another_contract').on('change', function() 
    {
        var checked = $('#another_contract').is(':checked');
        if(checked == 0) {
            $.get('/contract/set-values', {'id':$model->id, 'attribute': 'another_contract', 'value': 0}, function(data){} );
        }
        else {
            $.get('/contract/set-values', {'id':$model->id, 'attribute': 'another_contract', 'value': 1}, function(data){} );
        }
    });

JS
);
?>