<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
CrudAsset::register($this);
$this->title = 'Просмотр';
?>

<?= $this->render('contract_form', [
    'model' => $model,
]);?>

<?= Html::a('<i class="glyphicon glyphicon-plus"></i> Дополнительное приложение', ['/contract/create-application', 'contract_id' => $model->id], ['role'=>'modal-remote','title'=> 'Добавить', 'style' => 'margin-top:10px;', 'class'=>'btn btn-info btn-xs'])?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'applications-pjax']) ?>  
	<?= $this->render('applications', [
	    'model' => $model,
	    'applications' => $applications,
	]);?>
<?php Pjax::end() ?> 



<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>