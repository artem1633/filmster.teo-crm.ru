<?php
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\FilmsApplication;
use app\models\FilmsApplicationSearch;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\widgets\Bulk;
use yii\widgets\Pjax;

?>

<?php 
foreach ($applications as $application) {

    Yii::$app->session['settings'] = $application->settings;
    Yii::$app->session['uniform_condition'] = $application->uniform_condition;
    $query = FilmsApplication::find()
        ->with(['film'])
        ->with(['application'])
        ->where(['application_id' => $application->id]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
    ]);

    $name = 'grid' . $application->id;

    $searchModel = new FilmsApplicationSearch(['formNameParam'=>$name]);
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$application->id);
        
    $clas = "";
    if(empty(Yii::$app->request->queryParams[$name])) $clas="panel-toggled";
?>  
<!-- panel-toggled -->
<div class="panel panel-success <?=$clas?>" style="margin-top: 20px;">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Приложение к договору # <?=$application->number?></h3>
        <ul class="panel-controls pull-left">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
        </ul>
        <span class="pull-right"><?= Html::a('<span class="btn btn-danger btn-xs glyphicon glyphicon-trash"></span>', ['/contract/delete-application', 'id' => $application->id], [
                'role'=>'modal-remote','title'=>'Удалить', 
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-toggle'=>'tooltip',
                'data-confirm-title'=>'Подтвердите действие',
                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
            ])?></span>
    </div>
    <div class="panel-body">
        <?php Pjax::begin(['enablePushState' => false, 'id' => 'app_form_' . $application->id])?>
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <div class="form-group field-applications-number">
                    <label class="control-label"><?=$application->getAttributeLabel('number')?></label>
                    <input type="text" id="applications-number<?=$application->id?>" class="form-control" name="applications-number-<?=$application->id?>" value="<?=$application->number?>" maxlength="255" aria-invalid="false" onchange="$.get('/contract/set-values-application', {'id':<?=$application->id?>, 'attribute': 'number', 'value':$('#applications-number<?=$application->id?>').val()}, function(data){ $.pjax.reload({container:'#applications-pjax', async: false}); } );">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="form-group field-contract-number">
                    <label class="control-label"><?=$application->getAttributeLabel('date_cr')?></label>
                    <?= kartik\date\DatePicker::widget([
                        'name' => 'date_cr'.$application->id,
                        'value' => $application->date_cr !== null ? date('d.m.Y', strtotime($application->date_cr)) : '',
                        'options' => ['id' => 'date_cr'.$application->id, 'placeholder' => 'Выберите'],
                        'size' => kartik\select2\Select2::SMALL,
                        'layout' => '{picker}{input}',
                        'pluginEvents' => [
                            "changeDate" => "function(e) {  $.get('/contract/set-values-application', {'id':".$application->id.", 'attribute': 'date_cr', 'value':$('#date_cr".$application->id."').val() }, function(data){ } ) }",
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy',
                            'todayHighlight' => true,
                        ]
                    ]);?>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-6 col-xs-6">
                <div style="margin-top:20px;">
                    <div class="form-group field-uniform_condition">
                    <input type="hidden" name="Contract[uniform_condition]" value="0">
                        <label>
                            <input type="checkbox" id="uniform_condition<?=$application->id?>" name="applications-uniform_condition<?=$application->id?>" value="1"  <?= $application->uniform_condition == 1 ? 'checked=""' : '' ?> > <?=$application->getAttributeLabel('uniform_condition')?>
                        </label>
                    <div class="help-block"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="additional<?=$application->id?>" <?= $application->uniform_condition == 1 ? '' : 'style="display:none;"' ?> >
            <div class="col-md-2 col-xs-6">
                <div class="form-group field-applications-min_warranty">
                    <label class="control-label"><?=$application->getAttributeLabel('min_warranty')?></label>
                    <input type="number" id="applications-min_warranty<?=$application->id?>" class="form-control" name="application-min_warranty[]" value="<?=$application->min_warranty?>" aria-invalid="false" onchange="$.get('/contract/set-values-application', {'id':<?=$application->id?>, 'attribute': 'min_warranty', 'value':$('#applications-min_warranty<?=$application->id?>').val()}, function(data){} );">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="form-group field-applications-tex_adap">
                    <label class="control-label"><?=$application->getAttributeLabel('tex_adap')?></label>
                    <input type="number" id="applications-tex_adap<?=$application->id?>" class="form-control" name="application-tex_adap[]" value="<?=$application->tex_adap?>" aria-invalid="false" onchange="$.get('/contract/set-values-application', {'id':<?=$application->id?>, 'attribute': 'tex_adap', 'value':$('#applications-tex_adap<?=$application->id?>').val()}, function(data){} );">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="form-group field-applications-maintenance">
                    <label class="control-label"><?=$application->getAttributeLabel('maintenance')?></label>
                    <input type="number" id="applications-maintenance<?=$application->id?>" class="form-control" name="application-maintenance[]" value="<?=$application->maintenance?>" aria-invalid="false" onchange="$.get('/contract/set-values-application', {'id':<?=$application->id?>, 'attribute': 'maintenance', 'value':$('#applications-maintenance<?=$application->id?>').val()}, function(data){} );">
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-4 col-xs-6">
                <label class="control-label"><?=$application->getAttributeLabel('films')?></label>
                <?= kartik\select2\Select2::widget([
                    'name' => 'films'.$application->id,
                    'id' => 'films'.$application->id,
                    'data' => $application->getFilmsList($applications),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [ ],
                ]) ?>
            </div>
            <div class="col-md-4 col-xs-6">
                <a href="#" onclick=" $.get('/contract/set-film', {'id':<?=$application->id?>, 'film_id':$('#films<?=$application->id?>').val()}, function(data){ $.pjax.reload({container:'<?='#app_' . $application->id?>', async: false}); $.pjax.reload({container:'<?='#app_form_' . $application->id?>', async: false}); } ); alert('Успешно добавлено'); " style="margin-top: 22px;" class="btn btn-warning" role="button"> <i class="glyphicon glyphicon-plus"></i> Добавить </a>
                <?= Html::a('Добавление фильмов из приложения', ['contract/copy-application', 'to' => $application->id], ['class' => 'btn btn-success', 'role' => 'modal-remote', 'style' => 'margin-top: 22px;']) ?>
            </div>
        </div>
        <?php Pjax::end();?>
        <div class="row" style="margin-top: 20px;">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'app_' . $application->id])?>
            <?=GridView::widget([
                'id'=>'films_application'.$application->id,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => require(__DIR__.'/_films_application_columns.php'),
                'toolbar'=> [
                    [
                        'content'=>
                            "<div style='margin-top:10px;'>".
                             Html::a(' <i class="fa fa-cog"></i>', ['/contract/change-settings', 'id' => $application->id], ['role'=>'modal-remote', 'title'=>'Изменить настройку', 'class'=>'btn btn-warning btn-xs'])
                            ."</div>"
                        ],
                    ],     
                'striped' => true,
                'condensed' => true,
                'responsive' => true,   
                'responsiveWrap' => false,
                'summary' => false,
                'panel' => [
                    'type' => 'info', 
                    'heading' => '<i class="glyphicon glyphicon-list"></i> Список фильмов',
                    'before'=>'',
                    'after'=>BulkButtonWidget::widget([
                                'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить выделенные',
                                    ["bulk-delete-films-application"] ,
                                    [
                                        "class"=>"btn btn-danger btn-xs",
                                        'role'=>'modal-remote-bulk',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Подтвердите действие',
                                        'data-confirm-message'=>'Вы уверены что хотите удалить этих элементов?'
                                    ]),
                            ]). Bulk::widget([
                                'buttons'=>Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Изменить выделенные',
                                    ["change-all", 'application_id' => $application->id ] ,
                                    [
                                        "class"=>"btn btn-info btn-xs",
                                        'role'=>'modal-remote-bulk',
                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                        'data-request-method'=>'post',
                                        'data-confirm-title'=>'Подтвердите действие',
                                        'data-confirm-message'=>'Вы уверены чтобы изменить этих выделенных элементов?'
                                    ]),
                            ]).
                            '<div class="clearfix"></div>',
                ]
            ])?>
            <?php Pjax::end();?>
        </div>
    </div>
</div>

<?php 
$this->registerJs(<<<JS
    var checked = $('#uniform_condition$application->id').is(':checked');
    if(checked == 0) $('#additional$application->id').hide();
    else $('#additional$application->id').show(); 

    $('#uniform_condition$application->id').on('change', function() 
    {
        var checked = $('#uniform_condition$application->id').is(':checked');
        if(checked == 0) {
            $('#additional$application->id').hide();
            $.get('/contract/set-values-application', {'id':$application->id, 'attribute': 'uniform_condition', 'value': 0}, function(data){} );
        }
        else {
            $('#additional$application->id').show();
            $.get('/contract/set-values-application', {'id':$application->id, 'attribute': 'uniform_condition', 'value': 1}, function(data){} );
        }
         $.pjax.reload({container:'#applications-pjax', async: false});
    });

JS
);
?>

<?php  } ?>
