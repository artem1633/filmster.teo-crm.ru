<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3 col-xs-3">
            <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3 col-xs-3">   
            <?= $form->field($model, 'date_cr')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
            <div style="margin-top: 25px;">
                <?= $form->field($model, 'uniform_condition')->checkbox(['id' => 'uniform_condition']) ?>
            </div>
        </div>
    </div>

    <div class="row" id="add" <?= $model->uniform_condition == 1 ? 'style="padding-top: 20px;"' : 'style="padding-top: 20px; display:none;"' ?>  >
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'min_warranty')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2 col-xs-6">
            <div style="padding-top:22px;">
            <?= $form->field($model, 'min_warranty_currency')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCurrency(),
                'options' => [],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    /*'allowClear' => true,*/
                ],
            ])->label(false)?>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'tex_adap')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2 col-xs-6">
            <div style="padding-top:22px;">
            <?= $form->field($model, 'text_adap_currency')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCurrency(),
                'options' => [],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    /*'allowClear' => true,*/
                ],
            ])->label(false)?>
            </div>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= $form->field($model, 'maintenance')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2 col-xs-6">
            <div style="padding-top:22px;">
            <?= $form->field($model, 'maintenance_currency')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCurrency(),
                'options' => [],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    /*'allowClear' => true,*/
                ],
            ])->label(false)?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>
<?php 
$this->registerJs(<<<JS
    var checked = $('#uniform_condition').is(':checked');
    if(checked == 0) $('#add').hide(); 
    else $('#add').show(); 

    $('#uniform_condition').on('change', function() 
    {
        var checked = $('#uniform_condition').is(':checked');
        if(checked == 0) {
            $('#add').hide(); 
        }
        else {
            $('#add').show(); 
            $('select#applications-min_warranty_currency').show();
            $('select#applications-text_adap_currency').show();
            $('select#applications-maintenance_currency').show();
        }
    });
JS
);
?>