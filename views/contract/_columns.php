<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Contract;

$session = Yii::$app->session;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'number',
        'content' => function($data){
            return Html::a($data->number,  ['contract/view', 'id' => $data->id], ['data-pjax'=>'0', /*'target' => '_blank',*/ 'data-toggle'=>'tooltip']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'date_cr',
        'content'=>function($data){
            if($data->date_cr !== null) return date('d.m.Y', strtotime($data->date_cr));
            else return '';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_lic',
        'filter' => ArrayHelper::map($session['Contract[rightHolders]'],'id','name_ru'),
        'content'=>function($data){
            return $data->companyLic->name_ru;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_cont',
        'filter' => ArrayHelper::map($session['Contract[rightHolders]'],'id','name_ru'),
        'content'=>function($data){
            return $data->companyCont->name_ru;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'min_payment',
        'content' => function($data){
            return $data->min_payment . ' ' . $data->CurrencyDescription($data->min_payment_currency);
        }
    ],
    //[
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'min_payment_currency',
        //'content'=>function($data){
            //return $data->CurrencyDescription($data->min_payment_currency);
        //}
    //],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_period',
        'filter' => Contract::getActivity0(),
        'content' => function($data){
            return $data->getActivityDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'payment_currency',
        'filter' =>ArrayHelper::map($session['Contract[currency]'],'id','code'),
        'content'=>function($data){
            return $data->CurrencyCode($data->payment_currency);
        }
    ],
    //[
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'another_contract',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'another_contract_value',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view} {delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'], 
    ],

];   