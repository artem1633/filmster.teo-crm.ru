<?php
use yii\helpers\Url;
use app\models\PreferenceBooks;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$session = Yii::$app->session;
return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'film_id',
        'header' => '<div style="width:100px;">Фильм</div>',
        'content' => function($data){
            $filmName = '';
            if($data->film->type_id == 1) $filmName = '<b>' . $data->film->name . '</b>' . '<br>Серия ' . $data->film->number_seriya;
            else $filmName = '<b>' . $data->film->name . '</b>';
            return Html::a($filmName,  ['films/view', 'id' => $data->film->id], ['data-pjax'=>'0', 'data-toggle'=>'tooltip']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'territory',
        'header' => '<div style="width:150px;">Территория</div>',
        'content' => function($data){
            $result = [];
            foreach (json_decode($data->territory) as $value) {
                $result [] = $value->id;
            }

            return kartik\select2\Select2::widget([
                'name' => 'territory',
                'data' => $data->getCountryList(),
                'value' => $result,
                'size' => 'sm',
                'options' => [ 'id' => 'territory' . $data->id,'placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
                'pluginEvents' => [
                    "change" => "function() 
                    {
                        $.get('/contract/set-territory',
                        { 'val' : JSON.stringify($(this).val()), 'id' : $(this).attr('id') },
                            function(data){}
                        );
                    }",
                ],
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rolyati_x',
        'header' => '<div style="width:110px;">Роялти</div>',
        'width' => '120px',
        'content' => function($data){
            /*return Html::textInput('rolyati_x'.$data->id, $data->rolyati_x, [ 'type' => 'number', 'style' => 'width:80px;', 'min' => 0, 'max' => 100, 
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'rolyati_x', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);*/
            return '<div class="col-md-6">' . \yii\widgets\MaskedInput::widget([
                'name' => 'rolyati_x'.$data->id,
                'value' => $data->rolyati_x,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:50px; margin-left:-5px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'rolyati_x', 'val':$(this).val()}, function(data){ 
                        $.pjax.reload({container:'#app_".$data->application_id."', async: false});
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 2, 'greedy' => false]
            ]) . '</div><div class="col-md-6">' .
                \yii\widgets\MaskedInput::widget([
                    'name' => 'rolyati_y'.$data->id,
                    'value' => $data->rolyati_y,
                    'mask' => '9',
                    'options' => [
                        'class' =>'form-control',
                        'style' => 'width:50px;margin-left:-10px;',
                        'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'rolyati_y', 'val':$(this).val()}, function(data){ 
                            $.pjax.reload({container:'#app_".$data->application_id."', async: false});
                        } ); 
                        ",
                    ],
                    'clientOptions' => ['repeat' => 2, 'greedy' => false]
                ]) . '</div>';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'platforms',
        'header' => '<div style="width:130px;">Площадки</div>',
        'content' => function($data){
            $result = [];
            foreach (json_decode($data->platforms) as $value) {
                $result [] = $value->id;
            }

            return kartik\select2\Select2::widget([
                'name' => 'platforms',
                'data' => $data->getPlatformList(),
                'value' => $result,
                'size' => 'sm',
                'options' => [ 'id' => 'platforms' . $data->id,'placeholder' => 'Выберите ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                ],
                'pluginEvents' => [
                    "change" => "function() 
                    {
                        $.get('/contract/set-platforms',
                        { 'val' : JSON.stringify($(this).val()), 'id' : $(this).attr('id') },
                            function(data){}
                        );
                    }",
                ],
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'price',
        'header' => '<div style="width:100px;">Цена</div>',
        'content' => function($data){
            return Html::dropDownList(
                'data', 
                $data->price, 
                PreferenceBooks::priceList(), 
                [
                    'prompt' => 'Выберите', 
                    'id' => 'price'.$data->id,
                    'onchange'=>"
                        $.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'price', 'val':$(this).val()}, function(data){} ); 
                    ",
                    'class' =>'form-control',
                    'style'=>'width:100px;'
                ]
            );
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'est_open',
        'header' => '<div style="width:90px;">EST откр.</div>',
        'content' => function ($data) {
            return \yii\widgets\MaskedInput::widget([
                'name' => 'est_open'.$data->id,
                'value' => \Yii::$app->formatter->asDate($data->est_open, 'php:d.m.Y'),
                'options' => [
                    'class' =>'form-control',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'est_open', 'val':$(this).val()}, function(data){ } ); ",
                ],
                'clientOptions' => ['alias' =>  'dd.mm.yyyy']
            ]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vod_open',
        'header' => '<div style="width:90px;">VOD откр.</div>',
        'content' => function ($data) {
            return \yii\widgets\MaskedInput::widget([
                'name' => 'vod_open'.$data->id,
                'value' => \Yii::$app->formatter->asDate($data->vod_open, 'php:d.m.Y'),
                'options' => [
                    'class' =>'form-control',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'vod_open', 'val':$(this).val()}, function(data){ } ); ",
                ],
                'clientOptions' => ['alias' =>  'dd.mm.yyyy']
            ]);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'close_date',
        'header' => '<div style="width:90px;">Закрытие</div>',
        'content' => function ($data) {
            return \yii\widgets\MaskedInput::widget([
                'name' => 'close_date'.$data->id,
                'value' => \Yii::$app->formatter->asDate($data->close_date, 'php:d.m.Y'),
                'options' => [
                    'class' =>'form-control',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'close_date', 'val':$(this).val()}, function(data){ } ); ",
                ],
                'clientOptions' => ['alias' =>  'dd.mm.yyyy']
            ]);
        },
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'min_warranty',
        'header' => '<div style="width:130px;">Мин.гарантия</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-6">' . \yii\widgets\MaskedInput::widget([
                'name' => 'min_warranty'.$data->id,
                'value' => $data->min_warranty,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:70px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'min_warranty', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div><div class="col-md-6">' .
                    Html::dropDownList(
                        'data', 
                        $data->min_warranty_currency, 
                        $data::getCurrency(), 
                        [
                            'prompt' => 'Выберите',
                            'onchange'=>"
                                $.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'min_warranty_currency', 'val':$(this).val()}, function(data){} ); 
                            ",
                            'class' =>'form-control',
                            'style'=>'width:50px;'
                        ]
                    ) . 
            '</div>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tex_adap',
        'header' => '<div style="width:130px;">Тех.адаптация</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-6">' . \yii\widgets\MaskedInput::widget([
                'name' => 'tex_adap'.$data->id,
                'value' => $data->tex_adap,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:70px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'tex_adap', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div><div class="col-md-6">' .
                    Html::dropDownList(
                        'data', 
                        $data->text_adap_currency, 
                        $data::getCurrency(), 
                        [
                            'prompt' => 'Выберите',
                            'onchange'=>"
                                $.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'text_adap_currency', 'val':$(this).val()}, function(data){} ); 
                            ",
                            'class' =>'form-control',
                            'style'=>'width:50px;'
                        ]
                    ) . 
            '</div>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'maintenance',
        'header' => '<div style="width:130px;">Maintenance fee</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-6">' . \yii\widgets\MaskedInput::widget([
                'name' => 'maintenance'.$data->id,
                'value' => $data->maintenance,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:70px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'maintenance', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div><div class="col-md-6">' .
                    Html::dropDownList(
                        'data', 
                        $data->maintenance_currency, 
                        $data::getCurrency(), 
                        [
                            'prompt' => 'Выберите',
                            'onchange'=>"
                                $.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'maintenance_currency', 'val':$(this).val()}, function(data){} ); 
                            ",
                            'class' =>'form-control',
                            'style'=>'width:50px;'
                        ]
                    ) . 
            '</div>';
        },
    ],*/
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'min_warranty',
        'header' => '<div style="width:130px;">Мин.гарантия</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-12">' . \yii\widgets\MaskedInput::widget([
                'name' => 'min_warranty'.$data->id,
                'value' => $data->min_warranty,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:120px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'min_warranty', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div>';
        },
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'min_warranty',
        'header' => '<div style="width:130px;">Мин.гарантия</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-12">' . 
                Html::textInput('min_warranty'.$data->id, $data->min_warranty, 
                    [
                        'type' => 'number',
                        'class' => 'form-control',
                        'style' => 'width:120px; margin-left:-10px;',
                        'onchange' => "$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'min_warranty', 'val':$(this).val()}, function(data){ } );",
                    ]) 
                .'</div>';
        },
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tex_adap',
        'header' => '<div style="width:130px;">Тех.адаптация</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-12">' . \yii\widgets\MaskedInput::widget([
                'name' => 'tex_adap'.$data->id,
                'value' => $data->tex_adap,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:120px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'tex_adap', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div>';
        },
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tex_adap',
        'header' => '<div style="width:130px;">Тех.адаптация</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-12">' . 
                Html::textInput('tex_adap'.$data->id, $data->tex_adap, 
                    [
                        'type' => 'number',
                        'class' =>'form-control',
                        'style' => 'width:120px; margin-left:-10px;',
                        'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'tex_adap', 'val':$(this).val()}, function(data){ } );",
                    ])  . 
                '</div>';
        },
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'maintenance',
        'header' => '<div style="width:130px;">Maintenance fee</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-12">' . \yii\widgets\MaskedInput::widget([
                'name' => 'maintenance'.$data->id,
                'value' => $data->maintenance,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:120px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'maintenance', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div>';
        },
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'maintenance',
        'header' => '<div style="width:130px;">Тех.обслуживание</div>',
        'visible' => Yii::$app->session['uniform_condition'] ? false : true,
        'content' => function ($data) {
            return '<div class="col-md-12">' . 
                Html::textInput('maintenance'.$data->id, $data->maintenance, 
                    [
                        'type' => 'number',
                        'class' =>'form-control',
                        'style' => 'width:120px; margin-left:-10px;',
                        'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'maintenance', 'val':$(this).val()}, function(data){ } ); ",
                    ]) . 
                '</div>';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'svod',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function($data){
            return Html::textInput('svod'.$data->id, $data->svod, [
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'svod', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'pvod',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function($data){
            return Html::textInput('pvod'.$data->id, $data->pvod, [
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'pvod', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'avod',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function($data){
            return Html::textInput('avod'.$data->id, $data->avod, [
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'avod', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'catch_up',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function($data){
            return Html::textInput('catch_up'.$data->id, $data->catch_up, [
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'catch_up', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tv',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function($data){
            return Html::textInput('tv'.$data->id, $data->tv, [
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'tv', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'flat_rate',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function($data){
            return Html::textInput('flat_rate'.$data->id, $data->flat_rate, [
                'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'flat_rate', 'val':$(this).val()}, function(data){} ); ", 'class' => 'form-control']);
        }
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'flat_rate',
        'header' => '<div style="width:130px;">Flat Rate (открытие)</div>',
        'visible' => Yii::$app->session['settings'] ? true : false,
        'content' => function ($data) {
            return '<div class="col-md-6">' . \yii\widgets\MaskedInput::widget([
                'name' => 'flat_rate'.$data->id,
                'value' => $data->flat_rate,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style' => 'width:70px; margin-left:-10px;',
                    'onchange'=>"$.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'flat_rate', 'val':$(this).val()}, function(data){ 
                    } ); 
                    ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]) . 
                '</div><div class="col-md-6">' .
                    Html::dropDownList(
                        'data', 
                        $data->flat_rate_currency, 
                        $data::getCurrency(), 
                        [
                            'prompt' => 'Выберите',
                            'onchange'=>"
                                $.get('/contract/set-values-films-application', {'id':$data->id, 'attribute':'flat_rate_currency', 'val':$(this).val()}, function(data){} ); 
                            ",
                            'class' =>'form-control',
                            'style'=>'width:50px;'
                        ]
                    ) . 
            '</div>';
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'header' => '#',
        'width' => '20px',
        'vAlign'=>'middle',
        'template' => '{delete}',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to(['/contract/delete-films-application','id'=>$key]);
        },
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'], 
    ],

];   