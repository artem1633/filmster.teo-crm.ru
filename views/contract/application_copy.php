<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Contract;
use yii\helpers\ArrayHelper;
use app\models\RightHolders;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $form yii\widgets\ActiveForm */

$contracts = Contract::find()->all();
$contractsMap = [];

foreach ($contracts as $contract){
    $companyCont = RightHolders::find()->where(['id' => $contract->company_cont])->one();
    $companyLic = RightHolders::find()->where(['id' => $contract->company_lic])->one();

    $companyContName = '';
    $companyLicName = '';

    if($companyCont){
        $companyContName = $companyCont->name_ru;
    }

    if($companyLic){
        $companyLicName = $companyLic->name_ru;
    }


    $contractsMap[$contract->id] = "{$companyContName}, {$companyLicName}, {$contract->number}";
}

?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="form-group">
                <label for="">Договор</label>
                <?= Select2::widget([
                    'id' => 'contract-fld',
                    'name' => 'contract',
                    'data' => $contractsMap,
                    'options' => ['placeholder' => 'Выберите договор', 'allowClear' => true],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'applicationIdFrom')->widget(Select2::class, [
                'data' => [],
                'options' => ['placeholder' => 'Выберите приложение'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <?= $form->field($model, 'filmsPks')->widget(Select2::class, [
                'data' => [],
                'options' => [
                    'placeholder' => 'Выберите фильмы',
                    'multiple' => true,
                ],
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end() ?>

</div>

<?php

$script = <<< JS
$('#contract-fld').change(function(){
    var val = $(this).val();
    
    $.get('/contract/get-applications?id='+val, function(response){
        $('#applicationcopyform-applicationidfrom').html(response);
    });
});

$('#applicationcopyform-applicationidfrom').change(function(){
    var val = $(this).val();
    
    $.get('/contract/get-films?applicationId='+val, function(response){
        $('#applicationcopyform-filmspks').html(response);
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
