<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contract-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4 col-xs-4">
                <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-xs-4">   
                <?= $form->field($model, 'contract_date')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'size' => kartik\select2\Select2::SMALL,
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy',
                    //'startView'=>'year',
                    'todayHighlight' => true,
                ]
            ]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
            <?= $form->field($model, 'payment_currency')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCurrency(),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-xs-4">
                <?= $form->field($model, 'payment_period')->dropdownList($model->getActivity0(),[]) ?>
        </div>
        <div class="col-md-4 col-xs-4">
                <?= $form->field($model, 'min_payment')->textInput(['type' => 'number']) ?>
        </div>
        <!-- <div class="col-md-4 col-xs-4">
            <div style="padding-top:22px;">
            <?php /*$form->field($model, 'min_payment_currency')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCurrency(),
                'options' => [],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                ],
            ])->label(false)*/ ?>
            </div>
        </div> -->
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'company_lic')->dropdownList($model->getRightHolders(),[]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'company_cont')->dropdownList($model->getRightHolders(),[]) ?>
        </div>
    </div>
     <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'another_contract')->checkbox() ?>
        </div>
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
