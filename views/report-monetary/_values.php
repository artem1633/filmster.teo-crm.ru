<?php
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Currency;
use app\models\Country;
?>

<?=GridView::widget([ 
    'id'=>'report',
    'responsiveWrap' => false,
    'summary' => false,
    'dataProvider' => $model->AllValue,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'film_id',
            'content' => function($data){
                return $data->film->name;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'sale_count',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'est',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'vod',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'svod',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'pvod',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'avod',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'date_begin',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'date_end',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'currency',
            'content' => function($data){
                $currency = Currency::findOne($data->currency);
                return $currency->code;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'territory',
            'content' => function($data){
                $country = Country::findOne($data->territory);
                return $country->code;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'identificator',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'additional_identificator',
        ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>