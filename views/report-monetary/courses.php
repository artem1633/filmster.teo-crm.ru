<?php

use app\models\Currency;
?>
	
<?php
foreach ($currencySettings as $value) {
	?>
<div class="row">
	<div class="col-md-2 col-xs-2 col-sm-2">
        <input type="number" id="course_from<?=$value->id?>" class="form-control" value="<?=$value->currency_from_value?>" onchange="$.get('/report-monetary/set-course', {'id':<?=$value->id?>, 'attribute':'currency_from_value', 'val':$(this).val()}, function(data){ $.pjax.reload({container:'#report-monetary-pjax', async: false}); } ); ">
    </div>
    <div class="col-md-4 col-xs-4 col-sm-4">
    	<b><?=Currency::findOne($value->currency_from_id)->name . ' (' . Currency::findOne($value->currency_from_id)->code . ')' ?></b>
    </div>
    <div class="col-md-2 col-xs-2 col-sm-2">
        <input type="number" id="course_to<?=$value->id?>" class="form-control" value="<?=$value->currency_to_value?>" onchange="$.get('/report-monetary/set-course', {'id':<?=$value->id?>, 'attribute':'currency_to_value', 'val':$(this).val()}, function(data){ $.pjax.reload({container:'#report-monetary-pjax', async: false}); } ); ">
    </div>
    <div class="col-md-4 col-xs-4 col-sm-4">
    	<b><?=Currency::findOne($value->currency_to_id)->name . ' (' . Currency::findOne($value->currency_to_id)->code . ')' ?></b>
    </div>
</div>
	<?php
}
?>