<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportMonetary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-monetary-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    	<div class="col-md-6">
	    	<?= $form->field($model, 'platform_id')->widget(kartik\select2\Select2::classname(), [
	            'data' => $model->getPlayGroundList(),
	            'options' => ['placeholder' => 'Выберите ...'],
	            'size' => kartik\select2\Select2::SMALL,
	            'pluginOptions' => [
	                'allowClear' => true,
	            ],
	        ])?>
    	</div>
    	<div class="col-md-6">
		    <?= $form->field($model, 'date')->widget(kartik\date\DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите'],
                'layout' => '{picker}{input}',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'startView'=>'year',
                    'minViewMode'=>'months',
                    'format' => 'M-yyyy',
                    'todayHighlight' => true,
                ]
            ]) ?>
    	</div>
        <div class="col-md-6">
            <?= $form->field($model, 'territory')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getTerritoryList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'currency')->widget(kartik\select2\Select2::classname(), [
                'data' => $model->getCurrencyList(),
                'options' => ['placeholder' => 'Выберите ...'],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ])?>
        </div>
	    <div class="col-md-12 col-xs-12">
	        <?= $form->field($model, 'files')->fileInput(['class'=>"btn_image"]); ?>
	    </div>
    </div>



  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
