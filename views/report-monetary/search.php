<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
?>
    <div class="row">
        <?php $form = ActiveForm::begin(['options' => ['method' => 'post', 'autocomplete'=>"off" ]]); ?>
            <div class="col-md-4">
                <label>Месяц</label>
                <?= kartik\date\DatePicker::widget([
                    'name' => 'month',
                    'value' => $post['month'],
                    'options' => [ 'placeholder' => 'Выберите'],
                    'size' => kartik\select2\Select2::SMALL,
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'startView'=>'year',
                        'minViewMode'=>'months',
                        'format' => 'M-yyyy',
                        'todayHighlight' => true,
                    ]
                ]);?>
            </div>
            <div class="col-md-1">
                <label>.</label>
                <div class="form-group">
                    <?= Html::submitButton('Показать', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
