<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use app\models\ReportValues;
use app\models\Currency;
use app\models\SettingCourses;
use yii\widgets\Pjax;
use app\models\Applications;
use app\models\FilmsApplication;
use johnitvn\ajaxcrud\CrudAsset;
use app\models\CurrencySetting; 

CrudAsset::register($this);

$reportValues = ReportValues::find()->where(['report_monetary_id' => $model->id])->all();
$currencyID = [];
foreach ($reportValues as $report) {
    $currencyID [] = $report->currency;
}
$currencyID = array_unique($currencyID);

?>
<?php Pjax::begin(['enablePushState' => false, 'id' => 'report-monetary-pjax']) ?> 
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-warning">
		    <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Настройка курсов</h3>
                <span class="pull-right">
                    <?=Html::a('Курсы', ['/report-monetary/change-courses', 'report_monetary_id' => $model->id],['role'=>'modal-remote','title'=> 'Курсы', 'class'=>'btn btn-info'])?>
                </span>
		    </div>
		    <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-actions">
                        <thead>
                            <tr>
                                <th width="40" class="text-center">#</th>
                                <th width="200">Название компании - Договор</th>
                                <th width="150" class="text-center">Сумма из отчета</th>
                            <?php
                                foreach ($currencyID as $currency_id) 
                                {
                                    $currency = Currency::findOne($currency_id);
                                    echo "<th class=\"text-center\">{$currency->code}</th>";
                                }
                            ?>
                            </tr>
                        </thead>
                        <tbody>
                    <?php
                        $i = 0;
                        foreach ($contracts as $contract) 
                        {
                            $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
                            $applicationsID = [];
                            foreach ($applications as $value) {
                                $applicationsID [] = $value->id;
                            }

                            $applications = FilmsApplication::find()->where(['application_id' => $applicationsID])->all();
                            $filmsID = [];
                            foreach ($applications as $value) {
                                $filmsID [] = $value->film_id;
                            }
                            $i++;
                            $reportValues = ReportValues::find()->where(['film_id' => $filmsID, 'report_monetary_id' => $id])->all();
                            $sum = 0; $total_summary = 0;
                            foreach ($reportValues as $reportValue) 
                            {
                                $course = 0;
                                $summary = ($reportValue->est + $reportValue->vod + $reportValue->svod + $reportValue->pvod + $reportValue->avod);
                                $currencySetting = CurrencySetting::find()
                                    ->where(['currency_from_id' => $reportValue->currency, 'currency_to_id' => $contract->payment_currency, 'monetary_id' => $model->id])
                                    ->one();
                                if($currencySetting !== null) {
                                    if($currencySetting->currency_from_value != 0) $course = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                                    else $course = 0;
                                }
                                else {
                                    $currencySetting = CurrencySetting::find()
                                        ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $reportValue->currency, 'monetary_id' => $model->id])
                                        ->one();
                                    if($currencySetting !== null) {
                                        if($currencySetting->currency_to_value != 0) $course = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                                        else $course = 0;
                                    }
                                }
                                if(is_nan($course)) $course = 0;
                                if($currencySetting != null) $sum += $summary * $course;
                                else $sum += $summary;
                                $total_summary += $summary;
                            }
                        ?>
                            <tr>
                                <td class="text-center"><?=$i?></td>
                                <td>
                                    <strong><?=$contract->companyLic->name_ru . " / " . $contract->number?></strong>
                                    <br>Расчеты в <?=Currency::findOne($contract->payment_currency)->code?>
                                </td>
                                <td><?=round($sum, 2) .' ' . Currency::findOne($contract->payment_currency)->code?></td>
                                <!-- <td class="text-center"><?php //$sum?> <?php //Currency::findOne($contract->payment_currency)->code?></td> -->
                                <?php
                                    foreach ($currencyID as $currency_id) 
                                    {
                                        $currency = Currency::findOne($currency_id);

                                        //-------------------------------------------------------------------------------//
                                        $reportValuesSum = ReportValues::find()->where(['film_id' => $filmsID, 'report_monetary_id' => $id, 'currency' => $currency_id])->all();
                                        $summary = 0; $self_summary = 0;
                                        foreach ($reportValuesSum as $value) {
                                            $summary += ($value->est + $value->vod + $value->svod + $value->pvod + $value->avod);
                                            $self_summary += $value->est + $value->vod + $value->svod + $value->pvod + $value->avod;
                                        }
                                        //-------------------------------------------------------------------------------//

                                        $currencyValue = 0;
                                        $currencySetting = CurrencySetting::find()
                                            ->where(['currency_from_id' => $currency_id, 'currency_to_id' => $contract->payment_currency, 'monetary_id' => $model->id])
                                            ->one();
                                        if($currencySetting != null) 
                                        {
                                            if($currencySetting->currency_to_value !== 0) $currencyValue = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                                            else $currencyValue = 0;
                                        }
                                        else{
                                            $currencySetting = CurrencySetting::find()
                                                ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $currency_id, 'monetary_id' => $model->id])
                                                ->one();
                                            if($currencySetting != null) {
                                                if($currencySetting->currency_from_value !== 0) $currencyValue = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                                                else $currencyValue = 0;
                                            }
                                        }

                                        if(is_nan($currencyValue)) $currencyValue = 0;

                                        $settingCourse = SettingCourses::find()->where(['currency_id' => $currency_id, 'contract_id' => $contract->id])->one();
                                        if($settingCourse == null){
                                            $settingCourse = new SettingCourses();
                                            $settingCourse->currency_id = $currency_id;
                                            $settingCourse->contract_id = $contract->id;
                                            $settingCourse->course = $currencyValue;
                                        }
                                            $settingCourse->value = (float)($sum * $currencyValue);
                                            $settingCourse->save();
                                ?>
                                    <td>
                                        <?php 
                                        if($currency_id !== $contract->payment_currency){ 
                                            if($currencyValue == 0) {
                                                $value = 0;
                                                $second_summary = 0;
                                            }
                                            else {
                                                //$value = round(1/$currencyValue, 2);
                                                $value = 1 / $currencyValue;
                                                $second_summary = round($summary * 1/$currencyValue, 2);
                                            }
                                        ?>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <?=$summary . ' ' . $currency->code?>
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <input type="number" class="form-control" value="<?=$value?>" onchange="$.get('/report-monetary/set-setting-course', {'id':<?=$settingCourse->id?>, 'currency_id' : <?=$currency_id?>, 'payment_currency':<?=$contract->payment_currency?>, 'sum':<?=$sum?>, 'currencyValue':<?=$total_summary * $currencyValue?>, 'val':$(this).val(), 'monetary_id':<?=$model->id?> }, function(data){ $.pjax.reload({container:'#report-monetary-pjax', async: false});
                                                } ); ">
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <?= $second_summary . ' ' . Currency::findOne($contract->payment_currency)->code?>
                                        </div>
                                        <?php 
                                        } else {
                                            echo $self_summary . ' ' . Currency::findOne($contract->payment_currency)->code;
                                        } ?>
                                    </td>
                                <?php
                                    }
                                ?>
                            </tr>
                    <?php
                        }
                    ?>
                        </tbody>
                    </table>
                </div>

		    </div>
		</div>
	</div>

	<div class="col-md-12">
		<br/>
		<div class="panel panel-success">
		    <div class="panel-heading ui-draggable-handle">
                <span class="pull-left">
                    <b><?=$model->getAttributeLabel('platform_id')?> : </b><?=$model->platform->name_ru?>
                </span>
		        <span class="pull-right">
                    <b><?=$model->getAttributeLabel('date')?> : </b><?=$model->date?>
                </span>
		    </div>
		    <div class="panel-body">
		    	<div class="table-responsive">
		    		<?= $this->render('_values', [
				        'model' => $model,
				    ]) ?>
                </div>
		    </div>
		</div>
	</div>
</div>
<?php Pjax::end() ?> 

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
