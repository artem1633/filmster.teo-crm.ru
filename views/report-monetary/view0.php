<?php

use yii\helpers\Html;

$_csrf = \Yii::$app->request->getCsrfToken();
?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-warning">
		    <div class="panel-heading ui-draggable-handle">
		        <h3 class="panel-title">Загрузка отчета</h3>
		        <ul class="panel-controls">
		            
		        </ul>
		    </div>
		    <div class="panel-body">
		    	<div class="row">
		    		<div class="col-md-12">
		    			<label class="control-label">Выберите файл отчетности</label>
			            <div id="logo<?=$model->id?>"></div>
			            <button style="display:block;width:120px; height:30px;" name="button23<?=$model->id?>[]" id="button23<?=$model->id?>" onclick="document.getElementById('fileInput<?=$model->id?>').click()">Выберите файл</button>
			            <input type="file" name="fileInput<?=$model->id?>[]" style="display:none" id="fileInput<?=$model->id?>" accept=".xlsx, .xls" />
		    		</div>
		    	</div>
		    		<br>
		    	<div class="col-md-4 col-xs-4">
		            <label class="control-label" for="localization-name"><?=$model->getAttributeLabel('platform_id')?></label>
		            <?= kartik\select2\Select2::widget([
		                'name' => 'platform_id'.$model->id,
		                'id' => 'platform_id'.$model->id,
		                'value' => $model->platform_id, 
		                'data' => $model->getPlayGroundList(),
		                'options' => [
		                    'onchange'=>'
		                        var a = $( "#platform_id'.$model->id.'" ).val();
		                        $.post( "/report-monetary/set-values?id='.$model->id.'&attribute=platform_id&value="+a, function( data ){  });
		                        ' 
		                ],
		                'size' => kartik\select2\Select2::SMALL,
		                'pluginOptions' => [ ],
		            ]) ?>
		        </div>
		        <div class="col-md-4 col-xs-4">
                    <label class="control-label"><?=$model->getAttributeLabel('date')?></label>
                    <?= kartik\date\DatePicker::widget([
                        'name' => 'date'.$model->id,
                        'value' => $model->date,//date('M-y', $model->date),
                        'options' => ['id' => 'date'.$model->id, 'placeholder' => 'Выберите'],
                        'size' => kartik\select2\Select2::SMALL,
                        'layout' => '{picker}{input}',
                        'pluginEvents' => [
                            "changeDate" => "function(e) {  $.get('/report-monetary/set-values', {'id':".$model->id.", 'attribute': 'date', 'value':$('#date".$model->id."').val() }, function(data){ } ) }",
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'startView'=>'year',
		                    'minViewMode'=>'months',
		                    'format' => 'M-yyyy',
                            'todayHighlight' => true,
                        ]
                    ]);?>
                    <div class="help-block"></div>
            	</div>
		    </div>
		</div>
	</div>

	<div class="col-md-12">
		<br/>
		<br/>
		<div class="panel panel-success">
		    <div class="panel-heading ui-draggable-handle">
		        <h3 class="panel-title">Настройка курсов</h3>
		        <ul class="panel-controls">
		            
		        </ul>
		    </div>
		    <div class="panel-body">
		    	<div class="table-responsive">
		    	<table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>Сумма из отчета</th>
                            <th style="width:220px;text-align: center;">Итого в USD</th>
                            <th style="width:220px;text-align: center;">Итого в EUR</th>
                            <th style="width:220px;text-align: center;">Итого в Фунты</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>
                            	<b>Disney</b>
                            	<br>
                            	<span style="font-size: 10px;">Расчеты в RUB</span>
                            </td>
                            <td>18200 P</td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>
                            	<b>Disney</b>
                            	<br>
                            	<span style="font-size: 10px;">Расчеты в RUB</span>
                            </td>
                            <td>18200 P</td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>
                            	<b>Disney</b>
                            	<br>
                            	<span style="font-size: 10px;">Расчеты в RUB</span>
                            </td>
                            <td>18200 P</td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                            <td>
                            	<div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" aria-invalid="false" >
				                    </div>
				                </div>
				                <div class="col-md-6 col-xs-6 col-sm-6">
				                    <div class="form-group">
				                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="69.80" maxlength="255" disabled="" aria-invalid="false" >
				                    </div>
				                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
		    </div>
		</div>
	</div>
</div>



<?php 
$this->registerJs(<<<JS

	var fileCollection = new Array();

    /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#logo$model->id").click(function(){
            $("#fileInput$model->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput$model->id\']').on('change', fileUpload);
    });

    function fileUpload(event){
        //notify user about the file upload status
        $("#logo$model->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$model->id}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/report-monetary/set-file', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#logo$model->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#logo$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            
        }
    }
JS
);
?>
