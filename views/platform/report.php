<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use app\models\Conditions;
use app\models\ConditionColumns;
use app\models\PreferenceBooks;

CrudAsset::register($this);

?>

<?php if($report != null) { ?>
	<?php Pjax::begin(['enablePushState' => false, 'id' => 'report-pjax']) ?>
	<div class="row">
		<div class="col-md-12">
			<h3>
				Начало и окончание парсинга (по строкам)
			</h3>
			<p></p>
		</div>
		<div class="col-md-3 col-xs-3">
	        <div class="form-group">
	            <label class="control-label"><?=$report->getAttributeLabel('begin_parse')?></label>
	            <input type="number" id="begin_parse" class="form-control" name="ReportSetting[begin_parse]" value="<?=$report->begin_parse?>" aria-invalid="false" onchange="$.get('/platform/set-report-setting', {'id':<?=$report->id?>, 'attribute': 'begin_parse', 'value':$('#begin_parse').val()}, function(data){} );" >
	            <div class="help-block"></div>
	        </div>
	    </div>
	    <div class="col-md-3 col-xs-3">
	        <div class="form-group">
	            <label class="control-label"><?=$report->getAttributeLabel('end_parse')?></label>
	            <input type="number" id="end_parse" class="form-control" name="ReportSetting[end_parse]" value="<?=$report->end_parse?>" aria-invalid="false" onchange="$.get('/platform/set-report-setting', {'id':<?=$report->id?>, 'attribute': 'end_parse', 'value':$('#end_parse').val()}, function(data){} );" >
	            <div class="help-block"></div>
	        </div>
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
	    	<hr/>
			<?= Html::a('Добавить условие <i class="glyphicon glyphicon-plus"></i>', ['/platform/create-condition', 'report_setting_id' => $report->id], ['role'=>'modal-remote','title'=> 'Добавить', 'style' => 'margin-left:10px;margin-bottom:10px;', 'class'=>'btn btn-info btn-xs'])?>
		</div>

		<?php 
			$conditions = Conditions::find()->where(['report_setting_id' => $report->id])->all();
			foreach ($conditions as $condition) {
		?>
			<div class="col-md-12">
			<br>
				<div class="panel panel-warning">
	                <div class="panel-heading ui-draggable-handle">
	                    <h5 class="panel-title">Условие</h5>
	                    <span class="pull-right">
                            <a class="btn btn-danger btn-xs" title="Удалить" onclick="$.get('/platform/delete-condition', {'id' : <?=$condition->id?> }, function(data){ $.pjax.reload({container:'#report-pjax', async: false});  } );" >
                                Удалить <i class="glyphicon glyphicon-trash"></i>
                            </a>
	                    </span>
	                </div>
	                <div class="panel-body">
	                	<?php
	                		$columns = ConditionColumns::find()->where(['condition_id' => $condition->id])->all();
	                		foreach ($columns as $column) {
	                	?>
		                	<div class="row">
		                		<div class="col-md-3 col-xs-3">
		                			<label class="control-label"><?=$column->getAttributeLabel('column_name')?></label>
		                			<?= Html::activeDropDownList($column, 'column_name', PreferenceBooks::excelColumnName(), 
						                [
						                    //'prompt'=>'Выберите', 
						                    'class'=>"form-control",
						                    'onchange' => '
						                        var id = '.$column->id.';
						                        $.post( "/platform/set-condition-column?id="+ id+ "&attribute=column_name&value=" +$(this).val(), function( data ){});
						                    ',
						                ]
						            );?>
							    </div>
							    <div class="col-md-3 col-xs-3">
							        <label class="control-label"><?=$column->getAttributeLabel('condition_name')?></label>
						            <?= Html::activeDropDownList($column, 'condition_name', $column->getConditionsList(), 
						                [
						                    //'prompt'=>'Выберите', 
						                    'class'=>"form-control",
						                    'onchange' => '
						                        var id = '.$column->id.';
						                        if($(this).val() == 3) $("#columnValue'.$column->id.'").show(); 
						                        else $("#columnValue'.$column->id.'").hide(); 
						                        $.post( "/platform/set-condition-column?id="+ id+ "&attribute=condition_name&value=" +$(this).val(), function( data ){});
						                    ',
						                ]
						            );?>
							    </div>
								<div class="col-md-3 col-xs-3" id="columnValue<?=$column->id?>" <?php if($column->condition_name != 3) echo 'style=" display: none;"';?> >
							        <div class="form-group">
							            <label class="control-label"><?=$column->getAttributeLabel('value')?></label>
							            <input type="text" id="value<?=$column->id?>" class="form-control" name="ConditionColumns[value]" value="<?=$column->value?>" aria-invalid="false" onchange="$.get('/platform/set-condition-column', {'id':<?=$column->id?>, 'attribute': 'value', 'value':$('#value<?=$column->id?>').val()}, function(data){} );" >
								        <div class="help-block"></div>
								    </div>
								</div>
								<div class="col-md-1 col-xs-1">
									<br><p></p>
									<a class="btn btn-danger btn-xs" title="Удалить" onclick="$.get('/platform/delete-column', {'id' : <?=$column->id?> }, function(data){ $.pjax.reload({container:'#report-pjax', async: false});  } );" >
		                                <i class="glyphicon glyphicon-trash"></i>
		                            </a>
								</div>
		                	</div>
	                	<?php } ?>
						    	<hr/>
								<a class="btn btn-info btn-xs" style="margin-left:10px;margin-bottom:10px;", onclick="$.get('/platform/create-column', {'condition_id' : <?=$condition->id?> }, function(data){ $.pjax.reload({container:'#report-pjax', async: false});  } );" >
		                            Добавить столбец <i class="glyphicon glyphicon-plus"></i>
		                        </a>
	                	<div class="row">
	                		<div class="col-md-3 col-xs-3">
						        <label class="control-label"><?=$condition->getAttributeLabel('action_id')?></label>
							    <?= Html::activeDropDownList($condition, 'action_id', $condition->getActionsList(), 
							        [
							            /*'prompt'=>'Выберите', */
							            'class'=>"form-control",
							            'onchange' => '
							                var id = '.$condition->id.';
							                //$("#conditionFilm'.$condition->id.'").hide();
							                $("#conditionSale'.$condition->id.'").hide();
							                //$("#conditionBegin'.$condition->id.'").hide();
							                //$("#conditionEnd'.$condition->id.'").hide();
							                $("#conditionCurrency'.$condition->id.'").hide();
							                //if($(this).val() == 2) $("#conditionFilm'.$condition->id.'").show();
							                if($(this).val() == 3) $("#conditionSale'.$condition->id.'").show();
							                //if($(this).val() == 4) $("#conditionBegin'.$condition->id.'").show();
							                //if($(this).val() == 5) $("#conditionEnd'.$condition->id.'").show();
							                if($(this).val() == 6) $("#conditionCurrency'.$condition->id.'").show();
							                $.post( "/platform/set-condition?id="+ id+ "&attribute=action_id&value=" +$(this).val(), function( data ){});
							            ',
							        ]
							    );?>
						    </div>
						    <!-- <div class="col-md-3 col-xs-3" id="conditionFilm<?=$condition->id?>" <?php //if($condition->action_id != 2) echo 'style=" display: none;"';?> >
						        <label class="control-label"><?php //$condition->getAttributeLabel('film_id')?></label>
							    <?php /* Html::activeDropDownList($condition, 'film_id', $condition->getFilmsList(), 
							        [
							            'prompt'=>'Выберите', 
							            'class'=>"form-control",
							            'onchange' => '
							                var id = '.$condition->id.';
							                $.post( "/platform/set-condition?id="+ id+ "&attribute=film_id&value=" +$(this).val(), function( data ){});
							            ',
							        ]
							    );*/ ?>
						    </div> -->
						    <div class="col-md-3 col-xs-3" id="conditionSale<?=$condition->id?>" <?php if($condition->action_id != 3) echo 'style=" display: none;"';?> >
								<label class="control-label"><?=$condition->getAttributeLabel('sale_id')?></label>
		                			<?= Html::activeDropDownList($condition, 'sale_id', PreferenceBooks::sourceSalesmodelList(), 
						                [
						                    //'prompt'=>'Выберите', 
						                    'class'=>"form-control",
						                    'onchange' => '
						                        var id = '.$condition->id.';
						                        $.post( "/platform/set-condition?id="+ id+ "&attribute=sale_id&value=" +$(this).val(), function( data ){});
						                    ',
						                ]
						            );?>
						    </div>
						    <!-- <div class="col-md-3 col-xs-6" id="conditionBegin<?=$condition->id?>" <?php //if($condition->action_id != 4) echo 'style=" display: none;"';?> >
			                    <label class="control-label"><?php //$condition->getAttributeLabel('date_begin')?></label>
			                    <?php /*kartik\date\DatePicker::widget([
			                        'name' => 'date_begin'.$condition->id,
			                        'value' => $condition->date_begin != null ? date('d.m.Y', strtotime($condition->date_begin)) : '',
			                        'options' => ['id' => 'date_begin'.$condition->id, 'placeholder' => 'Выберите'],
			                        'size' => kartik\select2\Select2::SMALL,
			                        'layout' => '{picker}{input}',
			                        'pluginEvents' => [
			                            "changeDate" => "function(e) {  $.get('/platform/set-condition', {'id':".$condition->id.", 'attribute': 'date_begin', 'value':$('#date_begin".$condition->id."').val() }, function(data){ } ) }",
			                        ],
			                        'pluginOptions' => [
			                            'autoclose'=>true,
			                            'format' => 'dd.mm.yyyy',
			                            'todayHighlight' => true,
			                        ]
			                    ]);*/ ?>
			                    <div class="help-block"></div>
			            	</div> -->
			            	<!-- <div class="col-md-3 col-xs-3" id="conditionEnd<?=$condition->id?>" <?php //if($condition->action_id != 5) echo 'style=" display: none;"';?> >
			                    <label class="control-label"><?php //$condition->getAttributeLabel('date_end')?></label>
			                    <?php /*kartik\date\DatePicker::widget([
			                        'name' => 'date_end'.$condition->id,
			                        'value' => $condition->date_end != null ? date('d.m.Y', strtotime($condition->date_end)) : '',
			                        'options' => ['id' => 'date_end'.$condition->id, 'placeholder' => 'Выберите'],
			                        'size' => kartik\select2\Select2::SMALL,
			                        'layout' => '{picker}{input}',
			                        'pluginEvents' => [
			                            "changeDate" => "function(e) {  $.get('/platform/set-condition', {'id':".$condition->id.", 'attribute': 'date_end', 'value':$('#date_end".$condition->id."').val() }, function(data){ } ) }",
			                        ],
			                        'pluginOptions' => [
			                            'autoclose'=>true,
			                            'format' => 'dd.mm.yyyy',
			                            'todayHighlight' => true,
			                        ]
			                    ]);*/ ?>
			                    <div class="help-block"></div>
			            	</div> -->
			            	<div class="col-md-3 col-xs-3" id="conditionCurrency<?=$condition->id?>" <?php if($condition->action_id != 6) echo 'style=" display: none;"';?> >
						        <label class="control-label"><?=$condition->getAttributeLabel('currency_id')?></label>
							    <?= Html::activeDropDownList($condition, 'currency_id', $condition->getCurrencyList(), 
							        [
							            'prompt'=>'Выберите', 
							            'class'=>"form-control",
							            'onchange' => '
							                var id = '.$condition->id.';
							                $.post( "/platform/set-condition?id="+ id+ "&attribute=currency_id&value=" +$(this).val(), function( data ){});
							            ',
							        ]
							    );?>
						    </div>
	                	</div>
	                </div>
	            </div>
	        </div>

        <?php
			}
		?>
	</div>

	<?php Pjax::end() ?> 
<?php } ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>