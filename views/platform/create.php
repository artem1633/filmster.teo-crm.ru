<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Platform */

?>
<div class="platform-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
