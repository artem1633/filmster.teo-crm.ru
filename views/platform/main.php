<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use app\models\PreferenceBooks;

/* @var $this yii\web\View */
/* @var $model app\models\Localization */
/* @var $form yii\widgets\ActiveForm */
$_csrf = \Yii::$app->request->getCsrfToken();
if (!file_exists('uploads/platform/'.$model->logo) || $model->logo == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/images/images.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/platform/'.$model->logo;
}

?>

<div class="row">

        <div class="col-md-3">
            <div id="logo_file<?=$model->id?>">
                <img style="width:250px; height:250px;"  src="<?=$path?>">
            </div>
            <br>
            <div id="logo<?=$model->id?>"></div>
            <button style="display:block;width:120px; height:30px;" name="button23<?=$model->id?>[]" id="button23<?=$model->id?>" onclick="document.getElementById('fileInput<?=$model->id?>').click()">Логотип 1 к 1</button>
            <input type="file" name="fileInput<?=$model->id?>[]" style="display:none" id="fileInput<?=$model->id?>" class="poster23_image<?=$model->id?>" accept="image/*" />
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label"><?=$model->getAttributeLabel('name_ru')?></label>
                        <input type="text" id="name_ru" class="form-control" name="Platform[name_ru]" value="<?=$model->name_ru?>" maxlength="255" aria-invalid="false" onchange="$.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute': 'name_ru', 'value':$('#name_ru').val()}, function(data){} );" >
                        <div class="help-block"></div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label"><?=$model->getAttributeLabel('name_eng')?></label>
                        <input type="text" id="name_eng" class="form-control" name="Platform[name_eng]" value="<?=$model->name_eng?>" maxlength="255" aria-invalid="false" onchange="$.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute': 'name_eng', 'value':$('#name_eng').val()}, function(data){} );" >
                        <div class="help-block"></div>
                    </div>               
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label"><?=$model->getAttributeLabel('description_ru')?></label>
                        <textarea id="description_ru" class="form-control" name="Platform[description_ru]" rows="2" aria-invalid="false" onchange="$.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute': 'description_ru', 'value':$('#description_ru').val()}, function(data){} );" ><?=$model->description_ru?></textarea>
                        <div class="help-block"></div>    
                    </div>
                </div>
                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label class="control-label"><?=$model->getAttributeLabel('description_eng')?></label>
                        <textarea id="description_eng" class="form-control" name="Platform[description_eng]" rows="2" aria-invalid="false" onchange="$.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute': 'description_eng', 'value':$('#description_eng').val()}, function(data){} );" ><?=$model->description_eng?></textarea>
                        <div class="help-block"></div>    
                    </div>
                </div>
            </div>

            <div class="row">
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'video')->dropdownList(PreferenceBooks::videoFormatList(),[]) ?>
            <label class="text-danger control-label">Формат видео</label>
            <?php 
                foreach (PreferenceBooks::videoFormatList() as $key => $value) 
                {
                    $video = explode(',', $model->video);
                    if(in_array($key, $video)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="video[]" value="<?=$key?>" 
                        onchange="
                            var checked = $(this).is(':checked');
                            if(checked == 0) {
                                $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'video', 'value': $(this).val(), 'checked' : 0 }, function(data){ } );
                            }
                            else $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'video', 'value': $(this).val(), 'checked' : 1 }, function(data){} );
                        " >
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'audio')->dropdownList(PreferenceBooks::audioFormatList(),[]) ?>
            <label class="text-danger control-label">Формат аудио</label>
            <?php 
                foreach (PreferenceBooks::audioFormatList() as $key => $value) 
                {
                    $audio = explode(',', $model->audio);
                    if(in_array($key, $audio)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="audio[]" value="<?=$key?>"
                    onchange="
                            var checked = $(this).is(':checked');
                            if(checked == 0) {
                                $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'audio', 'value': $(this).val(), 'checked' : 0 }, function(data){ } );
                            }
                            else $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'audio', 'value': $(this).val(), 'checked' : 1 }, function(data){} );
                        "
                    >
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'subtitr')->dropdownList(PreferenceBooks::sourceSubtitrList(),[]) ?>
            <label class="text-danger control-label">Субтитры</label>
            <?php 
                foreach (PreferenceBooks::sourceSubtitrList() as $key => $value) 
                {
                    $subtitr = explode(',', $model->subtitr);
                    if(in_array($key, $subtitr)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="subtitr[]" value="<?=$key?>"
                    onchange="
                            var checked = $(this).is(':checked');
                            if(checked == 0) {
                                $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'subtitr', 'value': $(this).val(), 'checked' : 0 }, function(data){ } );
                            }
                            else $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'subtitr', 'value': $(this).val(), 'checked' : 1 }, function(data){} );
                        "
                    >
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'sales_model')->dropdownList(PreferenceBooks::sourceSalesmodelList(),[]) ?>
            <label class="text-danger control-label">Модель продаж</label>
            <?php 
                foreach (PreferenceBooks::sourceSalesmodelList() as $key => $value) 
                {
                    $sales_model = explode(',', $model->sales_model);
                    if(in_array($key, $sales_model)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="sales_model[]" value="<?=$key?>"
                    onchange="
                            var checked = $(this).is(':checked');
                            if(checked == 0) {
                                $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'sales_model', 'value': $(this).val(), 'checked' : 0 }, function(data){ } );
                            }
                            else $.get('/platform/set-values', {'id':<?=$model->id?>, 'attribute' : 'sales_model', 'value': $(this).val(), 'checked' : 1 }, function(data){} );
                        "
                    >
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
        <div class="row">
            <br>
            <div class="col-md-12">
                    <label class="control-label"><?=$model->getAttributeLabel('countries')?></label>
                    <?= kartik\select2\Select2::widget([
                        'name' => 'countries',
                        'data' => $model->getCountryList(),
                        'value' => $model->getCountries(),
                        'size' => 'sm',
                        'options' => [ 'id' => 'countries'.$model->id, 'placeholder' => 'Выберите ...'],
                        'pluginOptions' => [
                            /*'tags' => true,*/
                            'multiple' => true,
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            "change" => "function() 
                            {
                                $.get('/platform/set-values', {'id' : ".$model->id.", 'attribute': 'countries', 'value' : JSON.stringify($(this).val()) }, function(data){} );
                            }",
                        ],
                    ])
                    ?>
            </div>
        </div>
        </div>
</div>


<?php 
$this->registerJs(<<<JS

	var fileCollection = new Array();

    /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#logo$model->id").click(function(){
            $("#fileInput$model->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput$model->id\']').on('change', fileUpload);
    });

    function fileUpload(event){
        //notify user about the file upload status
        $("#logo$model->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#logo$model->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$model->id}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/platform/set-logo', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#logo$model->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#logo$model->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }

    $(document).on('change', '.poster23_image$model->id', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:250px; height:250px;" src="'+e.target.result+'"> ';
                $('#logo_file$model->id').html('');
                $('#logo_file$model->id').append(template);
            };
        });
    });

JS
);
?>