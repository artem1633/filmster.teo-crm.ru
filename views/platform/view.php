<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Films */
CrudAsset::register($this);
$this->title = $model->name_ru;
?>
<h1>
    <span>
        <a class="btn btn-info" title="Список площадок" href="<?=Url::toRoute(['/platform/index'])?>"><i style="font-size: 20px;" class="fa fa-arrow-left"></i></a>
    </span>
    <?=$model->name_ru?>
</h1>
<div class="films-view" style="margin-top: 10px;">
    <div class="panel panel-default tabs">
        <ul class="nav nav-tabs nav-justified">
            <li ><a href="#tab1" data-toggle="tab" aria-expanded="false">Основное</a></li>
            <li class="active"><a href="#tab2" data-toggle="tab" aria-expanded="false">Настройка отчета</a></li>
        </ul>
        <div class="panel-body tab-content">
            <div class="tab-pane" id="tab1">
                <?= $this->render('main', [
                    'model' => $model,
                ]);?>
            </div>
            <div class="tab-pane active" id="tab2">
                <?= $this->render('report', [
                    'model' => $model,
                    'report' => $report,
                ]); ?>
            </div>              
        </div>
    </div>

</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>