<?php
use yii\helpers\Url;
use app\models\Platform;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    //[
        //'class' => 'kartik\grid\SerialColumn',
        //'width' => '30px',
    //],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "logo",
        'width' =>'90px',
        'content' => function ($data) {
            if($data->logo == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/images.jpg';
            else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/platform/' . $data->logo;

            return '<img style="width: 90px; height:90px; border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name_ru',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name_eng',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description_ru',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description_eng',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'video',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'audio',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'subtitr',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'sales_model',
    // ],
    //[
        //'class'=>'\kartik\grid\DataColumn',
        //'attribute'=>'countries',
        //'filter' => Platform::getCountryList(),
        //'content' => function($data){
           // return $data->countries->name;
        //}
    //],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['data-pjax'=>'0','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этот элемент?'], 
    ],

];   