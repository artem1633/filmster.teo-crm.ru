<?php
use yii\helpers\Html;
use app\models\PreferenceBooks;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Platform */
/* @var $form yii\widgets\ActiveForm */

$multiple = 0;

?>

<div class="platform-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-xs-6">
                <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
                <?= $form->field($model, 'name_eng')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-xs-6">
                <?= $form->field($model, 'description_ru')->textarea(['rows' => 3]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
                <?= $form->field($model, 'description_eng')->textarea(['rows' => 3]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'video')->dropdownList(PreferenceBooks::videoFormatList(),[]) ?>
            <label class="text-danger control-label">Формат видео</label>
            <?php 
                foreach (PreferenceBooks::videoFormatList() as $key => $value) 
                {
                    $video = explode(',', $model->video);
                    if(in_array($key, $video)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="video[]" value="<?=$key?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'audio')->dropdownList(PreferenceBooks::audioFormatList(),[]) ?>
            <label class="text-danger control-label">Формат аудио</label>
            <?php 
                foreach (PreferenceBooks::audioFormatList() as $key => $value) 
                {
                    $audio = explode(',', $model->audio);
                    if(in_array($key, $audio)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="audio[]" value="<?=$key?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'subtitr')->dropdownList(PreferenceBooks::sourceSubtitrList(),[]) ?>
            <label class="text-danger control-label">Субтитры</label>
            <?php 
                foreach (PreferenceBooks::sourceSubtitrList() as $key => $value) 
                {
                    $subtitr = explode(',', $model->subtitr);
                    if(in_array($key, $subtitr)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="subtitr[]" value="<?=$key?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-3">
                <?php // $form->field($model, 'sales_model')->dropdownList(PreferenceBooks::sourceSalesmodelList(),[]) ?>
            <label class="text-danger control-label">Модель продаж</label>
            <?php 
                foreach (PreferenceBooks::sourceSalesmodelList() as $key => $value) 
                {
                    $sales_model = explode(',', $model->sales_model);
                    if(in_array($key, $sales_model)) $checked = 'checked=""';
                    else $checked = '';
            ?>
            <div class="row">
                <div class="col-md-1 col-sm-1 col-xs-1">
                    <input type="checkbox" <?=$checked?> name="sales_model[]" value="<?=$key?>">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <label class="control-label"><?=$value?></label>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <br>
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'countries')->widget(kartik\select2\Select2::classname(), [
                    'data' => $model->getCountryList(),
                    'options' => ['placeholder' => 'Выберите ...'],
                    'size' => kartik\select2\Select2::SMALL,
                    'pluginOptions' => [
                        'multiple' => true,
                        'allowClear' => true,
                    ],
                ])?>
        </div>
        <div class="col-md-6 col-xs-6">
            <div class="col-md-12 col-xs-12">
                <div id="images-to-upload">
                    <?= $model->logo != null ? '<img style="width:200px;height200px;" src="http://' . $_SERVER["SERVER_NAME"] . "/uploads/platform/" . $model->logo .' ">' : '' ?>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                    <?= $form->field($model, 'file')->fileInput(['class'=>"btn_image"]); ?>
            </div>
        </div>
    </div>
</div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php 
$this->registerJs(<<<JS

$(document).ready(function(){
    var fileCollection = new Array();

    $(document).on('change', '.btn_image', function(e){
        var files = e.target.files;
        var button_id = $(this).attr("id");
        //alert(button_id);
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:200px;height:200px;" src="'+e.target.result+'"> ';
                $('#images-to-upload').html('');
                //document.getElementById('theID').value = 'new value';
                $('#images-to-upload').append(template);
            };
        });
    });

});
JS
);
?>

