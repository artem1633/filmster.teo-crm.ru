<?php

namespace app\controllers;

use Yii;
use app\models\TechnicalInformation;
use app\models\TechnicalInformationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Treyler;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\Platform;
use app\models\TechnicalPlaygrounds;
use app\models\Films;

/**
 * TechnicalInformationController implements the CRUD actions for TechnicalInformation model.
 */
class TechnicalInformationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }
   
    /**
     * Creates a new TechnicalInformation model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTreyler($technical_information_id)
    {
        $request = Yii::$app->request;
        $model = new Treyler();
        $model->technical_information_id = $technical_information_id;
        $number = Treyler::getLastId(); 
        Directory::createTreylerDirectory($number);
        $model->save();
    }

    //Загрузить файл на сервер
    public function actionSendDocument($file)
    {
        return \Yii::$app->response->sendFile('uploads/' . $file);
    }

    public function actionSetValues($id, $attribute, $value)
    {
        $model = $this->findModel($id);

        if($attribute == 'video') $model->video = $value;
        if($attribute == 'audio') $model->audio = $value;
        if($attribute == 'duration') {
            $model->duration = $value;
            Yii::$app->db->createCommand()->update('films', ['duration' => $value ], [ 'id' => $model->film_id ])->execute();
        }
        if($attribute == 'playback_speed') $model->playback_speed = $value;
        if($attribute == 'video_language') $model->video_language = $value;
        if($attribute == 'audio_language') $model->audio_language = $value;
        if($attribute == 'source_availability') $model->source_availability = $value;
        if($attribute == 'source_link') $model->source_link = $value;
        if($attribute == 'burnt_subtitles') $model->burnt_subtitles = $value;
        if($attribute == 'forced_subtitles') $model->forced_subtitles = $value;
        if($attribute == 'subtitles') $model->subtitles = $value;
        if($attribute == 'sdh_subtitles') $model->sdh_subtitles = $value;
        if($attribute == 'presense_cc') $model->presense_cc = $value;
        if($attribute == 'burnt_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->burnt_value = $result;
        }
        if($attribute == 'forced_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->forced_value = $result;
        }
        if($attribute == 'subtitles_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->subtitles_value = $result;
        }
        if($attribute == 'sdh_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->sdh_value = $result;
        }
        if($attribute == 'presense_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->presense_value = $result;
        }
        $model->save();
    }

    public function actionSetPlayground($id, $value)
    {
        $technicalPlayground = TechnicalPlaygrounds::findOne($id);
        $technicalPlayground->playground_id = $value;
        $technicalPlayground->save();
    }

    public function actionSetValuesToTechnicalPlayground($id, $attribute, $value)
    {
        $technicalPlayground = TechnicalPlaygrounds::findOne($id);
        if($attribute == 'value_playground') $technicalPlayground->value_playground = $value;
        if($attribute == 'additional_value') $technicalPlayground->additional_value = $value;
        if($attribute == 'territory') $technicalPlayground->territory = $value;
        $technicalPlayground->save();
    }

    public function actionCreatePlayground($technical_id)
    {
        $technicalPlayground = new TechnicalPlaygrounds();
        $technicalPlayground->technical_id = $technical_id;
        $playgrounds = Platform::find()->all();
        foreach ($playgrounds as $value) {
            $technicalPlayground->playground_id = $value->id; 
            break;
        }
        $technicalPlayground->save();
    }

    public function actionDeletePlayground($id)
    {
        $technicalPlayground = TechnicalPlaygrounds::findOne($id);
        $technicalPlayground->delete();
    }

    public function actionSetFile()
    {
        if(isset($_POST) == true){
            $fileName = basename($_FILES["file"]["name"]);
            $id = $_POST['id'];
            $targetDir = "uploads/technical-information/" . $id. '/';
            $targetFilePath = $targetDir . $fileName;
            
            if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                $response['status'] = 'ok';
            }else{
                $response['status'] = 'err';
            }
            return json_encode($response);
        }
    }

    public function actionSetFilesValue($id, $attribute, $value)
    {
        $model = $this->findModel($id);
        if($attribute == 'forced_files') $model->forced_file = $value;
        if($attribute == 'subtitles_file') $model->subtitles_file = $value;
        if($attribute == 'sdh_file') $model->sdh_file = $value;
        if($attribute == 'presense_file') $model->presense_file = $value;
        $model->save();
    }

    public function actionSetTreyler($id, $attribute, $value)
    {
        $model = Treyler::findOne($id);
        if($attribute == 'video') $model->video = $value;
        if($attribute == 'audio') $model->audio = $value;
        if($attribute == 'duration') $model->duration = $value;
        if($attribute == 'playback_speed') $model->playback_speed = $value;
        if($attribute == 'video_language') $model->video_language = $value;
        if($attribute == 'audio_language') $model->audio_language = $value;
        if($attribute == 'source_availability') $model->source_availability = $value;
        if($attribute == 'source_link') $model->source_link = $value;
        if($attribute == 'burnt_subtitles') $model->burnt_subtitles = $value;
        if($attribute == 'forced_subtitles') $model->forced_subtitles = $value;
        if($attribute == 'subtitles') $model->subtitles = $value;
        if($attribute == 'sdh_subtitles') $model->sdh_subtitles = $value;
        if($attribute == 'presense_cc') $model->presense_cc = $value;
        if($attribute == 'burnt_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->burnt_value = $result;
        }
        if($attribute == 'forced_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->forced_value = $result;
        }
        if($attribute == 'subtitles_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->subtitles_value = $result;
        }
        if($attribute == 'sdh_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->sdh_value = $result;
        }
        if($attribute == 'presense_value') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->presense_value = $result;
        }
        $model->save();
    }

    public function actionSetTreylerFile()
    {
        if(isset($_POST) == true){
            $fileName = basename($_FILES["file"]["name"]);
            $id = $_POST['id'];
            $targetDir = "uploads/treyler/" . $id. '/';
            $targetFilePath = $targetDir . $fileName;
            
            if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                $response['status'] = 'ok';
            }else{
                $response['status'] = 'err';
            }
            return json_encode($response);
        }
    }

    public function actionSetTreylerFilesValue($id, $attribute, $value)
    {
        $model = Treyler::findOne($id);
        if($attribute == 'forced_file') $model->forced_file = $value;
        if($attribute == 'subtitles_file') $model->subtitles_file = $value;
        if($attribute == 'sdh_file') $model->sdh_file = $value;
        if($attribute == 'presense_file') $model->presense_file = $value;
        $model->save();
    }

    public function actionImportValues($film_id, $id, $playground_id)
    {
        sleep(2);
        $request = Yii::$app->request;
        $model = Films::findOne($film_id);
        $territory = TechnicalPlaygrounds::find()->where(['id' => $playground_id])->one()->territory;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $homepage = file_get_contents("https://itunes.apple.com/lookup?id={$id}&country={$territory}");
            if(isset(json_decode($homepage)->results[0])){

                $json = json_decode($homepage)->results[0];
                $html = Yii::$app->multiparser->init($json->trackViewUrl);
                $model->setImportValues($json, $html);
                return $this->redirect(['/films/view', 'id' => $film_id]);
                /*return [
                    'forceReload'=>'#main-pjax',
                    'title'=> "Загрузка",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => json_decode($homepage)->results[0],
                    ]),
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary','data-dismiss'=>"modal"])      
                ];*/

            }
            else{
                return [
                    'forceReload'=>'#main-pjax',
                    'title'=> "Ошибка",
                    'size' => 'normal',
                    'content'=>"<span class='text-danger'>Ошибка! Территория или Доп. значение площадки неверно</span>",
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary','data-dismiss'=>"modal"])      
                ];
            }
        }else{

            $homepage = file_get_contents("https://itunes.apple.com/lookup?id={$id}&country={$territory}");
            if(isset(json_decode($homepage)->results[0])){

                $json = json_decode($homepage)->results[0];
                $html = Yii::$app->multiparser->init($json->trackViewUrl);
                $model->setImportValues($json, $html);
                //return $this->redirect(['/films/view', 'id' => $film_id]);
                return [
                    'forceReload'=>'#main-pjax',
                    'title'=> "Загрузка",
                    'size' => 'normal',
                    'content'=>"<span class='text-danger'Ууспешно выполнено</span>",
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary','data-dismiss'=>"modal"])      
                ];
                /*return [
                    'forceReload'=>'#main-pjax',
                    'title'=> "Загрузка",
                    'size' => 'large',
                    'content'=>$this->renderAjax('view', [
                        'model' => json_decode($homepage)->results[0],
                    ]),
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary','data-dismiss'=>"modal"])      
                ];*/

            }
            else{
                /*return [
                    'forceReload'=>'#main-pjax',
                    'title'=> "Ошибка",
                    'size' => 'normal',
                    'content'=>"<span class='text-danger'>Ошибка! Территория или Доп. значение площадки неверно</span>",
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary','data-dismiss'=>"modal"])      
                ];*/
            }
        }
    }
    
    /**
     * Delete an existing TechnicalInformation model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteTreyler($id)
    {
        $request = Yii::$app->request;
        Treyler::findOne($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#technical-pjax'];
    }

    /**
     * Finds the TechnicalInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TechnicalInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TechnicalInformation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
