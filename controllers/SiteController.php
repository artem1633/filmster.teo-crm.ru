<?php

namespace app\controllers;

use app\components\iTunesSalesApi;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\Users;
use app\models\Questionary;
use app\modules\api\controllers\BotinfoController;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/questionary/questions', 'user_id' => Yii::$app->user->identity->id]);
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    public function actionTest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $reporter =  new iTunesSalesApi();

        $reporter->setAccessToken("6bf130ac-a509-424d-aa24-a392613ca223")->setVendor('86908296');

        $reporter->throwErrors = false;
//
//        if($accounts = $reporter->getAccounts()){
//            //Do something with data your're good to go
//            print_r(json_encode($accounts));
//        }
//        else{
//            echo "Api  Errors : ".$reporter->getErrorsAsString();
//        }

        $accounts = $reporter->getAccounts();

//Example with first account returned from list
//account is numeric but setAccount parses raw return from the getAccounts() method
        $reporter->setAccount($accounts[0]);


        $lastDayTime = time() - 86400;
        $lastDay = date('Ymd', $lastDayTime);


        if($data = $reporter->getSalesDailyReport($lastDay)) { //Will get last year by default
            //Do something with data your're good to go

            $output = [];

            $data = json_decode(json_encode($data), true);

            $details = $data['details'];

            foreach ($details as $detail)
            {
                $output[] = [
                    'apple_identifier' => $detail['Apple Identifier'],
                    'vendor_identifier' => $detail['Vendor Identifier'],
                    'title' => $detail['Title'],
                    'sales' => 0,
                    'type' => ($detail['Product Type Identifier'] == 'M' ? 'EST' : 'VOD'),
                    'customer_price' => $detail['Customer Price'],
                    'customer_currency' => $detail['Customer Currency'],
                    'country' => $detail['Country Code'],
                    'format' => $detail['Asset/Content Flavor'],
                    'genre' => $detail['Primary Genre'],
                    'begin_date' => $detail['Begin Date'],
                    'date' => date('Y-m-d', $lastDayTime),
                ];
            }

            return $output;
        }
        else{
            echo "Api  Errors : ".$reporter->getErrorsAsString();
        }
    }


    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if(isset(Yii::$app->user->identity->id))
        {
            return $this->render('error');
        }
        else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if($session['body'] == null | $session['body'] == 'small') $session['body'] = 'large';
        else $session['body'] = 'small';

        if($session['left'] == null | $session['left'] == 'small') $session['left'] = 'large';
        else $session['left'] = 'small';

    }
}
