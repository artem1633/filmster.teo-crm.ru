<?php

namespace app\controllers;

use Yii;
use app\models\Films;
use app\models\FilmsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Country;
use app\models\Genres;
use app\models\Currency;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\LocalizationSearch;
use app\models\TechnicalInformationSearch;
use app\models\TechnicalInformation;
use app\models\PreferenceBooks;
use app\models\FilmsApplication;
use app\models\TechnicalPlaygrounds;
use yii\data\ActiveDataProvider;
use app\models\RightHolders;
use app\models\Contract;
use app\models\Applications;


/**
 * FilmsController implements the CRUD actions for Films model.
 */
class FilmsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Films models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new FilmsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPars()
    {
        /*$url = 'https://itunes.apple.com/ru/movie/%D0%BB%D1%8E%D0%B1%D0%BE%D0%B2%D1%8C-%D0%B2-%D0%B1%D0%BE%D0%BB%D1%8C%D1%88%D0%BE%D0%BC-%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B5/id1142311369?ign-mpt=uo%3D4';

        $html = \garyjl\simplehtmldom\SimpleHTMLDom::file_get_html($url);

        $html = Yii::$app->multiparser->init($url);
        foreach($html->find('.cast-list__detail a') as $key => $value) 
        {
            $title_body = 'data-metrics-click';
            $actorType = Films::getActorType($value->$title_body);
            $actorName = preg_replace('/<a[^>]*?>([\\s\\S]*?)<\/a>/','\\1', $value);
            $actorName = substr($actorName, 23, strlen($actorName) - 23 );
            $actorName = substr($actorName, 0, strlen($actorName) - 23 );
            echo "{$actorType} = ";
            echo $actorName."\r\n";
        }

        return $this->redirect(['/films/view', 'id' => 10]);*/
        
        /*$url = 'https://itunes.apple.com/ru/movie/%D0%BB%D1%8E%D0%B1%D0%BE%D0%B2%D1%8C-%D0%B2-%D0%B1%D0%BE%D0%BB%D1%8C%D1%88%D0%BE%D0%BC-%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D0%B5/id1142311369?ign-mpt=uo%3D4';  

        $html = Yii::$app->multiparser->init($url);
        foreach($html->find('.cast-list__detail a') as $key => $value) 
        {
            $title_body = 'data-metrics-click';
            $actorType = Films::getActorType($value->$title_body);
            $actorName = preg_replace('/<a[^>]*?>([\\s\\S]*?)<\/a>/','\\1', $value);
            $actorName = substr($actorName, 23, strlen($actorName) - 23 );
            $actorName = substr($actorName, 0, strlen($actorName) - 23 );
            echo "{$actorType} = ";
            echo $actorName."\r\n";
        }

         return $this->redirect(['/films/view', 'id' => 10]);*/
    }


    /**
     * Displays a single Films model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    { 
        $localizationModel = new LocalizationSearch();
        $localizationProvider = $localizationModel->search(Yii::$app->request->queryParams,$id);

        $technicalModel = new TechnicalInformationSearch();
        $technicalProvider = $technicalModel->search(Yii::$app->request->queryParams,$id);
        $technical = TechnicalInformation::find()->where(['film_id' => $id])->one();

        $filmsApplication = FilmsApplication::find()->where(['film_id' => $id])->all();
            
        /*Yii::$app->session['films_id'] = $id;*/
        return $this->render('view', [
            'model' => $this->findModel($id),
            'localizationProvider' => $localizationProvider,
            'technicalProvider' => $technicalProvider,
            'technical' => $technical,
            'filmsApplication' => $filmsApplication,
        ]);        
    }

    /**
     * Creates a new Films model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Films();  

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){

            $post = $request->post();
            $string = '';
            $i = 0;
            foreach ($post['Films']['tags'] as $key => $value) {
                if($i == 0) $string .= $value;
                else $string .=  ',' . $value;
                $i++;
            }
            $model->tags = $string;

            $result = [];
            foreach ($post['Films']['genres'] as $value) {
                if($value != ''){
                    $result [] = [
                        'value' => $value
                    ];
                }
            }
            $model->genres = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['summa'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'currency' => $post['currency'][$i],
                        'summa' => $value,
                    ];
                }
            }
            $model->budget = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['country'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'youth' => $post['youth'][$i],
                        'country' => $value,
                    ];
                }
            }
            $model->reyting = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['festival'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'prize' => $post['prize'][$i],
                        'festival' => $value,
                    ];
                }
            }
            $model->awards = json_encode($result);

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }else{           
            return [
                'title'=> "Создать",
                'size' => 'large',
                'content'=>$this->renderAjax('create', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }       
    }

    /**
     * Updates an existing Films model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($model->rellase_date != null) $model->rellase_date = date('d/m/Y', strtotime($model->rellase_date));
        $result = [];
        foreach (json_decode($model->genres) as $value) {
            $result [] = $value->value;
        }

        $model->genres = $result;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){

            $post = $request->post();
            $string = '';
            $i = 0;
            foreach ($post['Films']['tags'] as $key => $value) {
                if($i == 0) $string .= $value;
                else $string .=  ',' . $value;
                $i++;
            }
            $model->tags = $string;

            $result = [];
            foreach ($post['Films']['genres'] as $value) {
                if($value != ''){
                    $result [] = [
                        'value' => $value
                    ];
                }
            }
            $model->genres = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['summa'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'currency' => $post['currency'][$i],
                        'summa' => $value,
                    ];
                }
            }
            $model->budget = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['country'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'youth' => $post['youth'][$i],
                        'country' => $value,
                    ];
                }
            }
            $model->reyting = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['festival'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'prize' => $post['prize'][$i],
                        'festival' => $value,
                    ];
                }
            }
            $model->awards = json_encode($result);

            $model->save();
            return [
                'forceReload'=>'#main-pjax',
                'title'=> "Фильмы",
                'size' => 'large',
                'forceClose'=>true,
            ];    
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('update', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];        
        }
    }

    public function actionGetCurrency()
    {
        $currency = Currency::find()->all();
        $result = '';
        foreach ($currency as $value) {
            $result .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $result;
    }

    public function actionGetCountry()
    {
        $country = Country::find()->all();
        $result = '';
        foreach ($country as $value) {
            $result .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $result;
    }

    public function actionSetValues($id, $attribute, $value)
    {
        $model = $this->findModel($id);
        if($attribute == 'name') $model->name = $value;
        if($attribute == 'type_id') $model->type_id = $value;
        if($attribute == 'language_id') $model->language_id = $value;
        if($attribute == 'number_season') $model->number_season = $value;
        if($attribute == 'number_seriya') $model->number_seriya = $value;
        if($attribute == 'alternative') $model->alternative = $value;
        if($attribute == 'year_issue') $model->year_issue = $value;
        if($attribute == 'country_id') $model->country_id = $value;
        if($attribute == 'genres') $model->genres = $value;
        if($attribute == 'manufacturer_id') $model->manufacturer_id = $value;
        if($attribute == 'duration') $model->duration = $value;
        if($attribute == 'actuality_id') $model->actuality_id = $value;
        if($attribute == 'tags') $model->tags = $value;
        if($attribute == 'certificate') $model->certificate = $value;
        if($attribute == 'hide_film') $model->hide_film = $value;
        if($attribute == 'description') $model->description = $value;
        if($attribute == 'rellase_date') 
        {  
            $array = explode('-', $value);
            $m = PreferenceBooks::monthCode($array[0]);
            $data = $array[1].'-'.$m. '-01';
            $model->rellase_date = $data;
        }
        $model->save();
    }

    public function actionSetGenres($val, $id)
    {
        $result = [];
        foreach (json_decode($val) as $value) {
            if($value != ''){
                $result [] = [
                    'value' => $value
                ];
            }
        }

        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }
         
        $model = $this::findModel($numeric);
        $model->genres = json_encode($result);
        $model->save();
    }

   public function actionSetBudget()
    {
        $id = $_POST['id'];
        $summa = $_POST['summa'];
        $currency = $_POST['currency'];
        $model = $this->findModel($id);

        $result = []; $i = -1;
        foreach ($summa as $value) {
            $i++;
                $result [] = [
                    'currency' => $currency[$i],
                    'summa' => $value,
                ];

        }
        $model->budget = json_encode($result);
        $model->save();
    }

     public function actionSetReyting()
    {
        $id = $_POST['id'];
        $country = $_POST['country'];
        $youth = $_POST['youth'];
        $model = $this->findModel($id);

        $result = []; $i = -1;
        foreach ($country as $value) {
            $i++;
                $result [] = [
                    'youth' => $youth[$i],
                    'country' => $value,
                ];

        }
        $model->reyting = json_encode($result);
        $model->save();
    }
     public function actionSetAward()
    {
        $id = $_POST['id'];
        $festival = $_POST['festival'];
        $prize = $_POST['prize'];
        $model = $this->findModel($id);

        $result = []; $i = -1;
        foreach ($festival as $value) {
            $i++;
                $result [] = [
                    'prize' => $prize[$i],
                    'festival' => $value,
                ];

        }
        $model->awards = json_encode($result);
        $model->save();
    }


    public function actionGetYouth()
    {
        $youth = PreferenceBooks::youthList();
        $result = '';
        foreach ($youth as $key => $value) {
            $result .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $result;
    }

    /**
     * Delete an existing Films model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Films model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Films model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Films the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Films::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport()
    {
        $request = Yii::$app->request;
        $model = new Films();  

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->post() && $model->load($request->post())){
            $model->files = UploadedFile::getInstance($model, 'files');
            if(!empty($model->files))
            {
                $fileName = 'Import.'.$model->files->extension;
                $model->files->saveAs($fileName);
                
                $csvData = \PHPExcel_IOFactory::load($fileName);
                $totalCount = count($csvData->getActiveSheet()->toArray(null, true, true, false));

                $allValues = [];
                for ($i = 5; $i <= $totalCount; $i++) 
                {
                    $allValues [] = [
                        'B' => (string)$csvData->getActiveSheet()->getCell('B' . $i)->getValue(),
                        'D' => (string)$csvData->getActiveSheet()->getCell('D' . $i)->getValue(),
                        'E' => (string)$csvData->getActiveSheet()->getCell('E' . $i)->getValue(),
                        'G' => (string)$csvData->getActiveSheet()->getCell('G' . $i)->getValue(),
                        'H' => (string)$csvData->getActiveSheet()->getCell('H' . $i)->getValue(),
                    ];
                }

                foreach ($allValues as $value)
                {
                    $film_name = $value['B'];
                    if($film_name !== null || $film_name !== '')
                    {
                        $film = Films::find()->where(['name' => $film_name])->one();
                        if($film === null){
                            $film = new Films();
                            $film->name = $film_name;
                            $film->type_id = 2;
                            $film->save();
                        }

                        $technicalInform = TechnicalInformation::find()->where(['film_id' => $film->id])->one();
                        if($technicalInform !== null)
                        {
                            /*ITunes*/
                            $techPlayground = TechnicalPlaygrounds::find()->where(['playground_id' => 1, 'technical_id' => $technicalInform->id])->one();
                            if($techPlayground === null) {
                                $techPlayground = new TechnicalPlaygrounds();
                                $techPlayground->technical_id = $technicalInform->id;
                                $techPlayground->playground_id = 1;
                            }
                            $techPlayground->value_playground = $value['D'];
                            $techPlayground->additional_value = $value['E'];
                            $techPlayground->save();

                            /*Google*/
                            $techPlayground = TechnicalPlaygrounds::find()->where(['playground_id' => 2, 'technical_id' => $technicalInform->id])->one();
                            if($techPlayground === null) {
                                $techPlayground = new TechnicalPlaygrounds();
                                $techPlayground->technical_id = $technicalInform->id;
                                $techPlayground->playground_id = 2;
                            }
                            $techPlayground->value_playground = $value['D'];
                            $techPlayground->additional_value = $value['G'];
                            $techPlayground->save();

                            /*YouTube*/
                            $techPlayground = TechnicalPlaygrounds::find()->where(['playground_id' => 3, 'technical_id' => $technicalInform->id])->one();
                            if($techPlayground === null) {
                                $techPlayground = new TechnicalPlaygrounds();
                                $techPlayground->technical_id = $technicalInform->id;
                                $techPlayground->playground_id = 3;
                            }
                            $techPlayground->value_playground = null;
                            $val = str_replace('https://youtu.be/','', $value['H']);
                            $techPlayground->additional_value = $val;
                            $techPlayground->save();
                        }
                    }
                }
                return [
                    'title'=> "Импорт",
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'content'=> 'Успешно выполнено',
                ];
            }
        }else{           
            return [
                'title'=> "Импорт",
                'size' => 'normal',
                'content'=>$this->renderAjax('_import_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }       
    }

    public function actionCarry()
    {
        $request = Yii::$app->request;
        $model = new Films();  

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->post() && $model->load($request->post())){
            $model->files = UploadedFile::getInstance($model, 'files');
            if(!empty($model->files))
            {
                $fileName = 'Import.'.$model->files->extension;
                $model->files->saveAs($fileName);
                $result = $model->excel_to_array($fileName, $model->sheet_number - 1);
                $model->setValues($result, $model->sheet_number);
                
                return [
                    'title'=> "Перенос БД",
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'large',
                    'content'=> 'Успешно выполнено',
                    /*'content'=>$this->renderAjax('result', [
	                    'result' => $result,
                    ]),*/
                ];
            }
        }else{           
            return [
                'title'=> "Перенос БД",
                'size' => 'normal',
                'content'=>$this->renderAjax('_carry_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }       
    }
 
    public function actionEndAccess()
    {   
        $identity = Yii::$app->user->identity;
        if($identity->type == 0){
            $query = FilmsApplication::find()->where(['between', 'close_date', date('Y-m-d'), date('Y-m-d', strtotime("+90 day")) ]);
        }
        else{

            $rightHolders = RightHolders::find()->where(['user_id' => $identity->id])->all();
            $idList = [];
            foreach ($rightHolders as $value) {
                $idList [] = $value->id;
            }

            $contracts = Contract::find()->where(['or',
                ['company_lic' => $idList],
                ['company_cont' => $idList]
            ])->all();

            $applicationId = [];
            foreach ($contracts as $value) {
                $app = Applications::find()->where(['contract_id' => $value->id])->all();
                foreach ($app as $ap) {
                    $applicationId [] = $ap->id;
                }
            }

            $query = FilmsApplication::find()
            ->where(['between', 'close_date', date('Y-m-d'), date('Y-m-d', strtotime("+90 day")) ])
                ->andWhere(['application_id' => $applicationId]);
           
        }

        // $query = FilmsApplication::find()->where(['between', 'close_date', date('Y-m-d'), date('Y-m-d', strtotime("+90 day")) ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('end', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
}
