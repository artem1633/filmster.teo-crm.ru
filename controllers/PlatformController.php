<?php

namespace app\controllers;

use Yii;
use app\models\Platform;
use app\models\PlatformSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\ReportSetting;
use app\models\Conditions;
use app\models\ConditionColumns;

/**
 * PlatformController implements the CRUD actions for Platform model.
 */
class PlatformController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Platform models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new PlatformSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Platform model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $report = ReportSetting::find()->where(['platform_id' => $id])->one();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'report' => $report,
        ]);
    }

    /**
     * Creates a new Platform model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Platform();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $post = $request->post();

                $i = 0;
                $video = '';
                foreach ($post['video'] as $value) {
                    if($i == 0) $video = $value;
                    else $video .= ',' . $value;
                    $i++;
                }
                $model->video = $video;

                $i = 0;
                $audio = '';
                foreach ($post['audio'] as $value) {
                    if($i == 0) $audio = $value;
                    else $audio .= ',' . $value;
                    $i++;
                }
                $model->audio = $audio;

                $i = 0;
                $subtitr = '';
                foreach ($post['subtitr'] as $value) {
                    if($i == 0) $subtitr = $value;
                    else $subtitr .= ',' . $value;
                    $i++;
                }
                $model->subtitr = $subtitr;

                $i = 0;
                $sales_model = '';
                foreach ($post['sales_model'] as $value) {
                    if($i == 0) $sales_model = $value;
                    else $sales_model .= ',' . $value;
                    $i++;
                }
                $model->sales_model = $sales_model;

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/platform/'.$model->file->baseName.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('platform', ['logo' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                }

                $result = [];
                foreach ($post['Platform']['countries'] as $value) {
                    if($value != ''){
                        $result [] = [
                            'value' => $value
                        ];
                    }
                }
                $model->countries = json_encode($result);
                $model->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                $model->logo = UploadedFile::getInstance($model, 'logo');
                if(!empty($model->logo))
                {
                    $model->logo->saveAs('uploads/platform/'.$model->logo->baseName.'.'.$model->logo->extension);
                    Yii::$app->db->createCommand()->update('platform', ['logo' => $model->logo->baseName.'.'.$model->logo->extension], [ 'id' => $model->id ])->execute();
                }

                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Platform model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);    
        
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                $post = $request->post();

                $i = 0;
                $video = '';
                foreach ($post['video'] as $value) {
                    if($i == 0) $video = $value;
                    else $video .= ',' . $value;
                    $i++;
                }
                $model->video = $video;

                $i = 0;
                $audio = '';
                foreach ($post['audio'] as $value) {
                    if($i == 0) $audio = $value;
                    else $audio .= ',' . $value;
                    $i++;
                }
                $model->audio = $audio;

                $i = 0;
                $subtitr = '';
                foreach ($post['subtitr'] as $value) {
                    if($i == 0) $subtitr = $value;
                    else $subtitr .= ',' . $value;
                    $i++;
                }
                $model->subtitr = $subtitr;

                $i = 0;
                $sales_model = '';
                foreach ($post['sales_model'] as $value) {
                    if($i == 0) $sales_model = $value;
                    else $sales_model .= ',' . $value;
                    $i++;
                }
                $model->sales_model = $sales_model;

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file))
                {
                    $model->file->saveAs('uploads/platform/'.$model->file->baseName.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('platform', ['logo' => $model->file->baseName.'.'.$model->file->extension], [ 'id' => $model->id ])->execute();
                }

                $result = [];
                foreach ($post['Platform']['countries'] as $value) {
                    if($value != ''){
                        $result [] = [
                            'value' => $value
                        ];
                    }
                }
                $model->countries = json_encode($result);
                $model->save();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Площадки",
                    'forceClose'=>true,
                ];    
            }else{
                $result = [];
                foreach (json_decode($model->countries) as $value) {
                    $result [] = $value->value;
                }

                $model->countries = $result;
                 return [
                    'title'=> "Изменить",
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
    }


    public function actionSetValues($id, $attribute, $value, $checked = null)
    {
        $model = $this->findModel($id);
        if($attribute == 'name_ru') $model->name_ru = $value;
        if($attribute == 'name_eng') $model->name_eng = $value;
        if($attribute == 'description_ru') $model->description_ru = $value;
        if($attribute == 'description_eng') $model->description_eng = $value;
        if($attribute == 'video') {
            if($checked == 1) $model->video = $model->video . ',' . $value;
            if($checked == 0) $model->video = $model->getValues($model->video, $value);
        }
        if($attribute == 'audio') {
            if($checked == 1) $model->audio = $model->audio . ',' . $value;
            if($checked == 0) $model->audio = $model->getValues($model->audio, $value);
        }
        if($attribute == 'subtitr') {
            if($checked == 1) $model->subtitr = $model->subtitr . ',' . $value;
            if($checked == 0) $model->subtitr = $model->getValues($model->subtitr, $value);
        }
        if($attribute == 'sales_model') {
            if($checked == 1) $model->sales_model = $model->sales_model . ',' . $value;
            if($checked == 0) $model->sales_model = $model->getValues($model->sales_model, $value);
        }
        if($attribute == 'countries') {
            $result = ''; $i = 0;
            foreach (json_decode($value) as $key => $value) {
                if($value != null && $value != '' ){
                    if($i == 0) $result .= $value;
                    else $result .=  ',' . $value;
                    $i++;
                }
            }
            $model->countries = $result;
        }
        $model->save();
    }

    public function actionSetLogo()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $id = $_POST['id'];
            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            //$fileName = $id.basename($_FILES["file"]["name"]);
            $fileName = $id. '.' .$ext;
            
            //file upload path
            $targetDir = "uploads/platform/";
            $targetFilePath = $targetDir . $fileName;
            
            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('jpg','png','jpeg','gif');
            
            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $model = $this->findModel($id);
                    $model->logo = $fileName;
                    $model->save();
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }
            
            //render response data in JSON format
            return json_encode($response);
        }
    }

    public function actionSetReportSetting($id, $attribute, $value)
    {
        $model = ReportSetting::findOne($id);
        if($attribute == 'begin_parse') $model->begin_parse = $value;
        if($attribute == 'end_parse') $model->end_parse = $value;
        $model->save();
    }

    public function actionCreateCondition($report_setting_id)
    {
        $request = Yii::$app->request;
        $model = new Conditions();
        $model->report_setting_id = $report_setting_id;
        $model->action_id = 1;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->save()){
            return [
                'forceReload'=>'#report-pjax',
                'title'=> "Создать",
                'content'=> 'Успешно выполнено',
            ];         
        }else{           
            return [
                'title'=> "<span style='color:red;'>Ошибка!</span>",
                'content'=> 'Не создано новая условия',
            ];         
        }       
    }

    public function actionSetConditionColumn($id, $attribute, $value)
    {
        $model = ConditionColumns::findOne($id);
        if($attribute == 'column_name') $model->column_name = $value;
        if($attribute == 'condition_name') $model->condition_name = $value;
        if($attribute == 'value') $model->value = $value;
        $model->save();
    }

    public function actionSetCondition($id, $attribute, $value)
    {
        $model = Conditions::findOne($id);
        if($attribute == 'action_id') 
        {
            $model->action_id = $value;
            if($model->action_id == 3 && $model->sale_id === null) $model->sale_id = 1;
        }
        if($attribute == 'film_id') $model->film_id = $value;
        if($attribute == 'sale_id') $model->sale_id = $value;
        if($attribute == 'date_begin') $model->date_begin = date('Y-m-d', strtotime($value));
        if($attribute == 'date_end') $model->date_end = date('Y-m-d', strtotime($value));
        if($attribute == 'currency_id') $model->currency_id = $value;
        $model->save();
    }

    public function actionDeleteCondition($id)
    {
        $model = Conditions::findOne($id);
        $request = Yii::$app->request;
        $model->delete();
    }

    public function actionCreateColumn($condition_id)
    {
        $request = Yii::$app->request;
        $column = new ConditionColumns(); 
        $column->condition_id = $condition_id;
        $column->condition_name = 1;
        $column->save();      
    }

    public function actionDeleteColumn($id)
    {
        $model = ConditionColumns::findOne($id);
        $request = Yii::$app->request;
        $model->delete();
    }

    /**
     * Delete an existing Platform model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Platform model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Platform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Platform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Platform::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
