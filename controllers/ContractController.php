<?php

namespace app\controllers;

use app\models\Films;
use app\models\forms\ApplicationCopyForm;
use Yii;
use app\models\Contract;
use app\models\ContractSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Applications;
use app\models\FilmsApplication;
use app\models\Country;
use app\models\Platform;
use app\models\RightHolders;

/**
 * ContractController implements the CRUD actions for Contract model.
 */
class ContractController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Contract models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        Contract::getQueryContract($request->post());
        $user = Yii::$app->user->identity->id;
        $litsensiyat = RightHolders::find()->where(['user_id'=>$user])->one();
        
        $searchModel = new ContractSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Contract model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $session = Yii::$app->session;
        FilmsApplication::getQueryAllLists($request->post());

        $model = $this->findModel($id);
        $applications = Applications::find()->where(['contract_id' => $id])->all();

        return $this->render('view', [
            'model' => $model,
            'applications' => $applications,
        ]);
    }

    /**
     * Creates a new Contract model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Contract();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return $this->redirect(['/contract/view', 'id' => $model->id]);
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Creates a new Applications model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateApplication($contract_id)
    {
        $request = Yii::$app->request;
        $model = new Applications();
        $model->contract_id = $contract_id;
        $model->date_cr = date('d/m/Y');

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            return [
                'title'=> "Создать",
                'size' => 'large',
                'forceClose'=>true,
                'forceReload'=>'#applications-pjax'
            ];
        }else{           
            return [
                'title'=> "Создать",
                'size' => 'large',
                'content'=>$this->renderAjax('_application_form', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }       
    }

    public function actionCopyApplication($to)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new ApplicationCopyForm(['applicationIdTo' => $to]);

        if($model->load($request->post()) && $model->copy()){



            return [
//                'title'=> "Создать",
//                'size' => 'large',
                'forceClose'=>true,
                'forceReload'=>'#applications-pjax'
            ];
        }else{
//            var_dump($model->er);
            return [
                'title'=> "Создать",
                'size' => 'large',
                'content'=>$this->renderAjax('application_copy', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionGetApplications($id)
    {
        $applications = ArrayHelper::map(Applications::find()->where(['contract_id' => $id])->all(), 'id', 'number');

        $html = '';

        foreach ($applications as $id => $number){
            $html .= "<option value='{$id}'>{$number}</option>";
        }

        return $html;
    }

    /**
     * @param int $applicationId
     * @return string
     */
    public function actionGetFilms($applicationId)
    {
        $filmsPks = ArrayHelper::getColumn(FilmsApplication::find()->where(['application_id' => $applicationId])->all(), 'film_id');
        $films = ArrayHelper::map(Films::find()->where(['id' => $filmsPks])->all(), 'id', 'name');

        $html = '';

        foreach ($films as $id => $name){
            $html .= "<option value='{$id}'>{$name}</option>";
        }

        return $html;
    }

    /**
     * Delete an existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    public function actionSetValues($id, $attribute, $value)
    {
        $model = $this->findModel($id);
        if($attribute == 'number') $model->number = $value;
        if($attribute == 'min_payment') $model->min_payment = $value;
        if($attribute == 'min_payment_currency') $model->min_payment_currency = $value;
        if($attribute == 'payment_period') $model->payment_period = $value;
        if($attribute == 'payment_currency') $model->payment_currency = $value;
        if($attribute == 'company_lic') $model->company_lic = $value;
        if($attribute == 'company_cont') $model->company_cont = $value;
        if($attribute == 'another_contract') $model->another_contract = $value;
        $model->save();
        if($attribute == 'date_cr') Yii::$app->db->createCommand()->update('contract', ['date_cr' => date('Y-m-d', strtotime($value) )], [ 'id' => $model->id ])->execute();
    }

    public function actionSetValuesApplication($id, $attribute, $value)
    {
        $model = Applications::findOne($id);
        if($attribute == 'number') $model->number = $value;
        if($attribute == 'date_cr') $model->date_cr = date('Y-m-d', strtotime($value) );
        if($attribute == 'uniform_condition') $model->uniform_condition = $value;
        if($attribute == 'min_warranty') $model->min_warranty = $value;
        if($attribute == 'min_warranty_currency') $model->min_warranty_currency = $value;
        if($attribute == 'tex_adap') $model->tex_adap = $value;
        if($attribute == 'text_adap_currency') $model->text_adap_currency = $value;
        if($attribute == 'maintenance') $model->maintenance = $value;
        if($attribute == 'maintenance_currency') $model->maintenance_currency = $value;
        $model->save();
    }

    public function actionSetFilm($id, $film_id)
    {
        if($film_id != null){
            $films_application = new FilmsApplication();
            $films_application->film_id = $film_id;
            $films_application->application_id = $id;
            $films_application->save();
        }
    }

    public function actionSetTerritory($val, $id)
    {
        $array = json_decode($val);
        $result = [];
        foreach ($array as $value) {
            $tag = Country::findOne($value);
            if($tag != null){
                $result [] = [
                    'id' => $tag->id,
                ];
            }
        }

        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }
         
        $filmApplication = FilmsApplication::findOne($numeric);
        $filmApplication->territory = json_encode($result);
        $filmApplication->save();
    }

    public function actionSetPlatforms($val, $id)
    {
        $array = json_decode($val);
        $result = [];
        foreach ($array as $value) {
            $platform = Platform::findOne($value);
            if($platform != null){
                $result [] = [
                    'id' => $platform->id,
                ];
            }
        }

        $strlen = strlen( $id );
        $numeric = ''; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $id, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric .= $char;            
        }
         
        $filmApplication = FilmsApplication::findOne($numeric);
        $filmApplication->platforms = json_encode($result);
        $filmApplication->save();
    }

    public function actionSetValuesFilmsApplication($id, $attribute, $val)
    {
        $model = FilmsApplication::findOne($id);
        if($attribute == 'rolyati_x') { $model->rolyati_x = $val; $model->rolyati_y = 100 - $val; }
        if($attribute == 'rolyati_y') { $model->rolyati_y = $val; $model->rolyati_x = 100 - $val; }
        if($attribute == 'price') $model->price = $val;
        if($attribute == 'est_open') $model->est_open = ($val != null ? date('Y-m-d', strtotime($val)) : null );
        if($attribute == 'vod_open') $model->vod_open = ($val != null ? date('Y-m-d', strtotime($val)) : null );
        if($attribute == 'close_date') $model->close_date = ($val != null ? date('Y-m-d', strtotime($val)) : null );
        if($attribute == 'text_adap_currency') $model->text_adap_currency = $val;
        if($attribute == 'maintenance') $model->maintenance = $val;
        if($attribute == 'maintenance_currency') $model->maintenance_currency = $val;
        if($attribute == 'svod') $model->svod = $val;
        if($attribute == 'pvod') $model->pvod = $val;
        if($attribute == 'avod') $model->avod = $val;
        if($attribute == 'catch_up') $model->catch_up = $val;
        if($attribute == 'tv') $model->tv = $val;
        if($attribute == 'flat_rate') $model->flat_rate = $val;
        if($attribute == 'flat_rate_currency') $model->flat_rate_currency = $val;
        if($attribute == 'min_warranty') $model->min_warranty = $val;
        if($attribute == 'min_warranty_currency') $model->min_warranty_currency = $val;
        if($attribute == 'tex_adap') $model->tex_adap = $val;
        if($attribute == 'text_adap_currency') $model->text_adap_currency = $val;
        if($attribute == 'maintenance') $model->maintenance = $val;
        if($attribute == 'maintenance_currency') $model->maintenance_currency = $val;
        $model->save();
    }

    public function actionChangeSettings($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Applications::findOne($id);
        if($model->settings == 1) $model->settings = 0;
        else $model->settings = 1;
        $model->save();
        return ['forceClose'=>true,'forceReload'=>'#applications-pjax'];
    }

    /**
     * Delete an existing Contract model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteApplication($id)
    {
        $request = Yii::$app->request;
        Applications::findOne($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#applications-pjax'];
    }

     /**
     * Delete an existing FilmsApplication model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteFilmsApplication($id)
    {
        $request = Yii::$app->request;
        FilmsApplication::findOne($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#applications-pjax'];
    }

    public function actionBulkDeleteFilmsApplication()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            FilmsApplication::findOne($pk)->delete();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#applications-pjax'];       
    }

    public function actionChangeAll($application_id)
    {        
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new FilmsApplication();
        $application = Applications::findOne($application_id);

        if($model->load($request->post()) && $model->validate()){
            $post = $request->post();
            $session = Yii::$app->session;
            $filmsApplication = FilmsApplication::find()->where(['id' => $session['pks'] ])->all();

//            var_dump($filmsApplication);
//            exit;

            foreach ($filmsApplication as $value) {

                $result = [];
                foreach ($post['FilmsApplication']['allTerritories'] as $country) {
                    $result [] = [
                        'id' => $country,
                    ];
                }
                if($post['FilmsApplication']['allTerritories'] != null) $value->territory = json_encode($result);
                if($model->rolyati_x != null) $value->rolyati_x = $model->rolyati_x;
                if($model->rolyati_y != null) $value->rolyati_y = $model->rolyati_y;

                $result = [];
                foreach ($post['FilmsApplication']['allPlatforms'] as $platforms) {
                    $result [] = [
                        'id' => $platforms,
                    ];
                }

                if($post['FilmsApplication']['allPlatforms'] != null) $value->platforms = json_encode($result);
                if($model->price != null) $value->price = $model->price;
                if($model->est_open != null) $value->est_open = date('Y-m-d', strtotime( $model->est_open ));
                if($model->vod_open != null) $value->vod_open = date('Y-m-d', strtotime( $model->vod_open ));
                if($model->close_date != null) $value->close_date = date('Y-m-d', strtotime( $model->close_date ));

                if($model->min_warranty != null) $value->min_warranty = $model->min_warranty;
                if($model->min_warranty_currency != null) $value->min_warranty_currency = $model->min_warranty_currency;
                if($model->tex_adap != null) $value->tex_adap = $model->tex_adap;
                if($model->text_adap_currency != null) $value->text_adap_currency = $model->text_adap_currency;
                if($model->maintenance != null) $value->maintenance = $model->maintenance;
                if($model->maintenance_currency != null) $value->maintenance_currency = $model->maintenance_currency;

                if($model->application_id != null) $value->application_id = $model->application_id;

                //if($application->settings == 1) {
                    if($model->svod != null) $value->svod = $model->svod;
                    if($model->pvod != null) $value->pvod = $model->pvod;
                    if($model->avod != null) $value->avod = $model->avod;
                    if($model->catch_up != null) $value->catch_up = $model->catch_up;
                    if($model->tv != null) $value->tv = $model->tv;
                    if($model->flat_rate != null) $value->flat_rate = $model->flat_rate;
                    //if($model->flat_rate_currency != null) $value->flat_rate_currency = $model->flat_rate_currency;
                //}
                    
                //if($application->uniform_condition == 0) {

                //}

                $value->save();
            }

            return [
                'forceReload'=>'#applications-pjax',
                'title'=> "Изменить выделенные",
                'size' => 'normal',
                'content'=>'<span class="text-success">Успешно выполнено</span>',
                'forceClose'=>true,
            ];         
        }
        else{
            $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
            if($pks != null) {
                $arrays = [];
                if($pks != null) {
                    foreach ( $pks as $pk ) {
                        $arrays [] = $pk;
                    }

                    $session = Yii::$app->session;
                    $session['pks'] = $arrays;     
                }

                return [
                    'title'=> "Изменить выделенные",
                    'size' => 'large',
                    'content'=>$this->renderAjax('_films_application_form', [
                        'model' => $model,
                        'application' => $application,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];         
            }
        }
    }

    public function actionCalendar()
    {    
        if(Yii::$app->user->identity->type == 0 || Yii::$app->user->identity->type == 2) $calendar = FilmsApplication::find()->all();
        else {
            $rightHolders = RightHolders::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
            $array = [];
            foreach ($rightHolders as $value) {
                $array [] = $value->id;
            }

            $contracts = Contract::find()->joinWith('companyLic')->where(['company_lic' => $array])->all();
            $contractId = [];
            foreach ($contracts as $value) {
                $contractId [] = $value->id;
            }

            $applications = Applications::find()->where(['contract_id' => $contractId])->all();
            $applicationId = [];
            foreach ($applications as $value) {
                $applicationId [] = $value->id;
            }
            $calendar = FilmsApplication::find()->where(['application_id' => $applicationId])->all();
            /*foreach ($calendar as $value) {
                echo "<br>id=".$value->id;
            }*/
        }
        $events = array();

        foreach ($calendar as  $calen) {
            if ($calen->est_open != null){
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $calen->id;
                $Event->title = $calen->film->name .' (EST)';
                $Event->editable=false;
                $Event->start = $calen->est_open;
                $events[] = $Event;  
            }
            if ($calen->vod_open != null){
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $calen->id;
                $Event->title = $calen->film->name . ' (VOD)';
                $Event->editable=false;
                $Event->start = $calen->vod_open;
                $events[] = $Event;  
            }
            if ($calen->svod != null){
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $calen->id;
                $Event->title = $calen->film->name . ' (SVOD)' ;
                $Event->editable=false;
                $Event->start = $calen->svod;
                $events[] = $Event;  
            }
            if ($calen->pvod != null){
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $calen->id;
                $Event->title = $calen->film->name . ' (PVOD)';
                $Event->editable=false;
                $Event->start = $calen->pvod;
                $events[] = $Event;  
            }
        }

/*        echo "<pre>";
        print_r($events);
        echo "</pre>";
        die;*/
           
        return $this->render('calendar',[
            'events' => $events,
        ]);
      
    }
     public function actionCalendarEvents(){    
        $calendar = FilmsApplication::find()->all();
        $tasks = [];

        foreach ($calendar as  $calen) {
            if ($calen->est_open != null)
              $tasks[] = $calen->est_open;
        }
    }

    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){

    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

    $calendar = FilmsApplication::find()->all();
    $events = array();

    foreach ($calendar AS $time){
    if ($time->est_open != null){
      $Event = new \yii2fullcalendar\models\Event();
      $Event->id = $time->id;
      $Event->title = $time->film_id;
      $events[] = $Event;
    }
    }

    return $events;
  }

    /**
     * Finds the Contract model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contract the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contract::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
