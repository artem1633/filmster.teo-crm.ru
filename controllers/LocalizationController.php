<?php

namespace app\controllers;

use Yii;
use app\models\Localization;
use app\models\LocalizationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\PreferenceBooks;
use app\models\Directory;
use yii\web\UploadedFile;

/**
 * LocalizationController implements the CRUD actions for Localization model.
 */
class LocalizationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

     //Скачать картинки товара на сервер.
    public function actionUpload($number)
    {
        $uploadPath = 'uploads/localization/'.$number;

        if (isset($_FILES['image'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('image');
            $original_name = $file->baseName;  
            $newFileName = $original_name.'.'.$file->extension;
            // you can write save code here before uploading.
            if ($file->saveAs($uploadPath . '/' . $newFileName)) {
            }
        }
    }

    // Удаление картинок товара из сервера.
    public function actionRemoveFile($id, $filename)
    {
        if (file_exists('uploads/localization/' . $id . '/' . $filename))  unlink(Yii::getAlias('uploads/localization/' . $id . '/' . $filename));
    }

    //Загружение картинки товара.
    public function actionSetImage($id, $photos)
    {
        $localization = Localization::findOne($id);
        if($localization != null)
        {
            $localization->photos = $photos;
            $localization->save();
        }
    }

    /**
     * Creates a new Localization model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($film_id, $type = 2)
    {
        $request = Yii::$app->request;
        $model = new Localization();
        $model->type = $type;
        $number = Localization::getLastId(); 
        Directory::createLocalizationDirectory($number);
        $model->film_id = $film_id;
        $model->name = $model->film->name;
        $model->language_id = $model->film->language_id;
        if($type == 2) $model->language_id = 2;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){

            $post = $request->post();

            $result = []; $i = -1;
            foreach ($post['akter'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'role' => $post['role'][$i],
                        'akter' => $value,
                    ];
                }
            }
            $model->cast = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['names_group'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'group_member' => $post['group_member'][$i],
                        'names_group' => $value,
                    ];
                }
            }
            $model->film_crew = json_encode($result);

            $model->poster23_file = UploadedFile::getInstance($model, 'poster23_file');
            if(!empty($model->poster23_file))
            {
                $model->poster23_file->saveAs('uploads/localization/'. $number . '/' .$model->poster23_file->baseName.'.'.$model->poster23_file->extension);
                Yii::$app->db->createCommand()->update('localization', ['poster23' => $model->poster23_file->baseName.'.'.$model->poster23_file->extension], [ 'id' => $model->id ])->execute();
            }

            $model->poster169_file = UploadedFile::getInstance($model, 'poster169_file');
            if(!empty($model->poster169_file))
            {
                $model->poster169_file->saveAs('uploads/localization/'. $number . '/' .$model->poster169_file->baseName.'.'.$model->poster169_file->extension);
                Yii::$app->db->createCommand()->update('localization', ['poster169' => $model->poster169_file->baseName.'.'.$model->poster169_file->extension], [ 'id' => $model->id ])->execute();
            }

            $model->save();
            return [
                'forceReload'=>'#localization-pjax',
                'title'=> "Создать",
                'size' => 'normal',
                'forceClose'=>true,
            ];
        }else{           
            return [
                'title'=> "Создать",
                'size' => 'large',
                'content'=>$this->renderAjax('create', [
                    'model' => $model,
                    'number' => $number,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];         
        }       
    }

    /**
     * Updates an existing Localization model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $number = $id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post()) && $model->save()){
            $post = $request->post();

            $result = []; $i = -1;
            foreach ($post['akter'] as $value) {
                $i++;
                if($value != ''){
                    $result [] = [
                        'role' => $post['role'][$i],
                        'akter' => $value,
                    ];
                }
            }
            $model->cast = json_encode($result);

            $result = []; $i = -1;
            foreach ($post['names_group'] as $value) {
                $i++;
                $result [] = [
                    'group_member' => $post['group_member'][$i],
                    'names_group' => $value,
                ];
            }
            $model->film_crew = json_encode($result);

            $model->poster23_file = UploadedFile::getInstance($model, 'poster23_file');
            if(!empty($model->poster23_file))
            {
                $model->poster23_file->saveAs('uploads/localization/'. $number . '/' .$model->poster23_file->baseName.'.'.$model->poster23_file->extension);
                Yii::$app->db->createCommand()->update('localization', ['poster23' => $model->poster23_file->baseName.'.'.$model->poster23_file->extension], [ 'id' => $model->id ])->execute();
            }

            $model->poster169_file = UploadedFile::getInstance($model, 'poster169_file');
            if(!empty($model->poster169_file))
            {
                $model->poster169_file->saveAs('uploads/localization/'. $number . '/' .$model->poster169_file->baseName.'.'.$model->poster169_file->extension);
                Yii::$app->db->createCommand()->update('localization', ['poster169' => $model->poster169_file->baseName.'.'.$model->poster169_file->extension], [ 'id' => $model->id ])->execute();
            }

            $model->save();
            return [
                'forceReload'=>'#localization-pjax',
                'title'=> "Локализация",
                'forceClose'=>true,
            ];    
        }else{
             return [
                'title'=> "Изменить",
                'size' => 'large',
                'content'=>$this->renderAjax('update', [
                    'model' => $model,
                    'number' => $number,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
            ];        
        }
    }

    public function actionDropzone($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $number = $id;
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'title'=> "Кадры из фильма",
            'size' => 'large',
            'content'=>$this->renderAjax('upload', [
                'model' => $model,
                'number' => $number,
            ]),
        ];
    }

    public function actionGetRole()
    {
        $roles = PreferenceBooks::groupMemberRoleList();
        $result = '';
        foreach ($roles as $key => $value) {
            $result .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $result;
    }

    /**
     * Delete an existing Localization model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#localization-pjax'];
    }

    /**
     * Finds the Localization model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Localization the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Localization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetValues($id, $attribute, $value)
    {
        $model = $this->findModel($id);
        if($attribute == 'name') $model->name = $value;
        if($attribute == 'language_id') $model->language_id = $value;
        if($attribute == 'description') $model->description = $value;
        $model->save();
    }

    

    public function actionSetActers()
    {
        $id = $_POST['id'];
        $akters = $_POST['akters'];
        $roles = $_POST['roles'];
        $model = $this->findModel($id);

        $result = []; $i = -1;
        foreach ($akters as $value) {
            $i++;
            if($value != '' || $roles[$i] != ''){
                $result [] = [
                    'role' => $roles[$i],
                    'akter' => $value,
                ];
            }
        }
        $model->cast = json_encode($result);
        $model->save();
    }

    public function actionSetCrew()
    {
        $id = $_POST['id'];
        $names_group = $_POST['names_group'];
        $group_member = $_POST['group_member'];
        $model = $this->findModel($id);

        $result = []; $i = -1;
        foreach ($names_group as $value) {
            $i++;
            $result [] = [
                'group_member' => $group_member[$i],
                'names_group' => $value,
            ];
        }
        $model->film_crew = json_encode($result);
        $model->save();
        return $_POST['group_member'][0];
    }

    public function actionSetPoster23()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $fileName = time().'_'.basename($_FILES["file"]["name"]);
            $id = $_POST['id'];
            
            //file upload path
            $targetDir = "uploads/localization/" . $id. '/';
            $targetFilePath = $targetDir . $fileName;
            
            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('jpg','png','jpeg','gif');
            
            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $model = $this->findModel($id);
                    $model->poster23 = $fileName;
                    $model->save();
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }
            
            //render response data in JSON format
            return json_encode($response);
        }
    }

    public function actionSetPoster169()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $fileName = time().'_'.basename($_FILES["file"]["name"]);
            $id = $_POST['id'];
            
            //file upload path
            $targetDir = "uploads/localization/" . $id. '/';
            $targetFilePath = $targetDir . $fileName;
            
            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('jpg','png','jpeg','gif');
            
            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $model = $this->findModel($id);
                    $model->poster169 = $fileName;
                    $model->save();
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }
            
            //render response data in JSON format
            return json_encode($response);
        }
    }
}
