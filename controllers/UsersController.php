<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use app\models\Settings;
use app\models\User;
use app\models\Contract;
use app\models\RightHolders;
use app\models\PreferenceBooks;
use yii\web\HttpException;
use app\models\Applications;
use app\models\FilmsApplication;
use app\models\ReportMonetary;
use app\models\ReportValues;
use app\models\Films;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if($action->id != 'statistic' and $action->id != 'nestable')
        {
            if(Yii::$app->user->identity->type == 1 || Yii::$app->user->identity->type == 2)
            {
                throw new HttpException(403,'У вас нет разрешения на доступ к этому действию.');
            }
        }
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionDashboard()
    {
        $identity = Yii::$app->user->identity;        

        return $this->render('dashboard', [
        ]);  
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $post = $request->post();
        $contracts = null;
        $months = [];
        if($post['date_time_from'] !== null && $post['date_time_to'] !== null && $post['contracts'] !== null)
        {
            $array = explode('-', $post['date_time_from']);
            $begin = $array[1] . '-' . PreferenceBooks::monthCode($array[0]) . '-01';

            $array = explode('-', $post['date_time_to']);
            $end = $array[1] . '-' . PreferenceBooks::monthCode($array[0]) . '-01';
            $x = strtotime($end);
            $end = date("Y-m-d",strtotime("+1 month",$x));
            for ($i = strtotime($begin); $i < strtotime($end); $i = strtotime("+1 month",$i)) { 
                $months [] = PreferenceBooks::monthName(date('m', $i)) . '-' . date('Y', $i);
            }
            $contracts = Contract::find()
                ->where(['id' => $post['contracts']])
                //->andWhere(['between', 'date_cr', $begin, $end ])
                ->all();
        }

        /*echo "<pre>";
        print_r($contracts);
        echo "</pre>";
        die;*/

        $model = $this->findModel($id);
        $right_holders = RightHolders::find()->where(['user_id' => $id])->all();
        return $this->render('view', [
            'model' => $model,
            'right_holders' => $right_holders,
            'post' => $post,
            'contracts' => $contracts,
            'months' => $months,
        ]);
    }

    public function actionStatistic()
    {   
        $request = Yii::$app->request;
        $post = $request->post();
        $session = Yii::$app->session;
        $contracts = null;
        $months = [];
        if($post['date_time_from'] !== null && $post['date_time_to'] !== null && $post['contracts'] !== null)
        {
            $array = explode('-', $post['date_time_from']);
            $begin = $array[1] . '-' . PreferenceBooks::monthCode($array[0]) . '-01';

            $array = explode('-', $post['date_time_to']);
            $end = $array[1] . '-' . PreferenceBooks::monthCode($array[0]) . '-01';
            $x = strtotime($end);
            $end = date("Y-m-d",strtotime("+1 month",$x));
            for ($i = strtotime($begin); $i < strtotime($end); $i = strtotime("+1 month",$i)) { 
                $months [] = PreferenceBooks::monthName(date('m', $i)) . '-' . date('Y', $i);
            }
            $contracts = Contract::find()
                ->where(['id' => $post['contracts']])
                //->andWhere(['between', 'date_cr', $begin, $end ])
                ->all();
            $session['contracts'] = $contracts;
            $session['months'] = $months;
        }

        $items = ReportValues::statistic($session['contracts'], $post, $session['months']);

        return $this->render('statistika', [
            'post' => $post,
            'contracts' => $contracts,
            'months' => $months,
            'items' => $items,
        ]);
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();
        $model->active = 1;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
               
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)) {
                    $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', 
                        [ 'foto' => $model->id.'.'.$model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Создать",
                    'content'=>'<span class="text-success">Успешно выпольнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->load($request->post()) && $model->save()) {
                
                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)) {
                    $model->file->saveAs('avatars/'.$model->id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', 
                        [ 'foto' => $model->id.'.'.$model->file->extension ], 
                        [ 'id' => $model->id ])
                    ->execute();
                }
                
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Пользователи",
                    'forceClose' => true,
                ];    
            }else{
                 return [
                    'title'=> $model->getTypeDescription(),
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAvatar()
    {
        $request = Yii::$app->request;
        $id = Yii::$app->user->identity->id;
        $model = Users::findOne(Yii::$app->user->identity->id);  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)){
                    $model->file->saveAs('avatars/'.$id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', ['foto' => $id.'.'.$model->file->extension], [ 'id' => $id ])->execute();
                }

                return [
                    'forceReload'=>'#profile-pjax',
                    'size' => 'normal',
                    'title'=> "Аватар",
                    'forceClose' => true,     
                ];         
            }else{           
                return [
                    'title'=> "Аватар",
                    'size' => 'small',
                    'content'=>$this->renderAjax('avatar', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }
       
    }

    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                
                return [
                    'forceClose' => true,
                    'forceReload'=>'#profile-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => "normal",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }
        else{
            echo "ddd";
        }
    }

    public function actionProfile()
    {
        $request = Yii::$app->request;
        return $this->render('profile', [
        ]);        
    }

    public function actionSetValues($id, $attribute, $value)
    {
        $model = $this->findModel($id);
        if($attribute == 'login') $model->login = $value;
        if($attribute == 'new_password') $model->new_password = $value;
        if($attribute == 'fio') $model->fio = $value;
        if($attribute == 'mail') $model->mail = $value;
        if($attribute == 'telephone') $model->telephone = $value;
        if($attribute == 'site') $model->site = $value;
        if($attribute == 'description') $model->description = $value;
        if($attribute == 'active') $model->active = $value;
        if($attribute == 'type') $model->type = $value;
        if($attribute == 'localization_id') $model->localization_id = $value;

        $model->save();
    }

    public function actionSetLogo()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $id = $_POST['id'];
            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            //$fileName = $id.basename($_FILES["file"]["name"]);
            $fileName = $id. '.' .$ext;
            
            //file upload path
            $targetDir = "avatars/";
            $targetFilePath = $targetDir . $fileName;
            
            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('jpg','png','jpeg','gif');
            
            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $model = $this->findModel($id);
                    $model->foto = $fileName;
                    $model->save();
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }
            
            //render response data in JSON format
            return json_encode($response);
        }
    }

    public function actionCreateRightHolder($user_id)
    {
        $request = Yii::$app->request;
        $model = new RightHolders();
        $model->user_id = $user_id;
        Yii::$app->response->format = Response::FORMAT_JSON;

        if($model->load($request->post()) && $model->save())
        {
            return [
                'forceReload'=>'#right-holder-pjax',
                'size' => 'normal',
                'title'=> "Правообладатели",
                'forceClose'=>true,
            ];         
        }
        else
        {           
            return [
                'title'=> "Правообладатели",
                'size' => 'large',
                'content'=>$this->renderAjax('_form_right_holder_create', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];         
            }
    }

    public function actionSetRightValues($id, $attribute, $value)
    {
        $model = RightHolders::findOne($id);
        if($attribute == 'name_ru') $model->name_ru = $value;
        if($attribute == 'name_eng') $model->name_eng = $value;
        if($attribute == 'registry_code') $model->registry_code = $value;
        if($attribute == 'primary_number') $model->primary_number = $value;
        if($attribute == 'tax_id') $model->tax_id = $value;
        if($attribute == 'street1') $model->street1 = $value;
        if($attribute == 'street2') $model->street2 = $value;
        if($attribute == 'country_id') $model->country_id = $value;
        if($attribute == 'city_id') $model->city_id = $value;
        if($attribute == 'state') $model->state = $value;
        if($attribute == 'index') $model->index = $value;
        if($attribute == 'bank_name') $model->bank_name = $value;
        if($attribute == 'bank_index') $model->bank_index = $value;
        if($attribute == 'bank_street1') $model->bank_street1 = $value;
        if($attribute == 'bank_street2') $model->bank_street2 = $value;
        if($attribute == 'bank_state') $model->bank_state = $value;
        if($attribute == 'bic_swift') $model->bic_swift = $value;
        if($attribute == 'bank_account') $model->bank_account = $value;
        if($attribute == 'bank_account_currency') $model->bank_account_currency = $value;
        if($attribute == 'iban') $model->iban = $value;
        if($attribute == 'bank_country_id') $model->bank_country_id = $value;
        if($attribute == 'iban_currency') $model->iban_currency = $value;
        if($attribute == 'correspondent_account') $model->correspondent_account = $value;
        if($attribute == 'correspondent_account_currency') $model->correspondent_account_currency = $value;

        $model->save();
    }


    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($id != 1) $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Vacancy model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            if($pk != 1) $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Delete an existing RightHolders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteRightHolder($id)
    {
        $request = Yii::$app->request;
        RightHolders::findOne($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose'=>true,'forceReload'=>'#right-holder-pjax'];
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetTab($tab, $value)
    {
        Yii::$app->session[$tab] = $value;
    }

    public function actionGetContracts($id)
    {
        $litsenziat = [];
        $rightHolders = RightHolders::find()->where(['user_id' => $id])->all();
        foreach ($rightHolders as $value) {
            $litsenziat [] = $value->id;
        }
        $contracts = Contract::find()->where(['company_lic' => $litsenziat])->all();
        foreach ($contracts as $value) {
            echo "<option value = '".$value->id."'>".$value->number."</option>" ;
        }
    }


    public function actionExport($type = 0)
    { 
        $session = Yii::$app->session;

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");

        $work_sheet=-1;
        foreach ($session['contracts'] as $contract) 
        {
            $work_sheet++;
            $objWorkSheet = $objPHPExcel->createSheet($work_sheet);

            $invalidCharacters = $objWorkSheet->getInvalidCharacters();
            $title = str_replace($invalidCharacters, '', $contract->number);
            $objWorkSheet->setTitle($title);
            //$objWorkSheet->setTitle($contract->number);
            $styleForHeaders = array('font' => array('size' => 13,'bold' => true,'color' => array('rgb' => '000000')));

            $objPHPExcel->setActiveSheetIndex($work_sheet)->getStyle('A1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray($styleForHeaders);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);

            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(0, 1,"Название фильма / Год выпуска");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(1, 1, "№ приложения");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(2, 1, "Роляти");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(3, 1,"Площадка");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(4, 1,"Кол-во");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(5, 1, "EST");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(6, 1, "Кол-во");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(7, 1,"VOD");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(8, 1, "Кол-во");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(9, 1,"SVOD" );
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(10, 1,"Кол-во" );
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(11, 1, "PVOD");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(12, 1, "Кол-во");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(13, 1, "AVOD");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(14, 1, "Всего кол-во продаж");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(15, 1, "Всего сумма продажи");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(16, 1, "Сумма роляти");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(17, 1, "Текущая Минимальная Гарантия");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(18, 1, "Остаток Минимальной Гарантии");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(19, 1, "Техническая Адаптация");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(20, 1, "Остаток Технической Адаптации");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(21, 1, "Плата за обслуживание");
            $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(22, 1, "Остаток Платы за Обслуживание");

            $index = 1; $rolyatiIndex = 0; $checkCount = 0; $rolyati = 0; $application = null;
            foreach ($session['filmsPrint'] as $films) 
            {
                if($films['contract_id'] == $contract->id)
                {
                    $index++;
                    $rolyati += (float)$films['old_rolyati_x_summa'];
                    if($films['check'] === 1)
                    { 
                        $checkCount++;
                        if($rolyatiIndex == 0) $rolyatiIndex = $index;
                        $application = Applications::findOne($films['application_id']);
                    }

                    $wizard = new \PHPExcel_Helper_HTML;
                    $richText = $wizard->toRichTextObject($films['platformNames']);

                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(0, $index, $films['filmName']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(1, $index, $films['application_number']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(2, $index, $films['rolyati']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(3, $index, $richText);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(4, $index, $films['estCount']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(5, $index, $films['est']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(6, $index, $films['vodCount']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(7, $index, $films['vod']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(8, $index, $films['svodCount']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(9, $index, $films['svod']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(10, $index, $films['pvodCount']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(11, $index, $films['pvod']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(12, $index, $films['avodCount']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(13, $index, $films['avod']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(14, $index, $films['sale']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(15, $index, $films['summary']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(16, $index, $films['rolyati_x_summa']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(17, $index, $films['mgt']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(18, $index, $films['mgs']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(19, $index, $films['tat']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(20, $index, $films['tas']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(21, $index, $films['mft']);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(22, $index, $films['mfs']);

                    if($films['check'] !== 1 && $type == 1){
                        $filmsApplication = FilmsApplication::findOne($films['filmsApplication_id']);
                        $filmsApplication->min_warranty = $films['mgs'];
                        $filmsApplication->tex_adap = $films['tas'];
                        $filmsApplication->maintenance = $films['mfs'];
                        $filmsApplication->save();
                    }
                }
            }

            if($checkCount > 0){
                $checkIndex = $checkCount + 1;
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("Q2:Q".$checkIndex);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("R2:R".$checkIndex);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("S2:S".$checkIndex);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("T2:T".$checkIndex);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("U2:U".$checkIndex);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("V2:V".$checkIndex);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("W2:W".$checkIndex);

                $min_warranty = $application->min_warranty;
                $tex_adap = $application->tex_adap;
                $maintenance = $application->maintenance;

                if($min_warranty === null) $min_warranty = 0;
                if($tex_adap === null) $tex_adap = 0;
                if($maintenance === null) $maintenance = 0;

                if($min_warranty - $rolyati > 0) {
                    $mgs = $min_warranty - $rolyati;
                    $rolyati = 0;
                }
                else {
                    $mgs = 0;
                    $rolyati = $rolyati - $min_warranty;
                }

                if($mgs == 0 && $rolyati > 0) {
                    if($tex_adap - $rolyati > 0) {
                        $tas = $tex_adap - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $tas = 0;
                        $rolyati = $rolyati - $tex_adap;
                    }
                }

                if($tas == 0 && $rolyati > 0) {
                    if($maintenance - $rolyati > 0) {
                        $mfs = $maintenance - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $mfs = 0;
                        $rolyati = $rolyati - $maintenance;
                    }
                }

                if($mgs === null) $mgs = $min_warranty;
                if($tas === null) $tas = $tex_adap;
                if($mfs === null) $mfs = $maintenance;

                if($type == 1){
                    $application->min_warranty = $mgs;
                    $application->tex_adap = $tas;
                    $application->maintenance = $mfs;
                    $application->save();
                }
                
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(16, $rolyatiIndex, $rolyati);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(17, $rolyatiIndex, $min_warranty);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(18, $rolyatiIndex, $mgs);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(19, $rolyatiIndex, $tex_adap);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(20, $rolyatiIndex, $tas);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(21, $rolyatiIndex, $maintenance);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(22, $rolyatiIndex, $mfs);
            }
        }

        if($type === 0){
            $filename='Eksport'.'.xlsx'; //save our workbook as this file name
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save('php://output');
            //$objWriter->save('test.xlsx');
            //без этой строки при открытии файла xlsx ошибка!!!!!!
            exit;
        }
    }

    public function actionExportApplication($type = 0)
    { 
        $session = Yii::$app->session;

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");

        $work_sheet=-1;
        foreach ($session['contracts'] as $contract) 
        {
            $work_sheet++;
            $objWorkSheet = $objPHPExcel->createSheet($work_sheet);

            $invalidCharacters = $objWorkSheet->getInvalidCharacters();
            $title = str_replace($invalidCharacters, '', $contract->number);
            $objWorkSheet->setTitle($title);
            $styleForHeaders = array('font' => array('size' => 13,'bold' => true,'color' => array('rgb' => '000000')));

            $objPHPExcel->setActiveSheetIndex($work_sheet)->getStyle('A1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray($styleForHeaders);
            $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray($styleForHeaders);

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);


            $index = 0;
            $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
            foreach ($applications as $application) 
            {
                $index++;
                $objPHPExcel->setActiveSheetIndex($work_sheet)->getStyle('A'.$index)->applyFromArray($styleForHeaders);
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(0, $index,"Приложение # " . $application->number);

                $index++;
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(0, $index,"Название фильма / Год выпуска");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(1, $index, "Роляти");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(2, $index,"Площадка");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(3, $index,"Кол-во");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(4, $index, "EST");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(5, $index, "Кол-во");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(6, $index,"VOD");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(7, $index, "Кол-во");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(8, $index,"SVOD" );
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(9, $index,"Кол-во" );
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(10, $index, "PVOD");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(11, $index, "Кол-во");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(12, $index, "AVOD");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(13, $index, "Всего кол-во продаж");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(14, $index, "Всего сумма продажи");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(15, $index, "Сумма роляти");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(16, $index, "Текущая Минимальная Гарантия");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(17, $index, "Остаток Минимальной Гарантии");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(18, $index, "Техническая Адаптация");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(19, $index, "Остаток Технической Адаптации");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(20, $index, "Плата за обслуживание");
                $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(21, $index, "Остаток Платы за Обслуживание");

                $rolyatiIndex = 0; $checkCount = 0; $rolyati = 0; $applicationNew = null;
                foreach ($session['filmsPrint'] as $films) 
                {
                    /*echo "<br>f=" . $films['application_id'];
                    echo "g=" . $application->id;*/
                    if($films['contract_id'] == $contract->id && $films['application_id'] == $application->id)
                    {
                        $index++;
                        $wizard = new \PHPExcel_Helper_HTML;
                        $richText = $wizard->toRichTextObject($films['platformNames']);

                        $rolyati += (float)$films['old_rolyati_x_summa'];
                        if($films['check'] === 1)
                        { 
                            $checkCount++;
                            if($rolyatiIndex == 0) $rolyatiIndex = $index;
                            $applicationNew = Applications::findOne($films['application_id']);
                        }

                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(0, $index, $films['filmName']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(1, $index, $films['rolyati']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(2, $index, $richText);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(3, $index, $films['estCount']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(4, $index, $films['est']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(5, $index, $films['vodCount']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(6, $index, $films['vod']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(7, $index, $films['svodCount']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(8, $index, $films['svod']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(9, $index, $films['pvodCount']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(10, $index, $films['pvod']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(11, $index, $films['avodCount']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(12, $index, $films['avod']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(13, $index, $films['sale']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(14, $index, $films['summary']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(15, $index, $films['rolyati_x_summa']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(16, $index, $films['mgt']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(17, $index, $films['mgs']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(18, $index, $films['tat']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(19, $index, $films['tas']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(20, $index, $films['mft']);
                        $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(21, $index, $films['mfs']);

                        if($films['check'] !== 1 && $type == 1){
                            $filmsApplication = FilmsApplication::findOne($films['filmsApplication_id']);
                            $filmsApplication->min_warranty = $films['mgs'];
                            $filmsApplication->tex_adap = $films['tas'];
                            $filmsApplication->maintenance = $films['mfs'];
                            $filmsApplication->save();
                        }
                    }
                }

                if($checkCount > 0){
                    $checkIndex = $checkCount + $rolyatiIndex - 1;
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("Q".$rolyatiIndex.":Q".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("R".$rolyatiIndex.":R".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("S".$rolyatiIndex.":S".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("T".$rolyatiIndex.":T".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("U".$rolyatiIndex.":U".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("V".$rolyatiIndex.":V".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->mergeCells("P".$rolyatiIndex.":P".$checkIndex);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(15, $rolyatiIndex, $rolyati);

                    $min_warranty = $applicationNew->min_warranty;
                    $tex_adap = $applicationNew->tex_adap;
                    $maintenance = $applicationNew->maintenance;

                    if($min_warranty === null) $min_warranty = 0;
                    if($tex_adap === null) $tex_adap = 0;
                    if($maintenance === null) $maintenance = 0;

                    if($min_warranty - $rolyati > 0) {
                        $mgs = $min_warranty - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $mgs = 0;
                        $rolyati = $rolyati - $min_warranty;
                    }

                    if($mgs == 0 && $rolyati > 0) {
                        if($tex_adap - $rolyati > 0) {
                            $tas = $tex_adap - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $tas = 0;
                            $rolyati = $rolyati - $tex_adap;
                        }
                    }

                    if($tas == 0 && $rolyati > 0) {
                        if($maintenance - $rolyati > 0) {
                            $mfs = $maintenance - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $mfs = 0;
                            $rolyati = $rolyati - $maintenance;
                        }
                    }

                    if($mgs === null) $mgs = $min_warranty;
                    if($tas === null) $tas = $tex_adap;
                    if($mfs === null) $mfs = $maintenance;

                    if($type == 1){
                        $application->min_warranty = $mgs;
                        $application->tex_adap = $tas;
                        $application->maintenance = $mfs;
                        $application->save();
                    }

                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(15, $rolyatiIndex, $rolyati);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(16, $rolyatiIndex, $min_warranty);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(17, $rolyatiIndex, $mgs);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(18, $rolyatiIndex, $tex_adap);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(19, $rolyatiIndex, $tas);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(20, $rolyatiIndex, $maintenance);
                    $objPHPExcel->setActiveSheetIndex($work_sheet)->setCellValueByColumnAndRow(21, $rolyatiIndex, $mfs);
                }
            }
        }

        if($type === 0){
            $filename='Eksport'.'.xlsx'; //save our workbook as this file name
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save('php://output');
            //$objWriter->save('test.xlsx');

            //без этой строки при открытии файла xlsx ошибка!!!!!!
            exit;
        }
    }

    public function actionXml()
    {
        $session = Yii::$app->session;
        //set content type xml in response
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');
         
        //set the view and render it partial to skip the layout to be rendered as well
        $values = $this->renderPartial('xml', [
           'contracts' => $session['contracts'],
           'filmsPrint' => $session['filmsPrint'],
        ]);

        file_put_contents('xml-report.xml', $values);
        return \Yii::$app->response->sendFile('xml-report.xml');
    }

    public function actionXmlApplication()
    {
        $session = Yii::$app->session;
        //set content type xml in response
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');
         
        //set the view and render it partial to skip the layout to be rendered as well
        $values =  $this->renderPartial('xml-application', [
           'contracts' => $session['contracts'],
           'filmsPrint' => $session['filmsPrint'],
        ]);

        file_put_contents('xml-report.xml', $values);
        return \Yii::$app->response->sendFile('xml-report.xml');
    }


    public function actionNestable()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $identity = Yii::$app->user->identity;;

        $identity = Yii::$app->user->identity;
        if($identity->type == 0){
            $users = Users::find()->all();
        }
        else {
            $users = Users::find()->where(['id'=>$identity->id])->all();
        }
        $platforms = '';
        $reportId = [];

        if($post['month'] !== null) {
            $report = ReportMonetary::find()->where(['date' => $post['month']])->all();
            foreach ($report as $value) {
                $platforms .= $value->platform->name_ru . '; ';
                $reportId [] = $value->id;
            }
        }

        return $this->render('nestable', [
            'post' => $post,
            'users' => $users,
            'platforms' => $platforms,
            'reportId' => $reportId,
        ]);
    }

    public function actionExportContract($contract_id, $type = 0, $month = null)
    { 
        $session = Yii::$app->session;
        $contract = Contract::findOne($contract_id);
        $monthArray = explode('-', $month);
        $currencyMonetary = $contract->CurrencyDescription($contract->payment_currency);
        $currency = $contract->CurrentPayment($contract->payment_currency);
        //$curCode = PreferenceBooks::getCurrencySymbol($currency);
        $curCode = $currency;


        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $work_sheet = 0;
        $objWorkSheet = $objPHPExcel->createSheet($work_sheet);

        $invalidCharacters = $objWorkSheet->getInvalidCharacters();
        $title = str_replace($invalidCharacters, '', $contract->number);
        $objWorkSheet->setTitle($title);
        $styleForHeaders = array('font' => array('size' => 9,'bold' => true,'color' => array('rgb' => '000000')));
        $styleForValues = array('font' => array('size' => 9,'color' => array('rgb' => '000000')));

        /*headerga yozuvlarni yozish*/ 
        if($contract->companyLic->user->localization_id == 2){
            //english
            $royalty = 'Royalty Statement';
            $between = "for Film usage on all Annexes under Agreement № {$contract->number} from ".date('d.m.Y', strtotime($contract->date_cr));
            $second_between = "between {$contract->companyLic->name_ru} (Licensor) and {$contract->companyCont->name_ru} (Licensee)";
            $period = 'Reporting period '. PreferenceBooks::periodDate($month, 'en', $contract->payment_period);
            $cityName = "Moscow";
            $filmTitle = "Film title";
            $onlineStore = "On-line Store";
            $annex = "Annex";
            $numberCopy = "Number of Copies";
            $grossIncome = "Gross Income";
            $totalGross = "Total Gross \r Income";
            $minimalGuarranted = "Minimal Guarantee";
            $technicalAdaptation = "Technical Adaptation";
            $technicalMaintenance = "Technical Maintenece";
            $royality = "Royalty (%)";
            $royalitySum = "Royalty";
            $leftMinimalGuar = "Minimal \r Guarantee \r leftover";
            $leftTechAdap = "Technical \r Adaptation \r leftover";
            $leftTechMain = "Technical \r Maintenece \r leftover";
            $royaltyPayment = "Royalty \r Payment";
            $taxSales = "Sales Tax (20%)";
            $closeDate = 'Дата закрытия';
            $companyName = 'Компания';
            $paymentEquavalent = "The payment of the equavalent of";
            $vTomChisle = "Inculding sales tax (20%)";
            $licensor = "LICENSOR";
            $licensee = "LICENSEE";
            $totalLabel = "Total";
        }
        else{
            //russian
            $royalty = 'Отчетная ведомость';
            $between = "об использовании Фильмов по всем приложениям к Лицензионному договору № {$contract->number} от ".date('d.m.Y', strtotime($contract->date_cr));
            $second_between = "между ООО «{$contract->companyLic->name_ru}» (Лицензиар) и «{$contract->companyCont->name_ru}» (Лицензиат)";
            $period = "Отчетный период " . PreferenceBooks::periodDate($month, 'ru', $contract->payment_period);
            $cityName = "г. Москва";
            $filmTitle = "Название фильма";
            $onlineStore = "Площадка";
            $annex = "№ Приложения";
            $numberCopy = "Количество копий";
            $grossIncome = "Валовые поступления";
            $totalGross = "Итого вал \r по площадкам";
            $minimalGuarranted = "Размер МГ";
            $technicalAdaptation = "Техническая \r адаптация";
            $technicalMaintenance = "Техническое \r обслуживание";
            $royality = "Ставка \r вознаграждения \r Лицензиара (%)";
            $royalitySum = "Лицензионное \r вознаграждение \r Лицензиара за \r отчетный период";
            $leftMinimalGuar = "Остаток МГ";
            $leftTechAdap = "Остаток \r технической \r адаптации";
            $leftTechMain = "Остаток \r технического \r обслуживания";
            $royaltyPayment = "Сумма к \r выплате";
            $taxSales = "НДС (20%)";
            $closeDate = 'Дата закрытия';
            $companyName = 'Компания';
            $paymentEquavalent = "Подлежит перечислению на расчетный счет Лицензиара сумма в размере ";
            $vTomChisle = "в том числе НДС (20%)";
            $licensor = "ЛИЦЕНЗИАР";
            $licensee = "ЛИЦЕНЗИАТ";
            $totalLabel = "Всего";
        }

        $closeDate = 'Дата закрытия';
        $companyName = 'Компания';

        $dayLast = PreferenceBooks::lastDayOfMonth($monthArray[1] . '-' .PreferenceBooks::monthCode($monthArray[0]) . '-01');
        $objSet = $objPHPExcel->setActiveSheetIndex();
        $objGet = $objPHPExcel->getActiveSheet();
        $objSet->setCellValueByColumnAndRow(0, 2, $royalty);
        $objSet->setCellValueByColumnAndRow(0, 3, $between);
        $objSet->setCellValueByColumnAndRow(0, 4, $second_between);
        $objSet->setCellValueByColumnAndRow(0, 5, $period);
        $objSet->setCellValueByColumnAndRow(0, 1, $cityName);
        $objSet->setCellValueByColumnAndRow(26, 1, $dayLast);
            
        /*begin header elements*/
        $objSet->mergeCells("A2:T2");
        $objSet->mergeCells("A3:T3");
        $objSet->mergeCells("A4:T4");
        $objSet->mergeCells("A5:T5");
        $objSet->mergeCells("A6:A7");
        $objSet->mergeCells("B6:B7");
        $objSet->mergeCells("C6:C7");
        $objSet->mergeCells("D6:D7");
        $objSet->mergeCells("E6:J6");
        $objSet->mergeCells("K6:P6");
        $objSet->mergeCells("E8:J8");
        $objSet->mergeCells("K8:P8");

        $center = \PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        $objSet->getStyle('A2:W2')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A3:P3')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A4:W4')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A5:W5')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A6:AC6')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A7:AC7')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A8:AC8')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A')->getAlignment()->setHorizontal('center')->setVertical('center');
        $objSet->getStyle('B')->getAlignment()->setVertical('center');
        $objSet->getStyle('Q')->getAlignment()->setVertical('center');
        $objSet->getStyle('A6:AC6')->getAlignment()->setVertical('center');
        $objSet->getStyle('A7:AC7')->getAlignment()->setVertical('center');
        $objSet->getStyle('R:Y')->getAlignment()->setHorizontal('center')->setVertical('center');
        $objSet->getStyle('Z')->getAlignment()->setVertical('center');
        $objSet->getStyle('C:J')->getAlignment()->setHorizontal('center');

        $objGet->getColumnDimension('A')->setWidth(5);
        $objGet->getColumnDimension('B')->setWidth(40);
        $objGet->getColumnDimension('C')->setWidth(15);
        $objGet->getColumnDimension('D')->setWidth(15);
        $objGet->getColumnDimension('E')->setWidth(12);
        $objGet->getColumnDimension('F')->setWidth(12);
        $objGet->getColumnDimension('G')->setWidth(12);
        $objGet->getColumnDimension('H')->setWidth(12);
        $objGet->getColumnDimension('I')->setWidth(12);
        $objGet->getColumnDimension('J')->setWidth(12);
        $objGet->getColumnDimension('L')->setWidth(12);
        $objGet->getColumnDimension('M')->setWidth(12);
        $objGet->getColumnDimension('N')->setWidth(12);
        $objGet->getColumnDimension('O')->setWidth(12);
        $objGet->getColumnDimension('P')->setWidth(12);
        $objGet->getColumnDimension('Q')->setWidth(12);
        $objGet->getColumnDimension('R')->setWidth(12);
        $objGet->getColumnDimension('S')->setWidth(12);
        $objGet->getColumnDimension('T')->setWidth(12);
        $objGet->getColumnDimension('U')->setWidth(18);
        $objGet->getColumnDimension('V')->setWidth(18);
        $objGet->getColumnDimension('W')->setWidth(12);
        $objGet->getColumnDimension('X')->setWidth(12);
        $objGet->getColumnDimension('Y')->setWidth(12);
        $objGet->getColumnDimension('Z')->setWidth(12);
        $objGet->getColumnDimension('AA')->setWidth(12);
        $objGet->getColumnDimension('AB')->setWidth(12);
        $objGet->getColumnDimension('AC')->setWidth(12);

        $objSet->setCellValueByColumnAndRow(4, 6, $numberCopy);
        $objSet->setCellValueByColumnAndRow(10, 6, $grossIncome);
        $objSet->setCellValueByColumnAndRow(0, 6, "№ п/п");
        $objSet->setCellValueByColumnAndRow(1, 6, $filmTitle);
        $objSet->setCellValueByColumnAndRow(2, 6, $onlineStore);
        $objSet->setCellValueByColumnAndRow(3, 6, $annex);
        $objSet->setCellValueByColumnAndRow(4, 7, "EST");
        $objSet->setCellValueByColumnAndRow(5, 7, "VOD");
        $objSet->setCellValueByColumnAndRow(7, 7, "PVOD" );
        $objSet->setCellValueByColumnAndRow(6, 7, "SVOD");
        $objSet->setCellValueByColumnAndRow(8, 7, "AVOD");
        $objSet->setCellValueByColumnAndRow(9, 7, $totalLabel);
        $objSet->setCellValueByColumnAndRow(10, 7, "EST \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(11, 7, "VOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(12, 7, "SVOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(13, 7, "PVOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(14, 7, "AVOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(15, 7, $totalLabel . " \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(16, 6, $totalGross);
        $objSet->setCellValueByColumnAndRow(17, 6, $minimalGuarranted);
        $objSet->setCellValueByColumnAndRow(18, 6, $technicalAdaptation);
        $objSet->setCellValueByColumnAndRow(19, 6, $technicalMaintenance);
        $objSet->setCellValueByColumnAndRow(20, 6, $royality);
        $objSet->setCellValueByColumnAndRow(21, 6, $royalitySum);
        $objSet->setCellValueByColumnAndRow(22, 6, $leftMinimalGuar);
        $objSet->setCellValueByColumnAndRow(23, 6, $leftTechAdap);
        $objSet->setCellValueByColumnAndRow(24, 6, $leftTechMain);
        $objSet->setCellValueByColumnAndRow(25, 6, $royaltyPayment);
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, 6, $taxSales);
        $objSet->setCellValueByColumnAndRow(27, 6, $closeDate);
        $objSet->setCellValueByColumnAndRow(28, 6, $companyName);
        $objSet->setCellValueByColumnAndRow(16, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(17, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(18, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(19, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(20, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(21, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(22, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(23, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(24, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(25, 7, "({$currency})");
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(0, 8, 1);
        $objSet->setCellValueByColumnAndRow(1, 8, 2);
        $objSet->setCellValueByColumnAndRow(2, 8, 3);
        $objSet->setCellValueByColumnAndRow(3, 8, 4);
        $objSet->setCellValueByColumnAndRow(4, 8, 5);
        $objSet->setCellValueByColumnAndRow(10, 8, 6);
        $objSet->setCellValueByColumnAndRow(16, 8, 7);
        $objSet->setCellValueByColumnAndRow(17, 8, 8);
        $objSet->setCellValueByColumnAndRow(18, 8, 9);
        $objSet->setCellValueByColumnAndRow(19, 8, 10);
        $objSet->setCellValueByColumnAndRow(20, 8, 11);
        $objSet->setCellValueByColumnAndRow(21, 8, 12);
        $objSet->setCellValueByColumnAndRow(22, 8, 13);
        $objSet->setCellValueByColumnAndRow(23, 8, 14);
        $objSet->setCellValueByColumnAndRow(24, 8, 15);
        $objSet->setCellValueByColumnAndRow(25, 8, 16);
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, 8, 17);

        $objGet->getStyle('K7:Z7')->getAlignment()->setWrapText(true);
        $objGet->getStyle('K6:Z6')->getAlignment()->setWrapText(true);
        /*end header elements*/

            $allApplications = []; $totalSale = 0; $totalSummary = 0; 
            $totalSummaryGross = 0; $totalRoyalityPayment = 0; $totalNds = 0;
            foreach ($session['contract_'.$contract_id] as $films) 
            {
                $allApplications [] = $films['application_id'];
            }
            $allApplications = array_unique($allApplications);

            $index = 8; $total_rolyati_sum = 0; $filmsMerge = []; $total_summary = 0; $tartib_raqam = 0;
            foreach ($allApplications as $application_id) 
            {
                $rolyatiIndex = 0; $checkCount = 0; $application = null; $rolyati = 0; $beginIndex = null;
                foreach ($session['contract_'.$contract_id] as $films) 
                {
                    if($films['application_id'] == $application_id)
                    {
                        $index++;
                        $total_summary += $films['summary'];
                        $rolyati += (float)$films['old_rolyati_x_summa'];
                        if($films['check'] === 1)
                        { 
                            if($beginIndex == null) $beginIndex = $index;
                            $checkCount++;
                            if($rolyatiIndex == 0) $rolyatiIndex = $index;
                            $application = Applications::findOne($films['application_id']);
                        }
                        $filmsMerge = Users::findFilmMerge($filmsMerge, $index, $films['contract_id'],$films['filmName'],$films['filmsApplication_id'],$films['application_id'],$films['application_number'], $films['check'], $films['certificate']);

                        $wizard = new \PHPExcel_Helper_HTML;
                        $richText = $wizard->toRichTextObject($films['platformNames']);
                        $royalityPayment = 0;
                        $yigindi = $films['mgt'] + $films['tat'] + $films['mft'];
                        if( $yigindi < $films['summary'] * $films['rolyati'] ) {
                            $royalityPayment = $films['summary'] * $films['rolyati'] - $yigindi;
                            $films['rolyati_x_summa'] = $films['summary'] * $films['rolyati'];
                        }
                        else{
                            $royalityPayment = 0;
                            $films['rolyati_x_summa'] = $films['summary'] * $films['rolyati'];
                        }
                        $nds = 0;
                        if($films['certificate'] != 1){
                            if($curCode == 'RUB'){
                                $nds = -($royalityPayment/1.2 - $royalityPayment);
                                $nds = round($nds, 2);
                            }
                        }

                        if($contract->companyLic->user->localization_id == 2) $filmName = Films::getFilmName($films['film_id'], $films['filmName']);
                        else $filmName = $films['filmName'];

                        $filAp = FilmsApplication::findOne($films['filmsApplication_id']);


                        $objGet->getStyle("A{$index}:AA{$index}")->getFont()->setSize(9);
                        $objSet->setCellValueByColumnAndRow(0, $index, ++$tartib_raqam);
                        $objSet->setCellValueByColumnAndRow(1, $index, $filmName);
                        $objSet->setCellValueByColumnAndRow(2, $index, $richText);
                        $objSet->setCellValueByColumnAndRow(3, $index, $films['application_number']);
                        $objSet->setCellValueByColumnAndRow(4, $index, $films['estCount']);
                        $objSet->setCellValueByColumnAndRow(5, $index, $films['vodCount']);
                        $objSet->setCellValueByColumnAndRow(6, $index, $films['pvodCount']);
                        $objSet->setCellValueByColumnAndRow(7, $index, $films['svodCount']);
                        $objSet->setCellValueByColumnAndRow(8, $index, $films['avodCount']);
                        $objSet->setCellValueByColumnAndRow(9, $index, $films['sale']);
                        $objSet->setCellValueByColumnAndRow(10, $index, $films['est']);
                        $objSet->setCellValueByColumnAndRow(11, $index, $films['vod']);
                        $objSet->setCellValueByColumnAndRow(12, $index, $films['pvod']);
                        $objSet->setCellValueByColumnAndRow(13, $index, $films['svod']);
                        $objSet->setCellValueByColumnAndRow(14, $index, $films['avod']);
                        $objSet->setCellValueByColumnAndRow(15, $index, $films['summary']);
                        $objSet->setCellValueByColumnAndRow(16, $index, (float)$films['summary']);
                        $objSet->setCellValueByColumnAndRow(17, $index, $films['mgt'] === 0 ? '' : $films['mgt'] );
                        $objSet->setCellValueByColumnAndRow(18, $index, $films['tat'] === 0 ? '' : $films['tat'] );
                        $objSet->setCellValueByColumnAndRow(19, $index, $films['mft'] === 0 ? '' : $films['mft'] );
                        $objSet->setCellValueByColumnAndRow(20, $index, $films['rolyati']);
                        $objSet->setCellValueByColumnAndRow(21, $index, $films['rolyati_x_summa']);
                        $objSet->setCellValueByColumnAndRow(22, $index, $films['mgs'] === 0 ? '' : $films['mgs'] );
                        $objSet->setCellValueByColumnAndRow(23, $index, $films['tas'] === 0 ? '' : $films['tas'] );
                        $objSet->setCellValueByColumnAndRow(24, $index, $films['mfs'] === 0 ? '' : $films['mfs'] );
                        $objSet->setCellValueByColumnAndRow(25, $index, $royalityPayment);
                        $objSet->setCellValueByColumnAndRow(27, $index, $filAp->close_date);
                        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, $index, $nds);
                        $objGet->getStyle("K$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("L$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("M$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("N$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("O$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("P$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("Q$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("R$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("S$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("T$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("U$index")->getNumberFormat()->setFormatCode( '0%;[Red]-0%' );
                        //$objGet->getStyle("V$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
                        $objGet->getStyle("V$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("W$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("X$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("Z$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("Y$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        if($curCode == 'RUB') $objGet->getStyle("AA$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );

                        $totalSale += $films['sale'];
                        $totalSummary += $films['summary'];
                        $totalSummaryGross += $films['summary'];
                        $totalRoyalityPayment += $royalityPayment;
                        $totalNds += $nds;

                        if($type == 1){
                            $filAp = FilmsApplication::findOne($films['filmsApplication_id']);
                            $filAp->min_warranty = $films['mgs'];
                            $filAp->tex_adap = $films['tas'];
                            $filAp->maintenance = $films['mfs'];
                            $filAp->save();
                        }
                    }
                }

                /*ustunlarni birlashtirish*/
                if($checkCount > 0){
                    $checkIndex = $checkCount + $beginIndex - 1;
                    $objSet->mergeCells("R".$beginIndex.":R".$checkIndex);
                    $objSet->mergeCells("W".$beginIndex.":W".$checkIndex);
                    $objSet->mergeCells("S".$beginIndex.":S".$checkIndex);
                    $objSet->mergeCells("X".$beginIndex.":X".$checkIndex);
                    $objSet->mergeCells("Y".$beginIndex.":Y".$checkIndex);
                    $objSet->mergeCells("T".$beginIndex.":T".$checkIndex);
                    $objSet->mergeCells("Z".$beginIndex.":Z".$checkIndex);

                    $min_warranty = $application->min_warranty;
                    $tex_adap = $application->tex_adap;
                    $maintenance = $application->maintenance;

                    if($min_warranty === null) $min_warranty = 0;
                    if($tex_adap === null) $tex_adap = 0;
                    if($maintenance === null) $maintenance = 0;

                    if($min_warranty - $rolyati > 0) {
                        $mgs = $min_warranty - $rolyati;
                    }
                    else {
                        $mgs = 0;
                        $rolyati = $rolyati - $min_warranty;
                    }

                    if($mgs == 0 && $rolyati > 0) {
                        if($tex_adap - $rolyati > 0) {
                            $tas = $tex_adap - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $tas = 0;
                            $rolyati = $rolyati - $tex_adap;
                        }
                    }

                    if($tas == 0 && $rolyati > 0) {
                        if($maintenance - $rolyati > 0) {
                            $mfs = $maintenance - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $mfs = 0;
                            $rolyati = $rolyati - $maintenance;
                        }
                    }

                    if($mgs === 0) $mgs = null;
                    if($tas === 0) $tas = null;
                    if($mfs === 0) $mfs = null;

                    if($type == 1){
                        $application->min_warranty = $mgs;
                        $application->tex_adap = $tas;
                        $application->maintenance = $mfs;
                        $application->save();
                    }

                    $r = (string)$objPHPExcel->getActiveSheet()->getCell("R".$rolyatiIndex)->getValue();
                    $sumR = PreferenceBooks::getFloat($r);
                    $sumV = 0;
                    for ($ii = $beginIndex; $ii <= $checkIndex; $ii++) { 
                        $v = (string)$objPHPExcel->getActiveSheet()->getCell("V".$ii)->getValue();
                        $sumV += (float)PreferenceBooks::getFloat($v);
                    }
                    
                    $objSet->setCellValueByColumnAndRow(17, $rolyatiIndex, $min_warranty);
                    $objSet->setCellValueByColumnAndRow(18, $rolyatiIndex, $tex_adap);
                    $objSet->setCellValueByColumnAndRow(19, $rolyatiIndex, $maintenance);
                    $objSet->setCellValueByColumnAndRow(21, $rolyatiIndex, $sumV);
                    $objSet->setCellValueByColumnAndRow(22, $rolyatiIndex, $min_warranty - $sumV);
                    $objSet->setCellValueByColumnAndRow(23, $rolyatiIndex, $tas);
                    $objSet->setCellValueByColumnAndRow(24, $rolyatiIndex, $mfs);

                    $objSet->mergeCells("V".$beginIndex.":V".$checkIndex);
                }
                /*ustunlarni birlashtirish tugashi*/
            }

            foreach ($filmsMerge as $merge) {
                $sums = 0;
                $sumR = '';
                $sumS = '';
                $sumT = '';
                for ($i = $merge['begin']; $i <= $merge['end']; $i++) { 
                    $sums += (float)$objPHPExcel->getActiveSheet()->getCell("Q".$i)->getValue();
                    $r = (string)$objPHPExcel->getActiveSheet()->getCell("R".$i)->getValue();
                    $sumR = PreferenceBooks::getFloat($r); 

                    $s = (string)$objPHPExcel->getActiveSheet()->getCell("S".$i)->getValue();
                    $sumS = PreferenceBooks::getFloat($s); 

                    $t = (string)$objPHPExcel->getActiveSheet()->getCell("T".$i)->getValue();
                    $sumT = PreferenceBooks::getFloat($t); 
                }
                $sumR = (float)$sumR;
                $sumS = (float)$sumS;
                $sumT = (float)$sumT;
                $percent = $objPHPExcel->getActiveSheet()->getCell("U".$merge['begin'])->getValue();
                $objSet->setCellValueByColumnAndRow(16, $merge['begin'], $sums);
                $objGet->getStyle("Q".$merge['begin'])->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                $objSet->mergeCells("Q".$merge['begin'].":Q".$merge['end']);

                $r = (string)$objPHPExcel->getActiveSheet()->getCell("R". $merge['begin'])->getValue();
                $ost = PreferenceBooks::getFloat($r); 

                $w = (string)$objPHPExcel->getActiveSheet()->getCell("W". $merge['begin'])->getValue();
                $prishlo = PreferenceBooks::getFloat($w); 
                
                if($prishlo > 0) {
                	$lit = $percent * $sums;
                    if($merge['check'] === 1) $kk = 0;
                    else $objSet->setCellValueByColumnAndRow(21, $merge['begin'], $lit);
                	$RR = (string)$objPHPExcel->getActiveSheet()->getCell("R".$merge['begin'])->getValue();
                	$sumRR = PreferenceBooks::getFloat($RR); 
                    if($merge['check'] === 1) $kk = 0;
                	else $objSet->setCellValueByColumnAndRow(22, $merge['begin'], $sumRR - $lit);
                }
                else $objSet->setCellValueByColumnAndRow(21, $merge['begin'], $merge['rolyati']);

                $ayirma = ($sums - $sumR - $sumS - $sumT) * $percent;
                if($ayirma < 0) $ayirma = 0;
                $sumsNds = $ayirma - $ayirma/1.2;
                if($sumsNds < 0) $sumsNds = 0;
                $r = (string)$objPHPExcel->getActiveSheet()->getCell("R".$merge['begin'])->getValue();
                $sumR = PreferenceBooks::getFloat($r); 

                $v = (string)$objPHPExcel->getActiveSheet()->getCell("V". $merge['begin'])->getValue();
                $sumV = PreferenceBooks::getFloat($v);

                if($sumV > $sumR) {
                    if($merge['check'] !== 1) $objSet->setCellValueByColumnAndRow(25, $merge['begin'], $ayirma);
                }
                else $objSet->setCellValueByColumnAndRow(25, $merge['begin'], 0);
                if($curCode == 'RUB') {
                    $w = (string)$objPHPExcel->getActiveSheet()->getCell("W". $merge['begin'])->getValue();
                    $sumW = PreferenceBooks::getFloat($w);
                    if($sumW > 0) $objSet->setCellValueByColumnAndRow(26, $merge['begin'], 0);
                    else {
                        if($merge['certificate'] != 1) $objSet->setCellValueByColumnAndRow(26, $merge['begin'], $sumsNds);
                        else $objSet->setCellValueByColumnAndRow(26, $merge['begin'], 0);
                        //$objSet->setCellValueByColumnAndRow(26, $merge['begin'], $sumsNds);
                    }
                }
                if($merge['rolyati'] > $merge['mgt']) $objSet->setCellValueByColumnAndRow(22, $merge['begin'], 0);
                //$objSet->setCellValueByColumnAndRow(27, $merge['begin'], $merge['begin'] . ' va ' . $merge['end'] . ' va ' . $merge['check']);
                
                //$objSet->mergeCells("W".$merge['begin'].":W".$merge['end']);
                $objSet->mergeCells("B".$merge['begin'].":B".$merge['end']);
                $objSet->mergeCells("U".$merge['begin'].":U".$merge['end']);
                if($merge['check'] !== 1) {
                    $objSet->mergeCells("R".$merge['begin'].":R".$merge['end']);
                    $objSet->mergeCells("S".$merge['begin'].":S".$merge['end']);
                    $objSet->mergeCells("T".$merge['begin'].":T".$merge['end']);
                    $objSet->mergeCells("V".$merge['begin'].":V".$merge['end']);
                    $objSet->mergeCells("W".$merge['begin'].":W".$merge['end']);
                    $objSet->mergeCells("X".$merge['begin'].":X".$merge['end']);
                    $objSet->mergeCells("Y".$merge['begin'].":Y".$merge['end']);
                    $objSet->mergeCells("Z".$merge['begin'].":Z".$merge['end']);
                }
                if($curCode == 'RUB') $objSet->mergeCells("AA".$merge['begin'].":AA".$merge['end']);
            }

        //tartib raqam ichin kod
        $tartib_raqami = 1;
        for ($i = 9; $i <= $index; $i++) {
            if($i != 9){
                $now_value = (string)$objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue();
                if($now_value != null) $objSet->setCellValueByColumnAndRow(0, $i, $tartib_raqami++);
                else {
                    $objSet->setCellValueByColumnAndRow(0, $i, $tartib_raqami-1);
                }
            }
            else $objSet->setCellValueByColumnAndRow(0, $i, $tartib_raqami++);
        }

        foreach ($filmsMerge as $merge) {
            $objSet->mergeCells("A".$merge['begin'].":A".$merge['end']);
        }
        //end

        /*begin footer*/
        $sumTotal = 0; $sumTotalNds = 0;
        for ($i = 9; $i <= $index; $i++) { 
            $r = (string)$objPHPExcel->getActiveSheet()->getCell("Z".$i)->getValue();
            $stringSum = PreferenceBooks::getFloat($r);
            $sumTotal += (float)$stringSum;

            $r = (string)$objPHPExcel->getActiveSheet()->getCell("AA".$i)->getValue();
            $stringSum = PreferenceBooks::getFloat($r);
            $sumTotalNds += (float)$stringSum;
        }
        $sumTotal = round($sumTotal, 2);
        $sumTotalNds = round($sumTotalNds, 2);

        $styleForHeaders = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000')));
        $styleForFooter = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'fd0505')));
        $index++;
        $objSet->setCellValueByColumnAndRow(1, $index, $totalLabel);
        $objSet->setCellValueByColumnAndRow(9, $index, $totalSale);
        $objSet->setCellValueByColumnAndRow(15, $index, $totalSummary);
        $objSet->setCellValueByColumnAndRow(16, $index, $totalSummaryGross);
        $objSet->setCellValueByColumnAndRow(25, $index, $sumTotal);
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, $index, $sumTotalNds);
        $objGet->getStyle("P$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("Q$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("Z$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("AA$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("A{$index}:AC{$index}")->applyFromArray($styleForHeaders);
        $index++;
        $objSet->mergeCells("B{$index}:J{$index}");
        $objSet->mergeCells("M{$index}:N{$index}");
        $objGet->getStyle('B'.$index)->applyFromArray($styleForFooter);
        $objGet->getStyle("K{$index}:N{$index}")->applyFromArray($styleForFooter);
        $objSet->setCellValueByColumnAndRow(1, $index, $paymentEquavalent);
        $objSet->setCellValueByColumnAndRow(10, $index, $sumTotal);
        $objSet->setCellValueByColumnAndRow(11, $index, $currency);
        if($curCode == 'RUB'){
            $objSet->setCellValueByColumnAndRow(12, $index, $vTomChisle);
            $objSet->setCellValueByColumnAndRow(14, $index, $sumTotalNds);
        }
        $objGet->getStyle("O$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $index++;$index++;
        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);
        $objGet->getStyle('U'.$index)->applyFromArray($styleForHeaders);
        $objSet->setCellValueByColumnAndRow(1, $index, $licensor);
        $objSet->setCellValueByColumnAndRow(20, $index, $licensee);
        $index++;
        $objSet->setCellValueByColumnAndRow(1, $index, $contract->companyLic->name_ru);
        $objSet->setCellValueByColumnAndRow(20, $index, $contract->companyCont->name_ru);
        $index++;$index++;
        $objSet->setCellValueByColumnAndRow(1, $index, '_______________/_________________ /');
        $objSet->setCellValueByColumnAndRow(20, $index, '_______________/_________________ /');
        /*end footer*/

        if($type === 0){
            $filename = $contract->companyLic->name_ru . '_' . $contract->companyCont->name_ru . '_' . $contract->number . '_' . $month . '.xlsx';
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save('php://output');
            //без этой строки при открытии файла xlsx ошибка!!!!!!
            exit;
        }
    }
    public function actionExportAllContract($type = 0, $month = null)
    {
        $session = Yii::$app->session;


        $contracts = Contract::find()->all();

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->getProperties()->setCreator("creater");
        $objPHPExcel->getProperties()->setLastModifiedBy("Middle field");
        $objPHPExcel->getProperties()->setSubject("Subject");
        $work_sheet = 0;
        $objWorkSheet = $objPHPExcel->createSheet($work_sheet);

        $invalidCharacters = $objWorkSheet->getInvalidCharacters();
        $title = "Отчет за месяц";
        $objWorkSheet->setTitle($title);
        $styleForHeaders = array('font' => array('size' => 9,'bold' => true,'color' => array('rgb' => '000000')));
        $styleForValues = array('font' => array('size' => 9,'color' => array('rgb' => '000000')));

//        /*headerga yozuvlarni yozish*/
//        if($contract->companyLic->user->localization_id == 2){
//            //english
//            $royalty = 'Royalty Statement';
//            $between = "for Film usage on all Annexes under Agreement № {$contract->number} from ".date('d.m.Y', strtotime($contract->date_cr));
//            $second_between = "between {$contract->companyLic->name_ru} (Licensor) and {$contract->companyCont->name_ru} (Licensee)";
//            $period = 'Reporting period '. PreferenceBooks::periodDate($month, 'en', $contract->payment_period);
//            $cityName = "Moscow";
//            $filmTitle = "Film title";
//            $onlineStore = "On-line Store";
//            $annex = "Annex";
//            $numberCopy = "Number of Copies";
//            $grossIncome = "Gross Income";
//            $totalGross = "Total Gross \r Income";
//            $minimalGuarranted = "Minimal Guarantee";
//            $technicalAdaptation = "Technical Adaptation";
//            $technicalMaintenance = "Technical Maintenece";
//            $royality = "Royalty (%)";
//            $royalitySum = "Royalty";
//            $leftMinimalGuar = "Minimal \r Guarantee \r leftover";
//            $leftTechAdap = "Technical \r Adaptation \r leftover";
//            $leftTechMain = "Technical \r Maintenece \r leftover";
//            $royaltyPayment = "Royalty \r Payment";
//            $taxSales = "Sales Tax (20%)";
//            $closeDate = 'Дата закрытия';
//            $paymentEquavalent = "The payment of the equavalent of";
//            $vTomChisle = "Inculding sales tax (20%)";
//            $licensor = "LICENSOR";
//            $licensee = "LICENSEE";
//            $totalLabel = "Total";
//        }
//        else{
            //russian
            $royalty = 'Отчетная ведомость';
//            $between = "об использовании Фильмов по всем приложениям к Лицензионному договору № {$contract->number} от ".date('d.m.Y', strtotime($contract->date_cr));
            $between = "";
//            $second_between = "между ООО «{$contract->companyLic->name_ru}» (Лицензиар) и «{$contract->companyCont->name_ru}» (Лицензиат)";
            $second_between = "";
            $period = "Отчетный период " . PreferenceBooks::periodDate($month, 'ru', $contract->payment_period);
            $cityName = "г. Москва";
            $filmTitle = "Название фильма";
            $onlineStore = "Площадка";
            $annex = "№ Приложения";
            $numberCopy = "Количество копий";
            $grossIncome = "Валовые поступления";
            $totalGross = "Итого вал \r по площадкам";
            $minimalGuarranted = "Размер МГ";
            $technicalAdaptation = "Техническая \r адаптация";
            $technicalMaintenance = "Техническое \r обслуживание";
            $royality = "Ставка \r вознаграждения \r Лицензиара (%)";
            $royalitySum = "Лицензионное \r вознаграждение \r Лицензиара за \r отчетный период";
            $leftMinimalGuar = "Остаток МГ";
            $leftTechAdap = "Остаток \r технической \r адаптации";
            $leftTechMain = "Остаток \r технического \r обслуживания";
            $royaltyPayment = "Сумма к \r выплате";
            $taxSales = "НДС (20%)";
            $closeDate = 'Дата закрытия';
            $companyName = 'Компания';
            $paymentEquavalent = "Подлежит перечислению на расчетный счет Лицензиара сумма в размере ";
            $vTomChisle = "в том числе НДС (20%)";
            $licensor = "ЛИЦЕНЗИАР";
            $licensee = "ЛИЦЕНЗИАТ";
            $totalLabel = "Всего";
//        }

        $monthArray = explode('-', $month);
        $currency = 'RUB';
        $curCode = 'RUB';


        $closeDate = 'Дата закрытия';
        $companyName = 'Компания';

        $dayLast = PreferenceBooks::lastDayOfMonth($monthArray[1] . '-' .PreferenceBooks::monthCode($monthArray[0]) . '-01');
        $objSet = $objPHPExcel->setActiveSheetIndex();
        $objGet = $objPHPExcel->getActiveSheet();
        $objSet->setCellValueByColumnAndRow(0, 2, $royalty);
        $objSet->setCellValueByColumnAndRow(0, 3, $between);
        $objSet->setCellValueByColumnAndRow(0, 4, $second_between);
        $objSet->setCellValueByColumnAndRow(0, 5, $period);
        $objSet->setCellValueByColumnAndRow(0, 1, $cityName);
        $objSet->setCellValueByColumnAndRow(26, 1, $dayLast);

        /*begin header elements*/
        $objSet->mergeCells("A2:T2");
        $objSet->mergeCells("A3:T3");
        $objSet->mergeCells("A4:T4");
        $objSet->mergeCells("A5:T5");
        $objSet->mergeCells("A6:A7");
        $objSet->mergeCells("B6:B7");
        $objSet->mergeCells("C6:C7");
        $objSet->mergeCells("D6:D7");
        $objSet->mergeCells("E6:J6");
        $objSet->mergeCells("K6:P6");
        $objSet->mergeCells("E8:J8");
        $objSet->mergeCells("K8:P8");

        $center = \PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        $objSet->getStyle('A2:W2')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A3:P3')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A4:W4')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A5:W5')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A6:AC6')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A7:AC7')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A8:AC8')->applyFromArray($styleForHeaders)->getAlignment()->setHorizontal($center);
        $objSet->getStyle('A')->getAlignment()->setHorizontal('center')->setVertical('center');
        $objSet->getStyle('B')->getAlignment()->setVertical('center');
        $objSet->getStyle('Q')->getAlignment()->setVertical('center');
        $objSet->getStyle('A6:AC6')->getAlignment()->setVertical('center');
        $objSet->getStyle('A7:AC7')->getAlignment()->setVertical('center');
        $objSet->getStyle('R:Y')->getAlignment()->setHorizontal('center')->setVertical('center');
        $objSet->getStyle('Z')->getAlignment()->setVertical('center');
        $objSet->getStyle('C:J')->getAlignment()->setHorizontal('center');

        $objGet->getColumnDimension('A')->setWidth(5);
        $objGet->getColumnDimension('B')->setWidth(40);
        $objGet->getColumnDimension('C')->setWidth(15);
        $objGet->getColumnDimension('D')->setWidth(15);
        $objGet->getColumnDimension('E')->setWidth(12);
        $objGet->getColumnDimension('F')->setWidth(12);
        $objGet->getColumnDimension('G')->setWidth(12);
        $objGet->getColumnDimension('H')->setWidth(12);
        $objGet->getColumnDimension('I')->setWidth(12);
        $objGet->getColumnDimension('J')->setWidth(12);
        $objGet->getColumnDimension('L')->setWidth(12);
        $objGet->getColumnDimension('M')->setWidth(12);
        $objGet->getColumnDimension('N')->setWidth(12);
        $objGet->getColumnDimension('O')->setWidth(12);
        $objGet->getColumnDimension('P')->setWidth(12);
        $objGet->getColumnDimension('Q')->setWidth(12);
        $objGet->getColumnDimension('R')->setWidth(12);
        $objGet->getColumnDimension('S')->setWidth(12);
        $objGet->getColumnDimension('T')->setWidth(12);
        $objGet->getColumnDimension('U')->setWidth(18);
        $objGet->getColumnDimension('V')->setWidth(18);
        $objGet->getColumnDimension('W')->setWidth(12);
        $objGet->getColumnDimension('X')->setWidth(12);
        $objGet->getColumnDimension('Y')->setWidth(12);
        $objGet->getColumnDimension('Z')->setWidth(12);
        $objGet->getColumnDimension('AA')->setWidth(12);
        $objGet->getColumnDimension('AB')->setWidth(12);
        $objGet->getColumnDimension('AC')->setWidth(12);

        $objSet->setCellValueByColumnAndRow(4, 6, $numberCopy);
        $objSet->setCellValueByColumnAndRow(10, 6, $grossIncome);
        $objSet->setCellValueByColumnAndRow(0, 6, "№ п/п");
        $objSet->setCellValueByColumnAndRow(1, 6, $filmTitle);
        $objSet->setCellValueByColumnAndRow(2, 6, $onlineStore);
        $objSet->setCellValueByColumnAndRow(3, 6, $annex);
        $objSet->setCellValueByColumnAndRow(4, 7, "EST");
        $objSet->setCellValueByColumnAndRow(5, 7, "VOD");
        $objSet->setCellValueByColumnAndRow(7, 7, "PVOD" );
        $objSet->setCellValueByColumnAndRow(6, 7, "SVOD");
        $objSet->setCellValueByColumnAndRow(8, 7, "AVOD");
        $objSet->setCellValueByColumnAndRow(9, 7, $totalLabel);
        $objSet->setCellValueByColumnAndRow(10, 7, "EST \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(11, 7, "VOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(12, 7, "SVOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(13, 7, "PVOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(14, 7, "AVOD \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(15, 7, $totalLabel . " \r ({$currency})");
        $objSet->setCellValueByColumnAndRow(16, 6, $totalGross);
        $objSet->setCellValueByColumnAndRow(17, 6, $minimalGuarranted);
        $objSet->setCellValueByColumnAndRow(18, 6, $technicalAdaptation);
        $objSet->setCellValueByColumnAndRow(19, 6, $technicalMaintenance);
        $objSet->setCellValueByColumnAndRow(20, 6, $royality);
        $objSet->setCellValueByColumnAndRow(21, 6, $royalitySum);
        $objSet->setCellValueByColumnAndRow(22, 6, $leftMinimalGuar);
        $objSet->setCellValueByColumnAndRow(23, 6, $leftTechAdap);
        $objSet->setCellValueByColumnAndRow(24, 6, $leftTechMain);
        $objSet->setCellValueByColumnAndRow(25, 6, $royaltyPayment);
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, 6, $taxSales);
        $objSet->setCellValueByColumnAndRow(27, 6, $closeDate);
        $objSet->setCellValueByColumnAndRow(28, 6, $companyName);
        $objSet->setCellValueByColumnAndRow(16, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(17, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(18, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(19, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(20, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(21, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(22, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(23, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(24, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(25, 7, "({$currency})");
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, 7, "({$currency})");
        $objSet->setCellValueByColumnAndRow(0, 8, 1);
        $objSet->setCellValueByColumnAndRow(1, 8, 2);
        $objSet->setCellValueByColumnAndRow(2, 8, 3);
        $objSet->setCellValueByColumnAndRow(3, 8, 4);
        $objSet->setCellValueByColumnAndRow(4, 8, 5);
        $objSet->setCellValueByColumnAndRow(10, 8, 6);
        $objSet->setCellValueByColumnAndRow(16, 8, 7);
        $objSet->setCellValueByColumnAndRow(17, 8, 8);
        $objSet->setCellValueByColumnAndRow(18, 8, 9);
        $objSet->setCellValueByColumnAndRow(19, 8, 10);
        $objSet->setCellValueByColumnAndRow(20, 8, 11);
        $objSet->setCellValueByColumnAndRow(21, 8, 12);
        $objSet->setCellValueByColumnAndRow(22, 8, 13);
        $objSet->setCellValueByColumnAndRow(23, 8, 14);
        $objSet->setCellValueByColumnAndRow(24, 8, 15);
        $objSet->setCellValueByColumnAndRow(25, 8, 16);
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, 8, 17);

        $objGet->getStyle('K7:Z7')->getAlignment()->setWrapText(true);
        $objGet->getStyle('K6:Z6')->getAlignment()->setWrapText(true);
        /*end header elements*/


        $index = 8; $total_rolyati_sum = 0; $filmsMerge = []; $total_summary = 0; $tartib_raqam = 0;
        foreach ($contracts as $contract)
        {

            $contract_id = $contract->id;

            $allApplications = []; $totalSale = 0; $totalSummary = 0;
            $totalSummaryGross = 0; $totalRoyalityPayment = 0; $totalNds = 0;
            foreach ($session['contract_'.$contract_id] as $films)
            {
                $allApplications [] = $films['application_id'];
            }
            $allApplications = array_unique($allApplications);

            foreach ($allApplications as $application_id)
            {
                $rolyatiIndex = 0; $checkCount = 0; $application = null; $rolyati = 0; $beginIndex = null;
                foreach ($session['contract_'.$contract_id] as $films)
                {
                    if($films['application_id'] == $application_id)
                    {
                        $index++;
                        $total_summary += $films['summary'];
                        $rolyati += (float)$films['old_rolyati_x_summa'];
                        if($films['check'] === 1)
                        {
                            if($beginIndex == null) $beginIndex = $index;
                            $checkCount++;
                            if($rolyatiIndex == 0) $rolyatiIndex = $index;
                            $application = Applications::findOne($films['application_id']);
                        }
                        $filmsMerge = Users::findFilmMerge($filmsMerge, $index, $films['contract_id'],$films['filmName'],$films['filmsApplication_id'],$films['application_id'],$films['application_number'], $films['check'], $films['certificate']);

                        $wizard = new \PHPExcel_Helper_HTML;
                        $richText = $wizard->toRichTextObject($films['platformNames']);
                        $royalityPayment = 0;
                        $yigindi = $films['mgt'] + $films['tat'] + $films['mft'];
                        if( $yigindi < $films['summary'] * $films['rolyati'] ) {
                            $royalityPayment = $films['summary'] * $films['rolyati'] - $yigindi;
                            $films['rolyati_x_summa'] = $films['summary'] * $films['rolyati'];
                        }
                        else{
                            $royalityPayment = 0;
                            $films['rolyati_x_summa'] = $films['summary'] * $films['rolyati'];
                        }
                        $nds = 0;
                        if($films['certificate'] != 1){
                            if($curCode == 'RUB'){
                                $nds = -($royalityPayment/1.2 - $royalityPayment);
                                $nds = round($nds, 2);
                            }
                        }

                        if($contract->companyLic->user->localization_id == 2) $filmName = Films::getFilmName($films['film_id'], $films['filmName']);
                        else $filmName = $films['filmName'];

                        $filAp = FilmsApplication::findOne($films['filmsApplication_id']);
                        $companyNameValue = '';

                        if($filAp){
                            $app = Applications::findOne($filAp->application_id);
                            if($app){
                                $cont = Contract::findOne($app->contract_id);
                                if($cont){
                                    $rightHolder = RightHolders::findOne($cont->company_lic);
                                    if($rightHolder){
                                        $companyNameValue = $rightHolder->name_ru;
                                    }
                                }
                            }
                        }


                        $objGet->getStyle("A{$index}:AA{$index}")->getFont()->setSize(9);
                        $objSet->setCellValueByColumnAndRow(0, $index, ++$tartib_raqam);
                        $objSet->setCellValueByColumnAndRow(1, $index, $filmName);
                        $objSet->setCellValueByColumnAndRow(2, $index, $richText);
                        $objSet->setCellValueByColumnAndRow(3, $index, $films['application_number']);
                        $objSet->setCellValueByColumnAndRow(4, $index, $films['estCount']);
                        $objSet->setCellValueByColumnAndRow(5, $index, $films['vodCount']);
                        $objSet->setCellValueByColumnAndRow(6, $index, $films['pvodCount']);
                        $objSet->setCellValueByColumnAndRow(7, $index, $films['svodCount']);
                        $objSet->setCellValueByColumnAndRow(8, $index, $films['avodCount']);
                        $objSet->setCellValueByColumnAndRow(9, $index, $films['sale']);
                        $objSet->setCellValueByColumnAndRow(10, $index, $films['est']);
                        $objSet->setCellValueByColumnAndRow(11, $index, $films['vod']);
                        $objSet->setCellValueByColumnAndRow(12, $index, $films['pvod']);
                        $objSet->setCellValueByColumnAndRow(13, $index, $films['svod']);
                        $objSet->setCellValueByColumnAndRow(14, $index, $films['avod']);
                        $objSet->setCellValueByColumnAndRow(15, $index, $films['summary']);
                        $objSet->setCellValueByColumnAndRow(16, $index, (float)$films['summary']);
                        $objSet->setCellValueByColumnAndRow(17, $index, $films['mgt'] === 0 ? '' : $films['mgt'] );
                        $objSet->setCellValueByColumnAndRow(18, $index, $films['tat'] === 0 ? '' : $films['tat'] );
                        $objSet->setCellValueByColumnAndRow(19, $index, $films['mft'] === 0 ? '' : $films['mft'] );
                        $objSet->setCellValueByColumnAndRow(20, $index, $films['rolyati']);
                        $objSet->setCellValueByColumnAndRow(21, $index, $films['rolyati_x_summa']);
                        $objSet->setCellValueByColumnAndRow(22, $index, $films['mgs'] === 0 ? '' : $films['mgs'] );
                        $objSet->setCellValueByColumnAndRow(23, $index, $films['tas'] === 0 ? '' : $films['tas'] );
                        $objSet->setCellValueByColumnAndRow(24, $index, $films['mfs'] === 0 ? '' : $films['mfs'] );
                        $objSet->setCellValueByColumnAndRow(25, $index, $royalityPayment);
                        $objSet->setCellValueByColumnAndRow(27, $index, $filAp->close_date);
                        $objSet->setCellValueByColumnAndRow(28, $index, $companyNameValue);
                        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, $index, $nds);
                        $objGet->getStyle("K$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("L$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("M$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("N$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("O$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("P$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("Q$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("R$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("S$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("T$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("U$index")->getNumberFormat()->setFormatCode( '0%;[Red]-0%' );
                        //$objGet->getStyle("V$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
                        $objGet->getStyle("V$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("W$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("X$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("Z$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        $objGet->getStyle("Y$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                        if($curCode == 'RUB') $objGet->getStyle("AA$index")->getNumberFormat()->setFormatCode( ' ###0.00_-' );

                        $totalSale += $films['sale'];
                        $totalSummary += $films['summary'];
                        $totalSummaryGross += $films['summary'];
                        $totalRoyalityPayment += $royalityPayment;
                        $totalNds += $nds;

                        if($type == 1){
                            $filAp = FilmsApplication::findOne($films['filmsApplication_id']);
                            $filAp->min_warranty = $films['mgs'];
                            $filAp->tex_adap = $films['tas'];
                            $filAp->maintenance = $films['mfs'];
                            $filAp->save();
                        }
                    }
                }

                /*ustunlarni birlashtirish*/
                if($checkCount > 0){
                    $checkIndex = $checkCount + $beginIndex - 1;
                    $objSet->mergeCells("R".$beginIndex.":R".$checkIndex);
                    $objSet->mergeCells("W".$beginIndex.":W".$checkIndex);
                    $objSet->mergeCells("S".$beginIndex.":S".$checkIndex);
                    $objSet->mergeCells("X".$beginIndex.":X".$checkIndex);
                    $objSet->mergeCells("Y".$beginIndex.":Y".$checkIndex);
                    $objSet->mergeCells("T".$beginIndex.":T".$checkIndex);
                    $objSet->mergeCells("Z".$beginIndex.":Z".$checkIndex);

                    $min_warranty = $application->min_warranty;
                    $tex_adap = $application->tex_adap;
                    $maintenance = $application->maintenance;

                    if($min_warranty === null) $min_warranty = 0;
                    if($tex_adap === null) $tex_adap = 0;
                    if($maintenance === null) $maintenance = 0;

                    if($min_warranty - $rolyati > 0) {
                        $mgs = $min_warranty - $rolyati;
                    }
                    else {
                        $mgs = 0;
                        $rolyati = $rolyati - $min_warranty;
                    }

                    if($mgs == 0 && $rolyati > 0) {
                        if($tex_adap - $rolyati > 0) {
                            $tas = $tex_adap - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $tas = 0;
                            $rolyati = $rolyati - $tex_adap;
                        }
                    }

                    if($tas == 0 && $rolyati > 0) {
                        if($maintenance - $rolyati > 0) {
                            $mfs = $maintenance - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $mfs = 0;
                            $rolyati = $rolyati - $maintenance;
                        }
                    }

                    if($mgs === 0) $mgs = null;
                    if($tas === 0) $tas = null;
                    if($mfs === 0) $mfs = null;

                    if($type == 1){
                        $application->min_warranty = $mgs;
                        $application->tex_adap = $tas;
                        $application->maintenance = $mfs;
                        $application->save();
                    }

                    $r = (string)$objPHPExcel->getActiveSheet()->getCell("R".$rolyatiIndex)->getValue();
                    $sumR = PreferenceBooks::getFloat($r);
                    $sumV = 0;
                    for ($ii = $beginIndex; $ii <= $checkIndex; $ii++) {
                        $v = (string)$objPHPExcel->getActiveSheet()->getCell("V".$ii)->getValue();
                        $sumV += (float)PreferenceBooks::getFloat($v);
                    }

                    $objSet->setCellValueByColumnAndRow(17, $rolyatiIndex, $min_warranty);
                    $objSet->setCellValueByColumnAndRow(18, $rolyatiIndex, $tex_adap);
                    $objSet->setCellValueByColumnAndRow(19, $rolyatiIndex, $maintenance);
                    $objSet->setCellValueByColumnAndRow(21, $rolyatiIndex, $sumV);
                    $objSet->setCellValueByColumnAndRow(22, $rolyatiIndex, $min_warranty - $sumV);
                    $objSet->setCellValueByColumnAndRow(23, $rolyatiIndex, $tas);
                    $objSet->setCellValueByColumnAndRow(24, $rolyatiIndex, $mfs);

                    $objSet->mergeCells("V".$beginIndex.":V".$checkIndex);
                }
                /*ustunlarni birlashtirish tugashi*/
            }

            foreach ($filmsMerge as $merge) {
                $sums = 0;
                $sumR = '';
                $sumS = '';
                $sumT = '';
                for ($i = $merge['begin']; $i <= $merge['end']; $i++) {
                    $sums += (float)$objPHPExcel->getActiveSheet()->getCell("Q".$i)->getValue();
                    $r = (string)$objPHPExcel->getActiveSheet()->getCell("R".$i)->getValue();
                    $sumR = PreferenceBooks::getFloat($r);

                    $s = (string)$objPHPExcel->getActiveSheet()->getCell("S".$i)->getValue();
                    $sumS = PreferenceBooks::getFloat($s);

                    $t = (string)$objPHPExcel->getActiveSheet()->getCell("T".$i)->getValue();
                    $sumT = PreferenceBooks::getFloat($t);
                }
                $sumR = (float)$sumR;
                $sumS = (float)$sumS;
                $sumT = (float)$sumT;
                $percent = $objPHPExcel->getActiveSheet()->getCell("U".$merge['begin'])->getValue();
                $objSet->setCellValueByColumnAndRow(16, $merge['begin'], $sums);
                $objGet->getStyle("Q".$merge['begin'])->getNumberFormat()->setFormatCode( ' ###0.00_-' );
                $objSet->mergeCells("Q".$merge['begin'].":Q".$merge['end']);

                $r = (string)$objPHPExcel->getActiveSheet()->getCell("R". $merge['begin'])->getValue();
                $ost = PreferenceBooks::getFloat($r);

                $w = (string)$objPHPExcel->getActiveSheet()->getCell("W". $merge['begin'])->getValue();
                $prishlo = PreferenceBooks::getFloat($w);

                if($prishlo > 0) {
                    $lit = $percent * $sums;
                    if($merge['check'] === 1) $kk = 0;
                    else $objSet->setCellValueByColumnAndRow(21, $merge['begin'], $lit);
                    $RR = (string)$objPHPExcel->getActiveSheet()->getCell("R".$merge['begin'])->getValue();
                    $sumRR = PreferenceBooks::getFloat($RR);
                    if($merge['check'] === 1) $kk = 0;
                    else $objSet->setCellValueByColumnAndRow(22, $merge['begin'], $sumRR - $lit);
                }
                else $objSet->setCellValueByColumnAndRow(21, $merge['begin'], $merge['rolyati']);

                $ayirma = ($sums - $sumR - $sumS - $sumT) * $percent;
                if($ayirma < 0) $ayirma = 0;
                $sumsNds = $ayirma - $ayirma/1.2;
                if($sumsNds < 0) $sumsNds = 0;
                $r = (string)$objPHPExcel->getActiveSheet()->getCell("R".$merge['begin'])->getValue();
                $sumR = PreferenceBooks::getFloat($r);

                $v = (string)$objPHPExcel->getActiveSheet()->getCell("V". $merge['begin'])->getValue();
                $sumV = PreferenceBooks::getFloat($v);

                if($sumV > $sumR) {
                    if($merge['check'] !== 1) $objSet->setCellValueByColumnAndRow(25, $merge['begin'], $ayirma);
                }
                else $objSet->setCellValueByColumnAndRow(25, $merge['begin'], 0);
                if($curCode == 'RUB') {
                    $w = (string)$objPHPExcel->getActiveSheet()->getCell("W". $merge['begin'])->getValue();
                    $sumW = PreferenceBooks::getFloat($w);
                    if($sumW > 0) $objSet->setCellValueByColumnAndRow(26, $merge['begin'], 0);
                    else {
                        if($merge['certificate'] != 1) $objSet->setCellValueByColumnAndRow(26, $merge['begin'], $sumsNds);
                        else $objSet->setCellValueByColumnAndRow(26, $merge['begin'], 0);
                        //$objSet->setCellValueByColumnAndRow(26, $merge['begin'], $sumsNds);
                    }
                }
                if($merge['rolyati'] > $merge['mgt']) $objSet->setCellValueByColumnAndRow(22, $merge['begin'], 0);
                //$objSet->setCellValueByColumnAndRow(27, $merge['begin'], $merge['begin'] . ' va ' . $merge['end'] . ' va ' . $merge['check']);

                //$objSet->mergeCells("W".$merge['begin'].":W".$merge['end']);
                $objSet->mergeCells("B".$merge['begin'].":B".$merge['end']);
                $objSet->mergeCells("U".$merge['begin'].":U".$merge['end']);
                if($merge['check'] !== 1) {
                    $objSet->mergeCells("R".$merge['begin'].":R".$merge['end']);
                    $objSet->mergeCells("S".$merge['begin'].":S".$merge['end']);
                    $objSet->mergeCells("T".$merge['begin'].":T".$merge['end']);
                    $objSet->mergeCells("V".$merge['begin'].":V".$merge['end']);
                    $objSet->mergeCells("W".$merge['begin'].":W".$merge['end']);
                    $objSet->mergeCells("X".$merge['begin'].":X".$merge['end']);
                    $objSet->mergeCells("Y".$merge['begin'].":Y".$merge['end']);
                    $objSet->mergeCells("Z".$merge['begin'].":Z".$merge['end']);
                }
                if($curCode == 'RUB') $objSet->mergeCells("AA".$merge['begin'].":AA".$merge['end']);
            }

            //tartib raqam ichin kod
            $tartib_raqami = 1;
            for ($i = 9; $i <= $index; $i++) {
                if($i != 9){
                    $now_value = (string)$objPHPExcel->getActiveSheet()->getCell("B".$i)->getValue();
                    if($now_value != null) $objSet->setCellValueByColumnAndRow(0, $i, $tartib_raqami++);
                    else {
                        $objSet->setCellValueByColumnAndRow(0, $i, $tartib_raqami-1);
                    }
                }
                else $objSet->setCellValueByColumnAndRow(0, $i, $tartib_raqami++);
            }

            foreach ($filmsMerge as $merge) {
                $objSet->mergeCells("A".$merge['begin'].":A".$merge['end']);
            }
            //end


        }

        /*begin footer*/
        $sumTotal = 0; $sumTotalNds = 0;
        for ($i = 9; $i <= $index; $i++) {
            $r = (string)$objPHPExcel->getActiveSheet()->getCell("Z".$i)->getValue();
            $stringSum = PreferenceBooks::getFloat($r);
            $sumTotal += (float)$stringSum;

            $r = (string)$objPHPExcel->getActiveSheet()->getCell("AA".$i)->getValue();
            $stringSum = PreferenceBooks::getFloat($r);
            $sumTotalNds += (float)$stringSum;
        }
        $sumTotal = round($sumTotal, 2);
        $sumTotalNds = round($sumTotalNds, 2);

        $styleForHeaders = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => '000000')));
        $styleForFooter = array('font' => array('size' => 12,'bold' => true,'color' => array('rgb' => 'fd0505')));
        $index++;
        $objSet->setCellValueByColumnAndRow(1, $index, $totalLabel);
        $objSet->setCellValueByColumnAndRow(9, $index, $totalSale);
        $objSet->setCellValueByColumnAndRow(15, $index, $totalSummary);
        $objSet->setCellValueByColumnAndRow(16, $index, $totalSummaryGross);
        $objSet->setCellValueByColumnAndRow(25, $index, $sumTotal);
        if($curCode == 'RUB') $objSet->setCellValueByColumnAndRow(26, $index, $sumTotalNds);
        $objGet->getStyle("P$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("Q$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("Z$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("AA$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $objGet->getStyle("A{$index}:AC{$index}")->applyFromArray($styleForHeaders);
        $index++;
        $objSet->mergeCells("B{$index}:J{$index}");
        $objSet->mergeCells("M{$index}:N{$index}");
        $objGet->getStyle('B'.$index)->applyFromArray($styleForFooter);
        $objGet->getStyle("K{$index}:N{$index}")->applyFromArray($styleForFooter);
        $objSet->setCellValueByColumnAndRow(1, $index, $paymentEquavalent);
        $objSet->setCellValueByColumnAndRow(10, $index, $sumTotal);
        $objSet->setCellValueByColumnAndRow(11, $index, $currency);
        if($curCode == 'RUB'){
            $objSet->setCellValueByColumnAndRow(12, $index, $vTomChisle);
            $objSet->setCellValueByColumnAndRow(14, $index, $sumTotalNds);
        }
        $objGet->getStyle("O$index")->getNumberFormat()->setFormatCode( '"'.$curCode.'" ###0.00_-' );
        $index++;$index++;
        $objGet->getStyle('B'.$index)->applyFromArray($styleForHeaders);
        $objGet->getStyle('U'.$index)->applyFromArray($styleForHeaders);
        $objSet->setCellValueByColumnAndRow(1, $index, $licensor);
        $objSet->setCellValueByColumnAndRow(20, $index, $licensee);
        $index++;
        $objSet->setCellValueByColumnAndRow(1, $index, $contract->companyLic->name_ru);
        $objSet->setCellValueByColumnAndRow(20, $index, $contract->companyCont->name_ru);
        $index++;$index++;
        $objSet->setCellValueByColumnAndRow(1, $index, '_______________/_________________ /');
        $objSet->setCellValueByColumnAndRow(20, $index, '_______________/_________________ /');
        /*end footer*/




        if($type === 0){
            $filename = 'Отчет за_' . $month . '.xlsx';
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            header('Cache-Control: max-age=0');
            $objWriter->setPreCalculateFormulas(false);
            $objWriter->save('php://output');
            //без этой строки при открытии файла xlsx ошибка!!!!!!
            exit;
        }
    }
}
