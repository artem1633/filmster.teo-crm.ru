<?php

namespace app\controllers;

use Yii;
use app\models\ReportMonetary;
use app\models\ReportMonetarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\ReportValues;
use yii\data\ActiveDataProvider;
use app\models\SettingCourses;
use app\models\Contract;
use app\models\FilmsApplication;
use app\models\Applications;
use app\models\CurrencySetting;
use app\models\ReportCheck;
use app\models\Users;
use app\models\Currency;

/**
 * ReportMonetaryController implements the CRUD actions for ReportMonetary model.
 */
class ReportMonetaryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReportMonetary models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $request = Yii::$app->request;
        $post = $request->post();

        //  $Month_r = [ "01" => "Янв", "02" => "Фев", "03" => "Мар", "04" => "Апр",  "05" => "Май", "06" => "Июн", "07" => "Июл", "08" => "Авг", "09" => "Сен", "10" => "Окт", "11" => "Ноя", "12" => "Дек"];

        // //  Dogovor boyicha
        //   for ($i = 1; $i <date('m'); $i++) { 
        //     if($i < 10) $day = '0'.$i;
        //     else $day = $i;
        // }
        
        // $month=$Month_r[$day]."-".date("Y");

        if($post['month'] !== null) {
            $query = ReportMonetary::find()->where(['date' => $post['month']]);
        }
        else {
            $query = ReportMonetary::find();
            // ->where(['date' => $month]);
        }

        $dataProvider = new ActiveDataProvider([
             'query' => $query,
         ]);

        

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single ReportMonetary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $filmsApplication = FilmsApplication::find()->all();
        $applicationID = [];
        foreach ($filmsApplication as $value) {
            foreach (json_decode($value->platforms) as $platform) {
                if($platform->id == $model->platform_id) $applicationID [] = $value->application_id;
            }
        }

        $applications = Applications::find()->where(['id' => array_unique($applicationID)])->all();
        $contractID = [];
        foreach ($applications as $application) {
            $contractID [] = $application->contract_id;
        }

        $contracts = Contract::find()->where(['id' => array_unique($contractID)])->all();
        $query = ReportValues::find()->where(['report_monetary_id' => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $session = Yii::$app->session;
        $session['contractID'] = $contractID;
        ReportValues::setCourses($id);

        return $this->render('view', [
            'id' => $id,
            'model' => $model,
            'contracts' => $contracts,
        ]);
    }

    /**
     * Creates a new ReportMonetary model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new ReportMonetary();
        //$model->territory = 1;
        //$model->currency = 1;
        $number = ReportMonetary::getLastId(); 
        Directory::createReportMonetaryDirectory($number);
        /*$model->save();
        return $this->redirect(['view', 'id' => $model->id]);*/

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){

                $model->files = UploadedFile::getInstance($model, 'files');
                if(!empty($model->files))
                {
                    $model->files->saveAs('uploads/report-monetary/' . $number . '/' . $model->files->baseName.'.'.$model->files->extension);
                    Yii::$app->db->createCommand()->update('report_monetary', ['file' => $model->files->baseName.'.'.$model->files->extension], [ 'id' => $model->id ])->execute();
                    
                    $value = $model->getValuesFromExcell($model->files->baseName.'.'.$model->files->extension);
                    //$model->setCurrency();
                }
                else $value = '<span style="color:red;font-weight:bold;font-size:14px;"><center>Ошибка! Со стороны пользователя exсel файл не загружено</center></span>';

                return [
                    'title'=> "Загрузка отчета",
                    'forceReload'=>'#crud-datatable-pjax',
                    'content' => $value,
                ];

                return $this->redirect(['view', 'id' => $model->id]);      
            }else{           
                return [
                    'title'=> "Создать",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];         
            }
        }else{
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing ReportMonetary model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update ReportMonetary #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "ReportMonetary #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update ReportMonetary #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionSetValues($id, $attribute, $value)
    {
        $model = $this->findModel($id);
        if($attribute == 'name') $model->name = $value;
        if($attribute == 'platform_id') $model->platform_id = $value;
        if($attribute == 'date') $model->date = $value;
        $model->save();
    }

    public function actionSetFile()
    {
        if(isset($_POST) == true){
            //generate unique file name
            $id = $_POST['id'];
            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            //$fileName = $id.basename($_FILES["file"]["name"]);
            $fileName = $id. '.' .$ext;
            
            //file upload path
            $targetDir = "uploads/report-monetary/" . $id . '/';
            $targetFilePath = $targetDir . $fileName;
            
            //allow certain file formats
            $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
            $allowTypes = array('xls','xlsx');
            
            if(in_array($fileType, $allowTypes)){
                //upload file to server
                if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
                    //insert file data into the database if needed
                    $model = $this->findModel($id);
                    $model->file = $fileName;
                    $model->save();
                    $response['status'] = 'ok';
                }else{
                    $response['status'] = 'err';
                }
            }else{
                $response['status'] = 'type_err';
            }
            
            //render response data in JSON format
            return json_encode($response);
        }
    }

    /**
     * Delete an existing ReportMonetary model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing ReportMonetary model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the ReportMonetary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReportMonetary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReportMonetary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetSettingCourse($id, $currency_id, $payment_currency, $sum, $currencyValue, $val, $monetary_id)
    {
        $reportMonetary = ReportMonetary::findOne($monetary_id);
        $currencySetting = CurrencySetting::find()
            ->where(['currency_from_id' => $currency_id, 'currency_to_id' => $payment_currency, 'monetary_id' => $reportMonetary->id])
            ->one();
        if($currencySetting !== null){
            Yii::$app->db->createCommand()->update('currency_setting', ['currency_to_value' => (float)$val, 'currency_from_value' => (float)1], [ 'id' => $currencySetting->id ])->execute();
        }
        else{
            $currencySetting = CurrencySetting::find()
                ->where(['currency_from_id' => $payment_currency, 'currency_to_id' => $currency_id, 'monetary_id' => $reportMonetary->id])
                ->one();
            if($currencySetting !== null){
                Yii::$app->db->createCommand()->update('currency_setting', ['currency_from_value' => (float)$val, 'currency_to_value' => (float)1], [ 'id' => $currencySetting->id ])->execute();
                
            }  
        }

        Yii::$app->db->createCommand()->update('setting_courses', ['course' => (float)$val, 'value' => $currencyValue / $val], [ 'id' => $id ])->execute();
    }

    public function actionSetCourse($id, $attribute, $val)
    {
        $currencySetting = CurrencySetting::findOne($id);
        if($attribute == 'currency_from_value') $currencySetting->currency_from_value = (float)$val;
        if($attribute == 'currency_to_value') $currencySetting->currency_to_value = (float)$val;
        $currencySetting->save();
    }

    public function actionChangeCourses($report_monetary_id)
    {
        $resultID = ReportValues::setCourses($report_monetary_id);
        $request = Yii::$app->request;
        $currencySettings = CurrencySetting::find()->where(['id' => $resultID])->all();

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->post()){
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Создать нового жанра",
                'content'=>'<span class="text-success">Успешно выполнено</span>',
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать ещё',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])
            ];         
        }else{           
            return [
                'title'=> "Настройка курсов",
                'size' => 'large',
                'content'=>$this->renderAjax('courses', [
                    'currencySettings' => $currencySettings,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-info','data-dismiss'=>"modal"])
            ];         
        }
    }

    public function actionAddValue($id, $attribute, $localization_id, $contract_id, $value)
    {
        $session = Yii::$app->session;
        $contract = Contract::findOne($contract_id);
        //$session['contract_report_id_'.$contract_id];
        $sum = Users::setKoeffitsiyent($contract, $session['contract_report_id_'.$contract_id], $localization_id, $value, 0);

        $cur = Currency::findOne($contract->payment_currency)->code;
        return $sum . ' ' .$cur;

        /*$model = ReportCheck::findOne($id);
        if($model->check != 1) {
            $model->$attribute = $value;
            $model->save();
            ReportCheck::getTotalSummary($contract_id, $localization_id, $value);
        }*/
    }

    public function actionCheckValue($id, $attribute, $localization_id, $contract_id, $value)
    {
        $session = Yii::$app->session;
        $contract = Contract::findOne($contract_id);
        //$session['contract_report_id_'.$contract_id];
        $cur = Currency::findOne($contract->payment_currency)->code;

        $model = ReportCheck::findOne($id);
        if($model->check != 1) {
            $sum = Users::setKoeffitsiyent($contract, $session['contract_report_id_'.$contract_id], $localization_id, $value, 1);
            $model->$attribute = $value;
        }
        else $sum = Users::setKoeffitsiyent($contract, $session['contract_report_id_'.$contract_id], $localization_id, $model->value, 0);
        $model->check = 1;
        $model->save();

        return $sum . ' ' .$cur;
    }

    public function actionChangeValue($id)
    {
        $model = ReportCheck::findOne($id);
        $model->check = 1;
        $model->save();
    }
}
