<?php

namespace app\controllers;

use app\components\ReportStatistic;
use app\models\ReportItunes;
use app\models\ReportItunesSearch;
use Yii;
use app\models\Actuality;
use app\models\ActualitySearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\helpers\DateTimeHelper;
use yii\data\ArrayDataProvider;

class SalesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Actuality models.
     * @return mixed
     */
    public function actionUnit()
    {    
        /*$searchModel = new ActualitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
*/

        $dataProvider = new ArrayDataProvider();

        $searchModel = new ReportItunesSearch();

        $searchModel->load(Yii::$app->request->queryParams);

        $searchModel->search([]);


        $objects = [];

        $contentData = [];
        $contentTypeData = [];
        $genreData = [];
        $regionData = [];
        $formatData = [];

		//        $report = new ReportStatistic(['filterData' => ['noRelation' => $searchModel->noRelation, 'dates' => $searchModel->dates]]);

		if($searchModel->dateStart == null || $searchModel->dateEnd == null){
		    $dates[] = date('Y-m-d', (time() - (86400 * 30)));
		    $dates[] = date('Y-m-d');
		} else {
		    $dates = [$searchModel->dateStart, $searchModel->dateEnd];
		}

		if(Yii::$app->user->identity->type == 0){
		    $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andFilterWhere(['title' => $searchModel->title])->groupBy('title')->all();
		} else {
		    if($searchModel->title == null){
		        $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andFilterWhere(['film_id' => Yii::$app->user->identity->getRelatedFilmsIds()])->groupBy('title')->all();
		    } else {
		        $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andFilterWhere(['title' => $searchModel->title])->groupBy('title')->all();
		    }
		}


		$titlesArr = []; // ['poster', 'title', 'type', 'sales']

		$totalSales = 0;

		$reportItunesModels = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->all();

		foreach ($allTitles as $model){

		    // $sales = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andWhere(['title' => $model->title])->all();
		    $sales = $this->filterByTitle($reportItunesModels, $model->title);
		    $salesSum = 0;

//		    foreach ($sales as $sale) {
//		    	$salesSum += $sale->sales;
//		    }
//
//		    $contentData[] = [
//		        'poster' =>'',
//		        'title' => $model->title,
//		        'film_id' => $model->film_id,
//		        'apple_identifier' => $model->apple_identifier,
//		        'sales' => $salesSum,
//		    ];

		    foreach ($sales as $sale) {
				$found = false;
                $salesSum += $sale->sales;
//		        for($i = 0; $i < count($contentData); $i++)
//		        {
//		        	$d = $contentData[$i];
//
//		        	if($d['title'] == $sale->title){
//		        		$contentData[$i]['sales'] = $contentData[$i]['sales'] + $sale->sales;
//		        		$found = true;
//		        	}
//		        }
//		        if($found == false){
//		        	$contentData[] = [
//		        		'country' => $sale->country,
//		        		'sales' => $sale->sales,
//		        	];
//		        }

				$found = false;
		        for($i = 0; $i < count($regionData); $i++)
		        {
		        	$d = $regionData[$i];

		        	if($d['country'] == $sale->country){
		        		$regionData[$i]['sales'] = $regionData[$i]['sales'] + $sale->sales;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$regionData[] = [
		        		'country' => $sale->country,
		        		'sales' => $sale->sales,
		        	];
		        }


		        $found = false;
		        for($i = 0; $i < count($genreData); $i++)
		        {
		        	$d = $genreData[$i];

		        	if($d['genre'] == $sale->genre){
		        		$genreData[$i]['sales'] = $genreData[$i]['sales'] + $sale->sales;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$genreData[] = [
		        		'genre' => $sale->genre,
		        		'sales' => $sale->sales,
		        	];
		        }


				$found = false;
		        for($i = 0; $i < count($formatData); $i++)
		        {
		        	$d = $formatData[$i];

		        	if($d['format'] == $sale->format){
		        		$formatData[$i]['sales'] = $formatData[$i]['sales'] + $sale->sales;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$formatData[] = [
		        		'format' => $sale->format,
		        		'sales' => $sale->sales,
		        	];
		        }

				$found = false;
		        for($i = 0; $i < count($contentTypeData); $i++)
		        {
		        	$d = $contentTypeData[$i];

		        	if($d['type'] == $sale->type){
		        		$contentTypeData[$i]['sales'] = $contentTypeData[$i]['sales'] + $sale->sales;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$contentTypeData[] = [
		        		'type' => $sale->type,
		        		'sales' => $sale->sales,
		        	];
		        }

//				$found = false;
//		        for($i = 0; $i < count($contentTransactionData); $i++)
//		        {
//		        	$d = $contentTransactionData[$i];
//
//		        	if($d['type_transaction'] == $sale->type_transaction){
//		        		$contentTransactionData[$i]['sales'] = $contentTransactionData[$i]['sales'] + $salesSum;
//		        		$found = true;
//		        	}
//		        }
//		        if($found == false){
//		        	$contentTransactionData[] = [
//		        		'type_transaction' => $sale->type_transaction,
//		        		'sales' => $salesSum,
//		        	];
//		        }
		    }


		     $contentData[] = [
		         'poster' =>'',
		         'title' => $model->title,
		         'film_id' => $model->film_id,
		         'apple_identifier' => $model->apple_identifier,
		         'sales' => $salesSum,
		     ];
		}

        usort($contentData, function($a, $b){
            return $a['sales'] < $b['sales'];
        });

        if(count($contentData) > 100){
            $contentData = array_slice($contentData, 0, 100);
        }
		$days = DateTimeHelper::getDates($dates[0], $dates[1]);

		$data = [];

		foreach ($days as $day)
		{
            if(Yii::$app->user->identity->type == 0){
                $data[] = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['title' => $searchModel->title])->sum('sales');
            } else {
                if($searchModel->title == null){
                    $data[] = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['film_id' => Yii::$app->user->identity->getRelatedFilmsIds()])->sum('sales');
                } else {
                    $data[] = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['title' => $searchModel->title])->sum('sales');
                }
            }
		}

		$objects[] = [
		    'label' => 'Контент',
		    'data' => $data,
		    'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
		];

		$labels = json_encode($days);
		$objects = json_encode($objects);


        return $this->render('unit', [
            'searchModel' => $searchModel,
        	'objects' => $objects,
        	'labels' => $labels,
        	'dataProvider' => $dataProvider,
        	'totalSales' => $totalSales,
            'searchModel' => $searchModel,
            'contentData' => $contentData,
        	'contentTypeData' => $contentTypeData,
        	'genreData' => $genreData,
        	'regionData' => $regionData,
        	'formatData' => $formatData,
        ]);
    }

    public function actionRoyalty()
    {
        $searchModel = new ReportItunesSearch();

        $searchModel->load(Yii::$app->request->queryParams);

        $searchModel->search([]);


        $objects = [];

        $contentData = [];
        $contentTypeData = [];
        $genreData = [];
        $regionData = [];
        $formatData = [];

		if($searchModel->dateStart == null || $searchModel->dateEnd == null){
		    $dates[] = date('Y-m-d', (time() - (86400 * 30)));
		    $dates[] = date('Y-m-d');
		} else {
		    $dates = [$searchModel->dateStart, $searchModel->dateEnd];
		}

		if(Yii::$app->user->identity->type == 0){
		    $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andFilterWhere(['customer_currency' => $searchModel->currency])->andFilterWhere(['title' => $searchModel->title])->groupBy('title')->all();
		} else {
		    if($searchModel->title == null){
		        $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andWhere(['customer_currency' => $searchModel->currency])->andFilterWhere(['film_id' => Yii::$app->user->identity->getRelatedFilmsIds()])->groupBy('title')->all();
		    } else {
		        $allTitles = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andWhere(['customer_currency' => $searchModel->currency])->andFilterWhere(['title' => $searchModel->title])->groupBy('title')->all();
		    }
		}

		// var_dump($allTitles);
		// exit;


		$titlesArr = []; // ['poster', 'title', 'type', 'sales']

		$totalSales = 0;

		$reportItunesModels = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();


		foreach ($allTitles as $model){

		    // $sales = \app\models\ReportItunes::find()->where(['between', 'date', $searchModel->dateStart, $searchModel->dateEnd])->andWhere(['title' => $model->title])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();

			$sales = $this->filterByTitle($reportItunesModels, $model->title);

		    $salesSum = 0;

		    foreach ($sales as $sale)
		    {
                $sale->sales = round($sale->sales);
                $sale->customer_price = round($sale->customer_price);
		        $salesSum += $sale->sales * $sale->customer_price;

		        $found = false;
		        for($i = 0; $i < count($regionData); $i++)
		        {
		        	$d = $regionData[$i];

		        	if($d['country'] == $sale->country){
		        		$regionData[$i]['sales'] = $regionData[$i]['sales'] + $sale->sales * $sale->customer_price;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$regionData[] = [
		        		'country' => $sale->country,
		        		'sales' => $sale->sales * $sale->customer_price,
		        	];
		        }


		        $found = false;
		        for($i = 0; $i < count($genreData); $i++)
		        {
		        	$d = $genreData[$i];

		        	if($d['genre'] == $sale->genre){
		        		$genreData[$i]['sales'] = $genreData[$i]['sales'] + $sale->sales * $sale->customer_price;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$genreData[] = [
		        		'genre' => $sale->genre,
		        		'sales' => $sale->sales * $sale->customer_price,
		        	];
		        }


				$found = false;
		        for($i = 0; $i < count($formatData); $i++)
		        {
		        	$d = $formatData[$i];

		        	if($d['format'] == $sale->format){
		        		$formatData[$i]['sales'] = $formatData[$i]['sales'] + $sale->sales * $sale->customer_price;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$formatData[] = [
		        		'format' => $sale->format,
		        		'sales' => $sale->sales * $sale->customer_price,
		        	];
		        }

				$found = false;
		        for($i = 0; $i < count($contentTypeData); $i++)
		        {
		        	$d = $contentTypeData[$i];

		        	if($d['type'] == $sale->type){
		        		$contentTypeData[$i]['sales'] = $contentTypeData[$i]['sales'] + $sale->sales * $sale->customer_price;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$contentTypeData[] = [
		        		'type' => $sale->type,
		        		'sales' => $sale->sales * $sale->customer_price,
		        	];
		        }

				$found = false;
		        for($i = 0; $i < count($contentTransactionData); $i++)
		        {
		        	$d = $contentTransactionData[$i];

		        	if($d['type_transaction'] == $sale->type_transaction){
		        		$contentTransactionData[$i]['sales'] = $contentTransactionData[$i]['sales'] + $sale->sales * $sale->customer_price;
		        		$found = true;
		        	}
		        }
		        if($found == false){
		        	$contentTransactionData[] = [
		        		'type_transaction' => $sale->type_transaction,
		        		'sales' => $sale->sales * $sale->customer_price,
		        	];
		        }

		    }

		    $contentData[] = [
		        'poster' =>'',
		        'title' => $model->title,
		        'film_id' => $model->film_id,
		        'apple_identifier' => $model->apple_identifier,
		        'sales' => $salesSum,
		    ];


		    $totalSales += round($salesSum);
		}



		usort($titlesArr, function($a, $b){
		    return $a['sales'] < $b['sales'];
		});

		    if(count($titlesArr) > 100){
		        $titlesArr = array_slice($titlesArr, 0, 100);
		    }



		//\yii\helpers\VarDumper::dump($titlesArr, 10, true);
		//exit;

		$dataProvider = new \yii\data\ArrayDataProvider(['allModels' => $titlesArr]);
		$dataProvider->pagination = false;

		$days = DateTimeHelper::getDates($dates[0], $dates[1]);

		$data = [];

		foreach ($days as $day)
		{
		    if(Yii::$app->user->identity->type == 0){
		        $dayModels = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['title' => $searchModel->title])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();
		    } else {
		        $dayModels = \app\models\ReportItunes::find()->where(['date' => $day])->andFilterWhere(['title' => $searchModel->title])->andFilterWhere(['customer_currency' => $searchModel->currency])->all();
		    }

		    $sum = 0;
		    foreach($dayModels as $dayModel)
		    {
		        $sum += $dayModel->customer_price * $dayModel->sales;
		    }
		    $data[] = round($sum);
		}

		$objects[] = [
		    'label' => 'Контент',
		    'data' => $data,
		    'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
		];

		//\yii\helpers\VarDumper::dump($dataset[0], 10, true);

		//foreach ($dataset[1] as $title => $arr){
		//    $objects[] = [
		//        'label' => $title,
		//        'data' => $arr,
		//        'backgroundColor' => "rgba(".rand(90, 255).",".rand(100, 255).",".rand(90, 255).",0.6)",
		//    ];
		//}

		$labels = json_encode($days);
		$objects = json_encode($objects);




		$titles = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'name', 'name');

		$films = \yii\helpers\ArrayHelper::map(\app\models\Films::find()->all(), 'id', 'name');

        return $this->render('royalty', [
        	'objects' => $objects,
        	'labels' => $labels,
        	'dataProvider' => $dataProvider,
        	'totalSales' => $totalSales,
            'searchModel' => $searchModel,
            'contentData' => $contentData,
        	'contentTypeData' => $contentTypeData,
        	'genreData' => $genreData,
        	'regionData' => $regionData,
        	'formatData' => $formatData,
        ]);
    }

    protected function filterByTitle($reportItunesModels, $title)
    {
    	return array_filter($reportItunesModels, function($model) use($title){
    		return $model->title == $title;
    	});
    }
}
