<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_check".
 *
 * @property int $id
 * @property int $report_id Отчет
 * @property string $date Дата
 * @property int $check Check
 * @property double $value Значение
 */
class ReportCheck extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_check';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_id', 'check'], 'integer'],
            [['value'], 'number'],
            [['date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_id' => 'Отчет',
            'date' => 'Дата',
            'check' => 'Check',
            'value' => 'Значение',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->value = 1;
        }
        return parent::beforeSave($insert);
    }


    public function getTotalSummary($contract_id, $localization_id, $protsent)
    {
        $session = Yii::$app->session;
        $reportId = $session['contract_report_id_'.$contract_id];

        $contract = Contract::findOne($contract_id);
        if($localization_id === null || $localization_id === 1) $localization = 1;
        else $localization = 2;

        $filmsPrint = [];
        $totalsummary = 0;
        $applicationID = [];
        $filmsId = [];
        $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
        foreach ($applications as $value) {
            $applicationID [] = $value->id;
        }

        $filmsApplication = FilmsApplication::find()->where(['application_id' => $applicationID ])->all();
        foreach ($filmsApplication as $value) 
        {
            $filmsId [] = $value->film_id;
        }

        foreach ($filmsId as $film_id) 
        {
            $film = Films::findOne($film_id);
            $filmName = $film->name . ( $film->year_issue !== null ? ' (' . $film->year_issue . ')' : '' );
            $filmsApplication = FilmsApplication::find()->joinWith('application')->where(['films_application.film_id' => $film_id, 'applications.contract_id' => $contract->id ])->one();

            $allTerritories = [];
            foreach (json_decode($filmsApplication->territory) as $territory) {
                $allTerritories [] = $territory->id;
            }
            $reportValues = ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $film_id])
                ->andWhere(['territory' => $allTerritories])
                ->all();

            foreach ($reportValues as $reportValue) 
            {
                $reportValue->sale_count *= $protsent;
                $reportValue->est *= $protsent;
                $reportValue->vod *= $protsent;
                $reportValue->svod *= $protsent;
                $reportValue->pvod *= $protsent;
                $reportValue->avod *= $protsent;
                $reportValue->save();
            }

            $reportValues = ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $film_id])
                ->andWhere(['territory' => $allTerritories])
                ->all();


            $platformJson = json_decode($filmsApplication->platforms);
            $platforms = [];
            $platformNames = '';
            foreach ($platformJson as $fff) {
                $platforms [] = $fff->id;
                $platformNames .= Platform::findOne($fff->id)->name_ru . '; ';
            }

            $proverka = 0;
            if($reportValues != null){
                foreach ($reportValues as $reportValue) 
                {
                    if(in_array($reportValue->territory, $allTerritories)) $proverka = 1;
                }
            }else $proverka = 1;
            if($proverka === 0) continue;

            foreach ($platforms as $platform) 
            {
                $platformName = Platform::findOne($platform)->name_ru;
                $estCount = null; $estSum = null;
                $vodCount = null; $vodSum = null;
                $svodCount = null; $svodSum = null;
                $pvodCount = null; $pvodSum = null;
                $avodCount = null; $avodSum = null;
                $summary = 0;
                foreach ($reportValues as $reportValue) 
                {
                    if($reportValue->reportMonetary->platform_id == $platform) 
                    {
                        $sum = 0;
                        if($reportValue->est !== null) { $estCount += $reportValue->sale_count; $sum += $reportValue->est;  }
                        if($reportValue->vod !== null) { $vodCount += $reportValue->sale_count; $sum += $reportValue->vod;  }
                        if($reportValue->svod !== null) { $svodCount += $reportValue->sale_count; $sum += $reportValue->svod; }
                        if($reportValue->pvod !== null) { $pvodCount += $reportValue->sale_count; $sum += $reportValue->pvod; }
                        if($reportValue->avod !== null) { $avodCount += $reportValue->sale_count; $sum += $reportValue->avod; }

                        $currencyValue = 0;
                        $currencySetting = CurrencySetting::find()
                            ->where(['currency_from_id' => $reportValue->currency, 'currency_to_id' => $contract->payment_currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                        if($currencySetting != null) 
                        {
                            $currencyValue = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                        }
                        else{
                            $currencySetting = CurrencySetting::find()
                                ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $reportValue->currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                            if($currencySetting != null)
                            {
                                $currencyValue = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                            }
                        }
                        if($currencySetting != null) { 
                            $summary += $currencyValue * $sum; 
                            if($reportValue->est != null) $estSum += $currencyValue * $reportValue->est;
                            if($reportValue->vod != null) $vodSum += $currencyValue * $reportValue->vod;
                            if($reportValue->svod != null) $svodSum += $currencyValue * $reportValue->svod;
                            if($reportValue->pvod != null) $pvodSum += $currencyValue * $reportValue->pvod;
                            if($reportValue->avod != null) $avodSum += $currencyValue * $reportValue->avod;
                        }
                        else {
                            $summary += $sum;
                            if($reportValue->est != null) $estSum += $reportValue->est;
                            if($reportValue->vod != null) $vodSum += $reportValue->vod;
                            if($reportValue->svod != null) $svodSum += $reportValue->svod;
                            if($reportValue->pvod != null) $pvodSum += $reportValue->pvod;
                            if($reportValue->avod != null) $avodSum += $reportValue->avod;
                        }
                    }
                }

                $sale = $estCount + $vodCount + $svodCount + $pvodCount  + $avodCount;
                $currency = Currency::findOne($contract->payment_currency)->code;
                $rolyati_x_summa = $summary*$filmsApplication->rolyati_x*0.01;
                $summary = round($summary, 2);
                $rolyati_x_summa = round($rolyati_x_summa, 2);
                if(is_nan($rolyati_x_summa)) $rolyati = 0;
                else $rolyati = $rolyati_x_summa;

                if($filmsApplication->application->uniform_condition === 1){
                    $min_warranty = $filmsApplication->application->min_warranty;
                    $tex_adap = $filmsApplication->application->tex_adap;
                    $maintenance = $filmsApplication->application->maintenance;
                }
                else{
                    $min_warranty = $filmsApplication->min_warranty;
                    $tex_adap = $filmsApplication->tex_adap;
                    $maintenance = $filmsApplication->maintenance;
                }
                                    
                if($min_warranty === null) $min_warranty = 0;
                if($tex_adap === null) $tex_adap = 0;
                if($maintenance === null) $maintenance = 0;

                $findLastMinWarranty = Users::findLastMinWarranty($filmsPrint, $contract->id, $filmName, $filmsApplication->id, $filmsApplication->application->id, $filmsApplication->application->number);
                if($findLastMinWarranty !== null) $min_warranty = $findLastMinWarranty;

                if($min_warranty - $rolyati > 0) {
                    $mgs = $min_warranty - $rolyati;
                    $rolyati = 0;
                }
                else {
                    $mgs = 0;
                    $rolyati = $rolyati - $min_warranty;
                }

                if($mgs == 0 && $rolyati > 0) {
                    if($tex_adap - $rolyati > 0) {
                        $tas = $tex_adap - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $tas = 0;
                        $rolyati = $rolyati - $tex_adap;
                    }
                }

                if($tas == 0 && $rolyati > 0) {
                    if($maintenance - $rolyati > 0) {
                        $mfs = $maintenance - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $mfs = 0;
                        $rolyati = $rolyati - $maintenance;
                    }
                }

                $currency = $contract->CurrencyDescription($contract->payment_currency);
                $curCode = PreferenceBooks::getCurrencySymbol($currency);
                if(is_nan($estSum)) $estSum = null;
                if(is_nan($vodSum)) $vodSum = null;
                if(is_nan($svodSum)) $svodSum = null;
                if(is_nan($pvodSum)) $pvodSum = null;
                if(is_nan($avodSum)) $avodSum = null;
                if(is_nan($summary)) $summary = 0;

                $filmsPrint [] = [
                    'contract_id' => $contract->id,
                    'filmName' => $filmName,
                    'certificate' => $film->certificate,
                    'filmsApplication_id' => $filmsApplication->id,
                    'application_id' => $filmsApplication->application->id,
                    'application_number' => $filmsApplication->application->number,
                    //'rolyati' => ($filmsApplication->rolyati_x !== null ? ($filmsApplication->rolyati_x . '/' . $filmsApplication->rolyati_y ): ''),
                    'rolyati' => ($filmsApplication->rolyati_x !== null ? 0.01 * $filmsApplication->rolyati_x : ''),
                    'platformNames' => $platformName,
                    'check' => $filmsApplication->application->uniform_condition,
                    'mgt' => $min_warranty,
                    'mgs' => $mgs == null ? $min_warranty : $mgs,
                    'tat' => $tex_adap,
                    'tas' => $tas == null ? $tex_adap : $tas,
                    'mft' => $maintenance,
                    'mfs' => $mfs == null ? $maintenance : $mfs,
                    'estCount' => $estCount,
                    'est' => $estSum != null ? round($estSum, 2) : null,
                    'vodCount' => $vodCount,
                    'vod' => $vodSum != null ? round($vodSum, 2) : null,
                    'svodCount' => $svodCount,
                    'svod' => $svodSum != null ? round($svodSum, 2) : null,
                    'pvodCount' => $pvodCount,
                    'pvod' => $pvodSum != null ? round($pvodSum, 2) : null,
                    'avodCount' => $avodCount,
                    'avod' => $avodSum != null ? round($avodSum, 2) : null,
                    'sale' => $sale,
                    'summary' => $summary,
                    'old_rolyati_x_summa' => $rolyati_x_summa,
                    'rolyati_x_summa' => $rolyati,
                ];

                $totalsummary += $summary;
            }
        }

        $session = Yii::$app->session;
        $session['contract_'.$contract->id] = $filmsPrint;

    }
}
