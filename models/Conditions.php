<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "conditions".
 *
 * @property int $id
 * @property int $report_setting_id Настройка отчета
 * @property int $action_id Действия
 * @property int $film_id Фильм
 * @property int $sale_id Модели продаж
 * @property string $date_begin Даты начала продаж
 * @property string $date_end Даты окончания продаж
 * @property int $currency_id Валюта
 *
 * @property ConditionColumns[] $conditionColumns
 * @property ReportSetting $reportSetting
 */
class Conditions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conditions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_setting_id', 'action_id', 'film_id', 'sale_id', 'currency_id'], 'integer'],
            [['date_begin', 'date_end'], 'safe'],
            [['report_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportSetting::className(), 'targetAttribute' => ['report_setting_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_setting_id' => 'Настройка отчета',
            'action_id' => 'Действия',
            'film_id' => 'Фильм',
            'sale_id' => 'Модели продаж',
            'date_begin' => 'Даты начала продаж',
            'date_end' => 'Даты окончания продаж',
            'currency_id' => 'Валюта',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $column = new ConditionColumns(); 
            $column->condition_id = $this->id;
            $column->condition_name = 1;
            $column->column_name = 'A';
            $column->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        $columns = ConditionColumns::find()->where(['condition_id' => $this->id])->all();
        foreach ($columns as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditionColumns()
    {
        return $this->hasMany(ConditionColumns::className(), ['condition_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportSetting()
    {
        return $this->hasOne(ReportSetting::className(), ['id' => 'report_setting_id']);
    }

    public function getActionsList()
    {
        return [
            1 => 'Является количеством продаж',
            2 => 'Является фильмом ',
            3 => 'Является моделью продаж',
            4 => 'Дата начала отчетного периода',
            5 => 'Дата окончания отчетного периода',
            6 => 'Валюта продаж',
            7 => 'Территория',
            8 => 'Идентификатор площадки',
            9 => 'Дополнительный идентификатор площадки',
        ];
    }

    public function getFilmsList()
    {
        $films = Films::find()->all();
        return ArrayHelper::map( $films , 'id', 'name');
    }

    public function getCurrencyList()
    {
        $payment_currency = Currency::find()->all();
        return ArrayHelper::map( $payment_currency , 'id', 'code');
    }
}
