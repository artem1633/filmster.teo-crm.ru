<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "right_holders".
 *
 * @property int $id
 * @property int $user_id Клиент/Пользователь
 * @property string $name_ru Название лицензиата RU
 * @property string $name_eng Название лицензиата ENG
 * @property string $tax_id TAX ID
 * @property string $registry_code Registry Registration Code
 * @property string $primary_number Primary State Registration Number
 * @property string $street1 Street1
 * @property string $street2 Street2
 * @property int $country_id Страна
 * @property string $city_id Город
 * @property string $state Регион/Штат
 * @property string $bank_name Bank name
 * @property string $bank_street1 Адрес банка Street1
 * @property string $bank_street2 Street2
 * @property int $bank_country_id Страна банка
 * @property string $bank_state Регион/штат
 * @property string $bank_index Индекс
 * @property string $bic_swift BIC/SWIFT
 * @property double $bank_account Bank account
 * @property int $bank_account_currency Выбор валюты
 * @property double $iban IBAN
 * @property int $iban_currency Валюта
 * @property double $correspondent_account Correspondent account
 * @property int $correspondent_account_currency Валюта
 *
 * @property Country $bankCountry
 * @property Country $country
 * @property Users $user
 */
class RightHolders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'right_holders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru'], 'required'],
            [['user_id', 'country_id', 'bank_country_id', 'bank_account_currency', 'iban_currency', 'correspondent_account_currency'], 'integer'],
            [['street1', 'street2', 'bank_street1', 'bank_street2'], 'string'],
            [['correspondent_account'], 'number'],
            [['name_ru', 'name_eng', 'tax_id', 'registry_code', 'primary_number', 'city_id', 'state', 'bank_name', 'bank_state', 'bank_index', 'bic_swift', 'iban', 'bank_account', 'index'], 'string', 'max' => 255],
            [['bank_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['bank_country_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Клиент/Пользователь',
            'name_ru' => 'Название Компании',
            'name_eng' => 'Английское название компании',
            'tax_id' => 'ИНН', //'TAX ID',
            'registry_code' => 'Регистрационный код регистрации', //'Registry Registration Code',
            'primary_number' => 'Основной государственный рег. номер', //'Primary State Registration Number',
            'street1' => 'Улица1', //'Street1',
            'street2' => 'Улица2', //'Street2',
            'country_id' => 'Страна',
            'city_id' => 'Город',
            'state' => 'Регион/Штат',
            'index' => 'Индекс',
            'bank_name' => 'Название банка', //'Bank name',
            'bank_street1' => 'Улица1', //'Street1',
            'bank_street2' => 'Улица2', //'Street2',
            'bank_country_id' => 'Страна банка',
            'bank_state' => 'Регион/штат',
            'bank_index' => 'Индекс',
            'bic_swift' => 'BIC/SWIFT',
            'bank_account' => 'Банковский счет', //'Bank account',
            'bank_account_currency' => 'Валюта',
            'iban' => 'IBAN',
            'iban_currency' => 'Валюта',
            'correspondent_account' => 'Корреспондент. учетная запись', //'Corresp. account',
            'correspondent_account_currency' => 'Валюта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'bank_country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function getCountriesList()
    {
        $countries = Country::find()->all();
        return ArrayHelper::map($countries, 'id', 'name');
    }

    public function getCurrency()
    {
        $bank_account_currency = Currency::find()->all();
        return ArrayHelper::map( $bank_account_currency , 'id', 'code');
    }

    public function getUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map( $users , 'id', 'fio');
    }
}
