<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "technical_playgrounds".
 *
 * @property int $id
 * @property int $playground_id Площадка
 * @property int $technical_id Техническая информация
 * @property string $value_playground Значение площадки
 * @property string $additional_value Доп. значение площадки
 *
 * @property Platform $playground
 * @property TechnicalInformation $technical
 */
class TechnicalPlaygrounds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'technical_playgrounds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['playground_id', 'technical_id'], 'integer'],
            [['value_playground', 'additional_value', 'territory'], 'string', 'max' => 255],
            [['playground_id'], 'exist', 'skipOnError' => true, 'targetClass' => Platform::className(), 'targetAttribute' => ['playground_id' => 'id']],
            [['technical_id'], 'exist', 'skipOnError' => true, 'targetClass' => TechnicalInformation::className(), 'targetAttribute' => ['technical_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'playground_id' => 'Площадка',
            'technical_id' => 'Техническая информация',
            'value_playground' => 'Значение площадки',
            'additional_value' => 'Доп. значение площадки',
            'territory' => "Территория",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayground()
    {
        return $this->hasOne(Platform::className(), ['id' => 'playground_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnical()
    {
        return $this->hasOne(TechnicalInformation::className(), ['id' => 'technical_id']);
    }

    public function getPlayGroundList()
    {
        $playGround = Platform::find()->all();
        return ArrayHelper::map($playGround, 'id', 'name_ru');
    }
}
