<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $name Название
 *
 * @property Films[] $films
 * @property RightHolders[] $rightHolders
 * @property RightHolders[] $rightHolders0
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name',], 'required'],
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Код',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilms()
    {
        return $this->hasMany(Films::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightHolders()
    {
        return $this->hasMany(RightHolders::className(), ['bank_country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightHolders0()
    {
        return $this->hasMany(RightHolders::className(), ['country_id' => 'id']);
    }
}
