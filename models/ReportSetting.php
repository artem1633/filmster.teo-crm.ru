<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_setting".
 *
 * @property int $id
 * @property int $platform_id Площадка
 * @property string $excel_file Файл
 * @property int $begin_parse Начинать со строки
 * @property int $end_parse Закончить на строке
 *
 * @property Conditions[] $conditions
 * @property Platform $platform
 */
class ReportSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['platform_id', 'begin_parse', 'end_parse'], 'integer'],
            [['excel_file'], 'string', 'max' => 255],
            [['platform_id'], 'exist', 'skipOnError' => true, 'targetClass' => Platform::className(), 'targetAttribute' => ['platform_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'platform_id' => 'Площадка',
            'excel_file' => 'Файл',
            'begin_parse' => 'Начинать со строки',
            'end_parse' => 'Закончить на строке',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $condition = new Conditions(); 
            $condition->report_setting_id = $this->id;
            $condition->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        $colditions = Conditions::find()->where(['report_setting_id' => $this->id])->all();
        foreach ($colditions as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditions()
    {
        return $this->hasMany(Conditions::className(), ['report_setting_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['id' => 'platform_id']);
    }
}
