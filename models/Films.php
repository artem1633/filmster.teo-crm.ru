<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;



/**
 * This is the model class for table "films".
 *
 * @property int $id
 * @property string $name Название фильма
 * @property int $type_id Тип
 * @property string $number_season Номер сезона
 * @property string $number_seriya Номер серии
 * @property string $alternative Альтернативная нумерация
 * @property int $language_id Основной язык локализации
 * @property int $certificate Наличие удостоверения Национального фильма
 * @property int $hide_film Скрыть данный фильм
 * @property string $description Краткое описание
 * @property string $imdb IMDB
 * @property string $rotten Rotten Tomatoes
 * @property string $eidr EIDR
 * @property string $kinopoisk Kinopoisk.ru
 * @property int $year_issue Год выпуска фильма
 * @property string $rellase_date Дата релиза фильма
 * @property int $country_id Страна производства
 * @property string $genres Жанр
 * @property int $duration Продолжительность
 * @property int $actuality_id Актуалность релиза
 * @property string $budget Бюджет фильма
 * @property string $reyting Возрастной рейтинг фильма
 * @property string $manufacturer_id
 * @property string $tags Теги
 * @property string $awards Награды
 *
 * @property Actuality $actuality
 * @property Country $country
 * @property Languages $language
 * @property FilmsType $type
 * @property FilmsApplication[] $filmsApplications
 * @property Localization[] $localizations
 * @property ReportValues[] $reportValues
 * @property TechnicalInformation[] $technicalInformations
 */
class Films extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'films';
    }

    public $files;
    public $sheet_number;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type_id'], 'required'],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',], 
            [['type_id', 'language_id', 'certificate', 'hide_film', 'country_id', 'duration', 'actuality_id', 'sheet_number'], 'integer'],
            [['description', 'budget', 'reyting'], 'string'],
            [['rellase_date', 'create_date'], 'safe'],
            [['name', 'number_season', 'number_seriya', 'alternative', 'imdb', 'rotten', 'eidr', 'kinopoisk', 'manufacturer_id', 'awards'], 'string', 'max' => 255],
            [['actuality_id'], 'exist', 'skipOnError' => true, 'targetClass' => Actuality::className(), 'targetAttribute' => ['actuality_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilmsType::className(), 'targetAttribute' => ['type_id' => 'id']],
            // [['number_season'], 'required', 'when' => function() {
            //        if($this->type->key == 'serial') return TRUE;
            //        else return FALSE;
            // }],
            // [['number_seriya'], 'required', 'when' => function() {
            //        if($this->type->key == 'serial') return TRUE;
            //        else return FALSE;
            // }],
            ['year_issue', 'validateYear'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название фильма',
            'type_id' => 'Тип',
            'number_season' => 'Номер сезона',
            'number_seriya' => 'Номер серии',
            'alternative' => 'Альтернативная нумерация',
            'language_id' => 'Основной язык локализации',
            'certificate' => 'Наличие удостоверения Национального фильма',
            'hide_film' => 'Скрыть данный фильм',
            'description' => 'Краткое описание',
            'imdb' => 'IMDB',
            'rotten' => 'Rotten Tomatoes',
            'eidr' => 'EIDR',
            'kinopoisk' => 'Kinopoisk.ru',
            'year_issue' => 'Год',
            'rellase_date' => 'Дата релиза фильма',
            'country_id' => 'Страна производства',
            'genres' => 'Жанр',
            'manufacturer_id' => 'Клиент правообладатель',
            'duration' => 'Продолжительность',
            'actuality_id' => 'Актуалность релиза',
            'budget' => 'Бюджет фильма',
            'reyting' => 'Возрастной рейтинг фильма',
            'tags' => 'Теги',
            'awards' => 'Награды',
            'create_date' => 'Дата создания',
            'sheet_number' => 'Лист',
        ];
    }

    public function beforeSave($insert)
    {  
        if($this->rellase_date != null) $this->rellase_date = date('Y-m-d', strtotime($this->rellase_date));
        
        if ($this->isNewRecord)
        {
            $this->create_date = date("Y-m-d");
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $number = Localization::getLastId(); 
            Directory::createLocalizationDirectory($number);
            $localization = new Localization();
            $localization->type = 1;
            $localization->language_id = $this->language_id;
            $localization->name = $this->name;
            $localization->description = $this->description;
            $localization->film_id = $this->id;
            $localization->save();

            $number = TechnicalInformation::getLastId(); 
            Directory::createTechnicalDirectory($number);
            $tech = new TechnicalInformation();
            $tech->film_id = $this->id;
            $tech->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        $applications = FilmsApplication::find()->where(['film_id' => $this->id])->all();
        foreach ($applications as $value) {
            $value->delete();
        }

        $localization = Localization::find()->where(['film_id' => $this->id])->all();
        foreach ($localization as $value) {
            $value->delete();
        }

        $technical = TechnicalInformation::find()->where(['film_id' => $this->id])->all();
        foreach ($technical as $value) {
            $value->delete();
        }

        $report = ReportValues::find()->where(['film_id' => $this->id])->all();
        foreach ($report as $value) {
            $value->delete();
        }

        Directory::deleteDirectory($this->id, 'films');
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActuality()
    {
        return $this->hasOne(Actuality::className(), ['id' => 'actuality_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Languages::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(FilmsType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilmsApplications()
    {
        return $this->hasMany(FilmsApplication::className(), ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalizations()
    {
        return $this->hasMany(Localization::className(), ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
/*    public function getRights()
    {
        return $this->hasMany(Rights::className(), ['film_id' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportValues()
    {
        return $this->hasMany(ReportValues::className(), ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnicalInformations()
    {
        return $this->hasMany(TechnicalInformation::className(), ['film_id' => 'id']);
    }

    public function getFilmName($id, $filmName)
    {
        $loc = Localization::find()->where(['film_id' => $id, 'language_id' => 1])->one();
        if($loc == null) return $filmName;
        else return $loc->name;
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'films'")
            ->one();
        if($res) return $res["AUTO_INCREMENT"];
    }

    public function validateYear($attribute)
    { 
        $year_issue = $this->year_issue;
        $strlen = strlen( $year_issue );
        $numeric = 0; 
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $year_issue, $i, 1 );             
            if(ord($char) > 47 && ord($char) < 58) $numeric++;            
        }
        if($numeric != 4) $this->addError($attribute, 'Вводите полностью полю «Год выпуска фильма»');
    }

    public function tagsDescription()
    {
        $array = explode(",", $this->tags);
        $result = '<ul class="list-tags">';
        foreach ($array as $value) {
            $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' . $value . '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

    public function awardsDescription()
    {
        $result = "";
        foreach (json_decode($this->awards) as $value) {
            $result .= 'Название фестиваля : ' . $value->festival . '; Приз : ' . $value->prize . '<br>';
        }
        return $result;
    }

    public function getReleaseDate()
    {
        $array = explode('-', $this->rellase_date);
        return PreferenceBooks::monthName($array[1]) . '-' . $array[0];
    }

    public function getFilmsTypeList()
    {
        $pro = FilmsType::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    
    public function getLanguagesList()
    {
        $pro = Languages::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    public function getCountryList()
    {
        $pro = Country::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    public function getManufacturerList()
    {
        $pro = Manufacturer::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    public function getGenresList()
    {
        $genre = Genres::find()->all();
        return ArrayHelper::map($genre, 'id', 'name');
    }

    public function getActualityList()
    {
        $actuality = Actuality::find()->all();
        return ArrayHelper::map($actuality, 'id', 'name');
    }

    public function getCurrency($id)
    {
        $currency = Currency::find()->all();
        $result = '';
        foreach ($currency as $value) {
            if($id == $value->id) $result .= '<option selected value="'.$value->id.'">'.$value->name.'</option>';
            else $result .= '<option value="'.$value->id.'">'.$value->name.'</option>';
            
        }
        return $result;
    }

    public function getCountrySelected($id)
    {
        $country = Country::find()->all();
        $result = '';
        foreach ($country as $value) {
            if($id == $value->id) $result .= '<option selected value="'.$value->id.'">'.$value->name.'</option>';
            else $result .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $result;
    }

    public function getYouth($youthName)
    {
        $youth = PreferenceBooks::youthList();
        $result = '';
        foreach ($youth as $key => $value) {
            if($youthName == $value) $result .= '<option selected value="'.$key.'">'.$value.'</option>';
            else $result .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $result;
    }

    public function excel_to_array($inputFileName, $sheet_number)
    {
        try {
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch(Exception $e) {
            return ('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet($sheet_number); 
        $highestRow = $sheet->getHighestRow(); 
        $highestColumn = $sheet->getHighestColumn();
        $keys = array();
        $results = array();           
        for ($row = 4; $row <= $highestRow; $row++){ 
            $rowData = $sheet->rangeToArray('C' . $row . ':' . $highestColumn . $row,null,true,false);
            if ($row === 4){
                $keys = $rowData[0];
            } else {
                $record = array();
                foreach($rowData[0] as $pos=>$value) {
                    if($pos == 18 || $pos == 19 || $pos == 20){
                        if($value !== null){
                            $excel_date = $value; //here is that value 41621 or 41631
                            $unix_date = ($excel_date - 25569) * 86400;
                            $excel_date = 25569 + ($unix_date / 86400);
                            $unix_date = ($excel_date - 25569) * 86400;
                            $record[$pos] = gmdate("Y-m-d", $unix_date); 
                        }
                        else $record[$pos] = null; 
                    }
                    else $record[$pos] = $value; 
                }
                $results[] = $record;           
            }
        } 
        return $results;
    }

    public function setValues($result, $sheet)
    {
        foreach ($result as $value) 
        {
            if($value[4] !== null){
                $rightHolder = RightHolders::find()->where(['name_ru' => (string)$value[4]])->one();
                if($rightHolder === null){
                    $rightHolder = new RightHolders();
                    $rightHolder->name_ru = (string)$value[4];
                    $rightHolder->save();
                }
            }
            else $rightHolder = null;

            if($value[6] !== null){
                $contract = Contract::find()->where(['number' => (string)$value[6]])->one();
                if($contract === null){
                    $contract = new Contract();
                    $contract->number = (string)$value[6];
                    if($rightHolder !== null) $contract->company_lic = $rightHolder->id;
                    $contract->save();
                }
                if($contract !== null){
                    if($value[8] !== null){
                        $application = Applications::find()->where(['contract_id' => $contract->id, 'number' => (string)$value[8]])->one();
                        if($application === null){
                            $application = new Applications();
                            $application->date_cr = date("Y-m-d");
                            $application->contract_id = $contract->id;
                            $application->number = (string)$value[8];
                            $application->save();
                        }

                        if($application !== null) {
                            if($sheet == 2) { 
                                $playground = 1;
                                $tech = TechnicalPlaygrounds::find()->where(['playground_id' => $playground, 'value_playground' => (string)$value[2]])->one();
                            }
                            if($sheet == 3) { 
                                $playground = 2;
                                $tech = TechnicalPlaygrounds::find()->where(['playground_id' => $playground, 'value_playground' => (string)$value[2]])->one();
                            }
                            if($sheet == 4) { 
                                $playground = 3;
                                $val = str_replace('https://youtu.be/','',(string)$value[24]);
                                $tech = TechnicalPlaygrounds::find()->where(['playground_id' => $playground, 'additional_value' => $val])->one();
                            }
                            if($tech !== null){
                                $filmsApp = FilmsApplication::find()->where(['film_id' => $tech->technical->film_id, 'application_id' => $application->id])->one();
                                if($filmsApp === null){
                                    $filmsApp = new FilmsApplication();
                                    $filmsApp->film_id = $tech->technical->film_id;
                                    $filmsApp->application_id = $application->id;
                                }
                                $filmsApp->rolyati_x = $value[12] * 100;
                                $filmsApp->rolyati_y = $value[15] * 100;
                                $filmsApp->est_open = $value[18];
                                $filmsApp->vod_open = $value[19];
                                $filmsApp->close_date = $value[20];
                                if($value[22] === 'World') $filmsApp->territory = $this->getAllTerritory();
                                else $filmsApp->territory = '[{"id":172}]';
                                $filmsApp->platforms = Films::setPlatform($filmsApp->platforms, $playground);
                                $filmsApp->save();
                            }
                        }

                    }
                }
            }
        }
    }

    public function setPlatform($items, $platform)
    {
        $result = [];
        $check = 0;
        foreach (json_decode($items) as $value) {
            if($value->id == $platform) $check = 1;
            $result [] = [
                'id' => $value->id,
            ];
        }
        if($check === 0) {
            $result [] = [
                'id' => $platform,
            ];
        }

        return json_encode($result);
    }

    public function setImportValues($json, $html)
    {
        $this->description = $json->shortDescription;
        $this->save();
        Localization::setActors($this->id, $html, $json);
    }

    public function getAllTerritory()
    {
        return '[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":12},{"id":13},{"id":14},{"id":15},{"id":16},{"id":17},{"id":18},{"id":19},{"id":20},{"id":21},{"id":22},{"id":23},{"id":24},{"id":25},{"id":26},{"id":27},{"id":28},{"id":29},{"id":30},{"id":31},{"id":32},{"id":33},{"id":34},{"id":35},{"id":36},{"id":37},{"id":38},{"id":39},{"id":40},{"id":41},{"id":42},{"id":43},{"id":44},{"id":45},{"id":46},{"id":47},{"id":48},{"id":49},{"id":50},{"id":51},{"id":52},{"id":53},{"id":54},{"id":55},{"id":56},{"id":57},{"id":58},{"id":59},{"id":60},{"id":61},{"id":62},{"id":63},{"id":64},{"id":65},{"id":66},{"id":67},{"id":68},{"id":69},{"id":70},{"id":71},{"id":72},{"id":73},{"id":74},{"id":75},{"id":76},{"id":77},{"id":78},{"id":79},{"id":80},{"id":81},{"id":82},{"id":83},{"id":84},{"id":85},{"id":86},{"id":87},{"id":88},{"id":89},{"id":90},{"id":91},{"id":92},{"id":93},{"id":94},{"id":95},{"id":96},{"id":97},{"id":98},{"id":99},{"id":100},{"id":101},{"id":102},{"id":103},{"id":104},{"id":105},{"id":106},{"id":107},{"id":108},{"id":109},{"id":110},{"id":111},{"id":112},{"id":113},{"id":114},{"id":115},{"id":116},{"id":117},{"id":118},{"id":119},{"id":120},{"id":121},{"id":122},{"id":123},{"id":124},{"id":125},{"id":126},{"id":127},{"id":128},{"id":129},{"id":130},{"id":131},{"id":132},{"id":133},{"id":134},{"id":135},{"id":136},{"id":137},{"id":138},{"id":139},{"id":140},{"id":141},{"id":142},{"id":143},{"id":144},{"id":145},{"id":146},{"id":147},{"id":148},{"id":149},{"id":150},{"id":151},{"id":152},{"id":153},{"id":154},{"id":155},{"id":156},{"id":157},{"id":158},{"id":159},{"id":160},{"id":161},{"id":162},{"id":163},{"id":164},{"id":165},{"id":166},{"id":167},{"id":168},{"id":169},{"id":170},{"id":171},{"id":172},{"id":173},{"id":174},{"id":175},{"id":176},{"id":177},{"id":178},{"id":179},{"id":180},{"id":181},{"id":182},{"id":183},{"id":184},{"id":185},{"id":186},{"id":187},{"id":188},{"id":189},{"id":190},{"id":191},{"id":192},{"id":193},{"id":194},{"id":195},{"id":196},{"id":197},{"id":198},{"id":199},{"id":200},{"id":201},{"id":202},{"id":203},{"id":204},{"id":205},{"id":206},{"id":207},{"id":208},{"id":209},{"id":210},{"id":211},{"id":212},{"id":213},{"id":214},{"id":215},{"id":216},{"id":217},{"id":218},{"id":219},{"id":220},{"id":221},{"id":222},{"id":223},{"id":224},{"id":225},{"id":226},{"id":227},{"id":228},{"id":229},{"id":230},{"id":231},{"id":232},{"id":233},{"id":234},{"id":235},{"id":236},{"id":237},{"id":238},{"id":239},{"id":240},{"id":241},{"id":242},{"id":243},{"id":244},{"id":245},{"id":246},{"id":247},{"id":248},{"id":249}]';
    }

    public function reytingDescription()
    {
        $result = "";
        foreach (json_decode($this->reyting) as $value) {
            $result .=  'Страна : ' . Country::findOne($value->country)->name . '; Возрастной рейтинг : ' . $value->youth . '+<br>';
        }
        return $result;
    }

    public function budgetDescription()
    {
        $result = "";
        foreach (json_decode($this->budget) as $value) {
            $result .= 'Сумма : ' . $value->summa . '; Валюта : ' . Currency::findOne($value->currency)->name . '<br>';
        }
        return $result;
    }

    public function genresDescription()
    {
        $result = '<ul class="list-tags">';
        foreach (json_decode($this->genres) as $value) {
            $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' .Genres::findOne($value->value)->name. '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

    public function getActorType($json)
    {
        $search = 'screenwriter';
        if(preg_match("/{$search}/i", $json)) {
            return 'screenwriter';
        }
        else {
            $search = 'producer';
            if(preg_match("/{$search}/i", $json)) {
                return 'producer';
            }
            else{
                $search = 'director';
                if(preg_match("/{$search}/i", $json)) {
                    return 'director';
                }
                else{
                    $search = 'cast';
                    if(preg_match("/{$search}/i", $json)) {
                        return 'cast';
                    }
                    else return 'unknown';
                }
            }
        }
    }
}
