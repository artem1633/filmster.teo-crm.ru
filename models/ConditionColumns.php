<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "condition_columns".
 *
 * @property int $id
 * @property int $condition_id Условия
 * @property string $column_name Название столбца
 * @property int $condition_name Условие
 * @property string $value Значение
 * @property int $additional Настройка между столбцами
 *
 * @property Conditions $condition
 */
class ConditionColumns extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'condition_columns';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['condition_id', 'condition_name', 'additional'], 'integer'],
            [['column_name', 'value'], 'string', 'max' => 255],
            [['condition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Conditions::className(), 'targetAttribute' => ['condition_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'condition_id' => 'Условия',
            'column_name' => 'Название столбца',
            'condition_name' => 'Условие',
            'value' => 'Значение',
            'additional' => 'Настройка между столбцами',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCondition()
    {
        return $this->hasOne(Conditions::className(), ['id' => 'condition_id']);
    }

    public function getConditionsList()
    {
        return [
            1 => 'Заполнено',
            2 => 'Пусто',
            3 => 'Содержит',
        ];
    }
}
