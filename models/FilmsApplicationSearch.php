<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FilmsApplication;

/**
 * FilmsApplicationSearch represents the model behind the search form about `app\models\FilmsApplication`.
 */
class FilmsApplicationSearch extends FilmsApplication
{
    public $formNameParam="FilmsApplicationSearch";
    
    public function formName()
    {
        return $this->formNameParam;    
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'application_id', 'price', 'min_warranty_currency', 'text_adap_currency', 'maintenance_currency', 'flat_rate_currency'], 'integer'],
            [['territory', 'platforms', 'est_open', 'vod_open', 'close_date', 'svod', 'pvod', 'avod', 'catch_up', 'tv', 'flat_rate','film_id'], 'safe'],
            [['rolyati_x', 'rolyati_y', 'min_warranty', 'tex_adap', 'maintenance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = FilmsApplication::find()
        ->with(['film'])
        ->where(['application_id'=>$id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                    'pageSize' => 1000,
                ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
 
        $query->joinWith('film');

        $query->andFilterWhere([
            'id' => $this->id,
            // 'film_id' => $this->film_id,
            'application_id' => $this->application_id,
            'rolyati_x' => $this->rolyati_x,
            'rolyati_y' => $this->rolyati_y,
            'price' => $this->price,
            'est_open' => $this->est_open,
            'vod_open' => $this->vod_open,
            'close_date' => $this->close_date,
            'min_warranty' => $this->min_warranty,
            'min_warranty_currency' => $this->min_warranty_currency,
            'tex_adap' => $this->tex_adap,
            'text_adap_currency' => $this->text_adap_currency,
            'maintenance' => $this->maintenance,
            'maintenance_currency' => $this->maintenance_currency,
            'flat_rate_currency' => $this->flat_rate_currency,
        ]);
        

        $query->andFilterWhere(['like', 'territory', $this->territory])
            ->andFilterWhere(['like', 'platforms', $this->platforms])
            ->andFilterWhere(['like', 'svod', $this->svod])
            ->andFilterWhere(['like', 'pvod', $this->pvod])
            ->andFilterWhere(['like', 'avod', $this->avod])
            ->andFilterWhere(['like', 'catch_up', $this->catch_up])
            ->andFilterWhere(['like', 'tv', $this->tv])
            ->andFilterWhere(['like', 'flat_rate', $this->flat_rate])
            ->andFilterWhere(['like', 'films.name', $this->film_id]);


        return $dataProvider;
    }
}
