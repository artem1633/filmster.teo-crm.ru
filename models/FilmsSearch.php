<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Films;

/**
 * FilmsSearch represents the model behind the search form about `app\models\Films`.
 */
class FilmsSearch extends Films
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'language_id', 'year_issue', 'country_id', 'manufacturer_id', 'duration', 'actuality_id'], 'integer'],
            [['name', 'number_season', 'number_seriya', 'alternative', 'certificate', 'hide_film', 'description', 'imdb', 'rotten', 'eidr', 'kinopoisk', 'rellase_date', 'genres', 'budget', 'reyting', 'create_date', /*'poster'*/], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        if($identity->type == 0){
            $query = Films::find()
            ->with(['type'])
            ->with(['country']);
        }
        else{
            $filmsID = $identity->getRelatedFilmsIds();

            $query = Films::find()
                ->where(['films.id' => $filmsID])
                ->with(['type'])
                ->with(['country']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->leftJoin('films_application', 'films_application.film_id=films.id');
        $query->leftJoin('applications', 'films_application.application_id=applications.id');
        $query->leftJoin('contract', 'contract.id=applications.contract_id');
        $query->leftJoin('right_holders', 'right_holders.id=contract.company_lic');


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'films.id' => $this->id,
            'type_id' => $this->type_id,
            'language_id' => $this->language_id,
            'year_issue' => $this->year_issue,
            'rellase_date' => $this->rellase_date,
            'country_id' => $this->country_id,
            'manufacturer_id' => $this->manufacturer_id,
            'duration' => $this->duration,
            'actuality_id' => $this->actuality_id,
            //'poster' => $this->poster,
            'create_date' => $this->create_date,
        ]);

        if(Yii::$app->user->identity->type == Users::CLIENT){
            $query->andWhere(['right_holders.user_id' => Yii::$app->user->getId()]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'number_season', $this->number_season])
            ->andFilterWhere(['like', 'number_seriya', $this->number_seriya])
            ->andFilterWhere(['like', 'alternative', $this->alternative])
            ->andFilterWhere(['like', 'certificate', $this->certificate])
            ->andFilterWhere(['like', 'hide_film', $this->hide_film])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'imdb', $this->imdb])
            ->andFilterWhere(['like', 'rotten', $this->rotten])
            ->andFilterWhere(['like', 'eidr', $this->eidr])
            ->andFilterWhere(['like', 'kinopoisk', $this->kinopoisk])
            ->andFilterWhere(['like', 'genres', $this->genres])
            ->andFilterWhere(['like', 'budget', $this->budget])
            ->andFilterWhere(['like', 'reyting', $this->reyting]);

        return $dataProvider;
    }
}
