<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_itunes".
 *
 * @property int $id
 * @property int $film_id Фильм
 * @property string $apple_identifier Дополнительный идентификатор площадки
 * @property string $vendor_identifier Идентификатор площадки
 * @property string $title Название фильма
 * @property int $sales Количество продаж
 * @property string $type Тип продажи
 * @property double $customer_price Сумма к выплате
 * @property string $customer_currency Валюта выплаты
 * @property string $country Территория
 * @property string $format Формат
 * @property string $genre Жанр
 * @property string $begin_date Дата начала
 * @property string $date Дата
 * @property string $type_transaction Тип транзакции
 *
 * @property Films $film
 */
class ReportItunes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_itunes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['film_id', 'sales'], 'integer'],
            [['customer_price'], 'number'],
            [['date'], 'safe'],
            [['apple_identifier', 'vendor_identifier', 'title', 'type', 'customer_currency', 'country', 'format', 'genre', 'begin_date', 'type_transaction'], 'string', 'max' => 255],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Films::className(), 'targetAttribute' => ['film_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'film_id' => 'Film ID',
            'apple_identifier' => 'Apple ID',
            'vendor_identifier' => 'Vendor Identifier',
            'title' => 'Наименование',
            'sales' => 'Продажи',
            'type' => 'Тип',
            'customer_price' => 'Customer Price',
            'customer_currency' => 'Customer Currency',
            'type_transaction' => 'Тип транзакции',
            'country' => 'Country',
            'format' => 'Format',
            'genre' => 'Genre',
            'begin_date' => 'Begin Date',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }
}
