<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "platform".
 *
 * @property int $id
 * @property string $name_ru Название площадки RU
 * @property string $name_eng Название площадки ENG
 * @property string $logo Логотип 1 к 1
 * @property string $description_ru Краткое описание площадки RU
 * @property string $description_eng Краткое описание площадки ENG
 * @property string $sales_model Модель продаж
 * @property string $countries Доступные страны распространения
 * @property string $video Формат видео
 * @property string $audio Формат аудио
 * @property string $subtitr Субтитры
 *
 * @property ReportMonetary[] $reportMonetaries
 * @property ReportSetting[] $reportSettings
 * @property TechnicalPlaygrounds[] $technicalPlaygrounds
 */
class Platform extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'platform';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description_ru', 'sales_model', /*'countries'*/], 'string'],
            [['video', 'audio', 'subtitr'], 'string'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['name_ru', 'name_eng', 'logo', 'description_eng'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Название площадки RU',
            'name_eng' => 'Название площадки ENG',
            'logo' => 'Логотип 1 к 1',
            'description_ru' => 'Краткое описание площадки RU',
            'description_eng' => 'Краткое описание площадки ENG',
            'video' => 'Формат видео',
            'audio' => 'Формат аудио',
            'subtitr' => 'Субтитры',
            'sales_model' => 'Модель продаж',
            'countries' => 'Доступные страны распространения',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $report = new ReportSetting(); 
            $report->platform_id = $this->id;
            $report->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        $report = ReportSetting::find()->where(['platform_id' => $this->id])->all();
        foreach ($report as $value) {
            $value->delete();
        }

        unlink(getcwd().'/uploads/platform/'.$this->logo);
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportMonetaries()
    {
        return $this->hasMany(ReportMonetary::className(), ['platform_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportSettings()
    {
        return $this->hasMany(ReportSetting::className(), ['platform_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnicalPlaygrounds()
    {
        return $this->hasMany(TechnicalPlaygrounds::className(), ['playground_id' => 'id']);
    }

    public function getCountryList()
    {
        $pro = Country::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    public function getCountrySelected($id)
    {
        $countries = Country::find()->all();
        $result = '';
        foreach ($countries as $value) {
            if($id == $value->id) $result .= '<option selected value="'.$value->id.'">'.$value->name.'</option>';
            else $result .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $result;
    }

    public function getCountries()
    {        
        if( $this->countries !== null || $this->countries !== '' ) {
            $result = explode(',', $this->countries);
            $data = [];
            foreach ($result as $value) {
                $country = Country::findOne($value);
                $data += [
                    $value => $value,
                ];
            }
            return $data;
        }
        else return null;
    }

    public function getValues($string, $value)
    {
        $result = '';
        $i = 0;
        foreach (explode(',', $string) as $valueName) {
            if($valueName != $value){
                if($i == 0) $result = $valueName;
                else $result .= ',' . $valueName;
                $i++;
            }
        }
        return $result;
    }
}
