<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency_setting".
 *
 * @property int $id
 * @property int $currency_from_id Первая валюта
 * @property int $currency_to_id Вторая валюта
 * @property double $currency_from_value Значения первого
 * @property double $currency_to_value Значения второго
 */
class CurrencySetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_from_id', 'currency_to_id', 'monetary_id'], 'integer'],
            [['currency_from_value', 'currency_to_value'], 'number'],
            [['date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_from_id' => 'Первая валюта',
            'currency_to_id' => 'Вторая валюта',
            'currency_from_value' => 'Значения первого',
            'currency_to_value' => 'Значения второго',
            'date' => 'Месяц',
            'monetary_id' => 'Monetary ID',
        ];
    }
}
