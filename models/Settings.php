<?php

namespace app\models;


/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string $key Ключ
 * @property string $value Значение
 * @property string $label Комментарий
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @const Тип "Тест". Настройка содержит строку
     */
    const TYPE_TEXT = 'text';

    /**
     * @const Тип "Чекбокс". Настройка содержит логическое значение (Да/Нет)
     */
    const TYPE_CHECKBOX = 'checkbox';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['label'], 'string'],
            [['key', 'value'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

//    /**
//     * @inheritdoc
//     */
//    public function beforeSave($insert)
//    {
//        if($this->type == self::TYPE_CHECKBOX){
//            if($this->value != '0')
//            {
//                $this->value = $this->value == 'on' ? '1' : '0';
//            }
//        }
//
//        return parent::beforeSave($insert);
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ключ',
            'value' => 'Значение',
            'label' => 'Комментарий',
        ];
    }

    /**
     * Ищет запись в БД по ключу
     * @param string $key
     * @return null|static
     */
    public static function findByKey($key)
    {
        return self::findOne(['key' => $key]);
    }
}
