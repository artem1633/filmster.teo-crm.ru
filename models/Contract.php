<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contract".
 *
 * @property int $id
 * @property string $number Номер договора
 * @property string $date_cr Дата создания договора
 * @property double $min_payment Мин.порог выплаты
 * @property int $min_payment_currency валюта 
 * @property int $payment_period Период выплат
 * @property int $payment_currency Валюта выплаты
 * @property int $another_contract Есть зависимость от другого договора
 * @property string $another_contract_value Значения другого договора
 * @property string $contract_date Дата договора
 * @property int $company_lic Лицензиат
 * @property int $company_cont Лицензиар
 *
 * @property Applications[] $applications
 * @property RightHolders $companyCont
 * @property RightHolders $companyLic
 */
class Contract extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_cr', 'contract_date'], 'safe'],
            [['min_payment'], 'number'],
            [['min_payment_currency', 'payment_period', 'payment_currency', 'company_lic', 'company_cont', 'another_contract'], 'integer'],
            [['number', 'another_contract_value'], 'string', 'max' => 255],
            [['company_cont'], 'exist', 'skipOnError' => true, 'targetClass' => RightHolders::className(), 'targetAttribute' => ['company_cont' => 'id']],
            [['company_lic'], 'exist', 'skipOnError' => true, 'targetClass' => RightHolders::className(), 'targetAttribute' => ['company_lic' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => '№ договора',
            'date_cr' => 'Дата создания договора',
            'contract_date' => 'Дата договора',
            'min_payment' => 'Мин.порог выплаты',
            'min_payment_currency' => 'Валюта',
            'payment_period' => 'Период выплат',
            'payment_currency' => 'Валюта выплаты',
            'company_lic' => 'Лицензиар',
            'company_cont' => 'Лицензиат',
            'another_contract' => 'Есть зависимость от другого договора',
            'another_contract_value' => 'Значения другого договора',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if($this->contract_date != null) {
                $this->contract_date = str_replace('/', '-', $this->contract_date);
                $this->contract_date = date('Y-m-d', strtotime($this->contract_date) );
            }
        }

        if($this->contract_date != null) {
            $this->date_cr = $this->contract_date;
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $applications = Applications::find()->where(['contract_id' => $this->id])->all();
        foreach ($applications as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyCont()
    {
        return $this->hasOne(RightHolders::className(), ['id' => 'company_cont']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyLic()
    {
        return $this->hasOne(RightHolders::className(), ['id' => 'company_lic']);
    }

    public function getActivity0()
    {
        return [
            0 => 'Ежемесячно',
            1 => 'Ежеквартально',
            2 => 'Раз в пол года',
            3 => 'Раз в год',
        ];
    }

    public function getActivityDescription()
    {
        if($this->payment_period == 0) return 'Ежемесячно';
        if($this->payment_period == 1) return 'Ежеквартально';
        if($this->payment_period == 2) return 'Раз в пол года';
        if($this->payment_period == 3) return 'Раз в год';
    }

    public function getRightHolders()
    {
        $rightHolders = RightHolders::find()->all();
        return ArrayHelper::map( $rightHolders , 'id', 'name_ru');
    }

/*    public function getCompanies2()
    {
       $companies = Companies::find()->where(['type' =>0])->all();
       return ArrayHelper::map( $companies , 'id', 'name');
    }*/

    public function getCurrency()
    {
        $payment_currency = Currency::find()->all();
        return ArrayHelper::map( $payment_currency , 'id', 'code');
    }

    public function getCurrency0()
    {
        $min_payment_currency = Currency::find()->all();
        return ArrayHelper::map( $min_payment_currency , 'id', 'code');
    }

    public function getQueryContract()
    {   
        $session = Yii::$app->session;
        $session['Contract[rightHolders]']= RightHolders::find()->asArray()->all();
        $session['Contract[currency]']= Currency::find()->asArray()->all();
    }

    public function CurrencyDescription($id)
    { 
        $localization_id = $this->companyLic->user->localization_id;
        if($localization_id == 1) return "RUB";
        else return "USD";
        /*bu xato kod bolishi mumkin*/
        $payment_currency = Currency::findOne($id);
        return $payment_currency->code;
        /*end*/
    }

    public function CurrencyCode($id)
    { 
        $payment_currency = Currency::findOne($id);
        return $payment_currency->code;
    }

    public function CurrentPayment($id)
    {
        /*bu xato kod bolishi mumkin*/
        $payment_currency = Currency::findOne($id);
        return $payment_currency->code;
        /*end*/
    }
    
    public function getContractsList($company_lic, $month)
    {
        $resultID = [];
        $array = explode('-', $month);
        $contracts = Contract::find()->where(['company_lic' => $company_lic])->all();
        foreach ($contracts as $contract) {
            if($contract->payment_period == 0) $resultID [] = $contract->id;
            else if($contract->payment_period == 1) {
                if($array[0] == 'Мар' || $array[0] == 'Июн' || $array[0] == 'Сен' || $array[0] == 'Дек') $resultID [] = $contract->id;
            }
            else if($contract->payment_period == 2) {
                if($array[0] == 'Июн' || $array[0] == 'Дек') $resultID [] = $contract->id;
            }
            else if($contract->payment_period == 3) {
                if($array[0] == 'Дек') $resultID [] = $contract->id;
            }
        }
        $contracts = Contract::find()->where(['id' => $resultID])->all();
        return $contracts;

    }
}
