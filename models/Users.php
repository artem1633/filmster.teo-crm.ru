<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property string $telephone
 * @property string $foto
 * @property int $type
 * @property int $active Активен
 * @property string $site Веб-сайт
 * @property string $mail E-mail
 * @property string $description Краткое описание
 *
 * @property RightHolders[] $rightHolders
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_password;
    public $file;

    const ADMIN = '0';
    const CLIENT = '1';
    const TECHNICAL_CLIENT = '2';
    const RUS = '1';
    const ENG = '2';

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'type' ,'mail'], 'required'],
            [['type', 'active', 'localization_id'], 'integer'],
            [['description'], 'string'],
            [['mail'], 'email'],
            [['login'],  'unique'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['fio', 'login', 'telephone', 'foto', 'site', 'mail'], 'string', 'max' => 255],
            [['password', 'new_password'], 'string', 'min' => 8, 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Название',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Должность',
            'new_password' => 'Новый пароль',
            'telephone' => 'Телефон',
            'foto' => 'Логотип',
            'file' => 'Логотип',
            'active' => 'Активен',
            'site' => 'Веб-сайт',
            'mail' => 'E-mail',
            'description' => 'Краткое описание',
            'localization_id' => 'Локализация',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }


        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {        
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        $rightHolders = RightHolders::find()->where(['user_id' => $this->id])->all();
        foreach ($rightHolders as $value) {
            $value->delete();
        }

        unlink(getcwd().'/avatars/'.$this->foto);
        return parent::beforeDelete();
    }

    //Получить список типов пользователя.
    public function getType()
    {
        return [
            self::ADMIN => 'Admin',
            self::CLIENT => 'Client',
            self::TECHNICAL_CLIENT => 'Technical Client',
        ];
    }
     public function getLocType()
    {
        return [
            self::RUS => 'Русский ',
            self::ENG => 'Английский',
        ];
    }

    //Получить описание типов пользователя.
    public function getTypeDescription()
    {
        switch ($this->type) {
            case 0: return "Admin";
            case 1: return "Client";
            case 2: return "Technical Client";
            default: return "Неизвестно";
        }
    }

    public function getLocTypeDescription()
    {
        switch ($this->localization_id) {
            case 1: return "Русский";
            case 2: return "Английский";
            default: return "Неизвестно";
        }
    }
    //Получить описание типов пользователя.
    public function getPermission()
    {
        if($this->type == 0) return "Admin";
        if($this->type == 1) return "Client";
        if($this->type == 2) return "Technical Client";
    }

    /**
     * @return array
     */
    public function getRelatedFilmsIds()
    {
        $rightHolders = RightHolders::find()->where(['user_id' => $this->id])->all();
        $idList = [];
        foreach ($rightHolders as $value) {
            $idList [] = $value->id;
        }

        $contracts = Contract::find()->where(['or',
            ['company_lic' => $idList],
            ['company_cont' => $idList]
        ])->all();

        $applicationId = [];
        foreach ($contracts as $value) {
            $app = Applications::find()->where(['contract_id' => $value->id])->all();
            foreach ($app as $ap) {
                $applicationId [] = $ap->id;
            }
        }

        $filmsApp = FilmsApplication::find()->where(['application_id' => $applicationId])->all();
        $filmsID = [];
        foreach ($filmsApp as $value) {
            $filmsID [] = $value->film_id;
        }

        return $filmsID;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRightHolders()
    {
        return $this->hasMany(RightHolders::className(), ['user_id' => 'id']);
    }

    public function getContractList()
    {
        $litsenziat = [];
        $rightHolders = RightHolders::find()->where(['user_id' => $this->id])->all();
        foreach ($rightHolders as $value) {
            $litsenziat [] = $value->id;
        }
        $contracts = Contract::find()->where(['company_lic' => $litsenziat])->all();
        return ArrayHelper::map($contracts, 'id', 'number');
    }

    public function getAllContractList()
    {
        $contracts = Contract::find()->all();
        return ArrayHelper::map($contracts, 'id', 'number');
    }

    public function getContractsList($user_id)
    {
        if(Yii::$app->user->identity->type != 0) $user_id = Yii::$app->user->identity->id;
        
        $litsenziat = [];
        $rightHolders = RightHolders::find()->where(['user_id' => $user_id])->all();
        foreach ($rightHolders as $value) {
            $litsenziat [] = $value->id;
        }
        $contracts = Contract::find()->where(['company_lic' => $litsenziat])->all();
        return ArrayHelper::map($contracts, 'id', 'number');
    }

    public function getAllUsersList()
    {
        $users = Users::find()->all();
        return ArrayHelper::map($users, 'id', 'fio');
    }

    public function getTotalSummary($contract, $reportId, $localization_id)
    {
//        if($localization_id === null || $localization_id === 1) $localization = 1;
//        else $localization = 2;

        $filmsPrint = [];
        $totalsummary = 0;
        $applicationID = [];
        $filmsId = [];
        $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
        foreach ($applications as $value) {
            $applicationID [] = $value->id;
        }

        $filmsApplication = FilmsApplication::find()->where(['application_id' => $applicationID ])->all();
        foreach ($filmsApplication as $value) 
        {
            $filmsId [] = $value->film_id;
        }

        $filmsId = array_unique($filmsId);
        foreach ($filmsId as $film_id) 
        {
            $film = Films::findOne($film_id);
            $filmName = $film->name . ( $film->year_issue !== null ? ' (' . $film->year_issue . ')' : '' );
            $filmsApplication = FilmsApplication::find()->joinWith('application')->where(['films_application.film_id' => $film_id, 'applications.contract_id' => $contract->id ])->one();

            $allTerritories = [];
            foreach (json_decode($filmsApplication->territory) as $territory) {
                $allTerritories [] = $territory->id;
            }
            $reportValues = ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $film_id])
                ->andWhere(['territory' => $allTerritories])
                ->all();

            $platformJson = json_decode($filmsApplication->platforms);
            $platforms = [];
            $platformNames = '';
            foreach ($platformJson as $fff) {
                $platforms [] = $fff->id;
                $platformNames .= Platform::findOne($fff->id)->name_ru . '; ';
            }

            $proverka = 0;
            if($reportValues != null){
                foreach ($reportValues as $reportValue) 
                {
                    if(in_array($reportValue->territory, $allTerritories)) $proverka = 1;
                }
            }else $proverka = 1;
            if($proverka === 0) continue;

            foreach ($platforms as $platform) 
            {
                $platformName = Platform::findOne($platform)->name_ru;
                $estCount = null; $estSum = null;
                $vodCount = null; $vodSum = null;
                $svodCount = null; $svodSum = null;
                $pvodCount = null; $pvodSum = null;
                $avodCount = null; $avodSum = null;
                $summary = 0;
                foreach ($reportValues as $reportValue) 
                {
                    if($reportValue->reportMonetary->platform_id == $platform) 
                    {
                        $sum = 0;
                        if($reportValue->est !== null) { $estCount += $reportValue->sale_count; $sum += $reportValue->est;  }
                        if($reportValue->vod !== null) { $vodCount += $reportValue->sale_count; $sum += $reportValue->vod;  }
                        if($reportValue->svod !== null) { $svodCount += $reportValue->sale_count; $sum += $reportValue->svod; }
                        if($reportValue->pvod !== null) { $pvodCount += $reportValue->sale_count; $sum += $reportValue->pvod; }
                        if($reportValue->avod !== null) { $avodCount += $reportValue->sale_count; $sum += $reportValue->avod; }

                        $currencyValue = 0;
                        $currencySetting = CurrencySetting::find()
                            ->where(['currency_from_id' => $reportValue->currency, 'currency_to_id' => $contract->payment_currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                        if($currencySetting != null) 
                        {
                            $currencyValue = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                        }
                        else{
                            $currencySetting = CurrencySetting::find()
                                ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $reportValue->currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                            if($currencySetting != null)
                            {
                                $currencyValue = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                            }
                        }
                        if($currencySetting != null) { 
                            $summary += $currencyValue * $sum; 
                            if($reportValue->est != null) $estSum += $currencyValue * $reportValue->est;
                            if($reportValue->vod != null) $vodSum += $currencyValue * $reportValue->vod;
                            if($reportValue->svod != null) $svodSum += $currencyValue * $reportValue->svod;
                            if($reportValue->pvod != null) $pvodSum += $currencyValue * $reportValue->pvod;
                            if($reportValue->avod != null) $avodSum += $currencyValue * $reportValue->avod;
                        }
                        else {
                            $summary += $sum;
                            if($reportValue->est != null) $estSum += $reportValue->est;
                            if($reportValue->vod != null) $vodSum += $reportValue->vod;
                            if($reportValue->svod != null) $svodSum += $reportValue->svod;
                            if($reportValue->pvod != null) $pvodSum += $reportValue->pvod;
                            if($reportValue->avod != null) $avodSum += $reportValue->avod;
                        }
                    }
                }

                $sale = $estCount + $vodCount + $svodCount + $pvodCount  + $avodCount;
                $currency = Currency::findOne($contract->payment_currency)->code;
                $rolyati_x_summa = $summary*$filmsApplication->rolyati_x*0.01;
                $summary = round($summary, 2);
                $rolyati_x_summa = round($rolyati_x_summa, 2);
                if(is_nan($rolyati_x_summa)) $rolyati = 0;
                else $rolyati = $rolyati_x_summa;

                if($filmsApplication->application->uniform_condition === 1){
                    $min_warranty = $filmsApplication->application->min_warranty;
                    $tex_adap = $filmsApplication->application->tex_adap;
                    $maintenance = $filmsApplication->application->maintenance;
                }
                else{
                    $min_warranty = $filmsApplication->min_warranty;
                    $tex_adap = $filmsApplication->tex_adap;
                    $maintenance = $filmsApplication->maintenance;
                }
                                    
                if($min_warranty === null) $min_warranty = 0;
                if($tex_adap === null) $tex_adap = 0;
                if($maintenance === null) $maintenance = 0;

                $findLastMinWarranty = Users::findLastMinWarranty($filmsPrint, $contract->id, $filmName, $filmsApplication->id, $filmsApplication->application->id, $filmsApplication->application->number);
                if($findLastMinWarranty !== null) $min_warranty = $findLastMinWarranty;

                if($min_warranty - $rolyati > 0) {
                    $mgs = $min_warranty - $rolyati;
                    $rolyati = 0;
                }
                else {
                    $mgs = 0;
                    $rolyati = $rolyati - $min_warranty;
                }

                if($mgs == 0 && $rolyati > 0) {
                    if($tex_adap - $rolyati > 0) {
                        $tas = $tex_adap - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $tas = 0;
                        $rolyati = $rolyati - $tex_adap;
                    }
                }

                if($tas == 0 && $rolyati > 0) {
                    if($maintenance - $rolyati > 0) {
                        $mfs = $maintenance - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $mfs = 0;
                        $rolyati = $rolyati - $maintenance;
                    }
                }

                $currency = $contract->CurrencyDescription($contract->payment_currency);
                $curCode = PreferenceBooks::getCurrencySymbol($currency);
                if(is_nan($estSum)) $estSum = null;
                if(is_nan($vodSum)) $vodSum = null;
                if(is_nan($svodSum)) $svodSum = null;
                if(is_nan($pvodSum)) $pvodSum = null;
                if(is_nan($avodSum)) $avodSum = null;
                if(is_nan($summary)) $summary = 0;

//                if($currency == 'USD') {
//                    if($min_warranty != null) $min_warranty = 0.7 * $min_warranty;
//                    if($mgs != null) $mgs = 0.7 * $mgs;
//                    if($tex_adap != null) $tex_adap = 0.7 * $tex_adap;
//                    if($tas != null) $tas = 0.7 * $tas;
//                    if($maintenance != null) $maintenance = 0.7 * $maintenance;
//                    if($estSum  != null) $estSum = 0.7 * $estSum;
//                    if($vodSum  != null) $vodSum = 0.7 * $vodSum;
//                    if($svodSum != null) $svodSum = 0.7 * $svodSum;
//                    if($pvodSum != null) $pvodSum = 0.7 * $pvodSum;
//                    if($avodSum != null) $avodSum = 0.7 * $avodSum;
//                    if($summary != null) $summary = 0.7 * $summary;
//                    if($rolyati_x_summa != null) $rolyati_x_summa = 0.7 * $rolyati_x_summa;
//                    if($rolyati != null) $rolyati = 0.7 * $rolyati;
//                }

                $filmsPrint [] = [
                    'contract_id' => $contract->id,
                    'film_id' => $film->id,
                    'filmName' => $filmName,
                    'certificate' => $film->certificate,
                    'filmsApplication_id' => $filmsApplication->id,
                    'application_id' => $filmsApplication->application->id,
                    'application_number' => $filmsApplication->application->number,
                    //'rolyati' => ($filmsApplication->rolyati_x !== null ? ($filmsApplication->rolyati_x . '/' . $filmsApplication->rolyati_y ): ''),
                    'rolyati' => ($filmsApplication->rolyati_x !== null ? 0.01 * $filmsApplication->rolyati_x : ''),
                    'platformNames' => $platformName,
                    'check' => $filmsApplication->application->uniform_condition,
                    'mgt' => $min_warranty,
                    'mgs' => $mgs == null ? $min_warranty : $mgs,
                    'tat' => $tex_adap,
                    'tas' => $tas == null ? $tex_adap : $tas,
                    'mft' => $maintenance,
                    'mfs' => $mfs == null ? $maintenance : $mfs,
                    'estCount' => $estCount,
                    'est' => $estSum != null ? round($estSum, 2) : null,
                    'vodCount' => $vodCount,
                    'vod' => $vodSum != null ? round($vodSum, 2) : null,
                    'svodCount' => $svodCount,
                    'svod' => $svodSum != null ? round($svodSum, 2) : null,
                    'pvodCount' => $pvodCount,
                    'pvod' => $pvodSum != null ? round($pvodSum, 2) : null,
                    'avodCount' => $avodCount,
                    'avod' => $avodSum != null ? round($avodSum, 2) : null,
                    'sale' => $sale,
                    'summary' => $summary,
                    'old_rolyati_x_summa' => $rolyati_x_summa,
                    'rolyati_x_summa' => $rolyati,
                ];

                $royalityPayment = 0;
                $ostatokSum = $mgs + $tas + $mfs;
                if( $ostatokSum < $rolyati_x_summa ) {
                    $royalityPayment = $rolyati_x_summa - $ostatokSum;
                }

                $totalsummary += $royalityPayment;
            }
        }

        $session = Yii::$app->session;
        $session['contract_'.$contract->id] = $filmsPrint;

        if(is_nan($totalsummary)) return 0;

        /*if(Currency::findOne($contract->payment_currency)->code == 'USD') return 0.7 * $totalsummary;
        else*/ return $totalsummary;
    }


    public function findLastMinWarranty($filmsPrint, $contract_id, $filmName, $filmsApplication_id, $application_id, $application_number)
    {
        $mgs = null;
        foreach ($filmsPrint as $value) {
            if( $value['contract_id'] == $contract_id & $value['filmName'] == $filmName & 
                $value['filmsApplication_id'] == $filmsApplication_id & $value['application_id'] == $application_id & 
                $value['application_number'] == $application_number) $mgs = $value['mgs'];
        }
        return $mgs;
    }

    public function findFilmMerge($filmsMerge, $index, $contract_id, $filmName, $filmsApplication_id, $application_id, $application_number, $check, $certificate)
    {
        $q = 0;
        foreach ($filmsMerge as $film) {
            if($film['filmName'] == $filmName) { $q = 1; break;}
        }

        if($q == 0){
            $session = Yii::$app->session;
            $count = 0; $val = ""; $rolyati = 0; $mgt = null;
            foreach ($session['contract_'.$contract_id] as $value) 
            {
                if( $value['contract_id'] == $contract_id & $value['filmName'] == $filmName & 
                    $value['filmsApplication_id'] == $filmsApplication_id & $value['application_id'] == $application_id & 
                    $value['application_number'] == $application_number) { 
                    $count++; 
                    $val = $value['mgs']; 
                    if($mgt == null) $mgt = $value['mgt']; 
                    $rolyati += $value['old_rolyati_x_summa']; 
                }
            }
            if($count > 1){
                $filmsMerge [] = [
                    'filmName' => $filmName,
                    'begin' => $index,
                    'end' => $index + $count - 1,
                    'val' => $val,
                    'rolyati' => $rolyati,
                    'mgt' => $mgt,
                    'count' => $count,
                    'check' => $check,
                    'certificate' => $certificate,
                ];
            }
            return $filmsMerge;
        }
        else return $filmsMerge;
    }

    public function setKoeffitsiyent($contract, $reportId, $localization_id, $koef, $check)
    {
        if($localization_id === null || $localization_id === 1) $localization = 1;
        else $localization = 2;

        $filmsPrint = [];
        $totalsummary = 0;
        $applicationID = [];
        $filmsId = [];
        $applications = Applications::find()->where(['contract_id' => $contract->id])->all();
        foreach ($applications as $value) {
            $applicationID [] = $value->id;
        }

        $filmsApplication = FilmsApplication::find()->where(['application_id' => $applicationID ])->all();
        foreach ($filmsApplication as $value) 
        {
            $filmsId [] = $value->film_id;
        }

        $filmsId = array_unique($filmsId);
        foreach ($filmsId as $film_id) 
        {
            $film = Films::findOne($film_id);
            $filmName = $film->name . ( $film->year_issue !== null ? ' (' . $film->year_issue . ')' : '' );
            $filmsApplication = FilmsApplication::find()->joinWith('application')->where(['films_application.film_id' => $film_id, 'applications.contract_id' => $contract->id ])->one();

            $allTerritories = [];
            foreach (json_decode($filmsApplication->territory) as $territory) {
                $allTerritories [] = $territory->id;
            }
            $reportValues = ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $film_id])
                ->andWhere(['territory' => $allTerritories])
                ->all();

            $platformJson = json_decode($filmsApplication->platforms);
            $platforms = [];
            $platformNames = '';
            foreach ($platformJson as $fff) {
                $platforms [] = $fff->id;
                $platformNames .= Platform::findOne($fff->id)->name_ru . '; ';
            }

            $proverka = 0;
            if($reportValues != null){
                foreach ($reportValues as $reportValue) 
                {
                    if(in_array($reportValue->territory, $allTerritories)) $proverka = 1;
                }
            }else $proverka = 1;
            if($proverka === 0) continue;

            foreach ($platforms as $platform) 
            {
                $platformName = Platform::findOne($platform)->name_ru;
                $estCount = null; $estSum = null;
                $vodCount = null; $vodSum = null;
                $svodCount = null; $svodSum = null;
                $pvodCount = null; $pvodSum = null;
                $avodCount = null; $avodSum = null;
                $summary = 0;
                foreach ($reportValues as $reportValue) 
                {

//                    var_dump($reportValue);
//                    exit;

                    if($reportValue->reportMonetary->platform_id == $platform) 
                    {
                        $sum = 0;
                        if($reportValue->est !== null) { 
                            $reportValue->est = (string)($reportValue->est * $koef);
                            $reportValue->sale_count = (string)($reportValue->sale_count *$koef);
                            $reportValue->sale_count = (string)round($reportValue->sale_count, 0);
                            if($check == 1) $reportValue->save();
                            $estCount += (float)$reportValue->sale_count; $sum += (float)$reportValue->est;  
                        }
                        if($reportValue->vod !== null) { 
                            $reportValue->vod = (string)($reportValue->vod *$koef);
                            $reportValue->sale_count = (string)($reportValue->sale_count *$koef);
                            $reportValue->sale_count = (string)round($reportValue->sale_count, 0);
                            if($check == 1) $reportValue->save();
                            $vodCount += (float)$reportValue->sale_count; $sum += (float)$reportValue->vod;  
                        }
                        if($reportValue->svod !== null) { 
                            $reportValue->svod = (string)($reportValue->svod *$koef);
                            $reportValue->sale_count = (string)($reportValue->sale_count *$koef);
                            $reportValue->sale_count = (string)round($reportValue->sale_count, 0);
                            if($check == 1) $reportValue->save();
                            $svodCount += (float)$reportValue->sale_count; $sum += (float)$reportValue->svod; 
                        }
                        if($reportValue->pvod !== null) { 
                            $reportValue->pvod = (string)($reportValue->pvod *$koef);
                            $reportValue->sale_count = (string)($reportValue->sale_count *$koef);
                            $reportValue->sale_count = (string)round($reportValue->sale_count, 0);
                            if($check == 1) $reportValue->save();
                            $pvodCount += (float)$reportValue->sale_count; $sum += (float)$reportValue->pvod; 
                        }
                        if($reportValue->avod !== null) { 
                            $reportValue->avod = (string)($reportValue->avod *$koef);
                            $reportValue->sale_count = (string)($reportValue->sale_count *$koef);
                            $reportValue->sale_count = (string)round($reportValue->sale_count, 0);
                            if($check == 1) $reportValue->save();
                            $avodCount += (float)$reportValue->sale_count; $sum += (float)$reportValue->avod; 
                        }

                        $currencyValue = 0;
                        $currencySetting = CurrencySetting::find()
                            ->where(['currency_from_id' => $reportValue->currency, 'currency_to_id' => $contract->payment_currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                        if($currencySetting != null) 
                        {
                            $currencyValue = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                        }
                        else{
                            $currencySetting = CurrencySetting::find()
                                ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $reportValue->currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                            if($currencySetting != null)
                            {
                                $currencyValue = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                            }
                        }
                        if($currencySetting != null) {
                            $currencyValue = 1;
                            $summary += $currencyValue * $sum; 
                            if($reportValue->est != null) $estSum += $currencyValue * $reportValue->est;
                            if($reportValue->vod != null) $vodSum += $currencyValue * $reportValue->vod;
                            if($reportValue->svod != null) $svodSum += $currencyValue * $reportValue->svod;
                            if($reportValue->pvod != null) $pvodSum += $currencyValue * $reportValue->pvod;
                            if($reportValue->avod != null) $avodSum += $currencyValue * $reportValue->avod;
                        }
                        else {
//                            $summary += $sum;
//                            if($reportValue->est != null) $estSum += $reportValue->est;
//                            if($reportValue->vod != null) $vodSum += $reportValue->vod;
//                            if($reportValue->svod != null) $svodSum += $reportValue->svod;
//                            if($reportValue->pvod != null) $pvodSum += $reportValue->pvod;
//                            if($reportValue->avod != null) $avodSum += $reportValue->avod;
                        }

//                        var_dump($summary);
//                        exit;



                        if($currencySetting == null) {
                            $summary += $sum;
                            if($reportValue->est != null) $estSum += $reportValue->est;
                            if($reportValue->vod != null) $vodSum += $reportValue->vod;
                            if($reportValue->svod != null) $svodSum += $reportValue->svod;
                            if($reportValue->pvod != null) $pvodSum += $reportValue->pvod;
                            if($reportValue->avod != null) $avodSum += $reportValue->avod;
                        }
                    }
                }

                $sale = $estCount + $vodCount + $svodCount + $pvodCount  + $avodCount;
                $currency = Currency::findOne($contract->payment_currency)->code;
                $rolyati_x_summa = $summary*$filmsApplication->rolyati_x*0.01;
                $summary = round($summary, 2);
                $rolyati_x_summa = round($rolyati_x_summa, 2);
                if(is_nan($rolyati_x_summa)) $rolyati = 0;
                else $rolyati = $rolyati_x_summa;

                if($filmsApplication->application->uniform_condition === 1){
                    $min_warranty = $filmsApplication->application->min_warranty;
                    $tex_adap = $filmsApplication->application->tex_adap;
                    $maintenance = $filmsApplication->application->maintenance;
                }
                else{
                    $min_warranty = $filmsApplication->min_warranty;
                    $tex_adap = $filmsApplication->tex_adap;
                    $maintenance = $filmsApplication->maintenance;
                }
                                    
                if($min_warranty === null) $min_warranty = 0;
                if($tex_adap === null) $tex_adap = 0;
                if($maintenance === null) $maintenance = 0;

                $findLastMinWarranty = Users::findLastMinWarranty($filmsPrint, $contract->id, $filmName, $filmsApplication->id, $filmsApplication->application->id, $filmsApplication->application->number);
                if($findLastMinWarranty !== null) $min_warranty = $findLastMinWarranty;

                if($min_warranty - $rolyati > 0) {
                    $mgs = $min_warranty - $rolyati;
                    $rolyati = 0;
                }
                else {
                    $mgs = 0;
                    $rolyati = $rolyati - $min_warranty;
                }

                if($mgs == 0 && $rolyati > 0) {
                    if($tex_adap - $rolyati > 0) {
                        $tas = $tex_adap - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $tas = 0;
                        $rolyati = $rolyati - $tex_adap;
                    }
                }

                if($tas == 0 && $rolyati > 0) {
                    if($maintenance - $rolyati > 0) {
                        $mfs = $maintenance - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $mfs = 0;
                        $rolyati = $rolyati - $maintenance;
                    }
                }

                $currency = $contract->CurrencyDescription($contract->payment_currency);
                $curCode = PreferenceBooks::getCurrencySymbol($currency);
                if(is_nan($estSum)) $estSum = null;
                if(is_nan($vodSum)) $vodSum = null;
                if(is_nan($svodSum)) $svodSum = null;
                if(is_nan($pvodSum)) $pvodSum = null;
                if(is_nan($avodSum)) $avodSum = null;
                if(is_nan($summary)) $summary = 0;

//                if($currency == 'USD') {
//                    if($min_warranty != null) $min_warranty = 0.7 * $min_warranty;
//                    if($mgs != null) $mgs = 0.7 * $mgs;
//                    if($tex_adap != null) $tex_adap = 0.7 * $tex_adap;
//                    if($tas != null) $tas = 0.7 * $tas;
//                    if($maintenance != null) $maintenance = 0.7 * $maintenance;
//                    if($estSum  != null) $estSum = 0.7 * $estSum;
//                    if($vodSum  != null) $vodSum = 0.7 * $vodSum;
//                    if($svodSum != null) $svodSum = 0.7 * $svodSum;
//                    if($pvodSum != null) $pvodSum = 0.7 * $pvodSum;
//                    if($avodSum != null) $avodSum = 0.7 * $avodSum;
//                    if($summary != null) $summary = 0.7 * $summary;
//                    if($rolyati_x_summa != null) $rolyati_x_summa = 0.7 * $rolyati_x_summa;
//                    if($rolyati != null) $rolyati = 0.7 * $rolyati;
//                }


                $filmsPrint [] = [
                    'contract_id' => $contract->id,
                    'film_id' => $film->id,
                    'filmName' => $filmName,
                    'certificate' => $film->certificate,
                    'filmsApplication_id' => $filmsApplication->id,
                    'application_id' => $filmsApplication->application->id,
                    'application_number' => $filmsApplication->application->number,
                    //'rolyati' => ($filmsApplication->rolyati_x !== null ? ($filmsApplication->rolyati_x . '/' . $filmsApplication->rolyati_y ): ''),
                    'rolyati' => ($filmsApplication->rolyati_x !== null ? 0.01 * $filmsApplication->rolyati_x : ''),
                    'platformNames' => $platformName,
                    'check' => $filmsApplication->application->uniform_condition,
                    'mgt' => $min_warranty,
                    'mgs' => $mgs == null ? $min_warranty : $mgs,
                    'tat' => $tex_adap,
                    'tas' => $tas == null ? $tex_adap : $tas,
                    'mft' => $maintenance,
                    'mfs' => $mfs == null ? $maintenance : $mfs,
                    'estCount' => $estCount,
                    'est' => $estSum != null ? round($estSum, 2) : null,
                    'vodCount' => $vodCount,
                    'vod' => $vodSum != null ? round($vodSum, 2) : null,
                    'svodCount' => $svodCount,
                    'svod' => $svodSum != null ? round($svodSum, 2) : null,
                    'pvodCount' => $pvodCount,
                    'pvod' => $pvodSum != null ? round($pvodSum, 2) : null,
                    'avodCount' => $avodCount,
                    'avod' => $avodSum != null ? round($avodSum, 2) : null,
                    'sale' => $sale,
                    'summary' => $summary,
                    'old_rolyati_x_summa' => $rolyati_x_summa,
                    'rolyati_x_summa' => $rolyati,
                ];

                $royalityPayment = 0;
                $ostatokSum = $mgs + $tas + $mfs;
                if( $ostatokSum < $rolyati_x_summa ) {
                    $royalityPayment = $rolyati_x_summa - $ostatokSum;
                }

                $totalsummary += $royalityPayment;
            }
        }

        $session = Yii::$app->session;
        $session['contract_'.$contract->id] = '';
        $session['contract_'.$contract->id] = $filmsPrint;

        if(is_nan($totalsummary)) return 0;

        /*if(Currency::findOne($contract->payment_currency)->code == 'USD') return 0.7 * $totalsummary;
        else*/ return $totalsummary;
    }

}
