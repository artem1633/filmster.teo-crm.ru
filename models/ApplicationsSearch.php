<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Applications;

/**
 * ApplicationsSearch represents the model behind the search form about `app\models\Applications`.
 */
class ApplicationsSearch extends Applications
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contract_id', 'min_warranty_currency', 'text_adap_currency', 'maintenance_currency'], 'integer'],
            [['date_cr', 'uniform_condition', 'number'], 'safe'],
            [['min_warranty', 'tex_adap', 'maintenance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Applications::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'contract_id' => $this->contract_id,
            'number' => $this->number,
            'date_cr' => $this->date_cr,
            'min_warranty' => $this->min_warranty,
            'min_warranty_currency' => $this->min_warranty_currency,
            'tex_adap' => $this->tex_adap,
            'text_adap_currency' => $this->text_adap_currency,
            'maintenance' => $this->maintenance,
            'maintenance_currency' => $this->maintenance_currency,
        ]);

        $query->andFilterWhere(['like', 'uniform_condition', $this->uniform_condition]);

        return $dataProvider;
    }
}
