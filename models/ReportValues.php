<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report_values".
 *
 * @property int $id
 * @property int $report_monetary_id Загрузка отчета
 * @property string $sale_count Количество продаж
 * @property int $film_id Фильм
 * @property string $film_name Название фильма
 * @property string $est EST
 * @property string $vod VOD
 * @property string $svod SVOD
 * @property string $pvod PVOD
 * @property string $avod AVOD
 * @property string $date_begin Дата начала отчетного периода
 * @property string $date_end Дата окончания отчетного периода
 * @property string $date_begin_value Дата начала отчетного периода
 * @property string $date_end_value Дата окончания отчетного периода
 * @property int $currency Валюта продаж
 * @property int $territory Территория
 * @property string $identificator Идентификатор площадки
 * @property string $additional_identificator Дополнительный идентификатор площадки
 *
 * @property Films $film
 * @property ReportMonetary $reportMonetary
 * @property SettingCourses[] $settingCourses
 */
class ReportValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['report_monetary_id', 'film_id', 'currency', 'territory'], 'integer'],
            [['date_begin', 'date_end'], 'safe'],
            [['sale_count', 'film_name', 'est', 'vod', 'svod', 'pvod', 'avod', 'date_begin_value', 'date_end_value', 'identificator', 'additional_identificator'], 'string', 'max' => 255],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Films::className(), 'targetAttribute' => ['film_id' => 'id']],
            [['report_monetary_id'], 'exist', 'skipOnError' => true, 'targetClass' => ReportMonetary::className(), 'targetAttribute' => ['report_monetary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_monetary_id' => 'Загрузка отчета',
            'sale_count' => 'Кол-во продаж',
            'film_id' => 'Фильм',
            'film_name' => 'Название фильма',
            'est' => 'EST',
            'vod' => 'VOD',
            'svod' => 'SVOD',
            'pvod' => 'PVOD',
            'avod' => 'AVOD',
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'date_begin_value' => 'Дата начала',
            'date_end_value' => 'Дата окончания',
            'currency' => 'Валюта продаж',
            'territory' => 'Территория',
            'identificator' => 'Иден. площадки',
            'additional_identificator' => 'Доп. иден. площадки',// 'Дополнительный идентификатор площадки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportMonetary()
    {
        return $this->hasOne(ReportMonetary::className(), ['id' => 'report_monetary_id']);
    }

    public function beforeDelete()
    {
        /*$setting = SettingCourses::find()->where(['contract_id' => $this->id])->all();
        foreach ($setting as $value) {
            $value->delete();
        }*/
        return parent::beforeDelete();
    }

    public function setCourses($report_monetary_id)
    {
        $session = Yii::$app->session;
        $monetary = ReportMonetary::find()->where(['id' => $report_monetary_id])->one();
        $contracts = Contract::find()->where(['id' => $session['contractID']])->all();
        $contractCurrencyID = [];
        foreach ($contracts as $contract) {
            $contractCurrencyID [] = $contract->payment_currency;
        }
        $contractCurrencyID = array_unique($contractCurrencyID);

        $reportValues = ReportValues::find()->where(['report_monetary_id' => $report_monetary_id])->all();
        $currencyID = [];
        foreach ($reportValues as $report) {
            $currencyID [] = $report->currency;
        }
        $currencyID = array_unique($currencyID);
        $resultID = [];

        foreach ($contractCurrencyID as $contractCurrency) {
            foreach ($currencyID as $currency) {
                if($contractCurrency !== $currency){
                    $currencySetting = CurrencySetting::find()
                        ->where(['currency_from_id' => $contractCurrency, 'currency_to_id' => $currency, 'monetary_id' => $monetary->id])
                        ->one();
                    if($currencySetting === null){
                        $currencySetting = CurrencySetting::find()
                            ->where(['currency_from_id' => $currency, 'currency_to_id' => $contractCurrency, 'monetary_id' => $monetary->id])
                            ->one();
                        if($currencySetting === null){
                            $currencySetting = new CurrencySetting();
                            $currencySetting->currency_from_id = $contractCurrency;
                            $currencySetting->currency_to_id = $currency;
                            $currencySetting->monetary_id = $monetary->id;
                            $currencySetting->save();
                        }
                        else{
                            $resultID [] = $currencySetting->id;
                        }
                    }
                    else{
                        $resultID [] = $currencySetting->id;
                    }
                }
            }
        }

        return $resultID;
    }

    public function statistic($contracts, $post, $months)
    {
        $filmsPrint = [];
        $results = [];
        $reportMonetary = ReportMonetary::find()->where(['date' => $months])->all();
        $reportId = [];
        foreach ($reportMonetary as $report) {
            $reportId [] = $report->id;
        }

        foreach ($contracts as $contract) 
        {
            $allModels = [];
            $filmsId = [];
            $currencyName = Currency::findOne($contract->payment_currency)->code;
            $filmsApplication = FilmsApplication::find()->joinWith('application')->where(['applications.contract_id' => $contract->id])->all();
            foreach ($filmsApplication as $value) {
                $filmsId [] = $value->film_id;
            }
            $filmsId = array_unique($filmsId);

            /*filmlarni yigish moduli*/
            foreach ($filmsId as $film_id) 
            {
                $index = 0;
                $platformCount = 0;
                $totalSummary = 0;
                $totalRolyati = 0;
                $royalityPayment = 0;
                $nds = 0;
                $totalMgs = 0;
                $totalTas = 0;
                $totalMfs = 0;
                $film = Films::findOne($film_id);
                $filmName = $film->name;
                $filmsApplication = FilmsApplication::find()->joinWith('application')->where(['films_application.film_id' => $film_id, 'applications.contract_id' => $contract->id ])->one();

                $allTerritories = [];
                foreach (json_decode($filmsApplication->territory) as $territory) {
                    $allTerritories [] = $territory->id;
                }
                $reportValues = ReportValues::find()
                    ->where(['report_monetary_id' => $reportId])
                    ->andWhere(['film_id' => $film_id])
                    ->andWhere(['territory' => $allTerritories])
                    ->all();

                $platformJson = json_decode($filmsApplication->platforms);
                $platforms = [];
                foreach ($platformJson as $fff) {
                    $platforms [] = $fff->id;
                }

                $proverka = 0;
                if($reportValues != null) {
                    foreach ($reportValues as $reportValue) {
                        if(in_array($reportValue->territory, $allTerritories)) $proverka = 1;
                    }
                }
                else $proverka = 1;

                if($proverka === 0) continue;

                foreach ($platforms as $platform) 
                {
                    $index++; 
                    $platformName = Platform::findOne($platform)->name_ru;
                    $estCount = ''; $estSum = '';
                    $vodCount = ''; $vodSum = '';
                    $svodCount = ''; $svodSum = '';
                    $pvodCount = ''; $pvodSum = '';
                    $avodCount = ''; $avodSum = '';
                    $summary = 0;
                    foreach ($reportValues as $reportValue) 
                    {
                        if($reportValue->reportMonetary->platform_id == $platform) 
                        {
                            $sum = 0;
                            if($reportValue->est !== null) { $estCount += $reportValue->sale_count; $sum += $reportValue->est;  }
                            if($reportValue->vod !== null) { $vodCount += $reportValue->sale_count; $sum += $reportValue->vod;  }
                            if($reportValue->svod !== null) { $svodCount += $reportValue->sale_count; $sum += $reportValue->svod; }
                            if($reportValue->pvod !== null) { $pvodCount += $reportValue->sale_count; $sum += $reportValue->pvod; }
                            if($reportValue->avod !== null) { $avodCount += $reportValue->sale_count; $sum += $reportValue->avod; }

                            $currencyValue = 0;
                            $currencySetting = CurrencySetting::find()
                                ->where(['currency_from_id' => $reportValue->currency, 'currency_to_id' => $contract->payment_currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                            if($currencySetting != null) 
                            {
                                $currencyValue = $currencySetting->currency_to_value / $currencySetting->currency_from_value;
                            }
                            else{
                                $currencySetting = CurrencySetting::find()
                                    ->where(['currency_from_id' => $contract->payment_currency, 'currency_to_id' => $reportValue->currency, 'monetary_id' => $reportValue->report_monetary_id])->one();
                                if($currencySetting != null)
                                {
                                    $currencyValue = $currencySetting->currency_from_value / $currencySetting->currency_to_value;
                                }
                            }
                            if($currencySetting != null) { 
                                $summary += $currencyValue * $sum; 
                                if($reportValue->est != null) $estSum += $currencyValue * $reportValue->est;
                                if($reportValue->vod != null) $vodSum += $currencyValue * $reportValue->vod;
                                if($reportValue->svod != null) $svodSum += $currencyValue * $reportValue->svod;
                                if($reportValue->pvod != null) $pvodSum += $currencyValue * $reportValue->pvod;
                                if($reportValue->avod != null) $avodSum += $currencyValue * $reportValue->avod;
                            }
                            else {
                                $summary += $sum;
                                if($reportValue->est != null) $estSum += $reportValue->est;
                                if($reportValue->vod != null) $vodSum += $reportValue->vod;
                                if($reportValue->svod != null) $svodSum += $reportValue->svod;
                                if($reportValue->pvod != null) $pvodSum += $reportValue->pvod;
                                if($reportValue->avod != null) $avodSum += $reportValue->avod;
                            }
                        }
                    }

                    $platformCount++;
                    $sale = $estCount + $vodCount + $svodCount + $pvodCount  + $avodCount;
                    $currency = Currency::findOne($contract->payment_currency)->code;
                    $rolyati_x_summa = $summary*$filmsApplication->rolyati_x*0.01;
                    $summary = round($summary, 2);
                    $rolyati_x_summa = round($rolyati_x_summa, 2);
                    if(is_nan($rolyati_x_summa)) $rolyati = 0;
                    else $rolyati = $rolyati_x_summa;

                    if($filmsApplication->application->uniform_condition === 1){
                        $min_warranty = $filmsApplication->application->min_warranty;
                        $tex_adap = $filmsApplication->application->tex_adap;
//                        var_dump($tex_adap);
//                        exit;
                        $maintenance = $filmsApplication->application->maintenance;
                    }
                    else{
                        $min_warranty = $filmsApplication->min_warranty;
                        $tex_adap = $filmsApplication->tex_adap;
                        $maintenance = $filmsApplication->maintenance;
                    }
                                                            
                    if($min_warranty === null) $min_warranty = 0;
                    if($tex_adap === null) $tex_adap = 0;
                    if($maintenance === null) $maintenance = 0;

                    $findLastMinWarranty = Users::findLastMinWarranty($filmsPrint, $contract->id, $filmName, $filmsApplication->id, $filmsApplication->application->id, $filmsApplication->application->number);
                    if($findLastMinWarranty !== null) $min_warranty = $findLastMinWarranty;

                    if($min_warranty - $rolyati > 0) {
                        $mgs = $min_warranty - $rolyati;
                        $rolyati = 0;
                    }
                    else {
                        $mgs = 0;
                        $rolyati = $rolyati - $min_warranty;
                    }

                    if($mgs == 0 && $rolyati > 0) {
                        if($tex_adap - $rolyati > 0) {
                            $tas = $tex_adap - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $tas = 0;
                            $rolyati = $rolyati - $tex_adap;
                        }
                    }

                    if($tas == 0 && $rolyati > 0) {
                        if($maintenance - $rolyati > 0) {
                            $mfs = $maintenance - $rolyati;
                            $rolyati = 0;
                        }
                        else {
                            $mfs = 0;
                            $rolyati = $rolyati - $maintenance;
                        }
                    }

                    $currency = $contract->CurrencyDescription($contract->payment_currency);
                    $curCode = PreferenceBooks::getCurrencySymbol($currency);
                    if(is_nan($estSum)) $estSum = '';
                    else $estSum = $estSum != '' ? round($estSum, 2) : '';
                    if(is_nan($vodSum)) $vodSum = '';
                    else $vodSum = $vodSum != '' ? round($vodSum, 2) : '';
                    if(is_nan($svodSum)) $svodSum = '';
                    else $svodSum = $svodSum != '' ? round($svodSum, 2) : '';
                    if(is_nan($pvodSum)) $pvodSum = '';
                    else $pvodSum = $pvodSum != '' ? round($pvodSum, 2) : '';
                    if(is_nan($avodSum)) $avodSum = '';
                    else $avodSum = $avodSum != '' ? round($avodSum, 2) : '';
                    if(is_nan($summary)) $summary = 0;
                    else $summary = $summary != '' ? round($summary, 2) : '';

                    $mgs =  $mgs == null ? $min_warranty : $mgs;
                    $tas =  $tas == null ? $min_warranty : $tas;
                    $mfs =  $mfs == null ? $min_warranty : $mfs;

                    $totalSummary += $summary;
                    $totalRolyati += $rolyati;
                    $totalMgs += $mgs;
                    $totalTas += $tas;
                    $totalMfs += $mfs;

                    $rolyatiPercent = ($filmsApplication->rolyati_x !== null ? 0.01 * $filmsApplication->rolyati_x : '');

                    
                    $ostatokSum = $mgs + $tas + $mfs;
                    if( $ostatokSum < $rolyati) {
                        $vaqtin = $rolyati - $ostatokSum;
                        $royalityPayment += $rolyati - $ostatokSum;

                        if($film->certificate != 1){
                            $nds += -($vaqtin/1.2 - $vaqtin);
                            //$nds = round($nds, 2);
                        }
                    }
                    

                    $filmsPrint [] = [
                        'contract_id' => $contract->id,
                        'filmName' => $filmName,
                        'certificate' => $film->certificate,
                        'filmsApplication_id' => $filmsApplication->id,
                        'application_id' => $filmsApplication->application->id,
                        'application_number' => $filmsApplication->application->number,
                        'rolyati' => $rolyatiPercent,
                        'platformNames' => $platformName,
                        'check' => $filmsApplication->application->uniform_condition,
                        'mgt' => $min_warranty,
                        'mgs' => $mgs,
                        'tat' => $tex_adap,
                        'tas' => $tas,
                        'mft' => $maintenance,
                        'mfs' => $mfs,
                        'estCount' => $estCount,
                        'est' => $estSum,
                        'vodCount' => $vodCount,
                        'vod' => $vodSum,
                        'svodCount' => $svodCount,
                        'svod' => $svodSum,
                        'pvodCount' => $pvodCount,
                        'pvod' => $pvodSum,
                        'avodCount' => $avodCount,
                        'avod' => $avodSum,
                        'sale' => $sale,
                        'summary' => $summary,
                        'old_rolyati_x_summa' => $rolyati_x_summa,
                        'rolyati_x_summa' => $rolyati,
                    ];

                    $allModels [] = [
                        'filmName' => $filmName,
                        'platformName' => $platformName,
                        'applicationNumber' => $filmsApplication->application->number,
                        'estCount' => $estCount,
                        'vodCount' => $vodCount,
                        'svodCount' => $svodCount,
                        'pvodCount' => $pvodCount,
                        'avodCount' => $avodCount,
                        'sale' => $sale,
                        'estSum' => $estSum,
                        'vodSum' => $vodSum,
                        'svodSum' => $svodSum,
                        'pvodSum' => $pvodSum,
                        'avodSum' => $avodSum,
                        'summary' => $summary,
                        'totalSummary' => $totalSummary,
                        'min_warranty' => $min_warranty,
                        'tex_adap' => $tex_adap,
                        'maintenance' => $maintenance,
                        'percent' => $rolyatiPercent*100,
                        'percentValue' => $totalRolyati,
                        'mgs' => $totalMgs,
                        'tas' => $totalTas,
                        'mfs' => $totalMfs,
                        'currencyValue' => $royalityPayment,
                        'nds' => round($nds, 2),
                        'curCode' => $curCode,
                        'platformCount' => $platformCount,
                    ];

                }

                /*foreach($allModels as &$value) {
                    if($value['filmName'] == $filmName) $value['totalSummary'] = $totalSummary;
                    if($value['filmName'] == $filmName) $value['totalRolyati'] = $totalRolyati;
                    if($value['filmName'] == $filmName) $value['totalMgs'] = $totalMgs;
                    if($value['filmName'] == $filmName) $value['totalTas'] = $totalTas;
                    if($value['filmName'] == $filmName) $value['totalMfs'] = $totalMfs;
                }*/

            }

            /*echo "<pre>";
            print_r($allModels);
            echo "</pre>";
            die;*/

            /*search bolimi*/
            $searchAttributes = ['filmName', 'platformName', 'applicationNumber', 'estCount', 'vodCount', 'svodCount', 'pvodCount', 'avodCount', 'sale', 'estSum', 'vodSum', 'svodSum', 'pvodSum', 'avodSum', 'summary', 'totalSummary', 'min_warranty', 'tex_adap', 'maintenance', 'percent', 'percentValue', 'mgs', 'tas', 'mfs', 'currencyValue', 'nds', 'curCode'];
            $searchModel = [];
            $searchColumns = [];

            foreach ($searchAttributes as $searchAttribute) {
                $filterName = 'filter' . $contract->id . $searchAttribute;
                $filterValue = Yii::$app->request->getQueryParam($filterName, '');
                $searchModel[$searchAttribute] = $filterValue;

                if($searchAttribute == 'filmName'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'Название <br> фильма',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'group' => true,  // enable grouping
                    ];
                }

                if($searchAttribute == 'platformName'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'Площадка',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'applicationNumber'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => '№ Приложения',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'estCount'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'EST',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'vodCount'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'VOD',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'svodCount'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'SVOD',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'pvodCount'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'PVOD',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'avodCount'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'AVOD',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'sale'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => 'Итого',
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                    ];
                }

                if($searchAttribute == 'estSum'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "EST <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            if($data['estSum'] == '') return "";
                            else return $data['curCode'] . $data['estSum'];
                        }
                    ];
                }

                if($searchAttribute == 'vodSum'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "VOD <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            if($data['vodSum'] == '') return "";
                            else return $data['curCode'] . $data['vodSum'];
                        }
                    ];
                }

                if($searchAttribute == 'svodSum'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "SVOD <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            if($data['svodSum'] == '') return "";
                            else return $data['curCode'] . $data['svodSum'];
                        }
                    ];
                }

                if($searchAttribute == 'pvodSum'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "PVOD <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            if($data['pvodSum'] == '') return "";
                            else return $data['curCode'] . $data['pvodSum'];
                        }
                    ];
                }

                if($searchAttribute == 'avodSum'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "AVOD <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            if($data['avodSum'] == '') return "";
                            else return $data['curCode'] . $data['avodSum'];
                        }
                    ];
                }

                if($searchAttribute == 'summary'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Итого <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            if($data['summary'] == '') return $data['curCode'] . '0';
                            else return $data['curCode'] . $data['summary'];
                        },
                    ];
                }

                if($searchAttribute == 'totalSummary'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Итого вал <br> по площадкам <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['totalSummary'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'min_warranty'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Размер МГ <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'tex_adap'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Техническая <br> адаптация <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'maintenance'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Техническое <br> обслуживание <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'percent'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Ставка <br> вознаграждения <br> Лицензиара (%) <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['percent'] . '%';
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'percentValue'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Лицензионное <br> вознаграждение <br> Лицензиара за <br> отчетный период <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['percentValue'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'mgs'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Остаток МГ <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['mgs'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'tas'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Остаток <br> технической <br> адаптации <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['tas'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'mfs'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Остаток <br> технического <br> обслуживания <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['mfs'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'currencyValue'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "Сумма к <br> выплате <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['currencyValue'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                if($searchAttribute == 'nds'){
                    $searchColumns[] = [
                        'attribute' => $searchAttribute,
                        'header' => "НДС (20%) <br>({$currencyName})",
                        'filter' => '<input class="form-control" name="' . $filterName . '" value="' . $filterValue . '" type="text">',
                        'value' => $searchAttribute,
                        'content' => function($data){
                            return $data['curCode'] . $data['nds'];
                        },
                        'group' => true, 
                        'subGroupOf' => 0,
                    ];
                }

                $allModels = array_filter($allModels, function($item) use (&$filterValue, &$searchAttribute) {
                    return strlen($filterValue) > 0 ? stripos('/^' . strtolower($item[$searchAttribute]) . '/', strtolower($filterValue)) : true;
                });
            }
            /*search tugadi*/


            /*chart uchun kerakli kodlar*/

            $est = (float)ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $filmsId])
                ->sum('est');
            $vod = (float)ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $filmsId])
                ->sum('vod');
            $svod = (float)ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $filmsId])
                ->sum('svod');
            $pvod = (float)ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $filmsId])
                ->sum('pvod');
            $avod = (float)ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $filmsId])
                ->sum('avod');
            $sale = (float)ReportValues::find()
                ->where(['report_monetary_id' => $reportId])
                ->andWhere(['film_id' => $filmsId])
                ->sum('sale_count');

            $filmName = "";
            $data = "";
            $backgroundColor = "";
            foreach ($filmsId as $film_id) {
                $filmName .= "'" . Films::findOne($film_id)->name . "', ";
                $sale_count = (float)ReportValues::find()
                    ->where(['report_monetary_id' => $reportId])
                    ->andWhere(['film_id' => $film_id])
                    ->sum('sale_count');
                $data .= $sale_count . ", ";
                $backgroundColor .= PreferenceBooks::getRGBA();                        
            }
            /*chart tugadi*/

            $results[] = [
                'result' => $allModels,
                'searchAttributes' => $searchAttributes,
                'searchColumns' => $searchColumns,
                'searchModel' => $searchModel,
                'contract' => $contract,
                'currency' => $currencyName,
                'filmName' => $filmName,
                'sale_count' => $sale_count,
                'est' => $est,
                'vod' => $vod,
                'svod' => $svod,
                'pvod' => $pvod,
                'avod' => $avod,
                'sale' => $sale,
                'data' => $data,
                'backgroundColor' => $backgroundColor,
            ];
        }
        
        $session = Yii::$app->session;
        $session['filmsPrint'] = $filmsPrint;
        return $results;
    }
}
