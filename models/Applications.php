<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property int $contract_id Договор
 * @property int $number № приложения
 * @property string $date_cr Дата создания приложения
 * @property int $uniform_condition У фильмов в приложении единые условия по финансам
 * @property double $min_warranty Мин.гарантия
 * @property int $min_warranty_currency Валюта выплаты
 * @property double $tex_adap Тех.адаптация
 * @property int $text_adap_currency Валюта выплаты
 * @property double $maintenance Maintenance fee
 * @property int $maintenance_currency Валюта выплаты
 * @property int $settings Настройка
 *
 * @property Contract $contract
 * @property Currency $maintenanceCurrency
 * @property Currency $minWarrantyCurrency
 * @property Currency $textAdapCurrency
 * @property FilmsApplication[] $filmsApplications
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public $films;
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['contract_id', 'uniform_condition', 'min_warranty_currency', 'text_adap_currency', 'maintenance_currency', 'settings', 'films'], 'integer'],
            [['date_cr'], 'safe'],
            [['number'], 'string', 'max' => 255],
            [['min_warranty', 'tex_adap', 'maintenance'], 'number'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
            [['maintenance_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['maintenance_currency' => 'id']],
            [['min_warranty_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['min_warranty_currency' => 'id']],
            [['text_adap_currency'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['text_adap_currency' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'Договор',
            'number' => '№ приложения',
            'date_cr' => 'Дата создания приложений',
            'uniform_condition' => 'У фильмов в приложении единые условия по финансам',
            'min_warranty' => 'Мин.гарантия',
            'min_warranty_currency' => 'Валюта выплаты',
            'tex_adap' => 'Тех.адаптация',
            'text_adap_currency' => 'Валюта выплаты',
            'maintenance' => 'Тех. обслуживание',
            'maintenance_currency' => 'Валюта выплаты',
            'settings' => 'Настройка',
            'films' => 'Добавление фильмов/сериалов',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            if($this->date_cr != null) {
                $this->date_cr = date('Y-m-d', strtotime($this->date_cr) );
            }
        }

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $filmsApplication = FilmsApplication::find()->where(['application_id' => $this->id])->all();
        foreach ($filmsApplication as $value) {
            $value->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaintenanceCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'maintenance_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinWarrantyCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'min_warranty_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextAdapCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'text_adap_currency']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilmsApplications()
    {
        return $this->hasMany(FilmsApplication::className(), ['application_id' => 'id']);
    }

    public function getCurrency()
    {
        $payment_currency = Currency::find()->all();
        return ArrayHelper::map( $payment_currency , 'id', 'code');
    }
    
    public function getFilmsList($application)
    {   
            // $identity = Yii::$app->user->identity;
            // $rightHolders = RightHolders::find()->where(['user_id' => $identity->id])->all();
            // $idList = [];
            // foreach ($rightHolders as $value) {
            //     $idList [] = $value->id;
            // }

            // $contracts = Contract::find()->where(['or',
            //     ['company_lic' => $idList],
            //     ['company_cont' => $idList]
            // ])->all();

            // $applicationId = [];
            // foreach ($contracts as $value) {
            //     $app = Applications::find()->where(['contract_id' => $value->id])->all();
            //     foreach ($app as $ap) {
            //         $applicationId [] = $ap->id;
            //     }
            // }

            // $filmsApp = FilmsApplication::find()->where(['application_id' => $applicationId])->all();

            // $filmsID = [];
            // foreach ($filmsApp as $value) {
            //     $filmsID [] = $value->film_id;
            // }

            // $film = Films::find()
            //     ->where(['id' => $filmsID])
            //     ->all();

        
        $app = [];
        foreach ($application as $value) {
                $app [] = $value->id;
            }

        $filmsApplication = FilmsApplication::find()->where(['application_id' => $app])->all();
        $id = [];

        foreach ($filmsApplication as $value) {
            $id [] = $value->film_id;
        }

        $film = Films::find()->where(['not in', 'id', $id ])
        ->all();
        return ArrayHelper::map( $film , 'id', 'name');
    }
}
