<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportItunes;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ReportItunesSearch represents the model behind the search form about `app\models\ReportItunes`.
 */
class ReportItunesSearch extends ReportItunes
{

    /** @var string */
    public $dateStart;

    /** @var string */
    public $dateEnd;

    /** @var int */
    public $noRelation;

    public $countTitles;

    public $currency;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'film_id', 'sales', 'countTitles'], 'integer'],
            [['apple_identifier', 'vendor_identifier', 'title', 'type', 'type_transaction', 'customer_currency', 'country', 'format', 'genre', 'begin_date', 'date', 'dateStart', 'dateEnd', 'noRelation', 'currency'], 'safe'],
            [['customer_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDefault($params)
    {
        $query = ReportItunes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'film_id' => $this->film_id,
            'sales' => $this->sales,
            'customer_price' => $this->customer_price,
            'date' => $this->date,
        ]);

        $query->andFilterWhere(['like', 'apple_identifier', $this->apple_identifier])
            ->andFilterWhere(['like', 'vendor_identifier', $this->vendor_identifier])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'customer_currency', $this->customer_currency])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'format', $this->format])
            ->andFilterWhere(['like', 'genre', $this->genre])
            ->andFilterWhere(['like', 'begin_date', $this->begin_date])
            ->andFilterWhere(['like', 'type_transaction', $this->type_transaction]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $groupBy = null, $select = 'report_itunes.*, sum(report_itunes.sales) as count')
    {
        $query = (new Query())->from('report_itunes')->select($select);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'film_id' => $this->film_id,
            'sales' => $this->sales,
            'customer_price' => $this->customer_price,
            'date' => $this->date,
        ]);

        $query->leftJoin('films', 'report_itunes.film_id=films.id');
        $query->leftJoin('films_application', 'films_application.film_id=films.id');
        $query->leftJoin('applications', 'films_application.application_id=applications.id');
        $query->leftJoin('contract', 'contract.id=applications.contract_id');
        $query->leftJoin('right_holders', 'right_holders.id=contract.company_lic');

        $titleLikes = [];
        $countryLikes = [];
        $typeLikes = [];
        $genreLikes = [];
        $typeTransactionLikes = [];
        $formatLikes = [];


        if($this->countTitles == null){
            $this->countTitles = 10;
        }

//                var_dump($this->countTitles);
//        exit;

        if($this->title == null){
            $titles = ArrayHelper::getColumn(Films::find()->limit($this->countTitles)->all(), 'name');
        } else {
            $titles = $this->title;
        }

        foreach ($titles as $str)
        {
            $titleLikes[] = ['like', 'title', $str];
        }

        foreach ($this->country as $str)
        {
            $countryLikes[] = ['like', 'country', $str];
        }

        foreach ($this->type as $str)
        {
            $typeLikes[] = ['like', 'type', $str];
        }

        foreach ($this->genre as $str)
        {
            $genreLikes[] = ['like', 'genre', $str];
        }

        foreach ($this->type_transaction as $str)
        {
            $typeTransactionLikes[] = ['like', 'type_transaction', $str];
        }

        foreach ($this->format as $str)
        {
            $formatLikes[] = ['like', 'format', $str];
        }


        $titleLikes = ArrayHelper::merge(['or'], $titleLikes);
        $countryLikes = ArrayHelper::merge(['or'], $countryLikes);
        $typeLikes = ArrayHelper::merge(['or'], $typeLikes);
        $genreLikes = ArrayHelper::merge(['or'], $genreLikes);
        $typeTransactionLikes = ArrayHelper::merge(['or'], $typeTransactionLikes);
        $formatLikes = ArrayHelper::merge(['or'], $formatLikes);

        $query->andFilterWhere(['like', 'apple_identifier', $this->apple_identifier])
            ->andFilterWhere(['like', 'vendor_identifier', $this->vendor_identifier])
            ->andFilterWhere($titleLikes)
            ->andFilterWhere($typeLikes)
            ->andFilterWhere($typeTransactionLikes)
            ->andFilterWhere($countryLikes)
            ->andFilterWhere($formatLikes)
            ->andFilterWhere($genreLikes)
            ->andFilterWhere(['like', 'customer_currency', $this->customer_currency])
            ->andFilterWhere(['like', 'begin_date', $this->begin_date]);

        if($this->dateStart != null && $this->dateEnd != null){
            $query->andWhere(['between', 'date', $this->dateStart, $this->dateEnd]);
        } else {
            $this->dateStart = date('Y-m-d', (time() - (86400 * 30)));
            $this->dateEnd = date('Y-m-d');

            $query->andWhere(['between', 'date', $this->dateStart, $this->dateEnd]);
        }

        if($this->noRelation){
            $query->andWhere(['is', 'report_itunes.film_id', null]);
        } else {
            $query->andWhere(['is not', 'report_itunes.film_id', null]);
        }

        if($select == 'report_itunes.*, sum(report_itunes.sales) as count'){
            $query->orderBy('count desc');
        }

        if(Yii::$app->user->identity->type == Users::CLIENT){
            $query->andWhere(['right_holders.user_id' => Yii::$app->user->getId()]);
        }

        if($groupBy){
            $query->groupBy($groupBy);
        }

//        if($groupBy == 'title'){
            $dataProvider->pagination = false;
//            $query->limit(10);
//        }

//        echo $query->createCommand()->getRawSql();
//        exit;

        return $dataProvider;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'noRelation' => 'Без связи'
        ]);
    }
}
