<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RightHolders;

/**
 * RightHoldersSearch represents the model behind the search form about `app\models\RightHolders`.
 */
class RightHoldersSearch extends RightHolders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'country_id', 'bank_country_id', 'bank_account_currency', 'iban_currency', 'correspondent_account_currency'], 'integer'],
            [['name_ru', 'name_eng', 'tax_id', 'registry_code', 'primary_number', 'street1', 'street2', 'city_id', 'state', 'index', 'bank_name', 'bank_street1', 'bank_street2', 'bank_state', 'bank_index', 'bic_swift'], 'safe'],
            [['bank_account', 'iban', 'correspondent_account'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RightHolders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'country_id' => $this->country_id,
            'bank_country_id' => $this->bank_country_id,
            'bank_account' => $this->bank_account,
            'bank_account_currency' => $this->bank_account_currency,
            'iban' => $this->iban,
            'iban_currency' => $this->iban_currency,
            'correspondent_account' => $this->correspondent_account,
            'correspondent_account_currency' => $this->correspondent_account_currency,
        ]);

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_eng', $this->name_eng])
            ->andFilterWhere(['like', 'tax_id', $this->tax_id])
            ->andFilterWhere(['like', 'registry_code', $this->registry_code])
            ->andFilterWhere(['like', 'primary_number', $this->primary_number])
            ->andFilterWhere(['like', 'street1', $this->street1])
            ->andFilterWhere(['like', 'street2', $this->street2])
            ->andFilterWhere(['like', 'city_id', $this->city_id])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'index', $this->index])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_street1', $this->bank_street1])
            ->andFilterWhere(['like', 'bank_street2', $this->bank_street2])
            ->andFilterWhere(['like', 'bank_state', $this->bank_state])
            ->andFilterWhere(['like', 'bank_index', $this->bank_index])
            ->andFilterWhere(['like', 'bic_swift', $this->bic_swift]);

        return $dataProvider;
    }
}
