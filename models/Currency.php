<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $name Наименование
 * @property string $code Код
 *
 * @property Applications[] $applications
 * @property Applications[] $applications0
 * @property Applications[] $applications1
 * @property SettingCourses[] $settingCourses
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'code' => 'Код',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['maintenance_currency' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications0()
    {
        return $this->hasMany(Applications::className(), ['min_warranty_currency' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplications1()
    {
        return $this->hasMany(Applications::className(), ['text_adap_currency' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettingCourses()
    {
        return $this->hasMany(SettingCourses::className(), ['currency_id' => 'id']);
    }
}
