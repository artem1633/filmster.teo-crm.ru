<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\FilmsType;

/**
 * This is the model class for table "films_application".
 *
 * @property int $id
 * @property int $film_id Фильм
 * @property int $application_id Приложения
 * @property string $territory Территория
 * @property double $rolyati_x Роялти лицензиат
 * @property double $rolyati_y Роялти лицензиар
 * @property string $platforms Площадки
 * @property int $price Цена
 * @property string $est_open EST откр.
 * @property string $vod_open VOD откр.
 * @property string $close_date Закрытие
 * @property string $svod SVOD (открытие)
 * @property string $pvod PVOD (открытие)
 * @property string $avod AVOD (открытие)
 * @property string $catch_up Catch-up (открытие)
 * @property string $tv TV (открытие)
 * @property string $flat_rate Flat Rate (открытие)
 * @property double $min_warranty Мин.гарантия
 * @property int $min_warranty_currency Валюта выплаты
 * @property double $tex_adap Тех.адаптация
 * @property int $text_adap_currency Валюта выплаты
 * @property double $maintenance Maintenance fee
 * @property int $maintenance_currency Валюта выплаты
 *
 * @property Applications $application
 * @property Films $film
 */
class FilmsApplication extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'films_application';
    }
    public $allTerritories;
    public $allPlatforms;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['film_id', 'application_id', 'price', 'min_warranty_currency', 'text_adap_currency', 'maintenance_currency', 'flat_rate_currency'], 'integer'],
            [['territory', 'platforms'], 'string'],
            [['rolyati_x', 'rolyati_y', 'min_warranty', 'tex_adap', 'maintenance'], 'number'],
            [['est_open', 'vod_open', 'close_date','svod', 'pvod', 'avod','tv'], 'safe'],
            [[ 'catch_up',  'flat_rate'], 'string', 'max' => 255],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => Applications::className(), 'targetAttribute' => ['application_id' => 'id']],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Films::className(), 'targetAttribute' => ['film_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'film_id' => 'Фильм',
            'application_id' => 'Приложения',
            'territory' => 'Территория',
            'rolyati_x' => 'Роялти лицензиат',
            'rolyati_y' => 'Роялти лицензиар',
            'platforms' => 'Площадки',
            'price' => 'Цена',
            'est_open' => 'EST откр.',
            'vod_open' => 'VOD откр.',
            'close_date' => 'Закрытие',
            'svod' => 'SVOD (открытие)',
            'pvod' => 'PVOD (открытие)',
            'avod' => 'AVOD (открытие)',
            'catch_up' => 'Catch-up (открытие)',
            'tv' => 'TV (открытие)',
            'flat_rate' => 'Flat Rate (открытие)',
            'flat_rate_currency' => 'Валюта',
            'min_warranty' => 'Мин.гарантия',
            'min_warranty_currency' => 'Валюта выплаты',
            'tex_adap' => 'Тех.адаптация',
            'text_adap_currency' => 'Валюта выплаты',
            'maintenance' => 'Техническое обслуживание',
            'maintenance_currency' => 'Валюта выплаты',
            'allTerritories' => 'Территория',
            'allPlatforms' => 'Площадки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(Applications::className(), ['id' => 'application_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }

    public function getCountryList()
    {
        $session = Yii::$app->session;
        // $country = Country::find()->all();
        return ArrayHelper::map( $session['Filapp[country]'] , 'id', 'name');
    }

    public function getQueryAllLists()
    {   
        $session = Yii::$app->session;
        $session['Filapp[country]']= Country::find()->asArray()->all();
        $session['Filapp[platforms]']= Platform::find()->asArray()->all();
    }

    public function getPlatformList()
    {   
        $session = Yii::$app->session;
        // $ground = Platform::find()->all();
        return ArrayHelper::map( $session['Filapp[platforms]'] , 'id', 'name_ru');
    }
    
    public function getCurrency()
    {
        $payment_currency = Currency::find()->all();
        return ArrayHelper::map( $payment_currency , 'id', 'code');
    }

    public function getTerritory()
    {
        $array = json_decode($this->territory);
        $result = '';
        foreach ($array as $value) {
            $country = Country::findOne($value->id);
            if($country != null){
                $result =  $result . $country->name . '<br>';
            }
        }
        return $result;
    }

    public function getPlatform()
    {
        $array = json_decode($this->platforms);
        $result = '';
        foreach ($array as $value) {
            $playground = Platform::findOne($value->id);
            if($playground != null){
                $result =  $result . $playground->name_ru . '<br>';
            }
        }
        return $result;
    }

    public function getMaintenance()
    {
        if($this->maintenance == 0 && $this->tex_adap == 0 && $this->min_warranty == 0){
            $this->application->uniform_condition = false;
        }

        if($this->application->uniform_condition){
            return $this->application->maintenance . ' ' . $this->application->getCurrency()[$this->application->maintenance_currency];
        }
        else{
            return $this->maintenance . ' ' . $this->getCurrency()[$this->maintenance_currency];
        }
    }

    public function getTexAdap()
    {
        if($this->maintenance == 0 && $this->tex_adap == 0 && $this->min_warranty == 0){
            $this->application->uniform_condition = false;
        }

        if($this->application->uniform_condition){
            return $this->application->tex_adap . ' ' . $this->application->getCurrency()[$this->application->text_adap_currency];
        }
        else{
            return $this->tex_adap . ' ' . $this->getCurrency()[$this->text_adap_currency];
        }
    }

    public function getMinWarranty()
    {
        if($this->maintenance == 0 && $this->tex_adap == 0 && $this->min_warranty == 0){
            $this->application->uniform_condition = false;
        }

        if($this->application->uniform_condition){
            return $this->application->min_warranty . ' ' . $this->application->getCurrency()[$this->application->min_warranty_currency];
        }
        else{
            return $this->min_warranty . ' ' . $this->getCurrency()[$this->min_warranty_currency];
        }
    }
}
