<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting_courses".
 *
 * @property int $id
 * @property int $currency_id Валюта
 * @property int $report_values_id Значения отчета
 * @property double $course Курс
 * @property double $value Значения
 *
 * @property Currency $currency
 * @property ReportValues $reportValues
 */
class SettingCourses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting_courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['currency_id', 'contract_id'], 'integer'],
            [['course', 'value'], 'number'],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_id' => 'Валюта',
            'contract_id' => 'Договор',
            'course' => 'Курс',
            'value' => 'Значения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
}
