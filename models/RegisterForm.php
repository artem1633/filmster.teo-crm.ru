<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $login;
    public $password;
    public $telephone;
    public $telegram;
    public $agree;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'agree'], 'required'],
            [['telephone'], 'string'],
            [['password', 'telegram'], 'string', 'max' => 255],
            [['login'], 'email'],
            [['agree'], 'integer'],
            [['login'], 'unique', 'targetClass' => '\app\models\User'],
            ['agree', 'validateAgree'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'telephone' => 'Телефон',
            'telegram' => 'Ид чат телеграма',
            'agree' => 'Я согласен с обработкой персональных данных',
        ];
    }

    public function validateAgree($attribute)
    { 
        if($this->agree != 1) $this->addError($attribute, 'Необходимо Ваше согласие');
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($type = 1)
    {
        if($this->validate() === false){
            return null;
        }

        $user = new Users();
        $user->fio = $this->fio;
        $user->login = $this->login;
        $user->telephone = $this->telephone;
        $user->password = $this->password;
        $user->telegram_id = $this->telegram;
        $user->type = $type;
        $user->agree = $agree;

        if($user->save()) {
            return $user;
        }

        return null;
    }

}