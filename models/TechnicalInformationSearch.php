<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TechnicalInformation;

/**
 * TechnicalInformationSearch represents the model behind the search form about `app\models\TechnicalInformation`.
 */
class TechnicalInformationSearch extends TechnicalInformation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'film_id', 'duration', 'source_availability'], 'integer'],
            [['video', 'audio', 'playback_speed', 'video_language', 'audio_language', 'source_link', 'burnt_subtitles', 'burnt_value', 'forced_subtitles', 'forced_value', 'subtitles', 'subtitles_value', 'sdh_subtitles', 'sdh_value', 'presense_cc', 'presense_value', 'treyler'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TechnicalInformation::find()
        ->with(['film']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'film_id' => $this->film_id,
            'duration' => $this->duration,
            'source_availability' => $this->source_availability,
        ]);

        $query->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'audio', $this->audio])
            ->andFilterWhere(['like', 'playback_speed', $this->playback_speed])
            ->andFilterWhere(['like', 'video_language', $this->video_language])
            ->andFilterWhere(['like', 'audio_language', $this->audio_language])
            ->andFilterWhere(['like', 'source_link', $this->source_link])
            ->andFilterWhere(['like', 'burnt_subtitles', $this->burnt_subtitles])
            ->andFilterWhere(['like', 'burnt_value', $this->burnt_value])
            ->andFilterWhere(['like', 'forced_subtitles', $this->forced_subtitles])
            ->andFilterWhere(['like', 'forced_value', $this->forced_value])
            ->andFilterWhere(['like', 'subtitles', $this->subtitles])
            ->andFilterWhere(['like', 'subtitles_value', $this->subtitles_value])
            ->andFilterWhere(['like', 'sdh_subtitles', $this->sdh_subtitles])
            ->andFilterWhere(['like', 'sdh_value', $this->sdh_value])
            ->andFilterWhere(['like', 'presense_cc', $this->presense_cc])
            ->andFilterWhere(['like', 'presense_value', $this->presense_value])
            ->andFilterWhere(['like', 'treyler', $this->treyler]);

        return $dataProvider;
    }
}
