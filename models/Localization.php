<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "localization".
 *
 * @property int $id
 * @property int $type Тип
 * @property int $language_id Язык
 * @property string $name Название фильма
 * @property string $description Краткое описание
 * @property string $cast Актерский состав
 * @property string $film_crew Съемочная группа
 * @property string $awards Награды
 * @property string $tags Поисковые теги
 * @property string $poster23 Постер 2х3
 * @property string $poster169 Постер 16х9
 * @property string $photos Кадры из фильма
 * @property int $film_id Фильм
 *
 * @property Films $film
 * @property Languages $language
 */
class Localization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $poster23_file;
    public $poster169_file;

    public static function tableName()
    {
        return 'localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['name', 'language_id'], 'required'],
            [['type', 'language_id', 'film_id'], 'integer'],
            [['description', 'cast', 'film_crew', 'poster23', 'poster169', 'photos'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Films::className(), 'targetAttribute' => ['film_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['poster23_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg',], 
            [['poster169_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 2097152, 'tooBig' => 'Limit 2 Mb'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'language_id' => $this->type == 1 ? 'Основной язык' : 'Дополнительный язык',
            'name' => 'Название фильма/сериала',
            'description' => 'Краткое описание',
            'cast' => 'Актерский состав',
            'film_crew' => 'Съемочная группа',
            'poster23' => 'Постер 2х3',
            'poster169' => 'Постер 16х9',
            'photos' => 'Кадры из фильма',
            'film_id' => 'Фильм',
            'poster23_file' => 'Постер 2X3',
            'poster169_file' => 'Постер 16X9',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($this->type == 1)
        {
            $this->film->name = $this->name;
            $this->film->language_id = $this->language_id;
            $this->film->description = $this->description;
            $this->film->save();
        }
    }

    public function beforeDelete()
    {
        /*$rights = Rights::find()->where(['film_id' => $this->id])->all();
        foreach ($rights as $value) {
            $value->delete();
        }*/
        Directory::deleteDirectory($this->id, 'localization');
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Languages::className(), ['id' => 'language_id']);
    }

    public function getLanguagesList()
    {
        $pro = Languages::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    public function getFilmsTypeList()
    {
        $pro = FilmsType::find()->all();
        return ArrayHelper::map($pro, 'id', 'name');
    }

    public function getRoleSelected($id)
    {
        $roles = PreferenceBooks::groupMemberRoleList();
        $result = '';
        foreach ($roles as $key => $value) {
            if($id == $key) $result .= '<option selected value="'.$key.'">'.$value.'</option>';
            else $result .= '<option value="'.$key.'">'.$value.'</option>';
        }
        return $result;
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'localization'")
            ->one();
        if($res) return $res["AUTO_INCREMENT"];
    }

    public function castDescription()
    {
        $result = "";
        foreach (json_decode($this->cast) as $value) {
            $result .= 'Имя и фамилия : ' . $value->akter . '; Роль : ' . $value->role . '<br>';
        }
        return $result;
    }

    public function crewDescription()
    {
        $result = "";
        foreach (json_decode($this->film_crew) as $value) {
            $result .= 'ФИО члена группы : ' . $value->names_group . '; Роль члена группы : ' . PreferenceBooks::groupMemberRoleList()[$value->group_member] . '<br>';
        }
        return $result;
    }

    public function setActors($film_id, $html, $json)
    {
        $actorsList = [];
        $crewList = [];
        $title_body = 'data-metrics-click';
        $film = Films::findOne($film_id);
        
        $loc = Localization::find()->where(['film_id' => $film_id])->one();
        if($loc == null){
            $loc = new Localization();
            $loc->type = 1;
            $loc->film_id = $film_id;
            $loc->name = $film->name;
            $loc->language_id = $film->language_id;
            $number = Localization::getLastId(); 
            Directory::createLocalizationDirectory($number);
        }
        else{
            foreach (json_decode($loc->cast) as $val) {
                $actorsList [] = [
                    'role' => $val->role,
                    'akter' => $val->akter,
                ];
            }
            foreach (json_decode($loc->film_crew) as $val) {
                $crewList [] = [
                    'names_group' => $val->names_group,
                    'group_member' => $val->group_member,
                ];
            }
        }

        foreach($html->find('.cast-list__detail a') as $key => $value) 
        {
            $actorType = Films::getActorType($value->$title_body);
            $parceValue = preg_replace('/<a[^>]*?>([\\s\\S]*?)<\/a>/','\\1', $value);
            $parceValue = substr($parceValue, 23, strlen($parceValue) - 23 );
            $parceValue = substr($parceValue, 0, strlen($parceValue) - 23 );
            if($actorType == 'cast'){
                $g = 0;
                foreach ($actorsList as $key => $val) {
                    if ($val['akter'] === $parceValue) {
                        $g = 1; break;
                    }
                }
                if($g == 0){
                    $actorsList [] = [
                        'role' => '',
                        'akter' => $parceValue,
                    ];
                }
            }

            if($actorType == 'director' || $actorType == 'producer' || $actorType == 'screenwriter'){
                $group_member = 0;
                if($actorType == 'director') $group_member = 2;
                if($actorType == 'producer') $group_member = 3;
                if($actorType == 'screenwriter') $group_member = 4;
                $g = 0;
                foreach ($crewList as $key => $val) {
                    if ($val['names_group'] === $parceValue && $val['group_member'] === $group_member) {
                        $g = 1; break;
                    }
                }
                if($g == 0){
                    $crewList [] = [
                        'names_group' => $parceValue,
                        'group_member' => $group_member,
                    ];
                }
            }
        }

        $loc->description = $json->shortDescription;
        $loc->cast = json_encode($actorsList);
        $loc->film_crew = json_encode($crewList);
        $loc->save();

        $loc->poster23 = 'poster23.jpg';
        $url = $json->artworkUrl100;
        $img = 'uploads/localization/'.$loc->id.'/'.$loc->poster23;

        $in = fopen($url, "rb");
        $out = fopen($img, "wb");
        while ($chunk = fread($in,8192))
        {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);

        $loc->save();
    }
}
