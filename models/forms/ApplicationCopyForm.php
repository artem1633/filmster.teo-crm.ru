<?php

namespace app\models\forms;

use app\models\FilmsApplication;
use yii\base\Model;

/**
 * Class ApplicationCopyForm
 * @package app\models\forms
 */
class ApplicationCopyForm extends Model
{
    /** @var int */
    public $applicationIdFrom;

    /** @var int */
    public $applicationIdTo;

    /** @var array */
    public $filmsPks;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['applicationIdFrom', 'applicationIdTo', 'filmsPks'], 'required'],
            [['applicationIdFrom', 'applicationIdTo'], 'integer'],
            [['filmsPks'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'applicationIdFrom' => 'Приложение',
            'applicationIdTo' => 'Приложение',
            'filmsPks' => 'Фильмы',
        ];
    }

    /**
     * @return bool
     */
    public function copy()
    {
        if($this->validate()){
            $applicationFromFilms = FilmsApplication::find()->where(['film_id' => $this->filmsPks, 'application_id' => $this->applicationIdFrom])->all();

            foreach ($applicationFromFilms as $applicationFromFilm)
            {
                $applicationToFilm = new FilmsApplication();
                $applicationToFilm->attributes = $applicationFromFilm->attributes;
                $applicationToFilm->application_id = $this->applicationIdTo;
                $applicationToFilm->save(false);
            }

            return true;
        }

        return false;
    }
}