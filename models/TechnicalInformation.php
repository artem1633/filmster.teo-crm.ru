<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "technical_information".
 *
 * @property int $id
 * @property int $film_id Фильм
 * @property string $video Формат видео
 * @property string $audio Формат аудио
 * @property int $duration Длительность (в минутах)
 * @property string $playback_speed Скорость воспроизведения
 * @property string $video_language Оригинальный язык видео
 * @property string $audio_language Оригинальный язык аудио
 * @property int $source_availability Доступность исходника
 * @property string $source_link Ссылку на исходник фильма
 * @property int $burnt_subtitles Наличие прожженных субтитров
 * @property string $burnt_value Язык прожженных субтитров
 * @property int $forced_subtitles Наличие принудительных субтитров
 * @property string $forced_value Язык принудительных субтитров
 * @property int $subtitles Наличие субтитров
 * @property string $subtitles_value Язык субтитров
 * @property int $sdh_subtitles Наличие субтитров в формате SDH
 * @property string $sdh_value Язык субтитров в формате SDH
 * @property int $presense_cc Наличие СС
 * @property string $presense_value Язык субтитров в формате СС
 * @property string $treyler Трейлер
 * @property string $forced_file Файл Наличие принудительных субтитров
 * @property string $subtitles_file Файл Наличие субтитров
 * @property string $sdh_file Файл Наличие субтитров в формате SDH
 * @property string $presense_file Файл Наличие СС
 *
 * @property Films $film
 * @property TechnicalPlaygrounds[] $technicalPlaygrounds
 * @property Treyler[] $treylers
 */
class TechnicalInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'technical_information';
    }

    public $forced_files;
    public $subtitles_files;
    public $sdh_files;
    public $presense_files;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['film_id', 'duration', 'source_availability', 'burnt_subtitles', 'forced_subtitles', 'subtitles', 'sdh_subtitles', 'presense_cc'], 'integer'],
            [['forced_file', 'subtitles_file', 'sdh_file', 'presense_file', 'treyler'], 'string'],
            [[/*'value_playground', 'additional_value',*/ 'video', 'audio', 'playback_speed', 'video_language', 'audio_language', 'source_link'], 'string', 'max' => 255],
            [['film_id'], 'exist', 'skipOnError' => true, 'targetClass' => Films::className(), 'targetAttribute' => ['film_id' => 'id']],
            /*['playground', 'validatePlayGround'],*/
            [['forced_files'], 'file', 'skipOnEmpty' => true, ], 
            [['subtitles_files'], 'file', 'skipOnEmpty' => true, ], 
            [['sdh_files'], 'file', 'skipOnEmpty' => true, ], 
            [['presense_files'], 'file', 'skipOnEmpty' => true, ], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'film_id' => 'Фильм',
            /*'playground' => 'Площадка ',
            'value_playground' => 'Значение площадки ',
            'additional_value' => 'Доп. значение площадки',*/
            'video' => 'Формат видео',
            'audio' => 'Формат аудио',
            'duration' => 'Длительность (в минутах)',
            'playback_speed' => 'Скорость воспроизведения',
            'video_language' => 'Оригинальный язык видео',
            'audio_language' => 'Оригинальный язык аудио',
            'source_availability' => 'Доступность исходника',
            'source_link' => 'Ссылку на исходник фильма',
            'burnt_subtitles' => 'Наличие прожженных субтитров',
            'burnt_value' => 'Язык прожженных субтитров',
            'forced_subtitles' => 'Наличие принудительных субтитров',
            'forced_value' => 'Язык принудительных субтитров',
            'subtitles' => 'Наличие субтитров',
            'subtitles_value' => 'Язык субтитров',
            'sdh_subtitles' => 'Наличие субтитров в формате SDH',
            'sdh_value' => 'Язык субтитров в формате SDH',
            'presense_cc' => 'Наличие СС',
            'presense_value' => 'Язык субтитров в формате СС',
            'treyler' => 'Трейлер',
            'forced_file' => 'Файл',
            'subtitles_file' => 'Файл',
            'sdh_file' => 'Файл',
            'presense_file' => 'Файл',

            'forced_files' => 'Файл',
            'subtitles_files' => 'Файл',
            'sdh_files' => 'Файл',
            'presense_files' => 'Файл',
        ];
    }

    public function beforeDelete()
    {
        $treylers = Treyler::find()->where(['technical_information_id' => $this->id])->all();
        foreach ($treylers as $value) {
            $value->delete();
        }

        $technical = TechnicalPlaygrounds::find()->where(['technical_id' => $this->id])->all();
        foreach ($technical as $value) {
            $value->delete();
        }

        Directory::deleteDirectory($this->id, 'technical-information');
        return parent::beforeDelete();
    }

    //Компания-производитель 
    /*public function validatePlayGround($attribute, $params)
    {
        $playGround = PlayGround::find()->where(['id' => $this->playground])->one();
        if (!isset($playGround))
        {                
            $playGround = new PlayGround();
            $playGround->name = $this->playground;
            $error = $playGround->errors;
            
            if ($playGround->save())
            {
                $this->playground = $playGround->id;
            }
            else
            {
                $this->addError($attribute,"Не создан площадка");
            }       
        }
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Films::className(), ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTechnicalPlaygrounds()
    {
        return $this->hasMany(TechnicalPlaygrounds::className(), ['technical_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTreylers()
    {
        return $this->hasMany(Treyler::className(), ['technical_information_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getPlayground0()
    {
        return $this->hasOne(PlayGround::className(), ['id' => 'playground']);
    }*/

    public function getPlayGroundList()
    {
        $playGround = PlayGround::find()->all();
        return ArrayHelper::map($playGround, 'id', 'name');
    }

    public function getLanguages()
    {
        $language = Languages::find()->all();
        return ArrayHelper::map($language, 'id', 'name');
    }

    public function languageDescription($id)
    {
        $language = Languages::findOne($id);
        return $language->name;
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'technical_information'")
            ->one();
        if($res) return $res["AUTO_INCREMENT"];
    }


    public function getBurnt()
    {        
        if( $this->burnt_value != null || $this->burnt_value != '' ) {
            $result = explode(',', $this->burnt_value);
            $this->burnt_value = $result;
        }
        else $this->burnt_value = null;
        $data = [];
        foreach ($result as $value) {
            $data += [
                $value => $value,
            ];
        }
        return $data;
    }

    public function getForced()
    {
        if( $this->forced_value != null || $this->forced_value != '' ) {
            $result = explode(',', $this->forced_value);
            $this->forced_value = $result;
        }
        else $this->forced_value = null;
        $data = [];
        foreach ($result as $value) {
            $data += [
                $value => $value,
            ];
        }
        return $data;
    }

    public function getSubtitles()
    {
        if( $this->subtitles_value != null || $this->subtitles_value != '' ) {
            $result = explode(',', $this->subtitles_value);
            $this->subtitles_value = $result;
        }
        else $this->subtitles_value = null;
        $data = [];
        foreach ($result as $value) {
            $data += [
                $value => $value,
            ];
        }
        return $data;
    }

    public function getSdh()
    {
        if( $this->sdh_value != null || $this->sdh_value != '' ) {
            $result = explode(',', $this->sdh_value);
            $this->sdh_value = $result;
        }
        else $this->sdh_value = null;
        $data = [];
        foreach ($result as $value) {
            $data += [
                $value => $value,
            ];
        }
        return $data;
    }

    public function getPresent()
    {
        if( $this->presense_value != null || $this->presense_value != '' ) {
            $result = explode(',', $this->presense_value);
            $this->presense_value = $result;
        }
        else $this->presense_value = null;
        $data = [];
        foreach ($result as $value) {
            $data += [
                $value => $value,
            ];
        }
        return $data;
    }

    public function burntSubtitles()
    {
        $array = explode(",", $this->burnt_value);
        $result = '<ul class="list-tags">';
        foreach ($array as $value) {
            if($value != null && $value != '' ) $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' . $value . '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

    public function forcedValue()
    {
        $array = explode(",", $this->forced_value);
        $result = '<ul class="list-tags">';
        foreach ($array as $value) {
            if($value != null && $value != '' ) $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' . $value . '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

    public function subtitlesValue()
    {
        $array = explode(",", $this->subtitles_value);
        $result = '<ul class="list-tags">';
        foreach ($array as $value) {
            if($value != null && $value != '' ) $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' . $value . '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

    public function sdhValue()
    {
        $array = explode(",", $this->sdh_value);
        $result = '<ul class="list-tags">';
        foreach ($array as $value) {
            if($value != null && $value != '' ) $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' . $value . '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

    public function presenseValue()
    {
        $array = explode(",", $this->presense_value);
        $result = '<ul class="list-tags">';
        foreach ($array as $value) {
            if($value != null && $value != '' ) $result .=  '<li><a href="#"><span class="fa fa-tag"></span> ' . $value . '</a></li>';
        }
        $result .='</ul>';
        return $result;
    }

}
