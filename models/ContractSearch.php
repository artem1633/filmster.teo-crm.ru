<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contract;
use app\models\RightHolders;

/**
 * ContractSearch represents the model behind the search form about `app\models\Contract`.
 */
class ContractSearch extends Contract
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'min_payment_currency', 'payment_period', 'payment_currency', 'company_lic', 'company_cont'], 'integer'],
            [['number', 'date_cr', 'another_contract', 'another_contract_value'], 'safe'],
            [['min_payment'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $user = Yii::$app->user->identity->id;

         $rightHolders = RightHolders::find()->where(['user_id' => $user])->all();
            $idList = [];
            foreach ($rightHolders as $value) {
                $idList [] = $value->id;
            }
        if(Yii::$app->user->identity->type == 0){
            $query = Contract::find()
                ->with(['companyCont'])
                ->with(['companyLic']);
            } 
        else {
            $query = Contract::find()
                ->with(['companyCont'])
                ->with(['companyLic'])
                ->andWhere(['or',
                   ['company_lic'=>$idList],
                   ['company_cont'=>$idList]
               ]);
            
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                    'pageSize' => 1000,
                ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_cr' => $this->date_cr,
            'min_payment' => $this->min_payment,
            'min_payment_currency' => $this->min_payment_currency,
            'payment_period' => $this->payment_period,
            'payment_currency' => $this->payment_currency,
            'company_lic' => $this->company_lic,
            'company_cont' => $this->company_cont,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'another_contract', $this->another_contract])
            ->andFilterWhere(['like', 'another_contract_value', $this->another_contract_value]);

        return $dataProvider;
    }
}
