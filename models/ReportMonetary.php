<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\base\AppActiveQuery;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "report_monetary".
 *
 * @property int $id
 * @property int $platform_id Площадка
 * @property string $file Файл
 * @property string $date Дата
 *
 * @property Platform $platform
 * @property ReportValues[] $reportValues
 */
class ReportMonetary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $files;
    public $territory;
    public $currency;
    public static function tableName()
    {
        return 'report_monetary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['platform_id', 'date'], 'required'],
            [['platform_id', 'territory', 'currency'], 'integer'],
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xlsx',], 
            [['file'], 'string', 'max' => 255],
            [['date'], 'string', 'max' => 10],
            [['platform_id'], 'exist', 'skipOnError' => true, 'targetClass' => Platform::className(), 'targetAttribute' => ['platform_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'platform_id' => 'Площадка',
            'file' => 'Файл',
            'date' => 'Дата',
            'files' => 'Файл',
            'territory' => 'Территория',
            'currency' => 'Валюта продажи',
        ];
    }

    public function beforeSave($insert)
    {
        //if($this->date !== null) $this->date = ''.strtotime('01.'.$this->date);

        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        $reportValues = ReportValues::find()->where(['report_monetary_id' => $this->id])->all();
        foreach ($reportValues as $value) {
            $value->delete();
        }
        Directory::deleteDirectory($this->id, 'report-monetary');
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['id' => 'platform_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportValues()
    {
        return $this->hasMany(ReportValues::className(), ['report_monetary_id' => 'id']);
    }

    public function getTerritoryList()
    {
        $country = Country::find()->all();
        return ArrayHelper::map($country, 'id', 'code');
    }

    public function getCurrencyList()
    {
        $currency = Currency::find()->all();
        return ArrayHelper::map($currency, 'id', 'code');
    }

    public function getPlayGroundList()
    {
        $playGround = Platform::find()->all();
        return ArrayHelper::map($playGround, 'id', 'name_ru');
    }

    //Получить последнего ид
    public function getLastId()
    {
        $q = new \yii\db\Query();
        $res = $q->select("AUTO_INCREMENT")
            ->from('INFORMATION_SCHEMA.TABLES')
            ->where("TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'report_monetary'")
            ->one();
        if($res) return $res["AUTO_INCREMENT"];
    }

    public function getAllValue()
    {
        $query = ReportValues::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 1000,
            ],
            //'sort' => ['defaultOrder'=>'ordering ASC'],
            //'sort' => ['defaultOrder' => ['ordering'=>SORT_ASC]]
        ]);

        $query->andFilterWhere(['report_monetary_id' => $this->id]);
        return $dataProvider;
    }

    //Является фильмам (проверить условия)
    public function clarification($csvData, $setting_id, $i)
    {
        $conditionsValue = [];
        $conditionsValue [] = 2;
        $conditionsValue [] = 8;
        $conditionsValue [] = 9;
        $conditions = Conditions::find()->where(['report_setting_id' => $setting_id, 'action_id' => $conditionsValue])->all();

        $clarification = 1;
        $film_id = null;
        $playgroundValue = null;
        $additionalValue = null;
        $value = null;
        $filmFind = 0;
        $valueFind = 0;
        $additionalFind = 0;
        foreach ($conditions as $condition) 
        {
            $columns = ConditionColumns::find()->where(['condition_id' => $condition->id])->all();
            //Является фильмом
            if($condition->action_id == 2)
            {
                foreach ($columns as $column) 
                {
                    $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                    //значения Заполнено
                    if($column->condition_name == 1)
                    {
                        $film = Films::find()->where(['name' => $value])->one();
                        if($film == null)
                        {
                            $localization = Localization::find()->where(['name' => $value])->one();
                            if($localization == null) $clarification = 0;
                            else { $film_id = $localization->film_id; $filmFind = 1; }
                        }
                        else { $film_id = $film->id; $filmFind = 1; }
                    }
                }
            }
            //Является Идентификатор площадки
            if($condition->action_id == 8)
            {
                foreach ($columns as $column) 
                {
                    $playgroundValue = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                    $techPlay = TechnicalPlaygrounds::find()->where(['playground_id' => $this->platform_id, 'value_playground' => $playgroundValue])->one();
                    if($techPlay == null) $clarification = 0;
                    else { $film_id = $techPlay->technical->film_id; $valueFind = 1; }
                }
            }
            //Является Дополнительный идентификатор площадки
//            if($condition->action_id == 9)
//            {
//                foreach ($columns as $column)
//                {
//                    $additionalValue = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
//                    $techPlay = TechnicalPlaygrounds::find()->where(['playground_id' => $this->platform_id, 'additional_value' => $additionalValue])->one();
//                    if($techPlay == null) $clarification = 0;
//                    else { $film_id = $techPlay->technical->film_id; $additionalFind = 1; }
//                }
//            }
        }

        if($filmFind == 1 ||  $valueFind == 1 || $additionalFind == 1) $clarification = 1;

        if($clarification == 1)
        {
            return 1;
            /*if($playgroundValue === null) return 'На "Настройке отчета" для определения значения Идентификатора площадки не дано условия';
            if($additionalValue === null) return 'На "Настройке отчета" для определения значения на Дополнительного идентификатора площадки не дано условия';

            $technicalInformation = TechnicalInformation::find()->where(['film_id' => $film_id])->all();
            $technicalID = [];
            foreach ($technicalInformation as $technical) 
            {
                $technicalID [] = $technical->id;
            }

            $techPlayground = TechnicalPlaygrounds::find()
                ->where(['technical_id' => $technicalID])
                ->andWhere(['value_playground' => $playgroundValue, 'additional_value' => $additionalValue])
                ->one();
            if($techPlayground == null) return "Нет фильма соответствуещего на значения Идентификатора площадки {$playgroundValue} и на Дополнительного идентификатора площадки {$additionalValue}";
            else return 1;*/
        }
        else
        {
            if($playgroundValue === null && $additionalValue === null)
            {
                return "Нет фильма на системе по названию " . '"' . $value . '"' . ". Нет фильма соответствуещего на значения Идентификатора площадки \"{$playgroundValue}\" и на Дополнительного идентификатора площадки \"{$additionalValue}\"";
            }

            if($playgroundValue !== null)
            {
                $techPlayground = TechnicalPlaygrounds::find()
                    ->where(['value_playground' => $playgroundValue])
                    ->one();
                if($techPlayground !== null) return 1;
            }
            else 
            {
                if($additionalValue !== null)
                {
                    $techPlayground = TechnicalPlaygrounds::find()
                        ->where(['additional_value' => $additionalValue])
                        ->one();
                    if($techPlayground !== null) return 1; 
                    
                }
            }
            return "Нет фильма на системе по названию " . '"' . $value . '"' . ". Нет фильма соответствуещего на значения Идентификатора площадки \"{$playgroundValue}\" и на Дополнительного идентификатора площадки \"{$additionalValue}\"";
        }

    }

    public function getValuesFromExcell($fileName)
    {
        $filePath = 'uploads/report-monetary/' . $this->id . '/' . $fileName;
        $setting = ReportSetting::find()->where(['platform_id' => $this->platform_id])->one();

        if ($setting != null) 
        {
            $csvData = \PHPExcel_IOFactory::load($filePath);
            $resultAnswer = null;
            $totalCount = count($csvData->getActiveSheet()->toArray(null, true, true, false));
            
            for ($i = $setting->begin_parse; $i <= $totalCount - $setting->end_parse ; $i++) 
            { 
                $conditions = Conditions::find()->where(['report_setting_id' => $setting->id])->all();

                $clarification = $this->clarification($csvData, $setting->id, $i);
                if($clarification == 1)
                {
                    $report = new ReportValues();
                    $report->report_monetary_id = $this->id;

                    foreach ($conditions as $condition) 
                    {
                        $columns = ConditionColumns::find()->where(['condition_id' => $condition->id])->all();
                        //Является количеством продаж
                        if($condition->action_id == 1)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения количеством продаж
                            if($proverka == 1) $report->sale_count = $resultValue;
                        }

                        //Является фильмом
                        if($condition->action_id == 2)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Является фильмом
                            if($proverka == 1) 
                            {
                                $film = Films::find()->where(['name' => $resultValue])->one();
                                if($film != null) $report->film_id = $film->id;
                                else
                                {
                                    $localization = Localization::find()->where(['name' => $resultValue])->one();
                                    if($localization == null) $report->film_name = $resultValue;
                                    else $report->film_id = $localization->film_id;                                    
                                }
                            }
                        }

                        //Является моделью продаж
                        if($condition->action_id == 3)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            $sale = null;
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                if($column->condition_name == 3)
                                {
                                    /*if($value == 'D') $sale = 1;
                                    if($value == 'M') $sale = 2;*/
                                    if($value != $column->value) $proverka = 0;
                                }
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    /*if($value === null) $proverka = 0;
                                    else*/ $resultValue = (string)$value;
                                }
                                //значения Пусто
                                /*if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = $value;
                                }*/
                            }
                            //сопоставления значения количеством продаж
                            if($proverka == 1) 
                            {
                                /*if($sale !== null)
                                {
                                    if($sale == 1) $report->est = $resultValue;
                                    if($sale == 2) $report->vod = $resultValue;
                                }
                                else
                                {*/
                                    if($condition->sale_id == 1) $report->est = $resultValue;
                                    if($condition->sale_id == 2) $report->vod = $resultValue;
                                    if($condition->sale_id == 3) $report->pvod = $resultValue;
                                    if($condition->sale_id == 4) $report->avod = $resultValue;
                                    if($condition->sale_id == 5) $report->svod = $resultValue;                                    
                                //}
                            }
                        }

                        //Является Дата начала отчетного периода
                        if($condition->action_id == 4)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Дата начала отчетного периода
                            if($proverka == 1) 
                            {
                                $report->date_begin =  date('Y-m-d', strtotime($resultValue));
                                $report->date_begin_value = $resultValue;
                            }
                        }

                        //Является Дата окончания отчетного периода
                        if($condition->action_id == 5)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Дата окончания отчетного периода
                            if($proverka == 1) 
                            {
                                $report->date_end =  date('Y-m-d', strtotime($resultValue));
                                $report->date_end_value = $resultValue;
                            }
                        }

                        //Является Валюта продаж
                        if($condition->action_id == 6)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Валюта продаж
                            if($proverka == 1) 
                            {
                                $currency = Currency::find()->where(['code' => $resultValue])->one();
                                if($currency != null) $report->currency = $currency->id;
                            }
                        }

                        //Является Территория
                        if($condition->action_id == 7)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Территория
                            if($proverka == 1) 
                            {
                                $country = Country::find()->where(['code' => $resultValue])->one();
                                if($country != null) $report->territory = $country->id;
                            }
                        }

                        //Является Идентификатор площадки
                        if($condition->action_id == 8)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Идентификатор площадки
                            if($proverka == 1) 
                            {
                                $report->identificator = $resultValue;
                                if($report->film_id === null)
                                {
                                    $techPlayground = TechnicalPlaygrounds::find()
                                        ->where(['value_playground' => $resultValue])
                                        ->one();
                                    $report->film_id = $techPlayground->technical->film_id;
                                }
                            }
                        }

                        //Является Дополнительный идентификатор площадки
                        if($condition->action_id == 9)
                        {
                            $proverka = 1;
                            $resultValue = '';
                            foreach ($columns as $column) 
                            {
                                //значения Содержит
                                $value = $csvData->getActiveSheet()->getCell( $column->column_name . $i )->getValue();
                                /*if($column->condition_name == 3)
                                {
                                    if($value != $column->value) $proverka = 0;
                                }*/
                                //значения Заполнено
                                if($column->condition_name == 1)
                                {
                                    if($value === null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                                //значения Пусто
                                if($column->condition_name == 2)
                                {
                                    if($value !== null) $proverka = 0;
                                    else $resultValue = (string)$value;
                                }
                            }
                            //сопоставления значения Дополнительный идентификатор площадки
                            if($proverka == 1) 
                            {
                                $report->additional_identificator = $resultValue;
                                if($report->film_id === null)
                                {
                                    $techPlayground = TechnicalPlaygrounds::find()
                                        ->where(['additional_value' => $resultValue])
                                        ->one();
                                    $report->film_id = $techPlayground->technical->film_id;
                                }
                            }
                        }
                    }

                    if($report->territory == null) $report->territory = $this->territory;
                    if($report->currency == null) $report->currency = $this->currency;

                    $oldReports = ReportValues::find()
                    ->where([
                        'report_monetary_id' => $this->id,
                        'film_id' => $report->film_id,
                        'date_begin' => $report->date_begin,
                        'date_end' => $report->date_end,
                        'currency' => $report->currency,
                        'territory' => $report->territory,
                    ])
                    ->all();
                    $find = 0;
                    foreach ($oldReports as $oldReport) 
                    {
                        if($report->est !== null && $oldReport->est !== null)
                        {
                            Yii::$app->db->createCommand()->update('report_values',['est'=>$oldReport->est + $report->est, 'sale_count'=>$oldReport->sale_count + $report->sale_count],['id'=>$oldReport->id])->execute();
                            $find = 1;
                        }
                        if($report->vod !== null && $oldReport->vod !== null)
                        {
                            Yii::$app->db->createCommand()->update('report_values',['vod'=>$oldReport->vod + $report->vod, 'sale_count'=>$oldReport->sale_count + $report->sale_count],['id'=>$oldReport->id])->execute();
                            $find = 1;
                        }
                        if($report->svod !== null && $oldReport->svod !== null)
                        {
                            Yii::$app->db->createCommand()->update('report_values',['svod'=>$oldReport->svod + $report->svod, 'sale_count'=>$oldReport->sale_count + $report->sale_count],['id'=>$oldReport->id])->execute();
                            $find = 1;
                        }
                        if($report->pvod !== null && $oldReport->pvod !== null)
                        {
                            Yii::$app->db->createCommand()->update('report_values',['pvod'=>$oldReport->pvod + $report->pvod, 'sale_count'=>$oldReport->sale_count + $report->sale_count],['id'=>$oldReport->id])->execute();
                            $find = 1;
                        }
                        if($report->avod !== null && $oldReport->avod !== null)
                        {
                            Yii::$app->db->createCommand()->update('report_values',['avod'=>$oldReport->avod + $report->avod, 'sale_count'=>$oldReport->sale_count + $report->sale_count],['id'=>$oldReport->id])->execute();
                            $find = 1;
                        }
                    }
                    if($find === 0) $report->save();
                }
                else
                {
                    $resultAnswer .= "<tr><td>{$i}-строка</td><td>{$clarification}</td></tr>";
                }
            }

            if($resultAnswer == null) return '<span style="color:blue;font-weight:bold;font-size:14px;"><center>Данные успешно загружено из exсel файла</span>';
            else return '<table class="table table-bordered table-striped table-condensed"><thead><tr><th>Строка</th><th>Ошибка</th></tr></thead><tbody>'.$resultAnswer.'</tbody></table>';
        } 
        else 
        {
            return '<span style="color:red;font-weight:bold;font-size:14px;"><center>Со стороны пользователя exсel файл не загружено</center></span>';
        }

        
    }

    function toNumber($dest)
    {
        if ($dest)
            return ord(strtolower($dest)) - 96;
        else
            return 0;
    }

    function myFunction($s,$x,$y){
        $x = toNumber($x);
        return $s->getCellByColumnAndRow($x, $y)->getFormattedValue();
    }

    public function setCurrency()
    {
        $reports = ReportValues::find()->where(['report_monetary_id' => $this->id])->all();
        $currencyID = [];
        foreach ($reports as $report) {
            $currencyID [] = $report->currency;
        }
        $currencyID = array_unique($currencyID);

        foreach ($reports as $report) {
            foreach ($currencyID as $currency_id) {
                $settingCourse = new SettingCourses();
                $settingCourse->currency_id = $currency_id;
                //$settingCourse->report_values_id = $report->id;
                $settingCourse->save();
            }
        }
    }
}
 