<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Platform;

/**
 * PlatformSearch represents the model behind the search form about `app\models\Platform`.
 */
class PlatformSearch extends Platform
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'video', 'audio', 'subtitr'], 'integer'],
            [['name_ru', 'name_eng', 'logo', 'description_ru', 'description_eng', 'sales_model', 'countries'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Platform::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'video' => $this->video,
            'audio' => $this->audio,
            'subtitr' => $this->subtitr,
        ]);

        $query->andFilterWhere(['like', 'name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'name_eng', $this->name_eng])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'description_ru', $this->description_ru])
            ->andFilterWhere(['like', 'description_eng', $this->description_eng])
            ->andFilterWhere(['like', 'sales_model', $this->sales_model])
            ->andFilterWhere(['like', 'countries', $this->countries]);

        return $dataProvider;
    }
}
