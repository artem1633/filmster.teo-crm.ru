<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class Bulk extends Widget{

	public $buttons;
	
	public function init(){
		parent::init();
		
	}
	
	public function run(){
		$content = '<div class="pull-left">'.
                   '&nbsp;'.
                   $this->buttons.
                   '</div>';
		return $content;
	}
}
?>
