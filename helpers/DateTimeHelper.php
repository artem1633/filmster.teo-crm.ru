<?php

namespace app\helpers;

/**
 * Class DateTimeHelper
 * @package app\helpers
 */
class DateTimeHelper
{
    public static function getDates($startTime, $endTime) {
        $day = 86400;
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        //$numDays = round(($endTime - $startTime) / $day) + 1;
        $numDays = round(($endTime - $startTime) / $day); // без +1

        $days = [date('Y-m-d',$startTime)];

        for ($i = 1; $i < $numDays; $i++) {
            $days[] = date('Y-m-d', ($startTime + ($i * $day)));
        }

        $days[] = date('Y-m-d', $endTime);

//        VarDumper::dump($days, 10, true);
//        exit;

        return $days;
    }
}