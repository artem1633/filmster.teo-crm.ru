<?php

use yii\db\Migration;

/**
 * Handles adding contract_date to table `contract`.
 */
class m181227_055358_add_contract_date_column_to_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contract', 'contract_date', $this->date()->comment('Дата договора'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contract', 'contract_date');
    }
}
