<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technical_playgrounds`.
 */
class m190110_052449_create_technical_playgrounds_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('technical_playgrounds', [
            'id' => $this->primaryKey(),
            'playground_id' => $this->integer()->comment('Площадка'),
            'technical_id' => $this->integer()->comment('Техническая информация'),
            'value_playground' => $this->string(255)->comment('Значение площадки'),
            'additional_value' => $this->string(255)->comment('Доп. значение площадки'),
        ]);

        $this->createIndex('idx-technical_playgrounds-playground_id', 'technical_playgrounds', 'playground_id', false);
        $this->addForeignKey("fk-technical_playgrounds-playground_id", "technical_playgrounds", "playground_id", "play_ground", "id");

        $this->createIndex('idx-technical_playgrounds-technical_id', 'technical_playgrounds', 'technical_id', false);
        $this->addForeignKey("fk-technical_playgrounds-technical_id", "technical_playgrounds", "technical_id", "technical_information", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-technical_playgrounds-playground_id','technical_playgrounds');
        $this->dropIndex('idx-technical_playgrounds-playground_id','technical_playgrounds');

        $this->dropForeignKey('fk-technical_playgrounds-technical_id','technical_playgrounds');
        $this->dropIndex('idx-technical_playgrounds-technical_id','technical_playgrounds');

        $this->dropTable('technical_playgrounds');
    }
}
