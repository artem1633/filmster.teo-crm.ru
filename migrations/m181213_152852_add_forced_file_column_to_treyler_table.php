<?php

use yii\db\Migration;

/**
 * Handles adding forced_file to table `treyler`.
 */
class m181213_152852_add_forced_file_column_to_treyler_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('treyler', 'forced_file', $this->string(255)->comment('Файл Наличие принудительных субтитров'));
        $this->addColumn('treyler', 'subtitles_file', $this->string(255)->comment('Файл Наличие субтитров'));
        $this->addColumn('treyler', 'sdh_file', $this->string(255)->comment('Файл Наличие субтитров в формате SDH'));
        $this->addColumn('treyler', 'presense_file', $this->string(255)->comment('Файл Наличие СС'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('treyler', 'forced_file');
        $this->dropColumn('treyler', 'subtitles_file');
        $this->dropColumn('treyler', 'sdh_file');
        $this->dropColumn('treyler', 'presense_file');
    }
}
