<?php

use yii\db\Migration;

/**
 * Handles the creation of table `setting_courses`.
 */
class m190227_103520_create_setting_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('setting_courses', [
            'id' => $this->primaryKey(),
            'currency_id' => $this->integer()->comment('Валюта'),
            'report_values_id' => $this->integer()->comment('Значения отчета'),
            'course' => $this->float()->comment('Курс'),
            'value' => $this->float()->comment('Значения'),
        ]);

        $this->createIndex('idx-setting_courses-currency_id', 'setting_courses', 'currency_id', false);
        $this->addForeignKey("fk-setting_courses-currency_id", "setting_courses", "currency_id", "currency", "id");

        $this->createIndex('idx-setting_courses-report_values_id', 'setting_courses', 'report_values_id', false);
        $this->addForeignKey("fk-setting_courses-report_values_id", "setting_courses", "report_values_id", "report_values", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-setting_courses-currency_id','setting_courses');
        $this->dropIndex('idx-setting_courses-currency_id','setting_courses');

        $this->dropForeignKey('fk-setting_courses-report_values_id','setting_courses');
        $this->dropIndex('idx-setting_courses-report_values_id','setting_courses');

        $this->dropTable('setting_courses');
    }
}
