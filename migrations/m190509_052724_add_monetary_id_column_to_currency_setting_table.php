<?php

use yii\db\Migration;

/**
 * Handles adding monetary_id to table `{{%currency_setting}}`.
 */
class m190509_052724_add_monetary_id_column_to_currency_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency_setting}}', 'monetary_id', $this->integer()->comment('Monetary ID'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%currency_setting}}', 'monetary_id');
    }
}
