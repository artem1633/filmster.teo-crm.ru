<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rights`.
 */
class m181204_052247_create_rights_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rights', [
            'id' => $this->primaryKey(),
            'film_id' => $this->integer()->comment('Фильм'),
            'number' => $this->integer()->comment('Номер догвора'),
            'date' => $this->date()->comment('Дата заключения договора'),
            'application_number' => $this->float()->comment('Номер приложения'),
            'begin_date' => $this->date()->comment('Дата от'),
            'end_date' => $this->date()->comment('Дата до'),
            'min_warranty' => $this->float()->comment('Минимальная гарантия'),
            'adaptation' => $this->float()->comment('Техническая адаптация'),
            'maintenance' => $this->float()->comment('Maintenance free'),
            'teritory' => $this->text()->comment('Территория'),
            'rolyati' => $this->string(255)->comment('Роялти'),
            'area' => $this->text()->comment('Площадки'),
            'price' => $this->float()->comment('Цена'),
            'model_begin' => $this->date()->comment('Дата открытия'),
            'model_end' => $this->date()->comment('Дата закрытия'),
            'actual' => $this->boolean()->comment('Активность'),
            'licensee_name' => $this->string(255)->comment('Наименование лицензиата'),
            'licensor_name' => $this->string(255)->comment('Наименование лицензиара'),
            'territory_rights' => $this->string(255)->comment('Территория прав'),
        ]);

        $this->createIndex('idx-rights-film_id', 'rights', 'film_id', false);
        $this->addForeignKey("fk-rights-film_id", "rights", "film_id", "films", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-rights-film_id','rights');
        $this->dropIndex('idx-rights-film_id','rights');
        
        $this->dropTable('rights');
    }
}
