<?php

use yii\db\Migration;

/**
 * Handles adding create_date to table `{{%films}}`.
 */
class m190404_172022_add_create_date_column_to_films_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%films}}', 'create_date', $this->date()->comment('Дата созданя'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%films}}', 'create_date');
    }
}
