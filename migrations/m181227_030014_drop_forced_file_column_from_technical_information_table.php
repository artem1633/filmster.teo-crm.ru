<?php

use yii\db\Migration;

/**
 * Handles dropping forced_file from table `technical_information`.
 */
class m181227_030014_drop_forced_file_column_from_technical_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('technical_information', 'forced_file');
        $this->dropColumn('technical_information', 'subtitles_file');
        $this->dropColumn('technical_information', 'sdh_file');
        $this->dropColumn('technical_information', 'presense_file');

        $this->addColumn('technical_information', 'forced_file', $this->text()->comment('Файл Наличие принудительных субтитров'));
        $this->addColumn('technical_information', 'subtitles_file', $this->text()->comment('Файл Наличие субтитров'));
        $this->addColumn('technical_information', 'sdh_file', $this->text()->comment('Файл Наличие субтитров в формате SDH'));
        $this->addColumn('technical_information', 'presense_file', $this->text()->comment('Файл Наличие СС'));

        $this->dropColumn('treyler', 'forced_file');
        $this->dropColumn('treyler', 'subtitles_file');
        $this->dropColumn('treyler', 'sdh_file');
        $this->dropColumn('treyler', 'presense_file');

        $this->addColumn('treyler', 'forced_file', $this->text()->comment('Файл Наличие принудительных субтитров'));
        $this->addColumn('treyler', 'subtitles_file', $this->text()->comment('Файл Наличие субтитров'));
        $this->addColumn('treyler', 'sdh_file', $this->text()->comment('Файл Наличие субтитров в формате SDH'));
        $this->addColumn('treyler', 'presense_file', $this->text()->comment('Файл Наличие СС'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('technical_information', 'forced_file');
        $this->dropColumn('technical_information', 'subtitles_file');
        $this->dropColumn('technical_information', 'sdh_file');
        $this->dropColumn('technical_information', 'presense_file');
        
        $this->addColumn('technical_information', 'forced_file', $this->string(255));
        $this->addColumn('technical_information', 'subtitles_file', $this->string(255));
        $this->addColumn('technical_information', 'sdh_file', $this->string(255));
        $this->addColumn('technical_information', 'presense_file', $this->string(255));

        $this->dropColumn('treyler', 'forced_file');
        $this->dropColumn('treyler', 'subtitles_file');
        $this->dropColumn('treyler', 'sdh_file');
        $this->dropColumn('treyler', 'presense_file');
        
        $this->addColumn('treyler', 'forced_file', $this->string(255));
        $this->addColumn('treyler', 'subtitles_file', $this->string(255));
        $this->addColumn('treyler', 'sdh_file', $this->string(255));
        $this->addColumn('treyler', 'presense_file', $this->string(255));
    }
}
