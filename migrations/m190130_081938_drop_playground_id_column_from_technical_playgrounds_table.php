<?php

use yii\db\Migration;

/**
 * Handles dropping playground_id from table `technical_playgrounds`.
 */
class m190130_081938_drop_playground_id_column_from_technical_playgrounds_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-technical_playgrounds-playground_id','technical_playgrounds');
        $this->dropIndex('idx-technical_playgrounds-playground_id','technical_playgrounds');

        $this->dropTable('play_ground');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('play_ground', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'value' => $this->string(255)->comment('Значения площадки'),
            'additional_value' => $this->string(255)->comment('Доп. значение площадки'),
        ]);

        $this->createIndex('idx-technical_playgrounds-playground_id', 'technical_playgrounds', 'playground_id', false);
        $this->addForeignKey("fk-technical_playgrounds-playground_id", "technical_playgrounds", "playground_id", "play_ground", "id");
    }
}
