<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `manufacturer`.
 */
class m181213_093305_drop_manufacturer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-films-manufacturer_id','films');
        $this->dropIndex('idx-films-manufacturer_id','films');

        $this->dropTable('manufacturer');

        $this->dropColumn('films', 'manufacturer_id');
        $this->addColumn('films', 'manufacturer_id', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('films', 'manufacturer_id');
        $this->addColumn('films', 'manufacturer_id', $this->integer());

        $this->createTable('manufacturer', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
        ]);

        $this->createIndex('idx-films-manufacturer_id', 'films', 'manufacturer_id', false);
        $this->addForeignKey("fk-films-manufacturer_id", "films", "manufacturer_id", "manufacturer", "id");
    }
}
