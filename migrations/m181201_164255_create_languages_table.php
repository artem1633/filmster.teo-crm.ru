<?php

use yii\db\Migration;

/**
 * Handles the creation of table `languages`.
 */
class m181201_164255_create_languages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('languages', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('languages');
    }
}
