<?php

use yii\db\Migration;

/**
 * Class m190220_135046_add_foreign_key_to_right_holders_table
 */
class m190220_135046_add_foreign_key_to_right_holders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->addColumn('contract', 'company_lic', $this->integer()->comment('Лицензиат'));
        $this->addColumn('contract', 'company_cont', $this->integer()->comment('Лицензиар'));

        $this->createIndex('idx-contract-company_lic', 'contract', 'company_lic', false);
        $this->addForeignKey("fk-contract-company_lic", "contract", "company_lic", "right_holders", "id");

        $this->createIndex('idx-contract-company_cont', 'contract', 'company_cont', false);
        $this->addForeignKey("fk-contract-company_cont", "contract", "company_cont", "right_holders", "id");
    }

    public function down()
    {
        $this->dropForeignKey('fk-contract-company_lic','contract');
        $this->dropIndex('idx-contract-company_lic','contract');

        $this->dropForeignKey('fk-contract-company_cont','contract');
        $this->dropIndex('idx-contract-company_cont','contract');

        $this->dropColumn('contract', 'company_lic');
        $this->dropColumn('contract', 'company_cont');
    }
}
