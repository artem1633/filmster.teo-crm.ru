<?php

use yii\db\Migration;

/**
 * Handles adding date to table `{{%currency_setting}}`.
 */
class m190507_054538_add_date_column_to_currency_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%currency_setting}}', 'date', $this->string(255)->comment('Дата'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%currency_setting}}', 'date');
    }
}
