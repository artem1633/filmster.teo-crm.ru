<?php

use yii\db\Migration;

/**
 * Handles dropping playground from table `technical_information`.
 */
class m190110_051556_drop_playground_column_from_technical_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-technical_information-playground','technical_information');
        $this->dropIndex('idx-technical_information-playground','technical_information');

        $this->dropColumn('technical_information', 'playground');
        $this->dropColumn('technical_information', 'value_playground');
        $this->dropColumn('technical_information', 'additional_value');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('technical_information', 'playground', $this->integer()->comment('Площадка'));
        $this->addColumn('technical_information', 'value_playground', $this->string(255)->comment('Значение площадки'));
        $this->addColumn('technical_information', 'additional_value', $this->string(255)->comment('Доп. значение площадки'));

        $this->createIndex('idx-technical_information-playground', 'technical_information', 'playground', false);
        $this->addForeignKey("fk-technical_information-playground", "technical_information", "playground", "play_ground", "id");
    }
}
