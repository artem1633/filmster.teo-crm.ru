<?php

use yii\db\Migration;
use app\models\TechnicalPlaygrounds;

/**
 * Handles the creation of table `foreign_key_for_technical_playground`.
 */
class m190130_084646_create_foreign_key_for_technical_playground_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tech = TechnicalPlaygrounds::find()->all();
        foreach ($tech as $value) {
            $value->delete();
        }
        $this->createIndex('idx-technical_playgrounds-playground_id', 'technical_playgrounds', 'playground_id', false);
        $this->addForeignKey("fk-technical_playgrounds-playground_id", "technical_playgrounds", "playground_id", "platform", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-technical_playgrounds-playground_id','technical_playgrounds');
        $this->dropIndex('idx-technical_playgrounds-playground_id','technical_playgrounds');
    }
}
