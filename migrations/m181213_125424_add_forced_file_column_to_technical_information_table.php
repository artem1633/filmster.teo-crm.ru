<?php

use yii\db\Migration;

/**
 * Handles adding forced_file to table `technical_information`.
 */
class m181213_125424_add_forced_file_column_to_technical_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('technical_information', 'forced_file', $this->string(255)->comment('Файл Наличие принудительных субтитров'));
        $this->addColumn('technical_information', 'subtitles_file', $this->string(255)->comment('Файл Наличие субтитров'));
        $this->addColumn('technical_information', 'sdh_file', $this->string(255)->comment('Файл Наличие субтитров в формате SDH'));
        $this->addColumn('technical_information', 'presense_file', $this->string(255)->comment('Файл Наличие СС'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('technical_information', 'forced_file');
        $this->dropColumn('technical_information', 'subtitles_file');
        $this->dropColumn('technical_information', 'sdh_file');
        $this->dropColumn('technical_information', 'presense_file');
    }
}
