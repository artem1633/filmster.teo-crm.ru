<?php

use yii\db\Migration;

/**
 * Handles the creation of table `applications`.
 */
class m190102_152425_create_applications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('applications', [
            'id' => $this->primaryKey(),
            'contract_id' => $this->integer()->comment('Договор'),
            'number' => $this->string(255)->comment('№ приложения'),
            'date_cr' => $this->date()->comment('Дата создания приложения'),
            'uniform_condition' => $this->boolean()->comment('У фильмов в приложении единые условия по финансам'),
            'min_warranty' => $this->float()->comment('Мин.гарантия'),
            'min_warranty_currency' => $this->integer()->comment('Валюта выплаты'),
            'tex_adap' => $this->float()->comment('Тех.адаптация'),
            'text_adap_currency' => $this->integer()->comment('Валюта выплаты'),
            'maintenance' => $this->float()->comment('Maintenance fee'),
            'maintenance_currency' => $this->integer()->comment('Валюта выплаты'),
        ]);

        $this->createIndex('idx-applications-contract_id', 'applications', 'contract_id', false);
        $this->addForeignKey("fk-applications-contract_id", "applications", "contract_id", "contract", "id");

        $this->createIndex('idx-applications-min_warranty_currency', 'applications', 'min_warranty_currency', false);
        $this->addForeignKey("fk-applications-min_warranty_currency", "applications", "min_warranty_currency", "currency", "id");

        $this->createIndex('idx-applications-text_adap_currency', 'applications', 'text_adap_currency', false);
        $this->addForeignKey("fk-applications-text_adap_currency", "applications", "text_adap_currency", "currency", "id");

        $this->createIndex('idx-applications-maintenance_currency', 'applications', 'maintenance_currency', false);
        $this->addForeignKey("fk-applications-maintenance_currency", "applications", "maintenance_currency", "currency", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-applications-contract_id','applications');
        $this->dropIndex('idx-applications-contract_id','applications');

        $this->dropForeignKey('fk-applications-min_warranty_currency','applications');
        $this->dropIndex('idx-applications-min_warranty_currency','applications');

        $this->dropForeignKey('fk-applications-text_adap_currency','applications');
        $this->dropIndex('idx-applications-text_adap_currency','applications');

        $this->dropForeignKey('fk-applications-maintenance_currency','applications');
        $this->dropIndex('idx-applications-maintenance_currency','applications');

        $this->dropTable('applications');
    }
}
