<?php

use yii\db\Migration;

/**
 * Class m190410_053320_add_localization_id_column_users_table
 */
class m190410_053320_add_localization_id_column_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users}}', 'localization_id', $this->integer()->comment('Локализация'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         $this->dropColumn('{{%users}}', 'localization_id');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_053320_add_localization_id_column_users_table cannot be reverted.\n";

        return false;
    }
    */
}
