<?php

use yii\db\Migration;

/**
 * Handles the creation of table `actuality`.
 */
class m181203_120324_create_actuality_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('actuality', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
        ]);

        $this->insert('actuality',array(
            'name' => 'Library ',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('actuality');
    }
}
