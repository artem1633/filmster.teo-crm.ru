<?php

use yii\db\Migration;

/**
 * Handles adding value to table `play_ground`.
 */
class m181226_062709_add_value_column_to_play_ground_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('play_ground', 'value', $this->string(255)->comment('Значение площадки'));
        $this->addColumn('play_ground', 'additional_value', $this->string(255)->comment('Доп. значение площадки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('play_ground', 'value');
        $this->dropColumn('play_ground', 'additional_value');
    }
}
