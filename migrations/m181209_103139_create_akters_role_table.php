<?php

use yii\db\Migration;

/**
 * Handles the creation of table `akters_role`.
 */
class m181209_103139_create_akters_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('akters_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);

        $this->insert('akters_role',array(
            'name' => 'Монтаж',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('akters_role');
    }
}
