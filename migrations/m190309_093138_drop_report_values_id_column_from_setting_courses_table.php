<?php

use yii\db\Migration;

/**
 * Handles dropping report_values_id from table `setting_courses`.
 */
class m190309_093138_drop_report_values_id_column_from_setting_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-setting_courses-report_values_id','setting_courses');
        $this->dropIndex('idx-setting_courses-report_values_id','setting_courses');

        $this->dropColumn('setting_courses', 'report_values_id');

        $this->addColumn('setting_courses', 'contract_id', $this->integer()->comment('Контракт'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('setting_courses', 'report_values_id', $this->integer());

        $this->createIndex('idx-setting_courses-report_values_id', 'setting_courses', 'report_values_id', false);
        $this->addForeignKey("fk-setting_courses-report_values_id", "setting_courses", "report_values_id", "report_values", "id");

        $this->dropColumn('setting_courses', 'contract_id');
    }
}
