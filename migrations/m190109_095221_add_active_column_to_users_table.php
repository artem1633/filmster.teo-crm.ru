<?php

use yii\db\Migration;

/**
 * Handles adding active to table `users`.
 */
class m190109_095221_add_active_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'active', $this->boolean()->comment('Активен'));
        $this->addColumn('users', 'site', $this->string(255)->comment('Веб-сайт'));
        $this->addColumn('users', 'mail', $this->string(255)->comment('E-mail'));
        $this->addColumn('users', 'description', $this->text()->comment('Краткое описание'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'active');
        $this->dropColumn('users', 'site');
        $this->dropColumn('users', 'mail');
        $this->dropColumn('users', 'description');
    }
}
