<?php

use yii\db\Migration;

/**
 * Handles the creation of table `condition_columns`.
 */
class m190121_070403_create_condition_columns_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('condition_columns', [
            'id' => $this->primaryKey(),
            'condition_id' => $this->integer()->comment('Условия'),
            'column_name' => $this->string(255)->comment('Название столбца'),
            'condition_name' => $this->integer()->comment('Условие'),
            'value' => $this->string(255)->comment('Значение'),
            'additional' => $this->integer()->comment('Настройка между столбцами'),
        ]);

        $this->createIndex('idx-condition_columns-condition_id', 'condition_columns', 'condition_id', false);
        $this->addForeignKey("fk-condition_columns-condition_id", "condition_columns", "condition_id", "conditions", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-condition_columns-condition_id','condition_columns');
        $this->dropIndex('idx-condition_columns-condition_id','condition_columns');

        $this->dropTable('condition_columns');
    }
}
