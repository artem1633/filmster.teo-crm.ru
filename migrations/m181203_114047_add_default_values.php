<?php

use yii\db\Migration;

/**
 * Class m181203_114047_add_default_values
 */
class m181203_114047_add_default_values extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->insert('films_type',array(
            'name' => 'Сериал',
            'key' => 'serial',
        ));

        $this->insert('films_type',array(
            'name' => 'Фильм',
            'key' => 'film',
        ));

        $this->insert('films_type',array(
            'name' => 'Мультфильм ',
            'key' => 'multfilm',
        ));






        $this->insert('languages',array(
            'name' => 'Английский',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (Автсралия)',
        ));

        $this->insert('languages',array(
            'name' => 'Английский  (Канада)',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (Франция)',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (Германия)',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (Ирландия)',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (Новая Зеландия)',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (Великобритания',
        ));

        $this->insert('languages',array(
            'name' => 'Английский (США)',
        ));

        $this->insert('languages',array(
            'name' => 'Арабский',
        ));

        $this->insert('languages',array(
            'name' => 'Африканский ',
        ));

        $this->insert('languages',array(
            'name' => 'Бенгальский',
        ));

        $this->insert('languages',array(
            'name' => 'Болгарский',
        ));

        $this->insert('languages',array(
            'name' => 'Вьетнамский',
        ));

        $this->insert('languages',array(
            'name' => 'Венгерский',
        ));

        $this->insert('languages',array(
            'name' => 'Греческий',
        ));

        $this->insert('languages',array(
            'name' => 'Греческий (Кипр)',
        ));

        $this->insert('languages',array(
            'name' => 'Датский',
        ));

        $this->insert('languages',array(
            'name' => 'Зулу',
        ));

        $this->insert('languages',array(
            'name' => 'Иврит',
        ));

        $this->insert('languages',array(
            'name' => 'Исландский',
        ));

        $this->insert('languages',array(
            'name' => 'Индонезийский',
        ));

        $this->insert('languages',array(
            'name' => 'Итальянский',
        ));

        $this->insert('languages',array(
            'name' => 'Испанский',
        ));

        $this->insert('languages',array(
            'name' => 'Испанский (Латинская Америка)',
        ));

        $this->insert('languages',array(
            'name' => 'Испанский (Мексика)',
        ));

        $this->insert('languages',array(
            'name' => 'Испанский (Испания)',
        ));

        $this->insert('languages',array(
            'name' => 'Кантонский (Разговорный)',
        ));

        $this->insert('languages',array(
            'name' => 'Кантонский (Писменный)',
        ));

        $this->insert('languages',array(
            'name' => 'Канада',
        ));

        $this->insert('languages',array(
            'name' => 'Казахский',
        ));

        $this->insert('languages',array(
            'name' => 'Корейский',
        ));

        $this->insert('languages',array(
            'name' => 'Китайсткий (Упрощенный)',
        ));

        $this->insert('languages',array(
            'name' => 'Китайсткий (Газговорный)',
        ));

        $this->insert('languages',array(
            'name' => 'Китайсткий (Традиционный)',
        ));

        $this->insert('languages',array(
            'name' => 'Лаосский',
        ));

        $this->insert('languages',array(
            'name' => 'Латышский',
        ));

        $this->insert('languages',array(
            'name' => 'Литовский',
        ));

        $this->insert('languages',array(
            'name' => 'Люксембургский',
        ));

        $this->insert('languages',array(
            'name' => 'Малайский',
        ));

        $this->insert('languages',array(
            'name' => 'Малаялам',
        ));

        $this->insert('languages',array(
            'name' => 'Мальтийский',
        ));

        $this->insert('languages',array(
            'name' => 'Маратхи',
        ));

        $this->insert('languages',array(
            'name' => 'Норвежский',
        ));

        $this->insert('languages',array(
            'name' => 'Нидерландский (Голландский)',
        ));

        $this->insert('languages',array(
            'name' => 'Немецкий',
        ));

        $this->insert('languages',array(
            'name' => 'Немецкий (Австрия)',
        ));

        $this->insert('languages',array(
            'name' => 'Немецкий (Германия)',
        ));

        $this->insert('languages',array(
            'name' => 'Немецкий (Швейцария)',
        ));

        $this->insert('languages',array(
            'name' => 'Польский',
        ));

        $this->insert('languages',array(
            'name' => 'Португальский',
        ));

        $this->insert('languages',array(
            'name' => 'Португальский (Бразилия)',
        ));

        $this->insert('languages',array(
            'name' => 'Португальский (Португалия)',
        ));

        $this->insert('languages',array(
            'name' => 'Пенджабский',
        ));

        $this->insert('languages',array(
            'name' => 'Румынский',
        ));

        $this->insert('languages',array(
            'name' => 'Русский',
        ));

        $this->insert('languages',array(
            'name' => 'Словацкий',
        ));

        $this->insert('languages',array(
            'name' => 'Словенский',
        ));

        $this->insert('languages',array(
            'name' => 'Тагальский',
        ));

        $this->insert('languages',array(
            'name' => 'Тамильский',
        ));

        $this->insert('languages',array(
            'name' => 'Телугу',
        ));

        $this->insert('languages',array(
            'name' => 'Тайский',
        ));

        $this->insert('languages',array(
            'name' => 'Турецкий',
        ));

        $this->insert('languages',array(
            'name' => 'Украинский',
        ));

        $this->insert('languages',array(
            'name' => 'Урду',
        ));

        $this->insert('languages',array(
            'name' => 'Финский (Suomi)',
        ));

        $this->insert('languages',array(
            'name' => 'Фламандский',
        ));

        $this->insert('languages',array(
            'name' => 'Французский',
        ));

        $this->insert('languages',array(
            'name' => 'Французский (Бельгия)',
        ));

        $this->insert('languages',array(
            'name' => 'Французский (Канада)',
        ));

        $this->insert('languages',array(
            'name' => 'Французский (Франция)',
        ));

        $this->insert('languages',array(
            'name' => 'Французский (Швейцария)',
        ));

        $this->insert('languages',array(
            'name' => 'Хинди',
        ));

        $this->insert('languages',array(
            'name' => 'Хорватский',
        ));

        $this->insert('languages',array(
            'name' => 'Чешский',
        ));

        $this->insert('languages',array(
            'name' => 'Шведский',
        ));

        $this->insert('languages',array(
            'name' => 'Эстонский',
        ));

        $this->insert('languages',array(
            'name' => 'Японский',
        ));






        $this->insert('country',array(
            'name' => 'Австралия',
        ));

        $this->insert('country',array(
            'name' => 'Австрия',
        ));

        $this->insert('country',array(
            'name' => 'Азербайджан',
        ));

        $this->insert('country',array(
            'name' => 'Аландские острова',
        ));

        $this->insert('country',array(
            'name' => 'Албания',
        ));

        $this->insert('country',array(
            'name' => 'Алжир',
        ));

        $this->insert('country',array(
            'name' => 'Американское Самоа',
        ));

        $this->insert('country',array(
            'name' => 'Ангилья',
        ));

        $this->insert('country',array(
            'name' => 'Ангола',
        ));

        $this->insert('country',array(
            'name' => 'Андорра',
        ));

        $this->insert('country',array(
            'name' => 'Антарктида',
        ));

        $this->insert('country',array(
            'name' => 'Антигуа и Барбуда',
        ));

        $this->insert('country',array(
            'name' => 'Аргентина ',
        ));

        $this->insert('country',array(
            'name' => 'Армения ',
        ));

        $this->insert('country',array(
            'name' => 'Аруба ',
        ));

        $this->insert('country',array(
            'name' => 'Афганистан',
        ));

        $this->insert('country',array(
            'name' => 'Багамы ',
        ));

        $this->insert('country',array(
            'name' => 'Бангладеш',
        ));

        $this->insert('country',array(
            'name' => 'Барбадос',
        ));

        $this->insert('country',array(
            'name' => 'Бахрейн',
        ));

        $this->insert('country',array(
            'name' => 'Белиз',
        ));

        $this->insert('country',array(
            'name' => 'Белоруссия',
        ));

        $this->insert('country',array(
            'name' => 'Бельгия',
        ));

        $this->insert('country',array(
            'name' => 'Бенин',
        ));

        $this->insert('country',array(
            'name' => 'Бермуды',
        ));

        $this->insert('country',array(
            'name' => 'Болгария',
        ));

        $this->insert('country',array(
            'name' => 'Боливия',
        ));

        $this->insert('country',array(
            'name' => 'Бонэйр, Синт-Эстатиус и Саба',
        ));

        $this->insert('country',array(
            'name' => 'Босния и Герцеговина',
        ));

        $this->insert('country',array(
            'name' => 'Ботсвана',
        ));

        $this->insert('country',array(
            'name' => 'Бразилия',
        ));

        $this->insert('country',array(
            'name' => 'Британская территория в Индийском океане',
        ));

        $this->insert('country',array(
            'name' => 'Бруней  ',
        ));

        $this->insert('country',array(
            'name' => 'Буркина-Фасо',
        ));

        $this->insert('country',array(
            'name' => 'Бурунди',
        ));

        $this->insert('country',array(
            'name' => 'Бутан',
        ));

        $this->insert('country',array(
            'name' => 'Вануату',
        ));

        $this->insert('country',array(
            'name' => 'Ватикан',
        ));

        $this->insert('country',array(
            'name' => 'Великобритания',
        ));

        $this->insert('country',array(
            'name' => 'Венгрия',
        ));

        $this->insert('country',array(
            'name' => 'Венесуэла',
        ));

        $this->insert('country',array(
            'name' => 'Виргинские Острова (Великобритания)',
        ));

        $this->insert('country',array(
            'name' => 'Виргинские Острова (США)  ',
        ));

        $this->insert('country',array(
            'name' => 'Внешние малые острова (США)',
        ));

        $this->insert('country',array(
            'name' => 'Восточный Тимор',
        ));

        $this->insert('country',array(
            'name' => 'Вьетнам',
        ));

        $this->insert('country',array(
            'name' => 'Габон',
        ));

        $this->insert('country',array(
            'name' => 'Гаити',
        ));

        $this->insert('country',array(
            'name' => 'Гайана',
        ));

        $this->insert('country',array(
            'name' => 'Гамбия',
        ));

        $this->insert('country',array(
            'name' => 'Гана',
        ));

        $this->insert('country',array(
            'name' => 'Гваделупа',
        ));

        $this->insert('country',array(
            'name' => 'Гватемала',
        ));

        $this->insert('country',array(
            'name' => 'Гвиана',
        ));

        $this->insert('country',array(
            'name' => 'Гвинея-Бисау',
        ));

        $this->insert('country',array(
            'name' => 'Гвинея',
        ));

        $this->insert('country',array(
            'name' => 'Германия',
        ));

        $this->insert('country',array(
            'name' => 'Гернси',
        ));

        $this->insert('country',array(
            'name' => 'Гибралтар',
        ));

        $this->insert('country',array(
            'name' => 'Гондурас',
        ));

        $this->insert('country',array(
            'name' => 'Гонконг ',
        ));

        $this->insert('country',array(
            'name' => 'Государство Палестина',
        ));

        $this->insert('country',array(
            'name' => 'Гренада',
        ));

        $this->insert('country',array(
            'name' => 'Гренландия',
        ));

        $this->insert('country',array(
            'name' => 'Греция',
        ));

        $this->insert('country',array(
            'name' => 'Грузия',
        ));

        $this->insert('country',array(
            'name' => 'Гуам',
        ));

        $this->insert('country',array(
            'name' => 'Дания',
        ));

        $this->insert('country',array(
            'name' => 'Демократическая Республика Конго',
        ));

        $this->insert('country',array(
            'name' => 'Джерси',
        ));

        $this->insert('country',array(
            'name' => 'Джибути',
        ));

        $this->insert('country',array(
            'name' => 'Доминика',
        ));

        $this->insert('country',array(
            'name' => 'Доминиканская Республика',
        ));

        $this->insert('country',array(
            'name' => 'Египет',
        ));

        $this->insert('country',array(
            'name' => 'Замбия',
        ));

        $this->insert('country',array(
            'name' => 'Зимбабве',
        ));

        $this->insert('country',array(
            'name' => 'Израиль',
        ));

        $this->insert('country',array(
            'name' => 'Индия',
        ));

        $this->insert('country',array(
            'name' => 'Индонезия',
        ));

        $this->insert('country',array(
            'name' => 'Иордания',
        ));

        $this->insert('country',array(
            'name' => 'Ирак',
        ));

        $this->insert('country',array(
            'name' => 'Иран',
        ));

        $this->insert('country',array(
            'name' => 'Исландия',
        ));

        $this->insert('country',array(
            'name' => 'Испания',
        ));

        $this->insert('country',array(
            'name' => 'Италия',
        ));

        $this->insert('country',array(
            'name' => 'Йемен',
        ));

        $this->insert('country',array(
            'name' => 'Кабо-Верде',
        ));

        $this->insert('country',array(
            'name' => 'Казахстан',
        ));

        $this->insert('country',array(
            'name' => 'Камбоджа',
        ));

        $this->insert('country',array(
            'name' => 'Камерун',
        ));

        $this->insert('country',array(
            'name' => 'Канада',
        ));

        $this->insert('country',array(
            'name' => 'Катар',
        ));

        $this->insert('country',array(
            'name' => 'Кения',
        ));

        $this->insert('country',array(
            'name' => 'Кипр',
        ));

        $this->insert('country',array(
            'name' => 'Киргизия',
        ));

        $this->insert('country',array(
            'name' => 'Кирибати',
        ));

        $this->insert('country',array(
            'name' => 'Тайвань Китайская Республика',
        ));

        $this->insert('country',array(
            'name' => 'КНДР',
        ));

        $this->insert('country',array(
            'name' => 'КНР (Китайская Народная Республика)',
        ));

        $this->insert('country',array(
            'name' => 'Кокосовые острова',
        ));

        $this->insert('country',array(
            'name' => 'Колумбия',
        ));

        $this->insert('country',array(
            'name' => 'Коморы',
        ));

        $this->insert('country',array(
            'name' => 'Коста-Рика',
        ));

        $this->insert('country',array(
            'name' => 'Кот-д’Ивуар',
        ));

        $this->insert('country',array(
            'name' => 'Куба',
        ));

        $this->insert('country',array(
            'name' => 'Кувейт',
        ));

        $this->insert('country',array(
            'name' => 'Кюрасао',
        ));

        $this->insert('country',array(
            'name' => 'Лаос',
        ));

        $this->insert('country',array(
            'name' => 'Латвия',
        ));

        $this->insert('country',array(
            'name' => 'Лесото',
        ));

        $this->insert('country',array(
            'name' => 'Либерия',
        ));

        $this->insert('country',array(
            'name' => 'Ливан',
        ));

        $this->insert('country',array(
            'name' => 'Ливия',
        ));

        $this->insert('country',array(
            'name' => 'Литва',
        ));

        $this->insert('country',array(
            'name' => 'Лихтенштейн',
        ));

        $this->insert('country',array(
            'name' => 'Люксембург',
        ));

        $this->insert('country',array(
            'name' => 'Маврикий',
        ));

        $this->insert('country',array(
            'name' => 'Мавритания',
        ));

        $this->insert('country',array(
            'name' => 'Мадагаскар',
        ));

        $this->insert('country',array(
            'name' => 'Майотта',
        ));

        $this->insert('country',array(
            'name' => 'Макао',
        ));

        $this->insert('country',array(
            'name' => 'Македония',
        ));

        $this->insert('country',array(
            'name' => 'Малави',
        ));

        $this->insert('country',array(
            'name' => 'Малайзия',
        ));

        $this->insert('country',array(
            'name' => 'Мали',
        ));

        $this->insert('country',array(
            'name' => 'Мальдивы',
        ));

        $this->insert('country',array(
            'name' => 'Мальта',
        ));

        $this->insert('country',array(
            'name' => 'Марокко',
        ));

        $this->insert('country',array(
            'name' => 'Мартиника',
        ));

        $this->insert('country',array(
            'name' => 'Маршалловы Острова',
        ));

        $this->insert('country',array(
            'name' => 'Мексика',
        ));

        $this->insert('country',array(
            'name' => 'Микронезия',
        ));

        $this->insert('country',array(
            'name' => 'Мозамбик ',
        ));

        $this->insert('country',array(
            'name' => 'Молдавия',
        ));

        $this->insert('country',array(
            'name' => 'Монако',
        ));

        $this->insert('country',array(
            'name' => 'Монголия',
        ));

        $this->insert('country',array(
            'name' => 'Монтсеррат',
        ));

        $this->insert('country',array(
            'name' => 'Мьянма',
        ));

        $this->insert('country',array(
            'name' => 'Намибия',
        ));

        $this->insert('country',array(
            'name' => 'Науру',
        ));

        $this->insert('country',array(
            'name' => 'Непал',
        ));

        $this->insert('country',array(
            'name' => 'Нигер',
        ));

        $this->insert('country',array(
            'name' => 'Нигерия',
        ));

        $this->insert('country',array(
            'name' => 'Нидерланды',
        ));

        $this->insert('country',array(
            'name' => 'Никарагуа',
        ));

        $this->insert('country',array(
            'name' => 'Ниуэ',
        ));

        $this->insert('country',array(
            'name' => 'Новая Зеландия',
        ));

        $this->insert('country',array(
            'name' => 'Новая Каледония',
        ));

        $this->insert('country',array(
            'name' => 'Норвегия',
        ));

        $this->insert('country',array(
            'name' => 'ОАЭ',
        ));

        $this->insert('country',array(
            'name' => 'Оман',
        ));

        $this->insert('country',array(
            'name' => 'Остров Буве',
        ));

        $this->insert('country',array(
            'name' => 'Остров Мэн',
        ));

        $this->insert('country',array(
            'name' => 'Остров Норфолк ',
        ));

        $this->insert('country',array(
            'name' => 'Остров Рождества',
        ));

        $this->insert('country',array(
            'name' => 'Острова Кайман',
        ));

        $this->insert('country',array(
            'name' => 'Острова Кука',
        ));

        $this->insert('country',array(
            'name' => 'Острова Питкэрн',
        ));

        $this->insert('country',array(
            'name' => 'Острова Святой Елены, Вознесения и Тристан-да-Кунья Тристан-да-Кунья',
        ));

        $this->insert('country',array(
            'name' => 'Пакистан',
        ));

        $this->insert('country',array(
            'name' => 'Палау',
        ));

        $this->insert('country',array(
            'name' => 'Панама',
        ));

        $this->insert('country',array(
            'name' => 'Папуа — Новая Гвинея',
        ));

        $this->insert('country',array(
            'name' => 'Парагвай',
        ));

        $this->insert('country',array(
            'name' => 'Перу Перу',
        ));

        $this->insert('country',array(
            'name' => 'Польша',
        ));

        $this->insert('country',array(
            'name' => 'Португалия',
        ));

        $this->insert('country',array(
            'name' => 'Пуэрто-Рико',
        ));

        $this->insert('country',array(
            'name' => 'Республика Конго',
        ));

        $this->insert('country',array(
            'name' => 'Республика Корея',
        ));

        $this->insert('country',array(
            'name' => 'Реюньон',
        ));

        $this->insert('country',array(
            'name' => 'Россия',
        ));

        $this->insert('country',array(
            'name' => 'Руанда',
        ));

        $this->insert('country',array(
            'name' => 'Румыния',
        ));

        $this->insert('country',array(
            'name' => 'САДР',
        ));

        $this->insert('country',array(
            'name' => 'Сальвадор',
        ));

        $this->insert('country',array(
            'name' => 'Самоа',
        ));

        $this->insert('country',array(
            'name' => 'Marino.svg Сан-Марино',
        ));

        $this->insert('country',array(
            'name' => 'Сан-Томе и Принсипи',
        ));

        $this->insert('country',array(
            'name' => 'Arabia.svg Саудовская Аравия',
        ));

        $this->insert('country',array(
            'name' => 'Свазиленд',
        ));

        $this->insert('country',array(
            'name' => 'Северные Марианские Острова',
        ));

        $this->insert('country',array(
            'name' => 'Сейшельские Острова',
        ));

        $this->insert('country',array(
            'name' => 'Сен-Бартелеми',
        ));

        $this->insert('country',array(
            'name' => 'Сен-Мартен Сен-Мартен',
        ));

        $this->insert('country',array(
            'name' => 'Сен-Пьер и Микелон',
        ));

        $this->insert('country',array(
            'name' => 'Сенегал',
        ));

        $this->insert('country',array(
            'name' => 'Сент-Винсент и Гренадины',
        ));

        $this->insert('country',array(
            'name' => 'Сент-Китс и Невис',
        ));

        $this->insert('country',array(
            'name' => 'Сент-Люсия',
        ));

        $this->insert('country',array(
            'name' => 'Сербия',
        ));

        $this->insert('country',array(
            'name' => 'Сингапур',
        ));

        $this->insert('country',array(
            'name' => 'Синт-Мартен',
        ));

        $this->insert('country',array(
            'name' => 'Сирия',
        ));

        $this->insert('country',array(
            'name' => 'Словакия',
        ));

        $this->insert('country',array(
            'name' => 'Словения',
        ));

        $this->insert('country',array(
            'name' => 'Соломоновы Острова',
        ));

        $this->insert('country',array(
            'name' => 'Сомали',
        ));

        $this->insert('country',array(
            'name' => 'Судан',
        ));

        $this->insert('country',array(
            'name' => 'Суринам',
        ));

        $this->insert('country',array(
            'name' => 'США',
        ));

        $this->insert('country',array(
            'name' => 'Сьерра-Леоне',
        ));

        $this->insert('country',array(
            'name' => 'Таджикистан',
        ));

        $this->insert('country',array(
            'name' => 'Таиланд',
        ));

        $this->insert('country',array(
            'name' => 'Танзания',
        ));

        $this->insert('country',array(
            'name' => 'Тёркс и Кайкос',
        ));

        $this->insert('country',array(
            'name' => 'Того',
        ));

        $this->insert('country',array(
            'name' => 'Токелау',
        ));

        $this->insert('country',array(
            'name' => 'Тонга',
        ));

        $this->insert('country',array(
            'name' => 'Тринидад и Тобаго',
        ));

        $this->insert('country',array(
            'name' => 'Тувалу',
        ));

        $this->insert('country',array(
            'name' => 'Тунис',
        ));

        $this->insert('country',array(
            'name' => 'Туркмения',
        ));

        $this->insert('country',array(
            'name' => 'Турция',
        ));

        $this->insert('country',array(
            'name' => 'Уганда',
        ));

        $this->insert('country',array(
            'name' => 'Узбекистан',
        ));

        $this->insert('country',array(
            'name' => 'Украина',
        ));

        $this->insert('country',array(
            'name' => 'Уоллис и Футуна',
        ));

        $this->insert('country',array(
            'name' => 'Уругвай',
        ));

        $this->insert('country',array(
            'name' => 'Фареры',
        ));

        $this->insert('country',array(
            'name' => 'Фиджи',
        ));

        $this->insert('country',array(
            'name' => 'Филиппины Филиппины',
        ));

        $this->insert('country',array(
            'name' => 'Финляндия',
        ));

        $this->insert('country',array(
            'name' => 'Ирландия',
        ));

        $this->insert('country',array(
            'name' => 'Ян-Майена Шпицберген и Ян-Майен',
        ));

        $this->insert('country',array(
            'name' => 'Фолклендские острова',
        ));

        $this->insert('country',array(
            'name' => 'Франция',
        ));

        $this->insert('country',array(
            'name' => 'Французская Полинезия Полинезия',
        ));

        $this->insert('country',array(
            'name' => 'Французские Южные и Антарктические Территории ',
        ));

        $this->insert('country',array(
            'name' => 'Херд и Макдональд',
        ));

        $this->insert('country',array(
            'name' => 'Хорватия',
        ));

        $this->insert('country',array(
            'name' => 'ЦАР',
        ));

        $this->insert('country',array(
            'name' => 'Чад',
        ));

        $this->insert('country',array(
            'name' => 'Черногория',
        ));

        $this->insert('country',array(
            'name' => 'Чехия',
        ));

        $this->insert('country',array(
            'name' => 'Чили',
        ));

        $this->insert('country',array(
            'name' => 'Швейцария',
        ));

        $this->insert('country',array(
            'name' => 'Швеция',
        ));

        $this->insert('country',array(
            'name' => 'Шри-Ланка',
        ));

        $this->insert('country',array(
            'name' => 'Эквадор ',
        ));

        $this->insert('country',array(
            'name' => 'Экваториальная Гвинея',
        ));

        $this->insert('country',array(
            'name' => 'Эритрея',
        ));

        $this->insert('country',array(
            'name' => 'Эстония',
        ));

        $this->insert('country',array(
            'name' => 'Эфиопия',
        ));

        $this->insert('country',array(
            'name' => 'ЮАР',
        ));

        $this->insert('country',array(
            'name' => 'Георгия и Южные Сандвичевы Острова',
        ));

        $this->insert('country',array(
            'name' => 'Южный Судан',
        ));

        $this->insert('country',array(
            'name' => 'Ямайка',
        ));

        $this->insert('country',array(
            'name' => 'Япония',
        ));







        $this->insert('manufacturer',array(
            'name' => 'Rotten ',
        ));

        $this->insert('manufacturer',array(
            'name' => 'Disney ',
        ));

    }

    public function down()
    {

    }
}
