<?php

use yii\db\Migration;

/**
 * Handles the creation of table `films`.
 */
class m181203_121354_create_films_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('films', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название фильма'),
            'type_id' => $this->integer()->comment('Тип'),
            'number_season' => $this->string(255)->comment('Номер сезона'),
            'number_seriya' => $this->string(255)->comment('Номер серии'),
            'alternative' => $this->string(255)->comment('Альтернативная нумерация'),
            'language_id' => $this->integer()->comment('Основной язык локализации'),
            'certificate' => $this->boolean()->comment('Наличие удостоверения Национального фильма'),
            'hide_film' => $this->boolean()->comment('Скрыть данный фильм'),
            'description' => $this->text()->comment('Краткое описание'),
            'imdb' => $this->string(255)->comment('IMDB'),
            'rotten' => $this->string(255)->comment('Rotten Tomatoes'),
            'eidr' => $this->string(255)->comment('EIDR'),
            'kinopoisk' => $this->string(255)->comment('Kinopoisk.ru'),
            'year_issue' => $this->integer()->comment('Год выпуска фильма'),
            'rellase_date' => $this->date()->comment('Дата релиза фильма'),
            'country_id' => $this->integer()->comment('Страна производства'),
            'genres' => $this->text()->comment('Жанр'),
            'manufacturer_id' => $this->integer()->comment('Компания-производитель'),
            'duration' => $this->integer()->comment('Продолжительность'),
            'actuality_id' => $this->integer()->comment('Актуалность релиза'),
            'budget' => $this->text()->comment('Бюджет фильма'),
            'reyting' => $this->text()->comment('Возрастной рейтинг фильма'),
            //'poster' => $this->string(255)->comment('Постер'),
        ]);

        $this->createIndex('idx-films-type_id', 'films', 'type_id', false);
        $this->addForeignKey("fk-films-type_id", "films", "type_id", "films_type", "id");

        $this->createIndex('idx-films-language_id', 'films', 'language_id', false);
        $this->addForeignKey("fk-films-language_id", "films", "language_id", "languages", "id");

        $this->createIndex('idx-films-country_id', 'films', 'country_id', false);
        $this->addForeignKey("fk-films-country_id", "films", "country_id", "country", "id");

        $this->createIndex('idx-films-manufacturer_id', 'films', 'manufacturer_id', false);
        $this->addForeignKey("fk-films-manufacturer_id", "films", "manufacturer_id", "manufacturer", "id");

        $this->createIndex('idx-films-actuality_id', 'films', 'actuality_id', false);
        $this->addForeignKey("fk-films-actuality_id", "films", "actuality_id", "actuality", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-films-type_id','films');
        $this->dropIndex('idx-films-type_id','films');

        $this->dropForeignKey('fk-films-language_id','films');
        $this->dropIndex('idx-films-language_id','films');

        $this->dropForeignKey('fk-films-country_id','films');
        $this->dropIndex('idx-films-country_id','films');

        $this->dropForeignKey('fk-films-manufacturer_id','films');
        $this->dropIndex('idx-films-manufacturer_id','films');

        $this->dropForeignKey('fk-films-actuality_id','films');
        $this->dropIndex('idx-films-actuality_id','films');

        $this->dropTable('films');
    }
}
