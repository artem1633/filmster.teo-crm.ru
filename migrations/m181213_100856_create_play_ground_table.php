<?php

use yii\db\Migration;

/**
 * Handles the creation of table `play_ground`.
 */
class m181213_100856_create_play_ground_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('play_ground', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);

        $this->createIndex('idx-technical_information-playground', 'technical_information', 'playground', false);
        $this->addForeignKey("fk-technical_information-playground", "technical_information", "playground", "play_ground", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-technical_information-playground','technical_information');
        $this->dropIndex('idx-technical_information-playground','technical_information');
        
        $this->dropTable('play_ground');
    }
}
