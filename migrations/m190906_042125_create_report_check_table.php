<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%report_check}}`.
 */
class m190906_042125_create_report_check_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%report_check}}', [
            'id' => $this->primaryKey(),
            'report_id' => $this->integer()->comment('Отчет'),
            'date' => $this->string(255)->comment('Дата'),
            'check' => $this->boolean()->comment('Check'),
            'value' => $this->float()->comment('Значение'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%report_check}}');
    }
}
