<?php

use yii\db\Migration;

/**
 * Handles adding flat_rate_currency to table `films_application`.
 */
class m190110_135340_add_flat_rate_currency_column_to_films_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('films_application', 'flat_rate_currency', $this->integer()->comment('Валюта'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('films_application', 'flat_rate_currency');
    }
}
