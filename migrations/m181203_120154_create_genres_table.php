<?php

use yii\db\Migration;

/**
 * Handles the creation of table `genres`.
 */
class m181203_120154_create_genres_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('genres', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Название'),
        ]);

        $this->insert('genres',array(
            'name' => 'Экшн и Приключения ',
        ));

        $this->insert('genres',array(
            'name' => 'Африканское кино ',
        ));

        $this->insert('genres',array(
            'name' => 'Аниме ',
        ));

        $this->insert('genres',array(
            'name' => 'Болливудское кино ',
        ));

        $this->insert('genres',array(
            'name' => 'Классика ',
        ));

        $this->insert('genres',array(
            'name' => 'Комедия ',
        ));

        $this->insert('genres',array(
            'name' => 'Концертные фильмы ',
        ));

        $this->insert('genres',array(
            'name' => 'Документальный фильм ',
        ));

        $this->insert('genres',array(
            'name' => 'Драма',
        ));

        $this->insert('genres',array(
            'name' => 'Иностранное кино',
        ));

        $this->insert('genres',array(
            'name' => 'Праздничное кино',
        ));

        $this->insert('genres',array(
            'name' => 'Ужасы',
        ));

        $this->insert('genres',array(
            'name' => 'Независимое кино',
        ));

        $this->insert('genres',array(
            'name' => 'Дети и Семья',
        ));

        $this->insert('genres',array(
            'name' => 'Сделано для ТВ',
        ));

        $this->insert('genres',array(
            'name' => 'Ближневосточное кино',
        ));

        $this->insert('genres',array(
            'name' => 'Музыкальный документальные фильм',
        ));

        $this->insert('genres',array(
            'name' => 'Музыкальный художественный фильм',
        ));

        $this->insert('genres',array(
            'name' => 'Мюзикл',
        ));

        $this->insert('genres',array(
            'name' => 'Индийское кино',
        ));

        $this->insert('genres',array(
            'name' => 'Мелодрама',
        ));

        $this->insert('genres',array(
            'name' => 'Русское кино',
        ));

        $this->insert('genres',array(
            'name' => 'Фантастика и фэнтези',
        ));

        $this->insert('genres',array(
            'name' => 'Короткометражный фильм',
        ));

        $this->insert('genres',array(
            'name' => 'Особый интерес',
        ));

        $this->insert('genres',array(
            'name' => 'Спортивный фильм',
        ));

        $this->insert('genres',array(
            'name' => 'Триллер',
        ));

        $this->insert('genres',array(
            'name' => 'Турецкое кино',
        ));

        $this->insert('genres',array(
            'name' => 'Аородское кино',
        ));

        $this->insert('genres',array(
            'name' => 'Вестерн',
        ));

        $this->insert('genres',array(
            'name' => 'Японское кино',
        ));

        $this->insert('genres',array(
            'name' => 'Дзидайгэки',
        ));

        $this->insert('genres',array(
            'name' => 'Токусацу',
        ));

        $this->insert('genres',array(
            'name' => 'Корейское кино',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('genres');
    }
}
