<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report_values`.
 */
class m190202_184407_create_report_values_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('report_values', [
            'id' => $this->primaryKey(),
            'report_monetary_id' => $this->integer()->comment('Загрузка отчета'),
            'sale_count' => $this->string(255)->comment('Количество продаж'),
            'film_id' => $this->integer()->comment('Фильм'),
            'film_name' => $this->string(255)->comment('Название фильма'),
            'est' => $this->string(255)->comment('EST'),
            'vod' => $this->string(255)->comment('VOD'),
            'svod' => $this->string(255)->comment('SVOD'),
            'pvod' => $this->string(255)->comment('PVOD'),
            'avod' => $this->string(255)->comment('AVOD'),
            'date_begin' => $this->date()->comment('Дата начала отчетного периода'),
            'date_end' => $this->date()->comment('Дата окончания отчетного периода'),
            'date_begin_value' => $this->string(255)->comment('Дата начала отчетного периода'),
            'date_end_value' => $this->string(255)->comment('Дата окончания отчетного периода'),
            'currency' => $this->integer()->comment('Валюта продаж'),
            'territory' => $this->integer()->comment('Территория'),
            'identificator' => $this->string(255)->comment('Идентификатор площадки'),
            'additional_identificator' => $this->string(255)->comment('Дополнительный идентификатор площадки'),
        ]);

        $this->createIndex('idx-report_values-report_monetary_id', 'report_values', 'report_monetary_id', false);
        $this->addForeignKey("fk-report_values-report_monetary_id", "report_values", "report_monetary_id", "report_monetary", "id");

        $this->createIndex('idx-report_values-film_id', 'report_values', 'film_id', false);
        $this->addForeignKey("fk-report_values-film_id", "report_values", "film_id", "films", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-report_values-report_monetary_id','report_values');
        $this->dropIndex('idx-report_values-report_monetary_id','report_values');

        $this->dropForeignKey('fk-report_values-film_id','report_values');
        $this->dropIndex('idx-report_values-film_id','report_values');

        $this->dropTable('report_values');
    }
}
