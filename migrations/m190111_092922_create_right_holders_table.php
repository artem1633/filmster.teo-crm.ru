<?php

use yii\db\Migration;

/**
 * Handles the creation of table `right_holders`.
 */
class m190111_092922_create_right_holders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('right_holders', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Клиент/Пользователь'),
            'name_ru' => $this->string(255)->comment('Название лицензиата RU'),
            'name_eng' => $this->string(255)->comment('Название лицензиата ENG'),
            'tax_id' => $this->string(255)->comment('TAX ID'),
            'registry_code' => $this->string(255)->comment('Registry Registration Code'),
            'primary_number' => $this->string(255)->comment('Primary State Registration Number'),
            'street1' => $this->text()->comment('Street1'),
            'street2' => $this->text()->comment('Street2'),
            'country_id' => $this->integer()->comment('Страна'),
            'city_id' => $this->string(255)->comment('Город'),
            'state' => $this->string(255)->comment('Регион/Штат'),
            'index' => $this->string(255)->comment('Индекс'),
            'bank_name' => $this->string(255)->comment('Bank name'),
            'bank_street1' => $this->text()->comment('Адрес банка Street1'),
            'bank_street2' => $this->text()->comment('Street2'),
            'bank_country_id' => $this->integer()->comment('Страна банка'),
            'bank_state' => $this->string(255)->comment('Регион/штат'),
            'bank_index' => $this->string(255)->comment('Индекс'),
            'bic_swift' => $this->string(255)->comment('BIC/SWIFT'),
            'bank_account' => $this->float()->comment('Bank account'),
            'bank_account_currency' => $this->integer()->comment('Выбор валюты'),
            'iban' => $this->float()->comment('IBAN'),
            'iban_currency' => $this->integer()->comment('Валюта'),
            'correspondent_account' => $this->float()->comment('Correspondent account'),
            'correspondent_account_currency' => $this->integer()->comment('Валюта'),
        ]);

        $this->createIndex('idx-right_holders-user_id', 'right_holders', 'user_id', false);
        $this->addForeignKey("fk-right_holders-user_id", "right_holders", "user_id", "users", "id");

        $this->createIndex('idx-right_holders-country_id', 'right_holders', 'country_id', false);
        $this->addForeignKey("fk-right_holders-country_id", "right_holders", "country_id", "country", "id");

        $this->createIndex('idx-right_holders-bank_country_id', 'right_holders', 'bank_country_id', false);
        $this->addForeignKey("fk-right_holders-bank_country_id", "right_holders", "bank_country_id", "country", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-right_holders-user_id','right_holders');
        $this->dropIndex('idx-right_holders-user_id','right_holders');

        $this->dropForeignKey('fk-right_holders-country_id','right_holders');
        $this->dropIndex('idx-right_holders-country_id','right_holders');

        $this->dropForeignKey('fk-right_holders-bank_country_id','right_holders');
        $this->dropIndex('idx-right_holders-bank_country_id','right_holders');

        $this->dropTable('right_holders');
    }
}
