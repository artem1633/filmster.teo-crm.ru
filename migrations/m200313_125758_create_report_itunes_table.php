<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%report_itunes}}`.
 */
class m200313_125758_create_report_itunes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%report_itunes}}', [
            'id' => $this->primaryKey(),
            'film_id' => $this->integer()->comment('Фильм'),
            'apple_identifier' => $this->string()->comment('Дополнительный идентификатор площадки'),
            'vendor_identifier' => $this->string()->comment('Идентификатор площадки'),
            'title' => $this->string()->comment('Название фильма'),
            'sales' => $this->integer()->comment('Количество продаж'),
            'type' => $this->string()->comment('Тип продажи'),
            'customer_price' => $this->float()->comment('Сумма к выплате'),
            'customer_currency' => $this->string()->comment('Валюта выплаты'),
            'country' => $this->string()->comment('Территория'),
            'format' => $this->string()->comment('Формат'),
            'genre' => $this->string()->comment('Жанр'),
            'begin_date' => $this->string()->comment('Дата начала'),
            'date' => $this->date()->comment('Дата'),
        ]);

        $this->createIndex(
            'idx-report_itunes-film_id',
            'report_itunes',
            'film_id'
        );

        $this->addForeignKey(
            'fk-report_itunes-film_id',
            'report_itunes',
            'film_id',
            'films',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-report_itunes-film_id',
            'report_itunes'
        );

        $this->dropIndex(
            'idx-report_itunes-film_id',
            'report_itunes'
        );

        $this->dropTable('{{%report_itunes}}');
    }
}
