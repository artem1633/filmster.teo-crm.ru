<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies`.
 */
class m181210_093012_create_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'type' => $this->integer()->comment('Тип'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies');
    }
}
