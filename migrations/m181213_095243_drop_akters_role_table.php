<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `akters_role`.
 */
class m181213_095243_drop_akters_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('akters_role');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('akters_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
        ]);
    }
}
