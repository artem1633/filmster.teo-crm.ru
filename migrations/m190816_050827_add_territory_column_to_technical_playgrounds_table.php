<?php

use yii\db\Migration;

/**
 * Handles adding territory to table `{{%technical_playgrounds}}`.
 */
class m190816_050827_add_territory_column_to_technical_playgrounds_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%technical_playgrounds}}', 'territory', $this->string(255)->comment("Территория"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%technical_playgrounds}}', 'territory');
    }
}
