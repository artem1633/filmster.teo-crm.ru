<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `companies`.
 */
class m190220_133919_drop_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-contract-company_lic','contract');
        $this->dropIndex('idx-contract-company_lic','contract');

        $this->dropForeignKey('fk-contract-company_cont','contract');
        $this->dropIndex('idx-contract-company_cont','contract');

        $this->dropColumn('contract', 'company_lic');
        $this->dropColumn('contract', 'company_cont');

        $this->dropTable('companies');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'type' => $this->integer()->comment('Тип'),
        ]);

        $this->addColumn('contract', 'company_lic', $this->integer());
        $this->addColumn('contract', 'company_cont', $this->integer());

        $this->createIndex('idx-contract-company_lic', 'contract', 'company_lic', false);
        $this->addForeignKey("fk-contract-company_lic", "contract", "company_lic", "companies", "id");

        $this->createIndex('idx-contract-company_cont', 'contract', 'company_cont', false);
        $this->addForeignKey("fk-contract-company_cont", "contract", "company_cont", "companies", "id");
    }
}
