<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180707_053230_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255),
            'login' => $this->string(255),
            'password' => $this->string(255),
            'telephone' => $this->string(255),
            'foto' => $this->string(255),
            'type' => $this->integer(),
        ]);

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',          
            'login' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'type' => 0,
            'telephone' => '',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
