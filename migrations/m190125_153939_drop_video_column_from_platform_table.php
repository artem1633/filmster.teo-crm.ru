<?php

use yii\db\Migration;

/**
 * Handles dropping video from table `platform`.
 */
class m190125_153939_drop_video_column_from_platform_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('platform', 'video');
        $this->dropColumn('platform', 'audio');
        $this->dropColumn('platform', 'subtitr');

        $this->addColumn('platform', 'video', $this->text()->comment('Формат видео'));
        $this->addColumn('platform', 'audio', $this->text()->comment('Формат аудио'));
        $this->addColumn('platform', 'subtitr', $this->text()->comment('Субтитры'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('platform', 'video');
        $this->dropColumn('platform', 'audio');
        $this->dropColumn('platform', 'subtitr');
        
        $this->addColumn('platform', 'video', $this->integer()->comment('Формат видео'));
        $this->addColumn('platform', 'audio', $this->integer()->comment('Формат аудио'));
        $this->addColumn('platform', 'subtitr', $this->integer()->comment('Субтитры'));

    }
}
