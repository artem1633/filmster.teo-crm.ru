<?php

use yii\db\Migration;

/**
 * Handles adding tags to table `films`.
 */
class m181213_115659_add_tags_column_to_films_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('films', 'tags', $this->text()->comment('Теги'));
        $this->addColumn('films', 'awards', $this->text()->comment('Награды'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('films', 'tags');
        $this->dropColumn('films', 'awards');
    }
}
