<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report_setting`.
 */
class m190121_062834_create_report_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('report_setting', [
            'id' => $this->primaryKey(),
            'platform_id' => $this->integer()->comment('Площадка'),
            'excel_file' => $this->string(255)->comment('Файл'),
            'begin_parse' => $this->integer()->comment('Начинать со строки'),
            'end_parse' => $this->integer()->comment('Закончить на строке'),
        ]);

        $this->createIndex('idx-report_setting-platform_id', 'report_setting', 'platform_id', false);
        $this->addForeignKey("fk-report_setting-platform_id", "report_setting", "platform_id", "platform", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-report_setting-platform_id','report_setting');
        $this->dropIndex('idx-report_setting-platform_id','report_setting');

        $this->dropTable('report_setting');
    }
}
