<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m181206_112708_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'code' => $this->string(255)->comment('Код'),
        ]);

        $this->insert('currency',array(
            'name' => 'Дирхам (дирхам ОАЭ)',
            'code' => 'AED',
        ));

        $this->insert('currency',array(
            'name' => 'Афгани',
            'code' => 'AFN',
        ));
        
        $this->insert('currency',array(
            'name' => 'Лек',
            'code' => 'ALL',
        ));

        $this->insert('currency',array(
            'name' => 'Драм (армянский драм)',
            'code' => 'AMD',
        ));

        $this->insert('currency',array(
            'name' => 'Гульден (нидерландский антильский гульден)',
            'code' => 'ANG',
        ));

        $this->insert('currency',array(
            'name' => 'Кванза',
            'code' => 'AOA',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (аргентинское песо)',
            'code' => 'ARS',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (австралийский доллар)',
            'code' => 'AUD',
        ));

        $this->insert('currency',array(
            'name' => 'Флорин (арубанский флорин)',
            'code' => 'AWG',
        ));

        $this->insert('currency',array(
            'name' => 'Манат (азербайджанский манат)',
            'code' => 'AZN',
        ));

        $this->insert('currency',array(
            'name' => 'Марка (конвертируемая марка)',
            'code' => 'BAM',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (барбадосский доллар)',
            'code' => 'BBD',
        ));

        $this->insert('currency',array(
            'name' => 'Така',
            'code' => 'BDT',
        ));

        $this->insert('currency',array(
            'name' => 'Лев (болгарский лев)',
            'code' => 'BGN',
        ));

        $this->insert('currency',array(
            'name' => 'Динар (бахрейнский динар)',
            'code' => 'BHD',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (бурундийский франк)',
            'code' => 'BIF',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (бермудский доллар)',
            'code' => 'BMD',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (брунейский доллар)[12]',
            'code' => 'BND',
        ));

        $this->insert('currency',array(
            'name' => 'Боливиано',
            'code' => 'BOB',
        ));

        $this->insert('currency',array(
            'name' => 'Реал (бразильский реал)',
            'code' => 'BRL',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (багамский доллар)',
            'code' => 'BSD',
        ));

        $this->insert('currency',array(
            'name' => 'Нгултрум[13]',
            'code' => 'BTN',
        ));

        $this->insert('currency',array(
            'name' => 'Пула',
            'code' => 'BWP',
        ));

        $this->insert('currency',array(
            'name' => 'Рубль (белорусский рубль)',
            'code' => 'BYN',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (белизский доллар)',
            'code' => 'BZD',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (канадский доллар)',
            'code' => 'CAD',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (конголезский франк)',
            'code' => 'CDF',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (швейцарский франк)',
            'code' => 'CHF',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (чилийское песо)',
            'code' => 'CLP',
        ));

        $this->insert('currency',array(
            'name' => 'Юань',
            'code' => 'CNY',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (колумбийское песо)',
            'code' => 'COP',
        ));

        $this->insert('currency',array(
            'name' => 'Колон (коста-риканский колон)',
            'code' => 'CRC',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (кубинское песо)[21]',
            'code' => 'CUP',
        ));

        $this->insert('currency',array(
            'name' => 'Эскудо (эскудо Кабо-Верде)',
            'code' => 'CVE',
        ));

        $this->insert('currency',array(
            'name' => 'Крона (чешская крона)',
            'code' => 'CZK',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (франк Джибути)',
            'code' => 'DJF',
        ));

        $this->insert('currency',array(
            'name' => 'Крона (датская крона)',
            'code' => 'DKK',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (доминиканское песо)',
            'code' => 'DOP',
        ));

        $this->insert('currency',array(
            'name' => 'Динар (алжирский динар)',
            'code' => 'DZD',
        ));

        $this->insert('currency',array(
            'name' => 'Фунт (египетский фунт)',
            'code' => 'EGP',
        ));

        $this->insert('currency',array(
            'name' => 'Накфа',
            'code' => 'ERN',
        ));

        $this->insert('currency',array(
            'name' => 'Быр (эфиопский быр)',
            'code' => 'ETB',
        ));

        $this->insert('currency',array(
            'name' => 'Евро',
            'code' => 'EUR',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (доллар Фиджи)',
            'code' => 'FJD',
        ));

        $this->insert('currency',array(
            'name' => 'Фунт (фунт Фолклендских островов)',
            'code' => 'FKP',
        ));

        $this->insert('currency',array(
            'name' => 'Фунт (фунт стерлингов)',
            'code' => 'GBP',
        ));

        $this->insert('currency',array(
            'name' => 'Лари',
            'code' => 'GEL',
        ));

        $this->insert('currency',array(
            'name' => 'Седи (ганский седи)',
            'code' => 'GHS',
        ));

        $this->insert('currency',array(
            'name' => 'Фунт (гибралтарский фунт)',
            'code' => 'GIP',
        ));

        $this->insert('currency',array(
            'name' => 'Даласи',
            'code' => 'GMD',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (гвинейский франк)',
            'code' => 'GNF',
        ));

        $this->insert('currency',array(
            'name' => 'Кетсаль',
            'code' => 'GTQ',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (гайанский доллар)',
            'code' => 'GYD',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (гонконгский доллар)',
            'code' => 'HKD',
        ));

        $this->insert('currency',array(
            'name' => 'Лемпира',
            'code' => 'HNL',
        ));

        $this->insert('currency',array(
            'name' => 'Куна (хорватская куна)',
            'code' => 'HRK',
        ));

        $this->insert('currency',array(
            'name' => 'Гурд[14]',
            'code' => 'HTG',
        ));

        $this->insert('currency',array(
            'name' => 'Форинт',
            'code' => 'HUF',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия',
            'code' => 'IDR',
        ));

        $this->insert('currency',array(
            'name' => 'Шекель (новый израильский шекель)',
            'code' => 'ILS',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия (индийская рупия)',
            'code' => 'INR',
        ));

       $this->insert('currency',array(
            'name' => 'Динар (иракский динар)',
            'code' => 'IQD',
        ));
        
        $this->insert('currency',array(
            'name' => 'Риал (иранский риал)[18]',
            'code' => 'IRR',
        ));

        $this->insert('currency',array(
            'name' => 'Крона (исландская крона)',
            'code' => 'ISK',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (ямайский доллар)',
            'code' => 'JMD',
        ));

        $this->insert('currency',array(
            'name' => 'Динар (иорданский динар)',
            'code' => 'JOD',
        ));

        $this->insert('currency',array(
            'name' => 'Иена',
            'code' => 'JPY',
        ));

        $this->insert('currency',array(
            'name' => 'Шиллинг (кенийский шиллинг)',
            'code' => 'KES',
        ));

        $this->insert('currency',array(
            'name' => 'Сом',
            'code' => 'KGS',
        ));

        $this->insert('currency',array(
            'name' => 'Риель',
            'code' => 'KHR',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (франк Комор)',
            'code' => 'KMF',
        ));

        $this->insert('currency',array(
            'name' => 'Вона (северокорейская вона)',
            'code' => 'KPW',
        ));

        $this->insert('currency',array(
            'name' => 'Вона (южнокорейская вона)',
            'code' => 'KRW',
        ));

        $this->insert('currency',array(
            'name' => 'Динар (кувейтский динар)',
            'code' => 'KWD',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (доллар Островов Кайман)',
            'code' => 'KYD',
        ));

        $this->insert('currency',array(
            'name' => 'Тенге',
            'code' => 'KZT',
        ));

        $this->insert('currency',array(
            'name' => 'Кип',
            'code' => 'LAK',
        ));

        $this->insert('currency',array(
            'name' => 'Фунт (ливанский фунт)',
            'code' => 'LBP',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия (шри-ланкийская рупия)',
            'code' => 'LKR',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (либерийский доллар)',
            'code' => 'LRD',
        ));

        $this->insert('currency',array(
            'name' => 'Лоти',
            'code' => 'LSL',
        ));

        $this->insert('currency',array(
            'name' => 'Динар (ливийский динар)',
            'code' => 'LYD',
        ));

        $this->insert('currency',array(
            'name' => 'Дирхам (марокканский дирхам)',
            'code' => 'MAD',
        ));

        $this->insert('currency',array(
            'name' => 'Лей (молдавский лей)',
            'code' => 'MDL',
        ));

        $this->insert('currency',array(
            'name' => 'Ариари (малагасийский ариари)',
            'code' => 'MGA',
        ));

        $this->insert('currency',array(
            'name' => 'Денар',
            'code' => 'MKD',
        ));

        $this->insert('currency',array(
            'name' => 'Кьят',
            'code' => 'MMK',
        ));

        $this->insert('currency',array(
            'name' => 'Тугрик',
            'code' => 'MNT',
        ));

        $this->insert('currency',array(
            'name' => 'Патака',
            'code' => 'MOP',
        ));

        $this->insert('currency',array(
            'name' => 'Угия',
            'code' => 'MRU',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия (маврикийская рупия)',
            'code' => 'MUR',
        ));

        $this->insert('currency',array(
            'name' => 'Руфия',
            'code' => 'MVR',
        ));

        $this->insert('currency',array(
            'name' => 'Квача',
            'code' => 'MWK',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (мексиканское песо)',
            'code' => 'MXN',
        ));

        $this->insert('currency',array(
            'name' => 'Ринггит (малайзийский ринггит)',
            'code' => 'MYR',
        ));

        $this->insert('currency',array(
            'name' => 'Метикал (мозамбикский метикал)',
            'code' => 'MZN',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (доллар Намибии)[23]',
            'code' => 'NAD',
        ));

        $this->insert('currency',array(
            'name' => 'Найра',
            'code' => 'NGN',
        ));

        $this->insert('currency',array(
            'name' => 'Кордоба (золотая кордоба)',
            'code' => 'NIO',
        ));

        $this->insert('currency',array(
            'name' => 'Крона (норвежская крона)',
            'code' => 'NOK',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия (непальская рупия)',
            'code' => 'NPR',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (новозеландский доллар)',
            'code' => 'NZD',
        ));

        $this->insert('currency',array(
            'name' => 'Риал (оманский риал)',
            'code' => 'OMR',
        ));

        $this->insert('currency',array(
            'name' => 'Бальбоа[14]',
            'code' => 'PAB',
        ));

        $this->insert('currency',array(
            'name' => 'Соль (новый соль)',
            'code' => 'PEN',
        ));

        $this->insert('currency',array(
            'name' => 'Кина',
            'code' => 'PGK',
        ));

        $this->insert('currency',array(
            'name' => 'Песо (филиппинское песо)',
            'code' => 'PHP',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия (пакистанская рупия)',
            'code' => 'PKR',
        ));

        $this->insert('currency',array(
            'name' => 'Злотый',
            'code' => 'PLN',
        ));

        $this->insert('currency',array(
            'name' => 'Гуарани',
            'code' => 'PYG',
        ));

        $this->insert('currency',array(
            'name' => 'Риал (катарский риал)',
            'code' => 'QAR',
        ));

        $this->insert('currency',array(
            'name' => 'Лей (новый румынский лей)',
            'code' => 'RON',
        ));

        $this->insert('currency',array(
            'name' => 'Динар (сербский динар)',
            'code' => 'RSD',
        ));

        $this->insert('currency',array(
            'name' => 'Рубль (российский рубль)',
            'code' => 'RUB',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (франк Руанды)',
            'code' => 'RWF',
        ));

        $this->insert('currency',array(
            'name' => 'Риял (саудовский риял)',
            'code' => 'SAR',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (доллар Соломоновых Островов)',
            'code' => 'SBD',
        ));

        $this->insert('currency',array(
            'name' => 'Рупия (сейшельская рупия)',
            'code' => 'SCR',
        ));

        $this->insert('currency',array(
            'name' => 'Фунт (суданский фунт)',
            'code' => 'SDG',
        ));

        $this->insert('currency',array(
            'name' => 'Крона (шведская крона)',
            'code' => 'SEK',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (сингапурский доллар)[27]',
            'code' => 'SGD',
        ));

         $this->insert('currency',array(
            'name' => 'Фунт (фунт Святой Елены)',
            'code' => 'SHP',
        ));

         $this->insert('currency',array(
            'name' => 'Леоне',
            'code' => 'SLL',
        ));

         $this->insert('currency',array(
            'name' => 'Шиллинг (сомалийский шиллинг)',
            'code' => 'SOS',
        ));

         $this->insert('currency',array(
            'name' => 'Доллар (суринамский доллар)',
            'code' => 'SRD',
        ));

         $this->insert('currency',array(
            'name' => 'Фунт (южносуданский фунт)',
            'code' => 'SSP',
        ));

         $this->insert('currency',array(
            'name' => 'Добра',
            'code' => 'STN',
        ));

         $this->insert('currency',array(
            'name' => 'Колон (сальвадорский колон)[14]',
            'code' => 'SVC',
        ));

         $this->insert('currency',array(
            'name' => 'Фунт (сирийский фунт)',
            'code' => 'SYP',
        ));

         $this->insert('currency',array(
            'name' => 'Лилангени',
            'code' => 'SZL',
        ));

         $this->insert('currency',array(
            'name' => 'Бат',
            'code' => 'THB',
        ));

         $this->insert('currency',array(
            'name' => 'Сомони',
            'code' => 'TJS',
        ));

         $this->insert('currency',array(
            'name' => 'Манат (новый туркменский манат)',
            'code' => 'TMT',
        ));

         $this->insert('currency',array(
            'name' => 'Динар (тунисский динар)',
            'code' => 'TND',
        ));

         $this->insert('currency',array(
            'name' => 'Паанга[29]',
            'code' => 'TOP',
        ));

         $this->insert('currency',array(
            'name' => 'Лира (турецкая лира)',
            'code' => 'TRY',
        ));

         $this->insert('currency',array(
            'name' => 'Доллар (доллар Тринидада и Тобаго)',
            'code' => 'TTD',
        ));

         $this->insert('currency',array(
            'name' => 'Доллар (новый тайваньский доллар)',
            'code' => 'TWD',
        ));

         $this->insert('currency',array(
            'name' => 'Шиллинг (танзанийский шиллинг)',
            'code' => 'TZS',
        ));

         $this->insert('currency',array(
            'name' => 'Гривна',
            'code' => 'UAH',
        ));

         $this->insert('currency',array(
            'name' => 'Шиллинг (угандийский шиллинг)',
            'code' => 'UGX',
        ));

         $this->insert('currency',array(
            'name' => 'Доллар (доллар США)',
            'code' => 'USD',
        ));

         $this->insert('currency',array(
            'name' => 'Песо (уругвайское песо)',
            'code' => 'UYU',
        ));

         $this->insert('currency',array(
            'name' => 'Сум (узбекский сум)',
            'code' => 'UZS',
        ));

         $this->insert('currency',array(
            'name' => 'Суверенный боливар',
            'code' => 'VES',
        ));

         $this->insert('currency',array(
            'name' => 'Донг',
            'code' => 'VND',
        ));

         $this->insert('currency',array(
            'name' => 'Вату',
            'code' => 'VUV',
        ));

         $this->insert('currency',array(
            'name' => 'Тала',
            'code' => 'WST',
        ));

         $this->insert('currency',array(
            'name' => 'Франк (франк КФА BEAC)',
            'code' => 'XAF',
        ));

         $this->insert('currency',array(
            'name' => 'Доллар (восточно-карибский доллар)',
            'code' => 'XCD',
        ));

         $this->insert('currency',array(
            'name' => 'Франк (франк КФА BCEAO)',
            'code' => 'XOF',
        ));

        $this->insert('currency',array(
            'name' => 'Франк (франк КФП)',
            'code' => 'XPF',
        ));

        $this->insert('currency',array(
            'name' => 'Риал (йеменский риал)',
            'code' => 'YER',
        ));

        $this->insert('currency',array(
            'name' => 'Рэнд',
            'code' => 'ZAR',
        ));

        $this->insert('currency',array(
            'name' => 'Квача (замбийская квача)',
            'code' => 'ZMW',
        ));

        $this->insert('currency',array(
            'name' => 'Доллар (доллар Зимбабве)',
            'code' => 'ZWL',
        )); 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency');
    }
}
