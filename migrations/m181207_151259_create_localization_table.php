<?php

use yii\db\Migration;

/**
 * Handles the creation of table `localization`.
 */
class m181207_151259_create_localization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('localization', [
            'id' => $this->primaryKey(),
            'type' => $this->integer()->comment('Тип'),
            'language_id' => $this->integer()->comment('Язык'),
            'name' => $this->string(255)->comment('Название фильма'),
            'description' => $this->text()->comment('Краткое описание'),
            'cast' => $this->text()->comment('Актерский состав'),
            'film_crew' => $this->text()->comment('Съемочная группа'),
            'awards' => $this->text()->comment('Награды'),
            'tags' => $this->text()->comment('Поисковые теги'),
            'poster23' => $this->text()->comment('Постер 2х3'),
            'poster169' => $this->text()->comment('Постер 16х9'),
            'photos' => $this->text()->comment('Кадры из фильма'),
            'film_id' => $this->integer()->comment('Фильм'),
        ]);

         $this->createIndex('idx-localization-film_id', 'localization', 'film_id', false);
         $this->addForeignKey("fk-localization-film_id", "localization", "film_id", "films", "id");

         $this->createIndex('idx-localization-language_id', 'localization', 'language_id', false);
         $this->addForeignKey("fk-localization-language_id", "localization", "language_id", "languages", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey('fk-localization-film_id','localization');
        $this->dropIndex('idx-localization-film_id','localization');

        $this->dropForeignKey('fk-localization-language_id','localization');
        $this->dropIndex('idx-localization-language_id','localization');

        $this->dropTable('localization');
    }
}
