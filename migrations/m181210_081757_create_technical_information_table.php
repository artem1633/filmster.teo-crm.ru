<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technical_information`.
 */
class m181210_081757_create_technical_information_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('technical_information', [
            'id' => $this->primaryKey(),
            'film_id' => $this->integer()->comment('Фильм'),
            'playground' => $this->integer()->comment('Площадка '),
            'value_playground' => $this->string(255)->comment('Значение площадки '),
            'additional_value' => $this->string(255)->comment('Доп. значение площадки'),
            'video' => $this->string(255)->comment('Формат видео'),
            'audio' => $this->string(255)->comment('Формат аудио'),
            'duration' => $this->integer()->comment('Длительность (в минутах)'),
            'playback_speed' => $this->string(255)->comment('Скорость воспроизведения'),
            'video_language' => $this->string(255)->comment('Оригинальный язык видео'),
            'audio_language' => $this->string(255)->comment('Оригинальный язык аудио'),
            'source_availability' => $this->integer()->comment('Доступность исходника'),
            'source_link' => $this->string(255)->comment('Ссылку на исходник фильма'),
            'burnt_subtitles' => $this->boolean()->comment('Наличие прожженных субтитров'),
            'burnt_value' => $this->text()->comment('Язык прожженных субтитров'),
            'forced_subtitles' => $this->boolean()->comment('Наличие принудительных субтитров'),
            'forced_value' => $this->text()->comment('Язык принудительных субтитров'),
            'subtitles' => $this->boolean()->comment('Наличие субтитров'),
            'subtitles_value' => $this->text()->comment('Язык субтитров'),
            'sdh_subtitles' => $this->boolean()->comment('Наличие субтитров в формате SDH'),
            'sdh_value' => $this->text()->comment('Язык субтитров в формате SDH'),
            'presense_cc' => $this->boolean()->comment('Наличие СС'),
            'presense_value' => $this->text()->comment('Язык субтитров в формате СС'),
            'treyler' => $this->text()->comment('Трейлер'),
        ]);

        $this->createIndex('idx-technical_information-film_id', 'technical_information', 'film_id', false);
        $this->addForeignKey("fk-technical_information-film_id", "technical_information", "film_id", "films", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-technical_information-film_id','technical_information');
        $this->dropIndex('idx-technical_information-film_id','technical_information');

        $this->dropTable('technical_information');
    }
}
