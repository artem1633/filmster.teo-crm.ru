<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency_setting`.
 */
class m190309_093938_create_currency_setting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('currency_setting', [
            'id' => $this->primaryKey(),
            'currency_from_id' => $this->integer()->comment('Первая валюта'),
            'currency_to_id' => $this->integer()->comment('Вторая валюта'),
            'currency_from_value' => $this->float()->comment('Значения первого'),
            'currency_to_value' => $this->float()->comment('Значения второго'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currency_setting');
    }
}
