<?php

use yii\db\Migration;

/**
 * Handles the creation of table `platform`.
 */
class m181218_044753_create_platform_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('platform', [
            'id' => $this->primaryKey(),
            'name_ru' => $this->string(255)->comment('Название площадки RU'),
            'name_eng' => $this->string(255)->comment('Название площадки ENG'),
            'logo' => $this->string(255)->comment('Логотип 1 к 1'),
            'description_ru' => $this->text()->comment('Краткое описание площадки RU'),
            'description_eng' => $this->string(255)->comment('Краткое описание площадки ENG'),
            'video' => $this->integer()->comment('Формат видео'),
            'audio' => $this->integer()->comment('Формат аудио'),
            'subtitr' => $this->integer()->comment('Субтитры'),
            'sales_model' => $this->text()->comment('Модель продаж'),
            'countries' => $this->text()->comment('Доступные страны распространения'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('platform');
    }
}
