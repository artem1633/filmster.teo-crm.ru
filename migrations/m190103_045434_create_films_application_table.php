<?php

use yii\db\Migration;

/**
 * Handles the creation of table `films_application`.
 */
class m190103_045434_create_films_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('films_application', [
            'id' => $this->primaryKey(),
            'film_id' => $this->integer()->comment('Фильм'),
            'application_id' => $this->integer()->comment('Приложения'),
            'territory' => $this->text()->comment('Территория'),
            'rolyati_x' => $this->float()->comment('Роялти лицензиат'),
            'rolyati_y' => $this->float()->comment('Роялти лицензиар'),
            'platforms' => $this->text()->comment('Площадки'),
            'price' => $this->integer()->comment('Цена'),
            'est_open' => $this->date()->comment('EST откр.'),
            'vod_open' => $this->date()->comment('VOD откр.'),
            'close_date' => $this->date()->comment('Закрытие'),
            'svod' => $this->string(255)->comment('SVOD (открытие)'),
            'pvod' => $this->string(255)->comment('PVOD (открытие)'),
            'avod' => $this->string(255)->comment('AVOD (открытие)'),
            'catch_up' => $this->string(255)->comment('Catch-up (открытие)'),
            'tv' => $this->string(255)->comment('TV (открытие)'),
            'flat_rate' => $this->string(255)->comment('Flat Rate (открытие)'),
            'min_warranty' => $this->float()->comment('Мин.гарантия'),
            'min_warranty_currency' => $this->integer()->comment('Валюта выплаты'),
            'tex_adap' => $this->float()->comment('Тех.адаптация'),
            'text_adap_currency' => $this->integer()->comment('Валюта выплаты'),
            'maintenance' => $this->float()->comment('Maintenance fee'),
            'maintenance_currency' => $this->integer()->comment('Валюта выплаты'),
        ]);

        $this->createIndex('idx-films_application-film_id', 'films_application', 'film_id', false);
        $this->addForeignKey("fk-films_application-film_id", "films_application", "film_id", "films", "id");

        $this->createIndex('idx-films_application-application_id', 'films_application', 'application_id', false);
        $this->addForeignKey("fk-films_application-application_id", "films_application", "application_id", "applications", "id");

        $this->addColumn('applications', 'settings', $this->boolean()->comment('Настройка'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-films_application-film_id','films_application');
        $this->dropIndex('idx-films_application-film_id','films_application');

        $this->dropForeignKey('fk-films_application-application_id','films_application');
        $this->dropIndex('idx-films_application-application_id','films_application');

        $this->dropTable('films_application');

        $this->dropColumn('applications', 'settings');
    }
}
