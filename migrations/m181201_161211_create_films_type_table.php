<?php

use yii\db\Migration;

/**
 * Handles the creation of table `films_type`.
 */
class m181201_161211_create_films_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('films_type', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->comment('Ключ'),
            'name' => $this->string(255)->comment('Название'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('films_type');
    }
}
