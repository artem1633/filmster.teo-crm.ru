<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contract`.
 */
class m181210_093025_create_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contract', [
            'id' => $this->primaryKey(),
            'number' => $this->string(255)->comment('Номер договора'),
            'date_cr' => $this->date()->comment('Дата создания договора'),
            'min_payment' => $this->float()->comment('Мин.порог выплаты'),
            'min_payment_currency' => $this->integer()->comment('валюта '),
            'payment_period' => $this->integer()->comment('Период выплат'),
            'payment_currency' => $this->integer()->comment('Валюта выплаты'),
            'company_lic' => $this->integer()->comment('Компания - подписант со стороны Лицензиата (LIC ID)'),
            'company_cont' => $this->integer()->comment('Компания - подписант со стороны Лицензиара (CONT ID)'),
            'another_contract' => $this->boolean()->comment('Есть зависимость от другого договора'),
            'another_contract_value' => $this->string(255)->comment('Значения другого договора'),
        ]);

        $this->createIndex('idx-contract-company_lic', 'contract', 'company_lic', false);
        $this->addForeignKey("fk-contract-company_lic", "contract", "company_lic", "companies", "id");

        $this->createIndex('idx-contract-company_cont', 'contract', 'company_cont', false);
        $this->addForeignKey("fk-contract-company_cont", "contract", "company_cont", "companies", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-contract-company_lic','contract');
        $this->dropIndex('idx-contract-company_lic','contract');

        $this->dropForeignKey('fk-contract-company_cont','contract');
        $this->dropIndex('idx-contract-company_cont','contract');

        $this->dropTable('contract');
    }
}
