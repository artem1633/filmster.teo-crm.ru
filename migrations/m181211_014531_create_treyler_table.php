<?php

use yii\db\Migration;

/**
 * Handles the creation of table `treyler`.
 */
class m181211_014531_create_treyler_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('treyler', [
            'id' => $this->primaryKey(),
            'technical_information_id' => $this->integer()->comment('Фильм'),
            'video' => $this->string(255)->comment('Формат видео'),
            'audio' => $this->string(255)->comment('Формат аудио'),
            'duration' => $this->integer()->comment('Длительность (в минутах)'),
            'playback_speed' => $this->string(255)->comment('Скорость воспроизведения'),
            'video_language' => $this->string(255)->comment('Оригинальный язык видео'),
            'audio_language' => $this->string(255)->comment('Оригинальный язык аудио'),
            'source_availability' => $this->integer()->comment('Доступность исходника'),
            'source_link' => $this->string(255)->comment('Ссылку на исходник фильма'),
            'burnt_subtitles' => $this->boolean()->comment('Наличие прожженных субтитров'),
            'burnt_value' => $this->text()->comment('Язык прожженных субтитров'),
            'forced_subtitles' => $this->boolean()->comment('Наличие принудительных субтитров'),
            'forced_value' => $this->text()->comment('Язык принудительных субтитров'),
            'subtitles' => $this->boolean()->comment('Наличие субтитров'),
            'subtitles_value' => $this->text()->comment('Язык субтитров'),
            'sdh_subtitles' => $this->boolean()->comment('Наличие субтитров в формате SDH'),
            'sdh_value' => $this->text()->comment('Язык субтитров в формате SDH'),
            'presense_cc' => $this->boolean()->comment('Наличие СС'),
            'presense_value' => $this->text()->comment('Язык субтитров в формате СС'),
        ]);

        $this->createIndex('idx-treyler-technical_information_id', 'treyler', 'technical_information_id', false);
        $this->addForeignKey("fk-treyler-technical_information_id", "treyler", "technical_information_id", "technical_information", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-treyler-technical_information_id','treyler');
        $this->dropIndex('idx-treyler-technical_information_id','treyler');

        $this->dropTable('treyler');
    }
}
