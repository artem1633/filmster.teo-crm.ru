<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report_monetary`.
 */
class m190130_111143_create_report_monetary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('report_monetary', [
            'id' => $this->primaryKey(),
            'platform_id' => $this->integer()->comment('Площадка'),
            'file' => $this->string(255)->comment('Файл'),
            'date' => $this->string(10)->comment('Дата'),
        ]);

        $this->createIndex('idx-report_monetary-platform_id', 'report_monetary', 'platform_id', false);
        $this->addForeignKey("fk-report_monetary-platform_id", "report_monetary", "platform_id", "platform", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-report_monetary-platform_id','report_monetary');
        $this->dropIndex('idx-report_monetary-platform_id','report_monetary');
        
        $this->dropTable('report_monetary');
    }
}
