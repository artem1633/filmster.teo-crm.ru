<?php

use yii\db\Migration;

/**
 * Handles the creation of table `conditions`.
 */
class m190121_065728_create_conditions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('conditions', [
            'id' => $this->primaryKey(),
            'report_setting_id' => $this->integer()->comment('Настройка отчета'),
            'action_id' => $this->integer()->comment('Действия'),
            'film_id' => $this->integer()->comment('Фильм'),
            'sale_id' => $this->integer()->comment('Модели продаж'),
            'date_begin' => $this->date()->comment('Даты начала продаж'),
            'date_end' => $this->date()->comment('Даты окончания продаж'),
            'currency_id' => $this->integer()->comment('Валюта'),
        ]);

        $this->createIndex('idx-conditions-report_setting_id', 'conditions', 'report_setting_id', false);
        $this->addForeignKey("fk-conditions-report_setting_id", "conditions", "report_setting_id", "report_setting", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-conditions-report_setting_id','conditions');
        $this->dropIndex('idx-conditions-report_setting_id','conditions');

        $this->dropTable('conditions');
    }
}
