<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `tags_column_form_localization`.
 */
class m181213_122728_drop_tags_column_form_localization_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('localization', 'tags');
        $this->dropColumn('localization', 'awards');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('localization', 'tags', $this->text()->comment('Теги'));
        $this->addColumn('localization', 'awards', $this->text()->comment('Награды'));
    }
}
