<?php

use yii\db\Migration;

/**
 * Class m191010_200340_change_column_type
 */
class m191010_200340_change_column_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->alterColumn('right_holders', 'iban', $this->string(255) );//timestamp new_data_type
        $this->alterColumn('right_holders', 'bank_account', $this->string(255) );//timestamp new_data_type
    }

    public function down() 
    {
        $this->alterColumn('right_holders','iban', $this->float() );//int is old_data_type
        $this->alterColumn('right_holders','bank_account', $this->float() );//int is old_data_type
    }
}
