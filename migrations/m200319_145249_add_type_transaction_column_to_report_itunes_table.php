<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%report_itunes}}`.
 */
class m200319_145249_add_type_transaction_column_to_report_itunes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('report_itunes', 'type_transaction', $this->string()->comment('Тип транзакции'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('report_itunes', 'type_transaction');
    }
}
